// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mySpaceUrl : 'https://192.168.204.212:8566/',
  lmsbaseUrl : 'https://192.168.204.212:8561/',
  tsbaseUrl : 'https://192.168.204.212:8563/',
  hrbaseUrl : 'https://192.168.204.212:8566/',
  corebaseUrl : 'https://192.168.204.212:8565/',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
