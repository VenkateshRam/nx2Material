export const environment = {
    production: false,
    mySpaceUrl : 'https://localhost:8566/',
    hrbaseUrl : 'https://localhost:8566/',
    lmsbaseUrl : 'https://localhost:8561/',
    tsbaseUrl : 'https://localhost:8563/',
    corebaseUrl : 'https://localhost:8565/',
  };