import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '../services/auth.service';
import { AlertService } from '../services/alert.service';
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthService,
    private alertSvc: AlertService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let errMsg = 'Something went wrong, please try again later';
    return next.handle(request).pipe(catchError(err => {
      if ([401, 403].indexOf(err.status) !== -1) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        if (this.authenticationService.currentUserSubject.value) {
          this.alertSvc.showToast('Session Timed Out, please login again', 'warning');
          this.authenticationService.clearSession();
        } else
          this.alertSvc.showToast(err.error?.message || errMsg, 'error');
      } else
        this.alertSvc.showToast(err.error?.message || errMsg, 'error');


      // return throwError(error);
      // return Observable.of(new HttpResponse({body: [{name: "Default value..."}]}))

      // return throwError(err);
      return of(new HttpResponse(({ body: null })));
    }))
  }
}