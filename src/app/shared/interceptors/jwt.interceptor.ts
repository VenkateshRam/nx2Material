import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { map } from 'rxjs/operators';
import { AlertService } from '../services/alert.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private authSvc: AuthService, private alertSvc: AlertService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let msg = 'Something went wrong, please try again later';
    const user = this.authSvc.currentUserValue;
    const isLoggedIn = user && user.token;
    const isApiUrl = request.url.includes('login');

    if (isLoggedIn && !isApiUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${user.token}`
        }
      });
    }

    return next.handle(request).pipe(map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if (event.body?.status != 'OK') {
          this.alertSvc.showToast(event.body?.message || msg, 'error');
          return event.clone({
            body: null
          });
        }
      };
      return event;
    }))
  }
}
