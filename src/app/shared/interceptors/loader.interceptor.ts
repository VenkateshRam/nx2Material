import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';

import { LoaderService, LoadingOverlayRef } from '../services/loader.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  constructor(private loadingService: LoaderService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let loadingRef: LoadingOverlayRef;

    const isApiUrl = req.url.includes('searchByNameOrEmployeeCode');

    if (!isApiUrl)
      Promise.resolve(null).then(() => loadingRef = this.loadingService.open());

    return next.handle(req).pipe(tap(event => {
      if (event instanceof HttpResponse && loadingRef) {
        loadingRef.close();
      }
    }, error => {
      if (loadingRef) {
        loadingRef.close();
      }

      // return Observable.throw(error);
    }));
  }
}
