import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';
import { HandleError, HttpErrorHandler } from './http-error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  lmsMasterUrl: string = ''; 
  tsMasterUrl: string = ''; 
  moduleUrl: string = '';
  coreBaseUrl: string = '';

  entityName: string = '';

  constructor(private api: ApiService,
    private HttpErrorHandler: HttpErrorHandler,
    private httpClient: HttpClient) {

    this.lmsMasterUrl = environment.lmsbaseUrl;
    this.tsMasterUrl = environment.tsbaseUrl;
    this.coreBaseUrl = environment.corebaseUrl;
    this.moduleUrl = 'lms/business/';
  }


  getAll(apiMethod: string): Observable<any> {
    return this.api.get(`${this.lmsMasterUrl}${this.moduleUrl}${apiMethod}`);
  }

  getData(apiMethod: string): Observable<any> {
    return this.api.get(`${this.lmsMasterUrl}${apiMethod}`);
  }

  postData(apiMethod: string, payload: any): Observable<any> {
    return this.api.post(`${this.lmsMasterUrl}${apiMethod}`, payload);
  }

  putData(apiMethod: string, payload: any = null): Observable<any> {
    return this.api.put(`${this.lmsMasterUrl}${apiMethod}`, payload);
  }

  downloadReport(apiMethod: string, payload: any): Observable<Blob> {
    return this.httpClient.post(`${this.lmsMasterUrl}${apiMethod}`, payload, { responseType: 'blob' });
  }


  // *************************************

  save(entityName: string, payload: any): Observable<any> {
    let method = payload.id == 0 ? 'create' : 'update';
    let apiUrl = `lms/crud/${method}?entityName=${entityName}`;

    if (method == 'create')
      return this.httpClient.post(`${this.lmsMasterUrl}${apiUrl}`, payload);
      else
      return this.httpClient.put(`${this.lmsMasterUrl}${apiUrl}`, payload);

  }

  getWithPagination(entityName: string, pageIndex: number, pageSize: number, colName: string = '', search: string = ''): Observable<any> {
    let apiUrl = `lms/crud/getAllWithPagination?entityName=${entityName}&pageNumber=${pageIndex}&pageSize=${pageSize}&fieldName=${colName}&valueToMatch=${search}`;
    return this.httpClient.get(`${this.lmsMasterUrl}${apiUrl}`);
  }

  getAllCrud(apiUrl: string) {;
    return this.httpClient.get(`${this.lmsMasterUrl}${apiUrl}`).toPromise();
  }

  delete(entityName: string, payload: any): Observable<any> {
    let apiUrl = `lms/crud/delete?entityName=${entityName}&ids=${payload}`;
    return this.httpClient.delete(`${this.lmsMasterUrl}${apiUrl}`);
  }



  // **************************************************** Timesheets

  getTsAll(apiMethod: string): Observable<any> {
    return this.api.get(`${this.tsMasterUrl}${this.moduleUrl}${apiMethod}`);
  }

  getTsData(apiMethod: string): Observable<any> {
    return this.api.get(`${this.tsMasterUrl}${apiMethod}`);
  }

  postTsData(apiMethod: string, payload: any): Observable<any> {
    return this.api.post(`${this.tsMasterUrl}${apiMethod}`, payload);
  }

  putTsData(apiMethod: string, payload: any = null): Observable<any> {
    return this.api.put(`${this.tsMasterUrl}${apiMethod}`, payload);
  }

  deleteTsData(apiMethod: string): Observable<any> {
    return this.api.delete(`${this.tsMasterUrl}${apiMethod}`);
  }

  // ********************************

  getCore(apiMethod: string): Observable<any> {
    return this.api.get(`${this.coreBaseUrl}${apiMethod}`);
  }
}
