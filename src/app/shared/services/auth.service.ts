import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';
import { HandleError, HttpErrorHandler } from './http-error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  private handleError: HandleError;

  baseUrl: string = ''; empCode: number = null;
  compOffEligible: boolean = true;
  constructor(private apiSvc: ApiService,
    private router: Router,
    private dialogRef: MatDialog,
    private HttpErrorHandler: HttpErrorHandler) {
    this.baseUrl = environment.mySpaceUrl;
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.handleError = this.HttpErrorHandler.createHandleError('Auth Service');
  }

  login(apiMethod: string, payload: any): Observable<any> {
    return this.apiSvc.post(`${this.baseUrl}${apiMethod}`, payload)
      .pipe(
        map((user: any) => {
          const responseData = user;
          const currentUser = responseData.data.employeeDetails;
          this.empCode = currentUser.employeeCode;
          currentUser.token = responseData.data.token;
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
          this.currentUserSubject.next(currentUser);
          return user;
        }),
        // catchError(this.handleError('login', null))
      );
  }

  logout() {
    this.dialogRef.closeAll();
    this.apiSvc.put(`${this.baseUrl}myspacenx/logout`, null).subscribe(resp => {
      if (resp) {
        localStorage.removeItem('currentUser');
        localStorage.clear();
        this.currentUserSubject.next(null);
        this.router.navigate(['/']);
      }
      else
        this.clearSession();
    })
  }

  isLoggedIn(): boolean {
    return this.currentUserSubject.value ? true : false;
  }

  clearSession() {
    this.dialogRef.closeAll();
    localStorage.removeItem('currentUser');
    localStorage.clear();
    this.currentUserSubject.next(null);
    this.router.navigate(['/']);
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  onRefresh() {
    this.compOffEligible = JSON.parse(localStorage.getItem('compOff'));
    this.empCode = JSON.parse(localStorage.getItem('currentUser')).employeeCode;
  }

}
