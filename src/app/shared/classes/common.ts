import { ValidationErrors } from '@angular/forms';
import { AuthService } from '../services/auth.service';

declare let $: any;

export enum Avail {
    // lclUrl = '',
}

export class Common {

    constructor(public authSvc: AuthService) {

    }

    static keycontrol(event) {
        if (event.ctrlKey) {
            event.preventDefault();
          }
    }

    static checkScreenPermissions(screenName: string) {
        let currentUser = this.loadUser();
        if (currentUser != null) {
            const result = currentUser.permissions.some(f => f == screenName);
            return result ? true : false;
        }
        else
            return false;
    }

    static setFocus(elementName: string) {
        let el: any = (<HTMLTextAreaElement>(document.getElementById(elementName)));
        el.value = '';
        el.focus();
    }

    static loadUser() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        return currentUser;
    }

    static getFormValidationErrors(formObj: any) {
        Object.keys(formObj.controls).forEach(key => {

            const controlErrors: ValidationErrors = formObj.get(key).errors;
            if (controlErrors != null) {
                Object.keys(controlErrors).forEach(keyError => {
                    console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                });
            }
        });
    }

    static showPDF_File(filePath: string) {
        window.open(filePath, '_blank', 'left=300,top=50,width=700,height=600,toolbar=1,resizable=0');
    }

    static openDocFile(pathUrl: any) {
        let link = document.createElement("a");

        link.setAttribute('href', pathUrl);
        link.setAttribute('visibility', 'hidden');
        link.setAttribute('display', 'none');
        link.setAttribute('target', '_blank');

        document.body.appendChild(link);
        link.click();
    }

}