import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { AlertComponent } from 'src/app/modules/common/modals/alert/alert.component';
import { AlertService } from '../../services/alert.service';
import { AppInjectorService } from '../../services/app-injector.service';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-base',
  template: `
    <p>
      base works!
    </p>
  `,
  styles: [
  ]
})
export class BaseComponent implements OnInit {

  protected cmnSvc: CommonService;
  protected alertSvc: AlertService;
  protected authSvc: AuthService;
  protected dialog: MatDialog;
  protected datePipe: DatePipe;

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];

  entityName: string = ''; pageTitle: string = '';
  searchField: string = ''; columnName: string = '';

  empCode: number;

  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);

  inputForm: FormGroup;

  @ViewChild("modalTemplate") template: TemplateRef<any>;
  protected templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor() {
    const injector = AppInjectorService.getInjector();
    this.cmnSvc = injector.get(CommonService);
    this.alertSvc = injector.get(AlertService);
    this.authSvc = injector.get(AuthService);
    this.dialog = injector.get(MatDialog);
    this.datePipe = injector.get(DatePipe);
    this.empCode = this.authSvc.currentUserSubject.value.employeeCode;
  }

  ngOnInit(): any {
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  // setPagination(length: number = 5, pageSize: number = 10) {
  //   this.length = length;
  //   this.pageSize = pageSize;
  // }

  // onPaginationChange(event) {
  //   this.pageIndex = event.pageIndex;
  //   this.pageSize = event.pageSize;
  //   this.loadData();
  // }

  loadData() {
    if(this.columnName == 'date') {
      this.searchField = this.datePipe.transform(this.searchField, 'yyyy-MM-dd');
    }
    this.cmnSvc.getWithPagination(this.entityName, this.pageIndex, this.pageSize, this.columnName, this.searchField).subscribe(resp => {
      if (resp) {
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content : []);
        this.selection = new SelectionModel<any>(true, []);
        this.pageData = {data: resp.data, table: 'lb'};
        this.length = resp.data ? resp.data.totalElements : 0;
      }
    })
  }

  onSave(payload: any) {
    this.cmnSvc.save(this.entityName, payload).subscribe(resp => {
      if (resp) {
        this.alertSvc.showToast(resp.message, 'success');
        this.loadData();
        this.templateDialogRef.close('OK');
      }
    })
  }

  onDelete() {
    if (this.selection.selected.length == 0) {
      this.alertSvc.showToast('Select atleast one record', 'warning');
      return;
    }

    const dialogRef = this.dialog.open(AlertComponent, {
      data: {
        header: 'Delete'
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        this.deleteRecords();
    });
  }

  deleteRecords() {
    let ids = this.selection.selected.map(f => f.id).join(',');
    this.cmnSvc.delete(this.entityName, ids).subscribe(resp => {
      if (resp) {
        this.alertSvc.showToast(resp.message, 'success');
        this.selection = new SelectionModel<any>(true, []);
        this.loadData();
      }
    })
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.lstGrid.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.lstGrid.data.forEach(row => this.selection.select(row));
  }

  openDialog() {
    this.templateDialogRef = this.dialog.open(this.template, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        this.loadData();
    });
  }

  getAll(entityName: string) {
    let apiUrl = `lms/crud/getAll?entityName=${entityName}`;
    return this.cmnSvc.getAllCrud(apiUrl);
  }

  onReinit() {
    this.pageIndex = 0;
    this.lstGrid = new MatTableDataSource([]);
  }
  
  onSearch() {
    this.onReinit();
    this.loadData();
  }




}
