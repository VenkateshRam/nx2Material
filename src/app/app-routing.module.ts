import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './modules/common/home/home.component';
import { LoginComponent } from './modules/common/login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'home', component: HomeComponent, children: [
      { path: 'shared', loadChildren: () => import('./modules/shared/shared.module').then(m => m.SharedModule) },
      { path: 'lms', loadChildren: () => import('./modules/lms/lms.module').then(m => m.LmsModule) },
      { path: 'config', loadChildren: () => import('./modules/configuration/configuration.module').then(m => m.ConfigurationModule) },
      { path: 'ts', loadChildren: () => import('./modules/time-sheets/time-sheets.module').then(m => m.TimeSheetsModule) },
      { path: 'invoice', loadChildren: () => import('./modules/invoice/invoice.module').then(m => m.InvoiceModule) },
      { path: 'hiringRequests', loadChildren: () => import('./hiring-modules/hiring-requests/hiring-requests.module').then(m => m.HiringRequestsModule) },
      { path: 'profiles', loadChildren: () => import('./hiring-modules/profiles/profiles.module').then(m => m.ProfilesModule) },
      { path: 'offers', loadChildren: () => import('./hiring-modules/offers/offers.module').then(m => m.OffersModule) },
      { path: 'interviews', loadChildren: () => import('./hiring-modules/interviews/interviews.module').then(m => m.InterviewsModule) },
      { path: 'features', loadChildren: () => import('./hiring-modules/features/features.module').then(m => m.FeaturesModule) },
    ]
  },
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
