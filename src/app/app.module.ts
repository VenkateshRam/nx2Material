import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material/material.module';
import { SharedModule } from './modules/shared/shared.module';
import { LoginComponent } from './modules/common/login/login.component';
import { HomeComponent } from './modules/common/home/home.component';
import { CommonService } from './shared/services/common.service';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorHandler } from './shared/services/http-error-handler.service';
import { LoaderComponent } from './modules/common/loader/loader.component';
import { LoadingInterceptor } from './shared/interceptors/loader.interceptor';
import { LoaderService } from './shared/services/loader.service';
import { AlertService } from './shared/services/alert.service';
import { JwtInterceptor } from './shared/interceptors/jwt.interceptor';
import { AuthService } from './shared/services/auth.service';
import { ErrorInterceptor } from './shared/interceptors/error.interceptor';
import { BaseComponent } from './shared/classes/base/base.component';
import { DatePipe } from '@angular/common';
import { AlertComponent } from './modules/common/modals/alert/alert.component';
import { ConfirmationService } from 'primeng/api';
import { AppConstants } from './hiring-modules/shared-hiring/config/app.constants';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    LoaderComponent,
    BaseComponent,
    AlertComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [CommonService, HttpErrorHandler, LoaderService, AlertService, AuthService, DatePipe,
    ConfirmationService, AppConstants,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
  ],
  entryComponents: [
   AlertComponent
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
