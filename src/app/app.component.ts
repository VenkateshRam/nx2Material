import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mySpaceNX';

  // @HostListener('document:keydown.control.v', ['$event'])
  //   onKeyDown(e) {
  //       e.preventDefault();
  //   }
  
}
