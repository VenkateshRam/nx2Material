import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from 'src/app/shared/classes/base/base.component';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent extends BaseComponent implements OnInit {

  displayedColumns = ['select', 'sl.no', 'name', 'edit'];

  constructor(private fb: FormBuilder) {
      super();
     }

  ngOnInit(): void {
    this.pageTitle = 'Roles';
    this.entityName = 'Role';
    this.formInit();
    this.loadData();
  }

  formInit() {
    this.inputForm = this.fb.group({
      id: [0],
      name: ['', Validators.required],
    })
  }

  get f() { return this.inputForm?.controls; }
  
  onAdd() {
    this.formInit();
    this.openDialog();
  }

  onEdit(item: any) {
    this.formInit();
    this.inputForm.patchValue(item);
    this.openDialog();
  }

  onSubmit() {
    if (this.inputForm.invalid)
      return;

    let payload = this.inputForm.getRawValue();
    this.onSave(payload);
  }

  // onSearch() {
  //   this.cmnSvc.getData(`lms/crud/searchByField?entityName=Role&fieldName=${this.columnName}&valueToMatch=${this.searchField}`).subscribe(resp => {
  //     console.log(resp)
  //   })
  // }
}
