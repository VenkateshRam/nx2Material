import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationRoutingModule } from './configuration-routing.module';
import { LeaveTypesComponent } from './leave-types/leave-types.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { RolesComponent } from './roles/roles.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { GlobalSettingsComponent } from './global-settings/global-settings.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { UserRolesComponent } from './user-roles/user-roles.component';
import { RolePrivilegesComponent } from './role-privileges/role-privileges.component';
import { AuditInfoComponent } from './audit-info/audit-info.component';


@NgModule({
  declarations: [
    LeaveTypesComponent,
    HolidaysComponent,
    RolesComponent,
    PermissionsComponent,
    GlobalSettingsComponent,
    UserRolesComponent,
    RolePrivilegesComponent,
    AuditInfoComponent
  ],
  imports: [
    CommonModule,
    ConfigurationRoutingModule,
    MaterialModule
  ]
})
export class ConfigurationModule { }
