import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from 'src/app/shared/classes/base/base.component';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.scss']
})
export class HolidaysComponent extends BaseComponent implements OnInit {
  
  minDate: Date; toDay: Date = new Date(); maxDate: Date;

  displayedColumns = ['select','sl.no', 'client', 'desc', 'date', 'edit'];

  clientLst: Array<any> = []; holidaysLst : Array<any> = [];

  empInfo: any; 

  constructor(private fb: FormBuilder) {
    super();

    this.minDate = new Date(this.toDay.getFullYear(), 0,1);
    this.maxDate = new Date(this.toDay.getFullYear()+1, 11,31);
   }

  ngOnInit(): void {
    this.pageTitle = 'Holidays';
    this.entityName = 'Holidays';
    this.formInit();
    this.getClients();
    // this.getEmpInfo();
    this.loadData();
  }

  formInit() {
    this.inputForm = this.fb.group({
      id: [0],
      clientId: ['', Validators.required],
      description: ['', Validators.required],
      date: ['', Validators.required],
    })
  }

  get f() { return this.inputForm?.controls; }

  getClients() {
    this.cmnSvc.getCore(`core/seeddata`).subscribe(resp => {
      this.clientLst = resp ? resp.data.CLIENT : [];
    });
  }

  getClientName(id: number) {
    return id ? this.clientLst.find(f=> f.id ==id)?.clientName : '';
  }

  onAdd() {
    this.formInit();
    this.openDialog();
  }
  
  getHolidaysByClient(clientId: any) {
    this.cmnSvc.getData(`lms/business/getHolidayDatesByClientId?clientId=${clientId}`).subscribe(resp => {
      this.holidaysLst = resp ? resp.data : [];
    });
  }

  onColumnChange() {
    this.searchField = null;
  }

  onEdit(item: any) {
    this.formInit();
    this.getHolidaysByClient(item.clientId);
    this.inputForm.patchValue(item);
    this.openDialog();
  }

  onSubmit() {
    if (this.inputForm.invalid)
      return;

    let payload = this.inputForm.getRawValue();
    payload.date = this.datePipe.transform(this.inputForm.value.date, 'yyyy-MM-dd');
    this.onSave(payload);
  }

  getEmpInfo() {
    var self = this;
    self.cmnSvc.getData(`lms/business/employeeInfo?employeeCode=${this.empCode}`).subscribe(resp => {
      if (resp)
        this.empInfo = resp.data;
    });
  }

  dateFilter = (d: Date | null): boolean => {
    const day = (d || new Date());
    const hldy = this.holidaysLst.find(x => this.datePipe.transform(x, 'yyyy-MM-dd') == this.datePipe.transform(day, 'yyyy-MM-dd'));
    return day.getDay() !== 0 && day.getDay() !== 6 && !hldy;
  };
}


