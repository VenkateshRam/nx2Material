import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditInfoComponent } from './audit-info/audit-info.component';
import { GlobalSettingsComponent } from './global-settings/global-settings.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { LeaveTypesComponent } from './leave-types/leave-types.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { RolePrivilegesComponent } from './role-privileges/role-privileges.component';
import { RolesComponent } from './roles/roles.component';
import { UserRolesComponent } from './user-roles/user-roles.component';

const routes: Routes = [
  {path:'roles', component: RolesComponent},
  {path:'holidays', component: HolidaysComponent},
  {path:'settings', component: GlobalSettingsComponent},
  {path:'permissions', component: PermissionsComponent},
  {path:'leavetypes', component: LeaveTypesComponent},
  {path:'userroles', component: UserRolesComponent},
  {path:'rolepriv', component: RolePrivilegesComponent},
  {path:'audit', component: AuditInfoComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
