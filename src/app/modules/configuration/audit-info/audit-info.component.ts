import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-audit-info',
  templateUrl: './audit-info.component.html',
  styleUrls: ['./audit-info.component.scss']
})
export class AuditInfoComponent implements OnInit {

  public objectKeys = Object.keys;
  entityLst: Array<any> = []; lstGrid: MatTableDataSource<any>;

  entityName: string = ''; entityId: string = ''; operation: string = ''; pageTitle: string = 'Audit Information';

  showData: boolean = false;

  response: any; viewData: any;

  displayedColumns = ['sl.no', 'createdBy', 'createdOn', 'data'];

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;

  @ViewChild("template") template: TemplateRef<any>;
  private templateDialogRef: MatDialogRef<TemplateRef<any>>

  constructor(private cmnSvc: CommonService,
    public dialog: MatDialog,
    private alertSvc: AlertService) { }

  ngOnInit(): void {
    this.getEntities();
  }

  onReinit() {
    this.pageIndex = 0;
    this.lstGrid = new MatTableDataSource([]);
  }
  
  onSubmit() {
    this.onReinit();
    this.loadData();
  }

  getEntities() {
    this.cmnSvc.getData(`lms/business/getAuditedEntityNames`).subscribe(resp => {
      this.entityLst = resp ? resp.data : [];
      this.entityName = this.entityLst[0];
    })
  }

  loadData() {
    this.cmnSvc.getData(`lms/business/getAuditData?entityName=${this.entityName}&entityId=${this.entityId}&operation=${this.operation}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(resp => {
      if (resp) {
        this.response = resp;
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content : []);
        this.pageData = {data: resp.data, table: 'lb'};
        this.length = resp.data ? resp.data.totalElements : 0;
        this.showData = true;
      }
    })
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  openDialog(data: any) {
    this.viewData = data;
    this.templateDialogRef = this.dialog.open(this.template, {
      maxWidth: '60%',
      maxHeight: 'calc(100vh - 90px)',
      minWidth: '400px'
    });

  }

}
