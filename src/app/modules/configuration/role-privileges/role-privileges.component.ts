import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from 'src/app/shared/classes/base/base.component';

@Component({
  selector: 'app-role-privileges',
  templateUrl: './role-privileges.component.html',
  styleUrls: ['./role-privileges.component.scss']
})
export class RolePrivilegesComponent extends BaseComponent implements OnInit {

  displayedColumns = ['select', 'sl.no', 'role', 'perm', 'edit'];

  searchLst: Array<any> = []; rolesLst: Array<any> = []; permissionsLst: Array<any> = [];
  selectedEmp: string;

  name: string = '';

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.pageTitle = 'Role Privileges';
    this.entityName = 'RolePermission';
    this.formInit();
    this.loadData();
    this.getRoles();
    this.getPerms();
  }

  formInit() {
    this.inputForm = this.fb.group({
      id: [0],
      roleId: [null, Validators.required],
      permissions: [[]],
    })
  }

  get f() { return this.inputForm?.controls; }

  // loadData() {
  //   this.cmnSvc.getData(`employee/business/getRolePermissions?pageNumber=${this.pageIndex}&pageSize=${this.pageSize}&pageSize=${this.pageSize}&fieldName=${this.columnName}&valueToMatch=${this.searchField}`).subscribe(resp => {
  //     if(resp) {
  //       this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content : []);
  //       this.selection = new SelectionModel<any>(true, []);
  //       this.setPagination(resp?.data.totalElements, resp.data?.size);
  //     }
  //   })
  // }

  getPermissions(perms: Array<any> = []) {
    return perms ? perms.map(f=> f.name).join(',') : '';
  }

  onAdd() {
    this.formInit();
    this.f.roleId.enable();
    this.openDialog();
  }

  onEdit(item: any) {
    this.formInit();
    let perms = item.permissions ? item.permissionsList.map(f=> f.id) : [];
    this.inputForm.patchValue({ id: item.id, roleId: item.roleId , permissions: perms });
    this.permissionsLst.sort((a, b) => perms.indexOf(b.id) - perms.indexOf(a.id));
    this.f.roleId.disable();
    this.openDialog();
  }

  getPermsByRole(roleId: string) {
    this.cmnSvc.getData(`employee/business/getPermissionsByRole?roleId=${roleId}`).subscribe(resp => {
      if (resp && resp.data) {
        let data = resp.data;
        let perms = data.permissions ? data.permissionsList.map(f=> f.id) : [];
        this.inputForm.patchValue({ id: data.id, permissions: perms });
        this.permissionsLst.sort((a, b) => perms.indexOf(b.id) - perms.indexOf(a.id));
      }
    })
  }

  getRoles() {
    this.getAll('Role').then((resp: any) => {
      this.rolesLst = resp ? resp.data : [];
    })
  }

  getPerms() {
    this.getAll('Permission').then((resp: any) => {
      this.permissionsLst = resp ? resp.data : [];
    })
  }

  onSubmit() {
    if(this.inputForm.invalid)
    return;

    let value = this.inputForm.getRawValue();
    if (value.permissions.length == 0) {
      this.alertSvc.showToast('Select atleast one Permission', 'warning');
      return;
    }

    let payload = {id: value.id, roleId: value.roleId, permissions: value.permissions.join(',')};
    this.onSave(payload);
  }

}
