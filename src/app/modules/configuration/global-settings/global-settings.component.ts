import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BaseComponent } from 'src/app/shared/classes/base/base.component';

@Component({
  selector: 'app-global-settings',
  templateUrl: './global-settings.component.html',
  styleUrls: ['./global-settings.component.scss']
})
export class GlobalSettingsComponent extends BaseComponent implements OnInit {

  displayedColumns = ['select', 'sl.no', 'name', 'edit'];
  types = ['int', 'float', 'double', 'boolean', 'String', 'char', 'LocalDate'];

  constructor(protected fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.pageTitle = 'Global Settings';
    this.entityName = 'GlobalSettings';
    this.formInit();
    this.loadData();
  }

  formInit() {
    this.inputForm = this.fb.group({
      id: [0],
      name: ['', Validators.required],
      description: ['', Validators.required],
      type: ['', Validators.required],
      value: ['', Validators.required],
    })
  }

  get f() { return this.inputForm?.controls; }

  onAdd() {
    this.formInit();
    this.openDialog();
  }

  onEdit(item: any) {
    this.formInit();
    this.inputForm.patchValue(item);
    this.openDialog();
  }

  onSubmit() {
    if (this.inputForm.invalid)
      return;

    let payload = this.inputForm.getRawValue();
    this.onSave(payload);
  }

  onReload() {
    this.cmnSvc.getData('gloablSettings/Load').subscribe(resp => {
      console.log(resp);
      
    })
  }
}

