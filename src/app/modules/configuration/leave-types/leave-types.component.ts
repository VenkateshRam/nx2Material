import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from 'src/app/shared/classes/base/base.component';

@Component({
  selector: 'app-leave-types',
  templateUrl: './leave-types.component.html',
  styleUrls: ['./leave-types.component.scss']
})
export class LeaveTypesComponent extends BaseComponent implements OnInit {

  displayedColumns = ['select', 'sl.no', 'code', 'desc', 'edit'];

  constructor(
    private fb: FormBuilder) {
      super();
     }

  ngOnInit(): void {
    this.pageTitle = 'Leave Types';
    this.entityName = 'LeaveType';
    this.formInit();
    this.loadData();
  }

  formInit() {
    this.inputForm = this.fb.group({
      id: [0],
      leaveCode: ['', Validators.required],
      leaveDescription: ['', Validators.required],
      enabled: [false],
      accruable: [false],
      daysPerYear: [null, Validators.required],
      className: ['', Validators.required],
      startDate: [null],
      endDate: [null],
    })
  }

  get f() { return this.inputForm?.controls; }
 
  onSubmit() {
    if (this.inputForm.invalid)
      return;

    let payload = this.inputForm.getRawValue();
    payload.startDate = this.datePipe.transform(this.inputForm.value.startDate, 'yyyy-MM-dd');
    payload.endDate = this.datePipe.transform(this.inputForm.value.endDate, 'yyyy-MM-dd');
    payload.daysPerYear = +payload.daysPerYear;
    this.onSave(payload);
  }

  onAdd() {
    this.formInit();
    this.openDialog();
  }

  onEdit(item: any) {
    this.formInit();
    this.inputForm.patchValue(item);
    this.openDialog();
  }

}

