import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from 'src/app/shared/classes/base/base.component';

@Component({
  selector: 'app-user-roles',
  templateUrl: './user-roles.component.html',
  styleUrls: ['./user-roles.component.scss']
})
export class UserRolesComponent extends BaseComponent implements OnInit {

  displayedColumns = ['select', 'sl.no', 'code', 'user', 'roles', 'edit'];

  searchLst: Array<any> = []; rolesLst: Array<any> = [];
  selectedEmp: string;

  name: string = '';

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.pageTitle = 'User Roles';
    this.entityName = 'UserRole';
    this.formInit();
    this.loadData();
    this.getData();
  }

  formInit() {
    this.inputForm = this.fb.group({
      id: [0],
      employeeCode: [null, Validators.required],
      roles: [[]]
    })
  }

  get f() { return this.inputForm?.controls; }

  search = (e: string) => {
    if (e && ((e.length >= 3 && isNaN(+e)) || (e.length >= 1 && !isNaN(+e))))
      this.cmnSvc.getData(`employee/business/searchByNameOrEmployeeCode?nameOrEmployeeCode=${e}`).subscribe(
        res => {
          this.searchLst = res ? res.data : [];
        }
      );
    else {
      this.selectedEmp = null;
      this.searchLst = [];
    }
  }

  // loadData() {
  //   this.cmnSvc.getData(`employee/business/getUserRoles?pageNumber=${this.pageIndex}&pageSize=${this.pageSize}&fieldName=${this.columnName}&valueToMatch=${this.searchField}`).subscribe(resp => {
  //     if (resp) {
  //       this.lstGrid = new MatTableDataSource(resp?.data ? resp.data.content : []);
  //       this.selection = new SelectionModel<any>(true, []);
  //       this.setPagination(resp?.data.totalElements, resp.data?.size);
  //     }
  //   })
  // }

  getOptionText(option) {
    return option?.fullName;
  }

  selected = (e: any) => {
    this.selectedEmp = e.option.value.empId;
    this.getRolesByUserId(this.selectedEmp);
  }

  getRolesByUserId(empId: string) {
    this.cmnSvc.getData(`employee/business/getRolesByUser?employeeCode=${empId}`).subscribe(resp => {
      if (resp && resp.data) {
        let data = resp.data;
        let roles = data.roles ? data.roles.split(',').map(Number) : [];
        this.inputForm.patchValue({ id: data.id, roles: roles });
        this.rolesLst.sort((a, b) => roles.indexOf(b.id) - roles.indexOf(a.id));
      }
    })
  }

  getRoles(lst: any) {
    return lst ? lst.map(f => f.name).join(',') : '';
  }

  onAdd() {
    this.selectedEmp = null;
    this.formInit();
    this.f.employeeCode.enable();
    this.openDialog();
  }

  onEdit(item: any) {
    this.formInit();
    let roles = item.roles ? item.rolesList.map(f=> f.id) : [];
    this.inputForm.patchValue({
      id: item.id, employeeCode: { id: item.employeeCode, fullName: item.employeeName }, roles: roles
    });
    this.rolesLst.sort((a, b) => roles.indexOf(b.id) - roles.indexOf(a.id));
    this.selectedEmp = item.employeeCode;
    this.f.employeeCode.disable();
    this.openDialog();
  }

  getData() {
    this.getAll('Role').then((resp: any) => {
      this.rolesLst = resp ? resp.data : [];
    })
  }

  onSubmit() {
    if (!this.selectedEmp) {
      this.alertSvc.showToast('Select User', 'warning');
      return;
    }
    let value = this.inputForm.getRawValue();
    if (value.roles.length == 0) {
      this.alertSvc.showToast('Select atleast one Role', 'warning');
      return;
    }

    let payload = { id: value.id, employeeCode: this.selectedEmp, roles: value.roles.join(',') };
    this.onSave(payload);
  }

}
