import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { delay } from 'rxjs/operators';
import { Common } from 'src/app/shared/classes/common';
import { AuthService } from 'src/app/shared/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;
  currentUser: any;
  userImg: string = '';

  expand1: boolean = false;
  expand2: boolean = false;
  expand3: boolean = false;
  expand4: boolean = false;
  expand5: boolean = true;

  today: Date = new Date();
  constructor(private observer: BreakpointObserver,
    private authSvc: AuthService) {
      let activePanel = localStorage.getItem('setActive');
      this[activePanel] = true;
      setInterval(() => {
        this.today = new Date()
      }, 1000)
  }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      this.authSvc.onRefresh();
      this.authSvc.currentUserSubject.next(this.currentUser);
      this.userImg = environment.lmsbaseUrl + this.currentUser.photoUrl;
    }
    else
      this.logout();
  }

  checkPermissions(screenName: string) {
    return Common.checkScreenPermissions(screenName);
  }

  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });

  }

  setActive(panel: string) {
    localStorage.setItem('setActive', panel);
  }

  logout() {
    this.authSvc.logout();
  }

}
