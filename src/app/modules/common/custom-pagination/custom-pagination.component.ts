import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-custom-pagination',
  templateUrl: './custom-pagination.component.html',
  styleUrls: ['./custom-pagination.component.scss']
})
export class CustomPaginationComponent implements OnInit {
  @Input() pageProperties: any;
  @Output() pageEvent = new EventEmitter<any>();

  length: number; pageIndex: number = 0; pageSize: number = 10; pageNumber: number = null; totalPages: number;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private alertSvc: AlertService) { }

  ngOnInit(): void {
    if (this.pageProperties) {
      // this.paginator?.pageIndex = 0;
      this.totalPages = this.pageProperties.data?.totalPages;
      this.pageNumber = this.totalPages != 0 ? this.pageIndex + 1 : 0;
      this.setPagination(this.pageProperties.data?.totalElements, this.pageProperties.data?.size);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.pageProperties && changes.pageProperties.currentValue && changes.pageProperties.currentValue.data) {
      this.totalPages = changes.pageProperties.currentValue.data?.totalPages;
      this.setPagination(changes.pageProperties.currentValue.data?.totalElements,
        changes.pageProperties.currentValue.data?.size);
      this.pageIndex = changes.pageProperties.currentValue.data?.number;
      if (this.paginator)
        this.paginator.pageIndex = this.pageIndex;
      this.pageNumber = this.totalPages != 0 ? this.pageIndex + 1 : 0;
    } else {
      this.setPagination(0, 0);
      this.pageIndex = 0;
      if (this.paginator)
        this.paginator.pageIndex = 0;
      this.pageNumber = 0;
      this.totalPages = 0;
    }
  }

  setPagination(length: number = 5, pageSize: number = 10) {
    this.length = length;
    this.pageSize = pageSize;
  }

  goToPage(pageNumber: any) {
    if (pageNumber != '' && (+pageNumber <= 0 || +pageNumber > this.totalPages)) {
      if (this.totalPages > 0)
        this.alertSvc.showToast(`Page Number must be between 1 and ${this.totalPages}`, 'info');
      if (this.totalPages == 0 && (+pageNumber > 0 || +pageNumber < 0))
        this.alertSvc.showToast(`Invalid Request`, 'info');
      return;
    }
    else if (+pageNumber > 0) {
      // this updates the paginator component
      this.paginator.pageIndex = +pageNumber - 1;
      // emit an event so that the table will refresh the data
      this.paginator.page.next({
        pageIndex: this.paginator.pageIndex,
        pageSize: this.paginator.pageSize,
        length: this.paginator.length
      });
    }
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.pageEvent.emit({
      table: this.pageProperties.table, pageIndex: this.pageIndex, pageSize: this.pageSize,
      length: this.length
    });
  }


}
