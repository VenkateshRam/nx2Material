import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { share } from 'rxjs/operators';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string = ''; password: string = '';

  constructor(private authSvc: AuthService,
    private alertSvc: AlertService,
    private cmnSvc: CommonService,
    private router: Router) { }

  ngOnInit(): void {
    Common.setFocus('userName');
  }

  onSubmit = () => {
    if (this.username == '' || this.password == '') {
      this.alertSvc.showToast('Please fill Username and Password', 'warning');
      return;
    }

    let payload = { username: this.username.trim(), password: this.password };
    this.authSvc.login('myspacenx/login', payload).subscribe(res => {
      if (res && res.status == 'OK') {
        this.router.navigate(['/home/interviews/open-positions']);
        this.isCompOffEligible(res.data.employeeDetails.employeeCode)
      }
    });
    
  }
  
  isCompOffEligible(empId: string) {
    this.cmnSvc.getData(`lms/business/isEmployeeEligibleForCompOff?employeeCode=${empId}`).subscribe(resp=> {
      if(resp) {
        localStorage.setItem('compOff', resp.data);
        this.authSvc.compOffEligible = resp.data;
      }
    })
  }


}
