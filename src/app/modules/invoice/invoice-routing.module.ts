import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddProjectComponent } from './add-project/add-project.component';
import { AllProjectsComponent } from './all-projects/all-projects.component';
import { FinanceComponent } from './finance/finance.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { MyProjectsComponent } from './my-projects/my-projects.component';

const routes: Routes = [
  { path: 'myprojs', component: MyProjectsComponent },
  { path: 'allprojs', component: AllProjectsComponent },
  { path: 'finance', component: FinanceComponent },
  { path: 'pending', component: InvoicesComponent },
  { path: 'addproject', component: AddProjectComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRoutingModule { }
