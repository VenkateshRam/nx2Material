import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceRoutingModule } from './invoice-routing.module';
import { MyProjectsComponent } from './my-projects/my-projects.component';
import { AllProjectsComponent } from './all-projects/all-projects.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { FinanceComponent } from './finance/finance.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { AddProjectComponent } from './add-project/add-project.component';


@NgModule({
  declarations: [
    MyProjectsComponent,
    AllProjectsComponent,
    FinanceComponent,
    InvoicesComponent,
    AddProjectComponent
  ],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    MaterialModule
  ]
})
export class InvoiceModule { }
