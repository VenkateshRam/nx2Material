import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {
  @Input() pending: boolean = true;

  invoiceColumns = ['sl', 'action', 'TimesheetMY', 'TimeSheetDocument', 'InvoiceDate', 'Currency', 'InvoiceAmount',
    'InvoiceRaisedBy', 'InvoiceRaisedOn', 'PaymentDate', 'PaymentReceived', 'AmountReceived', 'PaymentStatus', 'Comments'];

  lstGrid: MatTableDataSource<[]>;

  @ViewChild("template") template: TemplateRef<any>;
  private templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor( public dialog: MatDialog,) { }

  ngOnInit(): void {
  }

  getDisplayedColumns() {
    let col = this.pending ? 'sl' : 'action';
    return this.invoiceColumns.filter(e => e !== col);
  }

  onAction() {
    this.templateDialogRef = this.dialog.open(this.template, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      // if (result == 'OK')
      //   this.loadCompOffReqs();
    });
  }

}
