import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.scss']
})
export class FinanceComponent implements OnInit {

  displayedColumns = ['sl', 'project', 'empCode', 'name', 'loc', 'date', 'hours', 'Bhours', 'ebAmount',
    'ibAmount', 'work'];

  lstGrid: MatTableDataSource<[]>;
  constructor() { }

  ngOnInit(): void {
  }

}
