import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-my-projects',
  templateUrl: './my-projects.component.html',
  styleUrls: ['./my-projects.component.scss']
})
export class MyProjectsComponent implements OnInit {

  displayedColumns = ['sl.no', 'cust', 'pName', 'pManager', 'status', 'stage', 'pStartDate', 'pEndDate', 'practice',
    'eModel', 'cCenter', 'aEligibility', 'type', 'billable', 'shadow', 'investment', 'im', 'rd', 'total',
    'off', 'on'];

  length: number; pageIndex: number = 0; pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];

  lstGrid: MatTableDataSource<any>;
  constructor() { }

  ngOnInit(): void {
  }

  setPagination(length: number = 5, pageSize: number = 10) {
    this.length = length;
    this.pageSize = pageSize;
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
  }

}
