import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyRequestsComponent } from './sections/employee/my-requests/my-requests.component';
import { MyTeamRequestsComponent } from './sections/manager/my-team-requests/my-team-requests.component';

const routes: Routes = [
        { path: 'myrequests', component: MyRequestsComponent },
        { path: 'teamrequests', component: MyTeamRequestsComponent },
        { path: 'lmsReports', loadChildren: () => import('./lms-reports/lms-reports.module').then(m => m.LmsReportsModule) },
        { path: 'hr', loadChildren: () => import('./sections/hr/hr-functions-lms/hr-functions-lms.module').then(m => m.HrFunctionsLmsModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LmsRoutingModule { }
