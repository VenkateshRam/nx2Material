import { Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ApplyLeaveModalComponent } from '../../../modals/apply-leave-modal/apply-leave-modal.component';
import { LeaveActionsComponent } from '../../../modals/leave-actions/leave-actions.component';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';
import { Common } from 'src/app/shared/classes/common';

@Component({
  selector: 'app-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MyRequestsComponent implements OnInit {

  displayedColumns: string[] = ['demo-type', 'demo-balance'];
  expColumns: string[] = ['eDate', 'eStatus', 'eComments', 'eBalance'];
  expCompOffColumns: string[] = ['statusCO', 'actedBy', 'actedOn',
  'commentsCO'];
  compOffColumns: string[] = ['request', 'dateCO', 'expiryCO', 'appliedBy', 'appliedOnCO', 'reasonCO', 'statusCO'];
  summaryColumns: string[] = ['request', 'from', 'to', 'days', 'type', 'appliedTo', 'status', 'appliedOn', 'action'];

  balanceLst: any[] = []; summaryLst: MatTableDataSource<any>; compOffGrid: any[] = []; eligibleCompOffDatesLst: Array<any> = [];

  expandedElement = null; empInfo: any; compOffForm: FormGroup; summaryResp: any; pageData: any; CoPageData: any;

  empCode: string = '';

  length: number = 0; pageIndex: number = 0; pageSize: number = 10;
  lengthCO: number = 0; pageIndexCO: number = 0; pageSizeCO: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];

  today = new Date(); minDate: Date; maxDate: Date;

  showCompOffTab: boolean = true;

  @ViewChild(MatPaginator) paginator?: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("template") template: TemplateRef<any>;
  private templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor(
    private cmnSvc: CommonService,
    public dialog: MatDialog,
    private authSvc: AuthService,
    private fb: FormBuilder,
    private alertSvc: AlertService,
    private datePipe: DatePipe) {
    this.empCode = this.authSvc.currentUserSubject.value.employeeCode;
    this.showCompOffTab = this.authSvc.compOffEligible ;
  }

  ngOnInit(): void {
    this.loadBalance();
    this.loadSummary();
    this.getEmpInfo();
    this.formInit();

    if(this.showCompOffTab) {
      this.getEligibleCompOffDates();
      this.getAppliedCompOffReqs();
    }

    let mnDt = this.today.getFullYear() + '-' + this.today.getMonth() + '-' + '26';
    let mxDt = this.today.getFullYear() + '-' + (this.today.getMonth()+4) + '-' + '25';
    this.minDate = new Date(mnDt);
    this.maxDate = new Date(mxDt);
  }

  getDisplayedColumns = (type: string) => {
    if (type == 'Paternity leave' || type == 'Maternity leave')
      return ['eDate', 'eStatus', 'eComments', 'eBalance'];
    else
      return ['eStatus', 'eComments', 'eBalance'];
  }

  loadBalance = () => {
    this.cmnSvc.getData(`lms/business/leaveEligibility?employeeCode=${this.empCode}`).subscribe(res => {
      this.balanceLst = res ? res.data : [];
    });
  }

  loadSummary = () => {
    this.cmnSvc.getData(`lms/business/myLeaveSummary?employeeCode=${this.empCode}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(res => {
      if (res) {
        this.summaryLst = new MatTableDataSource(res.data ? res.data.content : []);
        this.pageData = {data: res.data, table: 'ls'};
        this.length = res.data.totalElements;
        this.summaryResp = res;
        // this.setPagination(res?.data.totalElements, res.data?.size);
      }
    });
  }

  checkEligible(res: any) {
    return (res?.statusStr == 'Applied' || (res.statusStr == 'Approved' && new Date(res.toDate).getTime() >= new Date().getTime())) && res.cancellable;
  }

  nestedFilterCheck(search, data, key) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
        }
      }
    } else {
      search += data[key];
    }
    return search;
  }

  getPropertyByPath(obj: Object, pathString: string) {
    return pathString.split('.').reduce((o, i) => o[i], obj);
  }

  applyFilter(filterValue: string) {
    this.summaryLst.filter = filterValue.trim().toLowerCase();
  }

  setPagination(length: number = 0, pageSize: number = 10, table: string = null) {
    if (table == 'CO') {
      this.lengthCO = length;
      this.pageSizeCO = pageSize;
    }
    else {
      this.length = length;
      this.pageSize = pageSize;
    }
  }

  onPaginationChange(event) {
    if (event.table == 'CO') {
      this.pageIndexCO = event.pageIndex;
      this.pageSizeCO = event.pageSize;
      this.lengthCO = event.length;
      this.getAppliedCompOffReqs();
    }
    else {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.length = event.length;
      this.loadSummary();
    }
  }


  openApplyLeaveModal = () => {
    const dialogRef = this.dialog.open(ApplyLeaveModalComponent, {
      data: {
        leaveTypes: this.balanceLst,
        empCode: this.empCode,
        empInfo: this.empInfo
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        Promise.all([this.loadBalance(), this.loadSummary()]);
    });
  }

  check = () => {

  }

  openActionModal = (reqData: any) => {
    const dialogRef = this.dialog.open(LeaveActionsComponent, {
      data: {
        reqData: reqData,
        header: 'Cancel Leave',
        empCode: this.empCode,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        Promise.all([this.loadBalance(), this.loadSummary()]);
    });
  }

  download = () => {
    // this.cmnSvc.getData('report/getReport/pdf/3886/LeaveRequest').subscribe(res => {
    //   console.log(res);
    // })
  }

  getEmpInfo() {
    var self = this;
    self.cmnSvc.getData(`lms/business/employeeInfo?employeeCode=${this.empCode}`).subscribe(resp => {
      if (resp)
        this.empInfo = resp.data;
    });
  }

  getDateFormat(val: any) {
    if (val) {
      let dateParts = val?.split("-");
      return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }
    else
      return new Date();
  }


  // ***************************** 

  formInit() {
    this.compOffForm = this.fb.group({
      empId: [this.empCode],
      compOffAgainstDate: [null, Validators.required],
      requestedBy: [this.empCode],
      comments: [null, Validators.required]
    });
  }

  get f() { return this.compOffForm?.controls; }

  getAppliedCompOffReqs() {
    this.cmnSvc.getData(`lms/business/compOffSummary?employeeCodeOrName=${this.empCode}&pageNumber=${this.pageIndexCO}&pageSize=${this.pageSizeCO}`).subscribe(res => {
      if (res) {
        this.compOffGrid = res ? res.data.content || res.data : [];
        this.CoPageData = {data: res.data, table: 'co'};
        this.lengthCO = res.data.totalElements;
        // this.setPagination(res.data?.totalElements, res.data?.size, 'CO');
      }
    });
  }

  onSubmit() {
    this.compOffForm.markAllAsTouched();
    if (this.compOffForm.invalid)
      return;
    let payload = this.compOffForm.value;
    this.cmnSvc.postData('lms/business/applyCompOff', payload).subscribe(resp => {
      if (resp) {
        this.alertSvc.showToast(resp.message, 'success');
        this.templateDialogRef.close('OK')
      }
    });
  }

  dateFilter = (d: Date | null): boolean => {
    const day = (d || new Date());
    const hldy = this.empInfo.holidays.find(x => this.datePipe.transform(x.date, 'yyyy-MM-dd') == this.datePipe.transform(day, 'yyyy-MM-dd'));
    return ((day.getDay() === 0 || day.getDay() === 6 || hldy) && (day.getTime() < new Date().getTime()));
  };

  openDialog(templateRef) {
    this.formInit();
    this.templateDialogRef = this.dialog.open(templateRef, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        this.getAppliedCompOffReqs();
    });
  }

  downloadReport(type: string = 'pdf', screen: string = 'mr') {
    let api = '';

    api = screen == 'mr' ? `lms/business/myLeaveSummary?employeeCode=${this.empCode}&pageNumber=0&pageSize=${this.length}&reportFormat=${type}` :
    `lms/business/compOffSummary?employeeCodeOrName=${this.empCode}&pageNumber=0&pageSize=${this.lengthCO}&reportFormat=${type}`
    this.cmnSvc.getData(api).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }

  getEligibleCompOffDates() {
    this.cmnSvc.getData(`lms/business/getEligibleCompOffDates?employeeCode=${this.empCode}`).subscribe(res => {
      this.eligibleCompOffDatesLst = res ? res.data : [];
    });
  }

}
