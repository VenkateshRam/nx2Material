import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { LeaveActionsComponent } from '../../../modals/leave-actions/leave-actions.component';

@Component({
  selector: 'app-my-team-requests',
  templateUrl: './my-team-requests.component.html',
  styleUrls: ['./my-team-requests.component.scss']
})
export class MyTeamRequestsComponent implements OnInit {
  lstGrid: MatTableDataSource<any>;
  compOffGrid: MatTableDataSource<any>;

  empCode: string = ''; compOffComments: string = ''; modalHeader: string = '';

  requestedData: any;

  length: number = 0; pageIndex: number = 0; pageSize: number = 10;
  lengthCO: number = 0; pageIndexCO: number = 0; pageSizeCO: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];

  pendingLeaveColumns: string[] = ['sl', 'name', 'from', 'to', 'days', 'type', 'reason', 'status', 'appliedOn', 'action'];
  compOffColumns: string[] = ['sl', 'empId', 'name', 'reqdate', 'reasonCO', 'status', 'appliedOn', 'action'];

  @ViewChild(MatPaginator) paginator?: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @ViewChild("compOffTemplate") compOffTemplate: TemplateRef<any>;
  private templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor(private cmnSvc: CommonService,
    public dialog: MatDialog,
    public alertSvc: AlertService,
    private authSvc: AuthService) {
    this.empCode = this.authSvc.currentUserSubject.value.employeeCode;
  }

  ngOnInit(): void {
    this.loadPendingReqs();
    this.loadCompOffReqs();
  }

  loadPendingReqs = () => {
    this.cmnSvc.getData(`lms/business/requestsPendingAction?pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(res => {
      if (res) {
        this.lstGrid = new MatTableDataSource(res.data ? res.data.content : []);
        this.setPagination(res.data?.totalElements, res.data?.size);
      }
    });
  }

  loadCompOffReqs = () => {
    this.cmnSvc.getData(`lms/business/compOffRequestsApplied?pageNumber=${this.pageIndexCO}&pageSize=${this.pageSizeCO}`).subscribe(res => {
      if (res) {
        this.compOffGrid = new MatTableDataSource(res.data ? res.data.content : []);
        this.setPagination(res.data?.totalElements, res.data?.size, 'CO');
      }
    });
  }

  nestedFilterCheck(search, data, key) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
        }
      }
    } else {
      search += data[key];
    }
    return search;
  }

  getPropertyByPath(obj: Object, pathString: string) {
    return pathString.split('.').reduce((o, i) => o[i], obj);
  }

  setPagination(length: number = 0, pageSize: number = 10, table: string = null) {
    if (table == 'CO') {
      this.lengthCO = length;
      this.pageSizeCO = pageSize;
    }
    else {
      this.length = length;
      this.pageSize = pageSize;
    }
  }

  applyFilter(filterValue: string) {
    this.lstGrid.filter = filterValue.trim().toLowerCase();
  }

  onPaginationChange(event, table: string = null) {
    if (table) {
      this.pageIndexCO = event.pageIndex;
      this.pageSizeCO = event.pageSize;
      this.loadCompOffReqs();
    }
    else {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.loadPendingReqs();
    }
  }

  openActionModal(reqData: any, type: string) {
    let [status, modalHeader] = [reqData.leaveRequest.statusStr, ''];
    if (status == 'Applied')
      modalHeader = type == 'Approve' ? 'Approve Leave' : 'Reject Leave';
    else if (status == 'Cancellation applied')
      modalHeader = type == 'Approve' ? 'Approve Cancellation' : 'Reject Cancellation';

    const dialogRef = this.dialog.open(LeaveActionsComponent, {
      data: {
        reqData: reqData,
        header: modalHeader,
        empCode: this.empCode
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        this.loadPendingReqs();
    });
  }

  getDateFormat(val: any) {
    if (val) {
      let dateParts = val.split("-");
      return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }
    else
      return new Date();
  }

  openCompOffModal(data: any, type: string, templateRef) {
    this.modalHeader = type == 'Approve' ? 'Approve Comp Off' : 'Reject Comp Off';
    this.requestedData = data;
    this.templateDialogRef = this.dialog.open(templateRef, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        this.loadCompOffReqs();
    });
  }

  onCompOffSubmit() {
    let baseUrl = this.modalHeader == 'Approve Comp Off' ? 'approveCompOff' : 'rejectCompOff';

    this.cmnSvc.putData(`lms/business/${baseUrl}?compOffRequestId=${this.requestedData.id}&comments=${this.compOffComments}`).subscribe(resp => {
      this.alertSvc.showToast(resp.message, 'success');
      this.templateDialogRef.close('OK')
    });
  }

  downloadReport(type: string = 'pdf', screen: string = 'mr') {
    let api = '';

    api = screen == 'mr' ? `lms/business/requestsPendingAction?pageNumber=0&pageSize=${this.length}&reportFormat=${type}` :
    `lms/business/compOffRequestsApplied?pageNumber=0&pageSize=${this.lengthCO}&reportFormat=${type}`
    this.cmnSvc.getData(api).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }
}
