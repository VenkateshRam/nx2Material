import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApplyLeaveModalComponent } from 'src/app/modules/lms/modals/apply-leave-modal/apply-leave-modal.component';
import { Common } from 'src/app/shared/classes/common';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-apply-leave-hr',
  templateUrl: './apply-leave-hr.component.html',
  styleUrls: ['./apply-leave-hr.component.scss']
})
export class ApplyLeaveHrComponent implements OnInit {

  searchLst: Array<any> = []; typeLst: Array<any> = []; summaryLst: Array<any> = [];

  name: string = ''; selectedEmp : any; empInfo :any;

  showData: boolean = false;

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;

  summaryColumns: string[] = ['sl.no','from', 'to', 'days', 'type', 'appliedTo', 'status'];
  constructor(private cmnSvc: CommonService,
    public dialog: MatDialog,) { }

  ngOnInit(): void {
  }

  search = (e: any) => {
    if (e && ((e.length >= 3 && isNaN(+e)) || (e.length >= 1 && !isNaN(+e))))
      this.cmnSvc.getData(`employee/business/searchByNameOrEmployeeCode?nameOrEmployeeCode=${e}`).subscribe(
        res => {
          this.searchLst = res ? res.data : [];
        }
      );
      else
        this.searchLst = [];
  }

  selected = (e: any) => {
    this.selectedEmp = e.option.value;
    // this.name= this.selectedEmp.fullName;
    this.apply();
  }

  getOptionText(option) {
    return option?.fullName;
  }

  apply = () => {
    Promise.all([this.loadBalance(), this.getEmpInfo(), this.loadSummary()]).then(()=> {
      this.showData = true;
    })
  }

  loadBalance = () => {
    this.cmnSvc.getData(`lms/business/leaveEligibility?employeeCode=${this.selectedEmp.empId}`).subscribe(res => {
      this.typeLst = res ? res.data : [];
    });
  }

  getEmpInfo() {
    this.cmnSvc.getData(`lms/business/employeeInfo?employeeCode=${this.selectedEmp.empId}`).subscribe(resp => {
      if (resp)
        this.empInfo = resp.data;
    });
  }

  loadSummary = () => {
    this.cmnSvc.getData(`lms/business/myLeaveSummary?employeeCode=${this.selectedEmp.empId}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(res => {
      if(res) {
        this.summaryLst = res ? res.data.content : [];
        this.pageData = {data: res.data, table: 'lb'};
        this.length = res.data.totalElements;
      }
    });
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadSummary();
  }

  getDateFormat(val: any) {
    if(val) {
      let dateParts = val?.split("-");
      return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }
    else
     return new Date();
  }

  openApplyLeaveModal = () => {
    const dialogRef = this.dialog.open(ApplyLeaveModalComponent, {
      data: {
        leaveTypes: this.typeLst,
        empCode: this.selectedEmp.empId,
        empInfo: this.empInfo,
        onBehalf: true
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
      if (result == 'OK')
        Promise.all([this.loadBalance(), this.loadSummary()]);
    });
  }

  downloadReport(type: string = 'pdf') {
    this.cmnSvc.getData(`lms/business/myLeaveSummary?employeeCode=${this.selectedEmp.empId}&pageNumber=0&pageSize=${this.length}&reportFormat=${type}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }
}
