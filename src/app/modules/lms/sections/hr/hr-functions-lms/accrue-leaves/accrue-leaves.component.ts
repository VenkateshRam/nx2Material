import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-accrue-leaves',
  templateUrl: './accrue-leaves.component.html',
  styleUrls: ['./accrue-leaves.component.scss']
})
export class AccrueLeavesComponent implements OnInit {

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;

  displayedColumns = ['sl.no', 'type', 'month', 'ltype', 'runby', 'updated'];

  lstGrid: MatTableDataSource<any>;

  constructor(private cmnSvc: CommonService,
    private alertSvc: AlertService) { }

  ngOnInit(): void {
    this.loadData();
  }

  onCreditLeaves = () => {
    this.cmnSvc.putData('lms/business/accrueLeavesForCurrentMonth').subscribe(resp => {
      console.log(resp);
      if(resp) {
        this.alertSvc.showToast(resp.message, 'success');
        this.loadData();
      }
    })
  }

  loadData = () => {
    this.cmnSvc.getData(`lms/business/getLeaveAcrrualHistory?pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(resp => {
      this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content || resp.data : []);
      this.pageData = {data: resp.data, table: 'lb'};
      this.length = resp.data.totalElements;
    })
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  downloadReport(type: string = 'pdf') {
    this.cmnSvc.getData(`lms/business/getLeaveAcrrualHistory?pageNumber=0&pageSize=${this.length}&reportFormat=${type}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }

}
