import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HrFunctionsLmsRoutingModule } from './hr-functions-lms-routing.module';
import { LeaveAdjustmentsHrComponent } from './leave-adjustments-hr/leave-adjustments-hr.component';
import { ApplyLeaveHrComponent } from './apply-leave-hr/apply-leave-hr.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccrueLeavesComponent } from './accrue-leaves/accrue-leaves.component';


@NgModule({
  declarations: [
    LeaveAdjustmentsHrComponent,
       ApplyLeaveHrComponent,
       AccrueLeavesComponent,
  ],
  imports: [
    CommonModule,
    HrFunctionsLmsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class HrFunctionsLmsModule { }
