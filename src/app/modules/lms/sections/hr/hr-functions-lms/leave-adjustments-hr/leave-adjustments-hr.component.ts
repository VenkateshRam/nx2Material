import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ApplyLeaveModalComponent } from 'src/app/modules/lms/modals/apply-leave-modal/apply-leave-modal.component';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-leave-adjustments-hr',
  templateUrl: './leave-adjustments-hr.component.html',
  styleUrls: ['./leave-adjustments-hr.component.scss']
})
export class LeaveAdjustmentsHrComponent implements OnInit {

  searchLst: Array<any> = []; typeLst: Array<any> = []; adjustedLst: Array<any> = [];

  name: string = ''; selectedEmp: any; empInfo: any; leaveAdjustForm: FormGroup;

  showData: boolean = false;

  length: number; pageIndex: number = 0; pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];

  adjustedColumns: string[] = ['sl.no', 'type', 'days', 'comments', 'updatedDate', 'updatedBy'];

  @ViewChild("adjustTemplate") adjustTemplate: TemplateRef<any>;
  private templateDialogRef: MatDialogRef<TemplateRef<any>>

  constructor(private cmnSvc: CommonService,
    public dialog: MatDialog,
    private alertSvc: AlertService,
    private fb: FormBuilder,) { }

  ngOnInit(): void {
    this.formInit();
  }

  search = (e: any) => {
    if (e && ((e.length >= 3 && isNaN(+e)) || (e.length >= 1 && !isNaN(+e))))
      this.cmnSvc.getData(`employee/business/searchByNameOrEmployeeCode?nameOrEmployeeCode=${e}`).subscribe(
        res => {
          this.searchLst = res ? res.data : [];
        }
      );
      else
        this.searchLst = [];
  }

  getOptionText(option) {
    return option?.fullName;
  }

  selected = (e: any) => {
    this.selectedEmp = e.option.value;
    // this.name = this.selectedEmp.fullName;
    this.apply();
  }

  apply = () => {
    Promise.all([this.getEmpInfo(), this.loadGrid(), this.getAdjustedLeaves()]).then(() => {
      this.showData = true;
    })
  }

  getAdjustedLeaves = () => {
    this.cmnSvc.getData(`lms/business/leaveTypesThatCanBeAdjusted?employeeCode=${this.selectedEmp.empId}`).subscribe(res => {
      this.typeLst = res ? res.data : [];
    });
  }

  getEmpInfo() {
    this.cmnSvc.getData(`lms/business/employeeInfo?employeeCode=${this.selectedEmp.empId}`).subscribe(resp => {
      if (resp)
        this.empInfo = resp.data;
    });
  }

  loadGrid = () => {
    this.cmnSvc.getData(`lms/business/getLeaveAdjustmentReportByEmpId?employeeCode=${this.selectedEmp.empId}`).subscribe(res => {
      if (res) {
        this.adjustedLst = res ? res.data : [];
        this.setPagination(res?.data?.totalElements, res?.data?.size)
      }
    });
  }

  setPagination(length: number = 5, pageSize: number = 10) {
    this.length = length;
    this.pageSize = pageSize;
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.loadGrid();
  }

  getDateFormat(val: any) {
    if (val) {
      let dateParts = val?.split("-");
      return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }
    else
      return new Date();
  }

  formInit() {
    this.leaveAdjustForm = this.fb.group({
      action: [null, Validators.required],
      leaveType: [null, Validators.required],
      days: [null, Validators.required],
      comments: [null, Validators.required]
    });
  }

  get f() { return this.leaveAdjustForm?.controls; }

  openDialog(templateRef) {
    this.templateDialogRef = this.dialog.open(templateRef, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      // console.log(result)
      if (result == 'OK')
        this.loadGrid();
    });
  }

  onSubmit() {
    if (this.leaveAdjustForm.invalid)
      return;

    let payload = this.leaveAdjustForm.value;
    let count = (payload.action == 'increase' ? '+' : '-') + payload.days;
    this.cmnSvc.putData(`lms/business/adjustLeave?employeeCode=${this.selectedEmp.empId}&leaveType=${payload.leaveType}&days=${count}&comments=${payload.comments}`).subscribe(resp => {
      if (resp) {
        this.alertSvc.showToast(resp.message, 'success');
        this.templateDialogRef.close('OK')
      }
    });
  }

  downloadReport(type: string = 'pdf') {
    this.cmnSvc.getData(`lms/business/getLeaveAdjustmentReportByEmpId?employeeCode=${this.selectedEmp.empId}&pageNumber=0&pageSize=${this.length}&reportFormat=${type}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }
}

