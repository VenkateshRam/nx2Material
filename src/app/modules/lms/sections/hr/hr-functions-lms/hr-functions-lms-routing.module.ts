import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';
import { AccrueLeavesComponent } from './accrue-leaves/accrue-leaves.component';
import { ApplyLeaveHrComponent } from './apply-leave-hr/apply-leave-hr.component';
import { LeaveAdjustmentsHrComponent } from './leave-adjustments-hr/leave-adjustments-hr.component';

const routes: Routes = [
  { path: 'applyLeaveHr', component: ApplyLeaveHrComponent, canActivate:[AuthGuard], data: { screen: 'hrLeaveReports' } },
  { path: 'leaveadjustments', component: LeaveAdjustmentsHrComponent, canActivate:[AuthGuard], data: { screen: 'hrLeaveReports' } },
  { path: 'cron', component: AccrueLeavesComponent, canActivate:[AuthGuard], data: { screen: 'hrLeaveReports' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HrFunctionsLmsRoutingModule { }
