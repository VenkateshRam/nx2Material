import { Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-leave-actions',
  templateUrl: './leave-actions.component.html',
  styleUrls: ['./leave-actions.component.scss']
})
export class LeaveActionsComponent implements OnInit {

  reason: string = ''; modalHeader: string = ''; empId: string = '';

  reasonRequired: boolean = false;

  requestedData: any;
  constructor(private cmnSvc: CommonService,
    private alertSvc: AlertService,
    public dialogRef: MatDialogRef<LeaveActionsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    if (this.data) {
      this.modalHeader = this.data.header;
      this.reasonRequired = this.modalHeader == 'Cancel Leave';
      this.requestedData = this.data.reqData.leaveRequest;
    }
  }

  onSubmit = (req: any, reason: string) => {
    if (this.reasonRequired && reason == '') 
      return;

    let baseUrl = this.modalHeader == 'Approve Leave' ? 'approveLeave' : this.modalHeader == 'Reject Leave' ? 'rejectLeave' :
      this.modalHeader == 'Approve Cancellation' ? 'approveLeaveCancellation' : this.modalHeader == 'Reject Cancellation' ? 'rejectLeaveCancellation' : 'cancelLeave';

    this.cmnSvc.putData(`lms/business/${baseUrl}?leaveRequestId=${+req.id}&comments=${reason}`).subscribe(resp => {
      if (resp) {
        this.alertSvc.showToast(resp.message, 'success');
        this.dialogRef.close('OK');
      }
    });
  }

}
