import { DatePipe } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { COMMA, ENTER, V } from '@angular/cdk/keycodes';
import { CommonService } from 'src/app/shared/services/common.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Common } from 'src/app/shared/classes/common';

@Component({
  selector: 'app-apply-leave-modal',
  templateUrl: './apply-leave-modal.component.html',
  styleUrls: ['./apply-leave-modal.component.scss']
})
export class ApplyLeaveModalComponent implements OnInit {

  typeLst: any[] = []; searchLst: any[]; notifyLst: any[] = []; compOffDatesLst: Array<any> = [];
  paternityFromDaysLst: Array<any> = []; endDatesLst: Array<any> = [];

  today = new Date(); minDate: Date; maxDate: Date; expMaxDate: Date;

  showAutocomplete: boolean = false; disableHalfDay: boolean = false; onBehalf: boolean = false;

  empInfo: any;

  notifyCtrl = new FormControl(); applyLeaveForm: FormGroup;

  separatorKeysCodes: number[] = [ENTER, COMMA]; availableBalance: number = null; noOfDays: number = null;
  @ViewChild('notifyInput') notifyInput: ElementRef<HTMLInputElement>;

  empCode: string = ''; appliedBy: string = '';
  constructor(
    public dialogRef: MatDialogRef<ApplyLeaveModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private datePipe: DatePipe,
    private cmnSvc: CommonService,
    private alertSvc: AlertService,
    private authSvc: AuthService,
    private fb: FormBuilder) {
    // dialogRef.disableClose = true;
    this.formInit();

    this.appliedBy = this.authSvc.currentUserSubject.value.employeeCode;
  }

  ngOnInit(): void {
    //   this.dialogRef.keydownEvents().subscribe(event => {
    //     if (event.key === "Escape") {
    //         this.dialogRef.close();
    //     }
    // });
    this.getLastPayrollDate();

    if (this.data) {
      this.typeLst = this.data?.leaveTypes;
      this.empCode = this.data?.empCode;
      this.empInfo = this.data?.empInfo;
      this.onBehalf = this.data?.onBehalf;
    }
  }

  formInit() {
    this.applyLeaveForm = this.fb.group({
      empId: [''],
      leaveType: [null, Validators.required],
      fromDate: [null, Validators.required],
      halfDayStart: [false],
      toDate: [null, Validators.required],
      halfDayEnd: [false],
      appliedBy: [''],
      comments: [null, Validators.required],
      alternateContactNumber: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      appliedDate: this.datePipe.transform(this.today, 'yyyy-MM-dd'),
      informTo: [[]],
      expectedOrNewBornDate: [null],
      babyGender: [null],
      compOffDate: [null]
    });
  }

  get f() { return this.applyLeaveForm?.controls; }

  onRightClick(event) {
    event.preventDefault();
  }

  onKeydown(event) {
    Common.keycontrol(event);
  }

  onTypeSelection = (val: any) => {
    this.applyLeaveForm.patchValue({
      fromDate: null, toDate: null, expectedOrNewBornDate: null,
      halfDayStart: false, halfDayEnd: false, compOffDate: null, babyGender: null
    });
    this.availableBalance = this.typeLst.find(f => f.leaveType == val)?.leaveBalance;
    let mxDt = this.today.getFullYear() + '-' + (this.today.getMonth() + 4) + '-' + '25';
    this.maxDate = new Date(mxDt);
    this.noOfDays = null;

    if (val == 4 || val == 5) {
      this.applyLeaveForm.controls['expectedOrNewBornDate'].setValidators([Validators.required]);
      this.applyLeaveForm.controls['toDate'].setValidators([Validators.required]);
      this.applyLeaveForm.controls['compOffDate'].clearValidators;

      if (val == 5) {
        this.expMaxDate = new Date(this.today.setDate(this.today.getDate() + 7));
        this.applyLeaveForm.controls['babyGender'].setValidators([Validators.required]);
      }
      else {
        this.expMaxDate = new Date((this.today.getFullYear() + 1) + '-' + (this.today.getMonth() + 1) + '-' + this.today.getDate());
        this.applyLeaveForm.controls['babyGender'].clearValidators;
      }
    }
    else if (val == 6) {
      this.getCompOffs();
      this.applyLeaveForm.controls['compOffDate'].setValidators([Validators.required]);
      this.applyLeaveForm.controls['expectedOrNewBornDate'].clearValidators();
      this.applyLeaveForm.controls['toDate'].clearValidators();
      this.applyLeaveForm.controls['babyGender'].clearValidators;
    } else {
      this.applyLeaveForm.controls['expectedOrNewBornDate'].clearValidators();
      this.applyLeaveForm.controls['compOffDate'].clearValidators;
      this.applyLeaveForm.controls['toDate'].setValidators([Validators.required]);
      this.applyLeaveForm.controls['babyGender'].clearValidators;
    }

    this.applyLeaveForm.controls['toDate'].updateValueAndValidity();
    this.applyLeaveForm.controls['compOffDate'].updateValueAndValidity();
    this.applyLeaveForm.controls['expectedOrNewBornDate'].updateValueAndValidity();
    this.applyLeaveForm.controls['babyGender'].updateValueAndValidity();
  }

  getCompOffs() {
    this.cmnSvc.getData(`lms/business/availableCompOffs?employeeCode=${this.empCode}`).subscribe(resp => {
      this.compOffDatesLst = resp ? resp.data?.content || resp.data : [];

      if (resp && this.compOffDatesLst.length == 1) {
        this.applyLeaveForm.patchValue({ compOffDate: this.compOffDatesLst[0].compOffAgainstDate })
        this.maxDate = this.compOffDatesLst[0].expiryDate;
      }
    })
  }

  onDateChange = (event: MatDatepickerInputEvent<Date>, picker: string) => {
    let val = this.applyLeaveForm.value;
    this.calculateDays();
    if (picker == 'from') {
      if ((val.leaveType == 4 || val.leaveType == 5 || val.leaveType == 7))
        this.getEndDates(event);
    }
    if ((picker == 'from' || picker == 'to') && (val.fromDate && val.toDate)) {
      if (this.datePipe.transform(val.fromDate, 'dd/MM/yyyy') == this.datePipe.transform(val.toDate, 'dd/MM/yyyy')) {
        this.applyLeaveForm.patchValue({ halfDayEnd: false });
        this.disableHalfDay = true;
      }
      else
        this.disableHalfDay = false;
    }
  }

  calculateDays() {
    let val = this.applyLeaveForm.value;
    if (val.fromDate && val.toDate && val.leaveType) {
      let payload = {
        empId: this.empCode,
        leaveType: val.leaveType,
        fromDate: val.fromDate,
        halfDayStart: val.halfDayStart,
        toDate: val.toDate,
        halfDayEnd: val.halfDayEnd,
      };
      this.cmnSvc.postData('lms/business/getNumberOfDays', payload).subscribe(resp => {
        this.noOfDays = resp ? resp.data : null;
      })
    }
  }

  onSubmit = () => {
    this.applyLeaveForm.markAllAsTouched();

    if (this.applyLeaveForm.invalid)
      return;

    let payload = this.applyLeaveForm.getRawValue();
    let val = this.applyLeaveForm.value;
    payload.empId = this.empCode;
    payload.appliedBy = this.appliedBy;
    payload.informTo = this.notifyLst.map(f => f.empId);
    payload.fromDate = this.datePipe.transform(val.fromDate, 'yyyy-MM-dd');
    payload.toDate = this.datePipe.transform(val.leaveType == 6 ? val.fromDate : val.toDate, 'yyyy-MM-dd');
    payload.expectedOrNewBornDate = (val.leaveType == 4 || val.leaveType == 5)
      ? this.datePipe.transform(val.expectedOrNewBornDate, 'yyyy-MM-dd') : null
    payload.compOffDate = (val.leaveType == 6)
      ? this.datePipe.transform(val.compOffDate, 'yyyy-MM-dd') : null

    this.cmnSvc.postData('lms/business/applyLeave', payload).subscribe(res => {
      if (res) {
        this.alertSvc.showToast(res.message, 'success');
        this.dialogRef.close('OK');
      }
    })
  }

  dateFilter = (d: Date | null): boolean => {
    const day = (d || new Date());
    const hldy = this.empInfo.holidays.find(x => this.datePipe.transform(x.date, 'yyyy-MM-dd') == this.datePipe.transform(day, 'yyyy-MM-dd'));
    return day.getDay() !== 0 && day.getDay() !== 6 && !hldy;
  };

  remove = (empId: number) => {
    const index = this.notifyLst.findIndex(f => f.empId == empId);

    if (index >= 0) {
      this.notifyLst.splice(index, 1);
    }
  }

  selected = (e: any) => {
    this.notifyLst.push(e.option.value);
    const ids = this.notifyLst.map(o => o.empId);
    const filtered = this.notifyLst.filter(({ empId }, index) => !ids.includes(empId, index + 1));
    this.notifyLst = filtered;
    this.notifyInput.nativeElement.value = '';
    this.notifyCtrl.setValue([]);
  }

  search = (e: any) => {
    if (e && ((e.length >= 3 && isNaN(+e)) || (e.length >= 1 && !isNaN(+e))))
      this.cmnSvc.getData(`employee/business/searchByNameOrEmployeeCode?nameOrEmployeeCode=${e}`).subscribe(
        res => {
          this.searchLst = res ? res.data : [];
        }
      );
    else
      this.searchLst = [];
  }

  getLastPayrollDate = () => {
    this.cmnSvc.getData(`lms/business/getLastPayrollDate`).subscribe(res => {
      this.minDate = res ? new Date(res.data) : new Date();
    })
  }

  getPaternityFromDate = (e: any) => {
    let dt = this.datePipe.transform(e.value, 'yyyy-MM-dd');
    this.cmnSvc.getData(`lms/business/paternityLeave/getDates?newBornBabyDOB=${dt}&&employeeCode=${this.empCode}`).subscribe(res => {
      this.paternityFromDaysLst = res?.data;
    })
  }

  fromDateFilter = (d: Date): boolean => {
    const day = (d || new Date());
    const hldy = this.empInfo.holidays.find(x => this.datePipe.transform(x.date, 'yyyy-MM-dd') == this.datePipe.transform(day, 'yyyy-MM-dd'));
    return this.paternityFromDaysLst?.find(x => this.datePipe.transform(x, 'yyyy-MM-dd') == this.datePipe.transform(day, 'yyyy-MM-dd')) && !hldy;
  }

  getEndDates = (e: any) => {
    this.endDatesLst = [];
    let dt = this.datePipe.transform(e.value, 'yyyy-MM-dd');
    this.cmnSvc.getData(`lms/business/getToDate?leaveType=${this.applyLeaveForm.value.leaveType}&fromDate=${dt}&&employeeCode=${this.empCode}`).subscribe(res => {
      this.endDatesLst = res.data?.toDate;
      this.applyLeaveForm.patchValue({ toDate: new Date(this.endDatesLst[this.endDatesLst.length - 1]) });
      this.calculateDays();
    });
  }

  endDateFilter = (d: Date): boolean => {
    const day = (d || new Date());
    const hldy = this.empInfo.holidays.find(x => this.datePipe.transform(x.date, 'yyyy-MM-dd') == this.datePipe.transform(day, 'yyyy-MM-dd'));
    return this.endDatesLst.find(x => this.datePipe.transform(x, 'yyyy-MM-dd') == this.datePipe.transform(day, 'yyyy-MM-dd')) && !hldy;
  }

  onCompOffDate(coDate: any) {
    if (coDate) {
      this.maxDate = this.compOffDatesLst.find(f => f.compOffAgainstDate == coDate)?.expiryDate;
      console.log(this.maxDate)
    }
  }

  // noOfDays(from: Date, to: Date) {
  //   var oneDay = 24 * 60 * 60 * 1000;
  //   console.log(Math.round(Math.abs((from.getTime() - to.getTime()) / (oneDay))));
  // }
}
