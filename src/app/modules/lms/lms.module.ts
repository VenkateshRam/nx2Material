import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { LmsRoutingModule } from './lms-routing.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ApplyLeaveModalComponent } from './modals/apply-leave-modal/apply-leave-modal.component';
import { MyRequestsComponent } from './sections/employee/my-requests/my-requests.component';
import { MyTeamRequestsComponent } from './sections/manager/my-team-requests/my-team-requests.component';
import { LeaveActionsComponent } from './modals/leave-actions/leave-actions.component';


@NgModule({
  declarations: [
    MyRequestsComponent,
    ApplyLeaveModalComponent,
    MyTeamRequestsComponent,
    LeaveActionsComponent,
  ],
  imports: [
    CommonModule,
    LmsRoutingModule,
    MaterialModule
  ],
  entryComponents: [
    ApplyLeaveModalComponent,
    LeaveActionsComponent
  ],
  providers: [DatePipe]
})
export class LmsModule { }
