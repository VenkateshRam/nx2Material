import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-availed-leaves',
  templateUrl: './availed-leaves.component.html',
  styleUrls: ['./availed-leaves.component.scss']
})
export class AvailedLeavesComponent implements OnInit {

  fromDate: Date = null; toDate: Date = null;

  locationLst: Array<any> = []; lstGrid: MatTableDataSource<any>;

  location: string = ''; search: string = '';

  showData: boolean = false;

  response: any;

  leaveTypes: string[] = ["", "Casual leave", "Earned leave", "optional holiday",
    "Maternity leave", "Paternity leave", "Compoff leave", "Covid leave",
    "Period timeout leave", "Lop leave", "Carry forward Casual Leave"];
  displayedColumns = ['sl.no', 'empId', 'fullName', 'cl', 'el', 'oh', 'co', 'lop'];

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;
  constructor(private cmnSvc: CommonService,
    private datePipe: DatePipe,
    private alertSvc: AlertService) { }

  ngOnInit(): void {
    this.getClients();
  }

  getClients() {
    this.cmnSvc.getCore(`core/seeddata`).subscribe(resp => {
      this.locationLst = resp ? resp.data.CLIENT : [];
      this.location = this.locationLst[0].id
    })
  }

  onReinit() {
    this.pageIndex = 0;
    this.lstGrid = new MatTableDataSource([]);
  }

  onSubmit() {
    this.onReinit();
    this.loadData();
  }

  loadData() {
    if (!this.fromDate || !this.toDate) {
      this.alertSvc.showToast('Please select From Date and To Date', 'warning');
      return;
    }

    let frmDate = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
    let toDte = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');
    this.cmnSvc.getData(`lms/business/availedLeaveReport?fromDate=${frmDate}&toDate=${toDte}&workLocationId=${this.location}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}&employeeCodeName=${this.search}`).subscribe(resp => {
      if (resp) {
        this.response = resp;
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content : []);
        this.pageData = {data: resp.data, table: 'lb'};
        this.length = resp.data.totalElements;
        this.showData = true;
      }
    })
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  downloadReport(type: string = 'pdf') {
    if (!this.fromDate || !this.toDate) {
      this.alertSvc.showToast('Please select From Date and To Date', 'warning');
      return;
    }

    let frmDate = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
    let toDte = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');

    this.cmnSvc.getData(`lms/business/availedLeaveReport?fromDate=${frmDate}&toDate=${toDte}&workLocationId=${this.location}&pageNumber=0&pageSize=${this.length}&employeeCodeName=${this.search}&reportFormat=${type}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }

}
