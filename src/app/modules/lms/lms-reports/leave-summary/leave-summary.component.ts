import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-leave-summary',
  templateUrl: './leave-summary.component.html',
  styleUrls: ['./leave-summary.component.scss']
})
export class LeaveSummaryComponent implements OnInit {

  report: string = 'creditMonthWise'; name: string = null; selectedEmp: any = null; empCode: number; searchField: string = '';
  jobStatus: string = '0'; dateSelected: Date = null; yearSelected: string = null; today: Date = new Date();

  searchLst: Array<any> = [];
  statusLst: Array<any> = [{ id: '0', status: 'All' }, { id: '1', status: 'Only Current Employee' }, { id: '4', status: 'Only Ex Employee' }];
  yearLst: Array<any> = ['2022', '2021', '2020', '2019', '2018', '2017', '2016', '2015', '2014'];

  lstGrid: MatTableDataSource<any>; leaveSummaryResp: any;
  displayedColumns = ['sl.no', 'empId', 'fullName', 'el', 'cl', 'oh', 'co'];
  summaryColumns = ['empId', 'fullName', 'el', 'cl', 'oh', 'co'];

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;
  constructor(private cmnSvc: CommonService,
    private authSvc: AuthService,
    private datePipe: DatePipe,
    private alertSvc: AlertService) {
    this.empCode = this.authSvc.currentUserSubject.value.employeeCode;
  }

  ngOnInit(): void {
    console.log(this.today.getMonth())
  }

  search = (e: any) => {
    if (e && ((e.length >= 3 && isNaN(+e)) || (e.length >= 1 && !isNaN(+e))))
      this.cmnSvc.getData(`employee/business/searchByNameOrEmployeeCode?nameOrEmployeeCode=${e}`).subscribe(
        res => {
          this.searchLst = res ? res.data : [];
        }
      );
    else {
      this.searchLst = [];
      this.selectedEmp = null;
    }
  }

  selected = (e: any) => {
    this.selectedEmp = e.option.value;
  }

  getOptionText(option) {
    return option?.fullName;
  }

  onYearsChange() {
    this.lstGrid = new MatTableDataSource([]);
  }

  onReportChange() {
    this.dateSelected = this.selectedEmp = this.name = null;
    this.length = this.pageIndex = 0; this.pageSize = 10;
    this.lstGrid = new MatTableDataSource([]);
    this.leaveSummaryResp = [];
    this.yearSelected = this.yearLst[0];
    this.searchField = '';
  }
  
  onSubmit() {
    this.pageIndex = 0;
    this.lstGrid = new MatTableDataSource([]);
    this.loadData();
  }

  loadData() {
    if (this.report == 'creditMonthWise' && !this.dateSelected) {
      this.alertSvc.showToast('Select Start Date', 'warning');
      return;
    } else if (this.report == 'creditYearWise' && !this.yearSelected) {
      this.alertSvc.showToast('Select Start Date', 'warning');
      return;
    } else if (this.report == 'leaveSummary' && !this.selectedEmp) {
      this.alertSvc.showToast('Select Employee', 'warning');
      return;
    }

    let date = this.report == 'creditMonthWise' ? this.datePipe.transform(this.dateSelected, 'yyyy-MM-dd') :
      this.report == 'creditYearWise' ? `${this.yearSelected}-01-01` : '';
    let empId = this.selectedEmp ? this.selectedEmp.empId : '';
    this.cmnSvc.getData(`lms/business/leaveSummaryReport?nameOrEmployeeCode=${this.report == 'leaveSummary' ? empId : this.searchField}&date=${date}&reportType=${this.report}&jobStatus=${+this.jobStatus}&employeeCode=${this.report == 'leaveSummary' ? empId : this.searchField}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(resp => {
      if (resp && this.report != 'leaveSummary') {
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content : []);
        this.pageData = {data: resp.data, table: 'lb'};
        this.length = resp.data.totalElements;
      } else if (resp && this.report == 'leaveSummary')
        this.leaveSummaryResp.push(resp.data);
    })
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  getDisplayedColumns() {
    let months = ['jan', 'feb', 'mar', 'apr', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'];
    let columns = ['sl.no', 'empId', 'fullName']
    if (this.lstGrid.data.length > 0) {
      let cols = [...columns, ...months.filter((m, i) => i < (+this.yearSelected == this.today.getFullYear() ? (this.today.getMonth() + 1) : this.lstGrid.data[0].leavesCredited.length))];
      return cols;
    }
    else
      return [...columns, ...months];
  }

  downloadReport(type: string = 'pdf', subreport: string = '') {
    if (this.report == 'creditMonthWise' && !this.dateSelected) {
      this.alertSvc.showToast('Select Start Date', 'warning');
      return;
    } else if (this.report == 'creditYearWise' && !this.yearSelected) {
      this.alertSvc.showToast('Select Start Date', 'warning');
      return;
    } else if (this.report == 'leaveSummary' && !this.selectedEmp) {
      this.alertSvc.showToast('Select Employee', 'warning');
      return;
    }

    let date = this.report == 'creditMonthWise' ? this.datePipe.transform(this.dateSelected, 'yyyy-MM-dd') :
      this.report == 'creditYearWise' ? `${this.yearSelected}-01-01` : '';
    let empId = this.selectedEmp ? this.selectedEmp.empId : '';
    this.cmnSvc.getData(`lms/business/leaveSummaryReport?nameOrEmployeeCode=${this.report == 'leaveSummary' ? empId : this.searchField}&date=${date}&reportType=${this.report}&jobStatus=${+this.jobStatus}&employeeCode=${this.report == 'leaveSummary' ? empId : this.searchField}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}&reportFormat=${type}&subReport=${subreport}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    })
  }
}
