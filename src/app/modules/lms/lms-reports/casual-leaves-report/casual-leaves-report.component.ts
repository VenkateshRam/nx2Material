import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-casual-leaves-report',
  templateUrl: './casual-leaves-report.component.html',
  styleUrls: ['./casual-leaves-report.component.scss']
})
export class CasualLeavesReportComponent implements OnInit {

  lstGrid: MatTableDataSource<any>; importFile: any;
  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;

  displayedColumns = ['sl.no', 'empId', 'fullName', 'cf', 'used', 'acl', 'ad', 'ecl'];
  statusLst: Array<any> = [{ id: 1, status: 'Only Current Employee' }, { id: 4, status: 'Only Ex Employee' }];

  display: FormControl = new FormControl("", Validators.required);
  file_store: FileList;
  file_list: Array<string> = [];

  @ViewChild("template") template: TemplateRef<any>;
  private templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor(private alertSvc: AlertService,
    public dialog: MatDialog,
    private cmnSvc: CommonService) { }

  ngOnInit(): void {
    this.loadData();
  }

  
  loadData() {
    this.cmnSvc.getData(`lms/business/casualLeaveReportCurrentYear?pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(resp => {
      if (resp) {
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content : []);
        this.pageData = {data: resp.data, table: 'lb'};
        this.length = resp.data.totalElements;
      }
    })
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  downloadReport(type: string = 'pdf') {
    this.cmnSvc.getData(`lms/business/casualLeaveReportCurrentYear?pageNumber=0&pageSize=${this.length}&reportFormat=${type}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }

  downloadSample() {
    this.cmnSvc.getData(`lms/business/getSampleCsvUrl?leaveType=10`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }

  onImportData() {
    if (!this.file_store || this.file_store.length == 0) {
      this.alertSvc.showToast('Select File', 'warning');
      return;
    }

    var fd = new FormData();
    this.file_list = [];
    for (let i = 0; i < this.file_store.length; i++) {
      fd.append("file", this.file_store[i]);
      this.file_list.push(this.file_store[i].name);
    }
    this.cmnSvc.putData(`lms/business/carryForwardLeaves?leaveType=10`, fd).subscribe(res => {
      if (res) {
        // this.alertSvc.showToast('')
        this.templateDialogRef.close('OK')
      }
    });
  }

  handleFileInputChange(l: FileList): void {
    this.file_store = l;
    console.log(this.file_store);
    if (l.length) {
      const f = l[0];
      const count = l.length > 1 ? `(+${l.length - 1} files)` : "";
      this.display.patchValue(`${f.name}${count}`);
    } else {
      this.display.patchValue("");
    }
  }

  openDialog() {
    this.display = new FormControl("", Validators.required);
    this.file_store = null;
    this.templateDialogRef = this.dialog.open(this.template, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        this.loadData();
    });
  }

}
