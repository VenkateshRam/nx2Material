import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LmsReportsRoutingModule } from './lms-reports-routing.module';
import { AvailedLeavesComponent } from './availed-leaves/availed-leaves.component';
import { LeavesStatusComponent } from './leaves-status/leaves-status.component';
import { LeavesBalanceComponent } from './leaves-balance/leaves-balance.component';
import { LeaveTabsComponent } from './leave-tabs/leave-tabs.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeaveSummaryComponent } from './leave-summary/leave-summary.component';
import { CasualLeavesReportComponent } from './casual-leaves-report/casual-leaves-report.component';


@NgModule({
  declarations: [
    AvailedLeavesComponent,
    LeavesStatusComponent,
    LeavesBalanceComponent,
    LeaveTabsComponent,
    LeaveSummaryComponent,
    CasualLeavesReportComponent
  ],
  imports: [
    CommonModule,
    LmsReportsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LmsReportsModule { }
