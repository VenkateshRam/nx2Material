import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';
import { LeaveTabsComponent } from './leave-tabs/leave-tabs.component';

const routes: Routes = [
  {path:'tabs', component:LeaveTabsComponent, canActivate:[AuthGuard], data: { screen: 'hrLeaveReports' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LmsReportsRoutingModule { }
