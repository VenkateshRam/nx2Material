import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-leaves-balance',
  templateUrl: './leaves-balance.component.html',
  styleUrls: ['./leaves-balance.component.scss']
})
export class LeavesBalanceComponent implements OnInit {

  status: any[] = []; lstGrid: MatTableDataSource<any>;
  name: string = ''; selectedEmp: any; empInfo: any;
  searchLst: Array<any> = []; searchField: string = '';

  statusIds: Array<any> = [ //API Side values
    { JOB_STATUS_ACTIVE: 1 },
    { JOB_STATUS_LONG_LEAVE: 2 },
    { JOB_STATUS_RESIGNED: 3 },
    { JOB_STATUS_EX_EMPLOYEE: 4 },
    { JOB_STATUS_NEW_JOINEE: 5 }
  ];

  showData: boolean = false; response: any; 

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;

  displayedColumns = ['sl.no', 'empId', 'fullName', 'el', 'cl', 'oh', 'co'];
  statusLst: Array<any> = [{ id: 1, status: 'Only Current Employee' }, { id: 4, status: 'Only Ex Employee' }]
  constructor(private alertSvc: AlertService,
    private cmnSvc: CommonService) { }

  ngOnInit(): void {
  }

  onReinit() {
    this.pageIndex = 0;
    this.lstGrid = new MatTableDataSource([]);
  }
  
  onSubmit() {
    this.onReinit();
    this.loadData();
  }
  
  loadData() {
    if (this.status.length == 0 && !this.searchField) {
      this.alertSvc.showToast('Select atleast one Status or Employee', 'warning');
      return;
    }
    
    let sts = this.status.length > 0 ? (this.status.length == 2 ? '' : this.status[0].id) : '';
    this.cmnSvc.getData(`lms/business/getLeaveBalanceReport?jobStatus=${sts}&employeeCodeOrName=${this.searchField}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(resp => {
      if (resp) {
        this.response = resp;
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data.content : []);
        this.pageData = {data: resp.data, table: 'lb'};
        this.length = resp.data.totalElements;
        this.showData = true;
      }
    })
  }
  
  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  downloadReport(type: string = 'pdf') {
    if (this.status.length == 0 && !this.selectedEmp) {
      this.alertSvc.showToast('Select atleast one Status or Employee', 'warning');
      return;
    }

    let sts = this.status.length > 0 ? (this.status.length == 2 ? '' : this.status[0].id) : '';
    this.cmnSvc.getData(`lms/business/getLeaveBalanceReport?jobStatus=${sts}&employeeCodeOrName=${this.searchField}&pageNumber=0&pageSize=${this.length}&reportFormat=${type}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }

  search = (e: any) => {
    if (e && ((e.length >= 3 && isNaN(+e)) || (e.length >= 1 && !isNaN(+e))))
      this.cmnSvc.getData(`employee/business/searchByNameOrEmployeeCode?nameOrEmployeeCode=${e}`).subscribe(
        res => {
          this.searchLst = res ? res.data : [];
        }
      );
    else {
      this.searchLst = [];
      this.selectedEmp = null;
    }
  }

  selected = (e: any) => {
    this.selectedEmp = e.option.value;
    // this.name= this.selectedEmp.fullName;
  }

  getOptionText(option) {
    return option?.fullName;
  }

}
