import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-leaves-status',
  templateUrl: './leaves-status.component.html',
  styleUrls: ['./leaves-status.component.scss']
})
export class LeavesStatusComponent implements OnInit {

  fromDate: Date = null; toDate: Date = null; location: string = ''; type: string = ''; status: string = '';

  locationLst: Array<any> = []; typesLst: Array<any> = []; statusLst: Array<any> = []; dataSource: Array<any> = []

  showData: boolean = false; response: any;

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;

  displayedColumns = ['sl.no', 'empId', 'fullName', 'doj', 'type', 'fmDt', 'toDt', 'days', 'sts', 'appliedTo']
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private cmnSvc: CommonService,
    private alertSvc: AlertService,
    private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.getClients();
    this.getTypes();
    this.getStatus();
  }

  getClients() {
    this.cmnSvc.getCore(`core/seeddata`).subscribe(resp => {
      this.locationLst = resp ? resp.data.CLIENT : [];
    });
  }

  getTypes() {
    this.cmnSvc.getData(`lms/crud/getAll?entityName=LeaveType`).subscribe(resp => {
      this.typesLst = resp ? resp.data : [];
    });
  }

  getStatus() {
    this.cmnSvc.getData(`lms/business/getActionStatus`).subscribe(resp => {
      let sts = [];
      let data = resp ? resp.data : [];
      Object.keys(data).forEach(e => {
        let obj = { id: e, name: data[e] };
        sts.push(obj);
      });
      this.statusLst = sts;
    });
  }

  onReinit() {
    this.pageIndex = 0;
  }

  onSubmit() {
    this.onReinit();
    this.loadData();
  }

  loadData() {
    if (!this.fromDate || !this.toDate) {
      this.alertSvc.showToast('Please select From Date and To Date', 'warning');
      return;
    }

    let frmDate = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
    let toDte = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');
    this.cmnSvc.getData(`lms/business/getLeaveWfhReports?startDate=${frmDate}&endDate=${toDte}&workLocation=${this.location}&leaveType=${this.type}&status=${this.status}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}`).subscribe(resp => {
      if (resp) {
        this.response = resp;
        this.dataSource = resp ? resp.data.content : [];
        this.pageData = {data: resp.data, table: 'lb'};
        this.length = resp.data.totalElements;
        this.showData = true;
      }
    });
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }
  
  downloadReport(type: string = 'pdf') {
    if (!this.fromDate || !this.toDate) {
      this.alertSvc.showToast('Please select From Date and To Date', 'warning');
      return;
    }

    let frmDate = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
    let toDte = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');
    this.cmnSvc.getData(`lms/business/getLeaveWfhReports?startDate=${frmDate}&endDate=${toDte}&workLocation=${this.location}&leaveType=${this.type}&status=${this.status}&pageNumber=0&pageSize=${this.length}&reportFormat=${type}`).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }
}
