import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyTeamTsComponent } from './my-team-ts/my-team-ts.component';
import { TasksComponent } from './tasks/tasks.component';
import { TimeSheetsComponent } from './time-sheets.component';
import { ViewTimeSheetsComponent } from './view-time-sheets/view-time-sheets.component';
import { TeamTsReportComponent } from './reports/team-ts-report/team-ts-report.component';
import { ComplianceReportComponent } from './reports/compliance-report/compliance-report.component';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';

const routes: Routes = [
  { path: 'ts', component: TimeSheetsComponent },
  { path: 'teamts', component: MyTeamTsComponent },
  { path: 'tasks', component: TasksComponent },
  { path: 'viewts', component: ViewTimeSheetsComponent },
  { path: 'cr', component: ComplianceReportComponent },
  { path: 'tsreport', component: TeamTsReportComponent },
  { path: 'empDb', component: EmployeeInfoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeSheetsRoutingModule { }
