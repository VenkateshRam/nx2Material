import { trigger, state, style, transition, animate } from '@angular/animations';
import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ViewTasksComponent } from '../modals/view-tasks/view-tasks.component';

@Component({
  selector: 'app-view-time-sheets',
  templateUrl: './view-time-sheets.component.html',
  styleUrls: ['./view-time-sheets.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ViewTimeSheetsComponent implements OnInit {

  displayedColumns = [{ day: 'Sun', pos: 1 }, { day: 'Mon', pos: 2 }, { day: 'Tue', pos: 3 }, { day: 'Wed', pos: 4 }, { day: 'Thu', pos: 5 }, { day: 'Fri', pos: 6 }, { day: 'Sat', pos: 0 }];
  columnsToDisplay = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  monthlyColumns = ['date', 'name', 'hours', 'work', 'status'];
  currentDates: any; selectedDate: any; showChild: boolean = false;

  viewDay = new Date(); today = new Date(); msDay = new Date();

  lstGrid: MatTableDataSource<any>;
  monthlyGrid: MatTableDataSource<any>;
  empCode: number; empName: string = '';

  @ViewChild("modalTemplate") template: TemplateRef<any>;
  protected templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor(private datePipe: DatePipe,
    private dialog: MatDialog,
    private cmnSvc: CommonService,
    private authSvc: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params = {}) => {
      this.empCode = params.id;
      this.empName = params.name;
      this.loadData();
      this.getMonthlySummary();
    });
  }

  getWeeksInMonth() {
    const weeks = [],
      firstDate = new Date(this.lstGrid.data[0].date);

    let dayOfWeekCounter = firstDate.getDay();

    this.lstGrid.data.forEach(e => {
      if (dayOfWeekCounter === 1 || weeks.length === 0) {
        weeks.push([]);
      }

      dayOfWeekCounter = (dayOfWeekCounter + 1) % 7;
      Object.assign(e, { position: dayOfWeekCounter });
      weeks[weeks.length - 1].push(e);
    });

    let wks = [];
    weeks.forEach(e => {
      var object = e.reduce((obj, item) => (obj[item.position] = item, obj), {});
      wks.push(object)
    });

    return wks;
  }


  getDays(details: any, position: number) {
    return details.dates.find(g => g.position == position)?.date;
  }

  onDay(details: any, position: number) {
    console.log(details.dates.find(g => g.position == position)?.date)
  }

  isToday(details: any) {
    //  let date = details.dates.find(g=> g.position == position)?.date
    return this.datePipe.transform(details, 'dd/MM/yyyy') == this.datePipe.transform(this.today, 'dd/MM/yyyy');
  }

  onArrow(type: string) {
    if (type == 'prev') {
      this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() - 1, 1);
      this.loadData();
    }
    else if (type == 'next') {
      this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() + 1, 1);
      this.loadData();
    }
    else if (type == 'today') {
      this.viewDay = this.today;
      this.loadData();
    }
  }

  onMSArrow(action: string) {
    if (action == 'prev') {
      this.msDay = new Date(this.msDay.getFullYear(), this.msDay.getMonth() - 1, 1);
      this.getMonthlySummary();
    }
    else {
      this.msDay = new Date(this.msDay.getFullYear(), this.msDay.getMonth() + 1, 1);
      this.getMonthlySummary();
    }
  }

  openDialog() {
    this.templateDialogRef = this.dialog.open(this.template, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      // if (result == 'OK')
      //   this.loadData();
    });
  }

  openViewModal = (date: string, projId: number, title: string, hrs: string) => {
    const dialogRef = this.dialog.open(ViewTasksComponent, {
      width: '700px',
      data: {
        empCode: this.empCode,
        date: date,
        projectId: projId,
        title: title,
        hours: hrs
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        console.log('OK')
    });
  }

  loadData() {
    let dt = this.datePipe.transform(this.viewDay, 'yyyy-MM-dd');
    this.cmnSvc.getTsData(`timesheets/business/getTimeSheets?employeeCode=${this.empCode}&month=${dt}`).subscribe(resp => {
      if (resp) {
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data : []);
        this.currentDates = this.lstGrid ? this.getWeeksInMonth() : [];
      }
    })
  }

  onDate(val: any) {
    this.selectedDate = val.date;
    this.showChild = true;
  }

  onView(val: any, item: any) {
    let info = item.info.split('-');
    this.openViewModal(val.date, item.projectId, info[0], info[1]);
  }

  receiveMessage($event) {
    let data = $event;
    this.showChild = false;
    if (data == 'OK') {
      this.loadData();
      this.getMonthlySummary();
    }
    // else
    //   Common.loadDataTable(this.lstGrid, '#example');
  }

  getMonthlySummary() {
    let dt = this.datePipe.transform(this.msDay, 'yyyy-MM-dd');
    this.cmnSvc.getTsData(`timesheets/business/getMonthSummary?employeeCode=${this.empCode}&date=${dt}`).subscribe(resp => {
      if (resp) {
        this.monthlyGrid = new MatTableDataSource(resp.data ? resp.data : []);
      }
    })
  }

}

