import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimeSheetsRoutingModule } from './time-sheets-routing.module';
import { TimeSheetsComponent } from './time-sheets.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { MyTeamTsComponent } from './my-team-ts/my-team-ts.component';
import { ViewTasksComponent } from './modals/view-tasks/view-tasks.component';
import { TasksComponent } from './tasks/tasks.component';
import { ViewTimeSheetsComponent } from './view-time-sheets/view-time-sheets.component';
import { TeamTsReportComponent } from './reports/team-ts-report/team-ts-report.component';
import { ComplianceReportComponent } from './reports/compliance-report/compliance-report.component';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';


@NgModule({
  declarations: [
    TimeSheetsComponent,
    MyTeamTsComponent,
    ViewTasksComponent,
    TasksComponent,
    ViewTimeSheetsComponent,
    ComplianceReportComponent,
    TeamTsReportComponent,
    EmployeeInfoComponent,
  ],
  imports: [
    CommonModule,
    TimeSheetsRoutingModule,
    MaterialModule
  ]
})
export class TimeSheetsModule { }
