import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ViewTasksComponent } from '../modals/view-tasks/view-tasks.component';

@Component({
  selector: 'app-my-team-ts',
  templateUrl: './my-team-ts.component.html',
  styleUrls: ['./my-team-ts.component.scss']
})
export class MyTeamTsComponent implements OnInit {

  selected: Date | null; today = new Date(); viewDay = new Date();
  expandedElement = null; empCode: number; revertId: number = null; currentMonthIndex: number = 0;

  displayedColumns = []; columnsToDisplay = []; reportingColumns = []; approvingItems = []; currentData = [];
  tsLst = []; lvLst = [];
  selectAll: boolean = false;

  lstGrid: MatTableDataSource<any>;
  reportingGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  colsLst: Array<any> = [];

  @ViewChild("alertTemplate") alertTemplate: TemplateRef<any>;
  private templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor(private datePipe: DatePipe,
    private cmnSvc: CommonService,
    private authSvc: AuthService,
    private alertSvc: AlertService,
    private dialog: MatDialog,
    private router: Router) { }

  ngOnInit(): void {
    // this.displayedColumns = this.getWeeksInMonth(this.today.getFullYear(), this.today.getMonth());
    this.empCode = this.authSvc.currentUserSubject.value.employeeCode;
    this.loadData();
  }


  openViewModal = (date: string, projId: number, title: string, hrs: string, empCode: string) => {
    const dialogRef = this.dialog.open(ViewTasksComponent, {
      width: '700px',
      data: {
        empCode: empCode,
        date: date,
        projectId: projId,
        title: title,
        hours: hrs
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        console.log('OK')
    });
  }

  onView(val: any, item: any) {
    this.openViewModal(item.date, val.projectId, val.projectName, item.info, val.employeeCode);
  }

  loadData() {
    this.cmnSvc.getTsData(`timesheets/business/getTimeSheetsSummary?payrollCycle=${this.currentMonthIndex}`).subscribe(resp => {
      if (resp) {
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data.approvalDto.employeeInfoList : []);
        this.reportingGrid = new MatTableDataSource(resp.data ? resp.data.infoDto.employeeInfoList : []);
        // this.currentData = resp.data.approvalDto.employeeInfoList;
        this.displayedColumns = resp.data.datesList;
        this.columnsToDisplay = ['select', 'Name', 'Project', ...this.displayedColumns];
        this.reportingColumns = ['Name', 'Project', ...this.displayedColumns];
      }
    })
  }

  checkFilled(arr: any) {
    let item = arr.find(f => f != null);
    return item;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.lstGrid.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.lstGrid.data.forEach(row => this.selection.select(row));
  }

  onRowSelection(k, event) {
    let data = [];
    data = this.lstGrid.data[k].lineInfoArray;
    let dta = [].concat(...data);

    if (!event)
      dta = dta.map(f => f ? f.checked = false : null);
    else
      dta = dta.map(f => f ? f.checked = true : null);
  }

  onItemSelection(id, event, type: string = 'ts') {
    if (type == 'ts') {
      if (event.checked)
        this.tsLst.push(id);
      else
        this.tsLst.splice(this.tsLst.indexOf(id), 1);
    }
    if (type == 'lv') {
      if (event.checked)
        this.lvLst.push(id);
      else
        this.lvLst.splice(this.lvLst.indexOf(id), 1);
    }

    console.log(this.tsLst, this.lvLst);
  }

  onSave(action: string) {
    let data = this.lstGrid.data.filter(f => f.checked);
    if (data.length == 0 && this.tsLst.length == 0 && this.lvLst.length == 0) {
      this.alertSvc.showToast('Select atleast one', 'warning');
      return;
    }

    let [ts, lv] = [this.tsLst, this.lvLst];
    data.forEach(e => {
      let tsIds = [].concat(...e.lineInfoArray).filter(f => (f != null && f?.checked && (f.type == 11))).map(g => g.id);
      let lvIds = [].concat(...e.lineInfoArray).filter(f => (f != null && f?.checked && (f.type == 21))).map(g => g.id);
      ts = [...ts, ...tsIds];
      lv = [...lv, ...lvIds];
    });
    console.log(ts, lv);

    if (ts.length > 0 || lv.length > 0)
      this.onAction(action, [...new Set(ts)], [...new Set(lv)]);
    else
      this.alertSvc.showToast('Select atleast one', 'warning');

  }

  onSelectAll(val: boolean) {
    this.lstGrid.data.forEach((e, i) => {
      e.checked = val;
      this.onRowSelection(i, val);
    });
  }

  onAction(action: string, tsLst: any, leavesLst: any) {
    let lvbaseUrl = action == 'approve' ? 'approveLeave' : 'rejectLeave';
    let tsbaseUrl = action == 'approve' ? 'approveTimeSheets' : 'rejectTimeSheets';
    let payloads = [];

    if (tsLst.length > 0) {
      let ids = tsLst.join(',');
      payloads.push(this.cmnSvc.putTsData(`timesheets/business/${tsbaseUrl}?ids=${ids}`));
    }

    //Leaves
    leavesLst.forEach(e => {
      let call = this.cmnSvc.putData(`lms/business/${lvbaseUrl}?leaveRequestId=${e}&comments=`);
      payloads.push(call);
    });

    forkJoin(payloads).subscribe(resp => {
      if (resp) {
        this.alertSvc.showToast(`${action == 'approve' ? 'Approved' : 'Rejected'} Successfully`, 'success');
        this.loadData();
        this.onRefresh();
      }
    });
  }

  onRefresh() {
    this.tsLst = [];
    this.lvLst = [];
    this.selectAll = false;
  }

  onRevert(id: number) {
    this.revertId = +id;
    this.templateDialogRef = this.dialog.open(this.alertTemplate, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      if (result == 'OK') {
        this.alertSvc.showToast(`Reverted Successfully`, 'success');
        this.loadData();
        this.onRefresh();
      }
    });
  }

  onRevertSubmit() {
    this.cmnSvc.putTsData(`timesheets/business/revertTimeSheets?ids=${this.revertId}`).subscribe(res => {
      if (res) {
        this.revertId = null;
        this.templateDialogRef.close('OK');
      }
    })
  }

  onArrow(arg: string = 'next') {
    if (arg == 'next') {
      this.currentMonthIndex += 1;
      this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() + 1, 1);
    }
    else {
      this.currentMonthIndex -= 1;
      this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() - 1, 1);
    }
      this.loadData();
  }

  checkNext() {
    return new Date(this.viewDay).getMonth() >= new Date().getMonth()
  }
}

