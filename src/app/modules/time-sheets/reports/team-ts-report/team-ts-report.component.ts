  import { DatePipe } from '@angular/common';
  import { Component, OnInit } from '@angular/core';
  import { MatTableDataSource } from '@angular/material/table';
  import { forkJoin } from 'rxjs';
import { Common } from 'src/app/shared/classes/common';
  import { AlertService } from 'src/app/shared/services/alert.service';
  import { CommonService } from 'src/app/shared/services/common.service';
  
  @Component({
    selector: 'app-team-ts-report',
    templateUrl: './team-ts-report.component.html',
    styleUrls: ['./team-ts-report.component.scss']
  })
  export class TeamTsReportComponent implements OnInit {
  
    fromDate: Date = null; toDate: Date = null; today = new Date(); viewDay = new Date();
  
    type: string = ''; status: string = ''; departmentId: string = ''; practiceId: string = '';
    accountId: string = ''; projectId: string = ''; managerId: string = ''; empType: string = '';
    searchEmp: string = ''; empCode: string = '';
  
    dataSource: Array<any> = [];
    columnsToDisplay: Array<any> = [];
  
    showData: boolean = false; response: any;
  
    length: number; pageIndex: number = 0; pageSize: number = 10; currentMonthIndex: number = 0;
    pageData: any;
  
    displayedColumns = [];
    lstGrid: MatTableDataSource<any>;
  
    constructor(private cmnSvc: CommonService,
      private alertSvc: AlertService,
      private datePipe: DatePipe) { }
  
    ngOnInit(): void {
      this.loadData();
    }
  
    loadData() {
      this.cmnSvc.getTsData(`timesheets/business/getTeamTimesheetReport?payrollCycle=${this.currentMonthIndex}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}` + ((this.empCode) ? ('&nameOrEmployeeCode=' + this.empCode) : '')).subscribe(resp => {
        if (resp && resp.data) {
          this.response = resp;
          this.lstGrid = new MatTableDataSource(resp.data.timeSheetInfoList.content);
          this.displayedColumns = resp.data.datesList;
          this.columnsToDisplay = ['Name', 'Project', 'ProjectManager', 'ReportingManager', ...this.displayedColumns];
          this.pageData = {data: resp.data.timeSheetInfoList, table: 'lb'};
        this.length = resp.data.timeSheetInfoList.totalElements;
          this.showData = true;
        } 
        else if (resp && !resp.data) {
          this.alertSvc.showToast(resp.message, 'info');
          this.displayedColumns = [];
          this.columnsToDisplay = ['Name', 'Project', 'ProjectManager', 'ReportingManager', ...this.displayedColumns];
          this.lstGrid = new MatTableDataSource([]);
        }
      });
    }
  
    onReinit() {
      this.pageIndex = 0;
      this.lstGrid = new MatTableDataSource([]);
    }
    
    onSubmit() {
      this.onReinit();
      this.loadData();
    }
    
  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

    onArrow(arg: string = 'next') {
      if (arg == 'next') {
        this.currentMonthIndex += 1;
        this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() + 1, 1);
      }
      else {
        this.currentMonthIndex -= 1;
        this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() - 1, 1);
      }
        this.onSubmit();
    }

    checkNext() {
      return new Date(this.viewDay).getMonth() >= new Date().getMonth()
    }
  
    downloadReport(type: string = 'xlsx') {
      this.cmnSvc.getTsData(`timesheets/business/getTeamTimesheetReport?payrollCycle=${this.currentMonthIndex}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}&reportFormat=${type}` + ((this.empCode) ? ('&nameOrEmployeeCode=' + this.empCode) : '')).subscribe(res => {
        if (res)
          Common.openDocFile(res.data);
      });
    }
  }
  