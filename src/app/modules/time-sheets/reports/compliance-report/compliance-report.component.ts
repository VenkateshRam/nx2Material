import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin } from 'rxjs';
import { Common } from 'src/app/shared/classes/common';
import { AlertService } from 'src/app/shared/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-compliance-report',
  templateUrl: './compliance-report.component.html',
  styleUrls: ['./compliance-report.component.scss']
})
export class ComplianceReportComponent implements OnInit {

  fromDate: Date = null; toDate: Date = null;

  type: string = ''; status: string = ''; departmentId: string = ''; practiceId: string = '';
  accountId: string = ''; projectId: string = ''; managerId: string = ''; empType: string = '';
  searchEmp: string = '';

  managersLst: Array<any> = []; departmentLst: Array<any> = []; practiceLst: Array<any> = [];
  clientsLst: Array<any> = []; projectsLst: Array<any> = []; dataSource: Array<any> = [];
  columnsToDisplay: Array<any> = [];

  showData: boolean = false; response: any; coreSeedData: any;

  length: number; pageIndex: number = 0; pageSize: number = 10; pageData: any;

  displayedColumns = [];
  lstGrid: MatTableDataSource<any>;

  constructor(private cmnSvc: CommonService,
    private alertSvc: AlertService,
    private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.getMasters();
  }

  getMasters() {
    const managers = this.cmnSvc.getTsData(`employee/business/getReportingManagers`);
    const seed = this.cmnSvc.getCore(`core/seeddata`);
    const projects = this.cmnSvc.getData(`lms/crud/getAll?entityName=ProjectMaster`);
    // const practice = this.cmnSvc.getData(`lms/crud/getAll?entityName=PracticeMaster`);
    // const department = this.cmnSvc.getData(`lms/crud/getAll?entityName=DepartmentMaster`);
    forkJoin([managers, seed, projects]).subscribe(
      {
        next: res => {
          res.forEach((list, ind) => {
            if (list.status === 'OK') {
              const assignList = {
                '0': () => this.managersLst = list.data,
                '1': () => this.coreSeedData = list.data,
                '2': () => this.projectsLst = list.data,
              }
              assignList[ind]();
            }
          });
        },
        error: err => this.alertSvc.showToast('something wrong occurred', 'error'),
        complete: () => {
          this.departmentLst = this.coreSeedData.DEPARTMENT;
          this.clientsLst = this.coreSeedData.CLIENT;
        }
      });
  }

  onReinit() {
    this.pageIndex = 0;
    this.lstGrid = new MatTableDataSource([]);
  }

  onSubmit() {
    this.onReinit();
    this.loadData();
  }

  onDeptChange(deptId: any) {
    this.practiceLst = deptId ? this.departmentLst.find(f=> f.id == +deptId)?.practices : [];
  }

  loadData() {
    if (!this.fromDate || !this.toDate) {
      this.alertSvc.showToast('Please select Start Date and End Date', 'warning');
      return;
    }

    let frmDate = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
    let toDte = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');
    this.cmnSvc.getTsData(`timesheets/business/getTimesheetComplianceReport?startDate=${frmDate}&endDate=${toDte}&departmentId=${this.departmentId}&practiceId=${this.practiceId}&accountId=${this.accountId}&projectId=${this.projectId}&managerId=${this.managerId}&employeeType=${this.empType}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}` + ((this.searchEmp) ? ('&nameOrEmployeeCode=' + this.searchEmp) : '')).subscribe(resp => {
      if (resp && resp.data) {
        this.response = resp;
        this.lstGrid = new MatTableDataSource(resp.data.timeSheetInfoList.content);
        this.displayedColumns = resp.data.datesList;
        this.columnsToDisplay = ['EmpId', 'Name', 'Location', 'PracticeName', 'RM',
          'NotFilled', 'Filled', 'Approved', 'Rejected', 'Pending', 'Leaves', 'Holidays', ...this.displayedColumns];
        // this.setPagination(resp?.data?.timeSheetInfoList?.totalElements, resp.data?.timeSheetInfoList?.size);
        this.pageData = { data: resp.data.timeSheetInfoList, table: 'lb' };
        this.length = resp.data.timeSheetInfoList.totalElements;
        this.showData = true;
      }
      else if (resp && !resp.data) {
        this.alertSvc.showToast(resp.message, 'info');
        this.displayedColumns = [];
        this.columnsToDisplay = ['EmpId', 'Name', 'Location', 'PracticeName', 'RM',
          'NotFilled', 'Filled', 'Approved', 'Rejected', 'Pending', 'Leaves', 'Holidays', ...this.displayedColumns];
        this.lstGrid = new MatTableDataSource([]);
      }
    });
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadData();
  }

  downloadReport(type: string = 'xlsx') {
    if (!this.fromDate || !this.toDate) {
      this.alertSvc.showToast('Please select Start Date and End Date', 'warning');
      return;
    }

    let frmDate = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
    let toDte = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');
    this.cmnSvc.getTsData(`timesheets/business/getTimesheetComplianceReport?startDate=${frmDate}&endDate=${toDte}&departmentId=${this.departmentId}&practiceId=${this.practiceId}&accountId=${this.accountId}&projectId=${this.projectId}&managerId=${this.managerId}&employeeType=${this.empType}&pageNumber=${this.pageIndex}&pageSize=${this.pageSize}&reportFormat=${type}` + ((this.searchEmp) ? ('&nameOrEmployeeCode=' + this.searchEmp) : '')).subscribe(res => {
      if (res)
        Common.openDocFile(res.data);
    });
  }
}
