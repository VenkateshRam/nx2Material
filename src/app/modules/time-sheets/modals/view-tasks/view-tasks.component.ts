import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.scss']
})
export class ViewTasksComponent implements OnInit {

  displayedColumns: string[] = ['demo-type', 'demo-balance']; lstGrid: Array<any> = [];
  disableAnimation = true;

  title: string = ''; hours: string = '';
  constructor(
    public dialogRef: MatDialogRef<ViewTasksComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cmnSvc: CommonService,
  ) { }

  ngOnInit(): void {
    if (this.data) {
      this.title = this.data.title;
      this.hours = this.data.hours;
      this.loadData(this.data);
    }
  }

  loadData(val: any) {
    this.cmnSvc.getTsData(`timesheets/business/getAddedTasks?employeeCode=${val.empCode}&date=${val.date}&projectId=${val.projectId}`).subscribe(resp => {
      if(resp) {
        this.lstGrid = resp.data;
      }
    })
  }

  ngAfterViewInit(): void {
    // timeout required to avoid the dreaded 'ExpressionChangedAfterItHasBeenCheckedError'
    setTimeout(() => this.disableAnimation = false);
  }

}
