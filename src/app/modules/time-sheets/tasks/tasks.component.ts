import { DatePipe, ViewportScroller } from '@angular/common';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin } from 'rxjs';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  @Input() taskDate: any;
  @Input() projectDetails: any;
  @Input() employeeCode: any;

  @Output() messageEvent = new EventEmitter<any>();

  today: Date = new Date();
  viewDay: Date = new Date();

  tasksLst: Array<any> = []; backupLst: Array<any> = []; existingTaskIdsLst: Array<any> = [];
  displayedColumns = ['task', 'hours', 'desc'];
  constructor(private cmnSvc: CommonService,
    private authSvc: AuthService,
    private viewport: ViewportScroller,
    private datePipe: DatePipe,
    private alertSvc: AlertService) {
  }

  ngOnInit(): void {
    if (this.taskDate && this.employeeCode)
      this.loadData();
    // console.log(this.getAllDaysInMonth(this.today.getFullYear(), this.today.getMonth()));
  }

  loadData() {
    this.existingTaskIdsLst = [];
    this.cmnSvc.getTsData(`timesheets/business/getTasks?employeeCode=${this.employeeCode}&date=${this.datePipe.transform(new Date(this.taskDate), 'yyyy-MM-dd')}`).subscribe(resp => {
      if (resp) {
        this.tasksLst = resp.data ? resp.data : [];
        let ids = [];
        this.tasksLst.forEach(e => {
          e.totalHours = +e.timesheetDtoList?.map(li => { if (li.hours) return +li.hours; else return 0 }).reduce((sum, val) => sum + +val, 0).toFixed(2);
          if (!this.checkStatus(e.projectId))
            ids.push(e.timesheetDtoList?.filter(f => f.id != 0).map(li => li.id));
        });
        this.existingTaskIdsLst = [].concat(...ids);
        this.backupLst = JSON.parse(JSON.stringify(this.tasksLst));
      }
    })
  }

  onHours(projId: number) {
    let dta = this.tasksLst.find(f => f.projectId == projId);
    dta.totalHours = +dta.timesheetDtoList?.map(li => { if (li.hours) return +li.hours; else return 0 }).reduce((sum, val) => sum + +val, 0).toFixed(2);
  }

  onHoursChange(val: any) {
    if (val && +val == 0) {
      this.alertSvc.showToast('Zero Hours are not allowed', 'warning');
    }
    if (!Number.isInteger(+val)) {
      let decPart = val.split(".")[1];
      if (+decPart != 50 && +decPart != 25 && decPart !== 5 && +decPart != 75)
        this.alertSvc.showToast('Please enter minutes in scale: .25, .5, .75', 'warning');
    }
  }

  onHoursCheck(val: string) {
    if (val && !Number.isInteger(+val)) {
      let decPart = val.toString().split(".")[1];
      if (+decPart != 50 && +decPart != 25 && decPart !== '5' && +decPart != 75)
        return true;
    }

    return false;
  }

  getAllDaysInMonth(year, month) {
    const date = new Date(year, month, 1);

    const dates = [];

    while (date.getMonth() === month) {
      dates.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }

    return dates;
  }

  onArrow(action: string) {
    if (action == 'prev') {
      this.taskDate = new Date(new Date(this.taskDate).getTime() - 24 * 60 * 60 * 1000);
      this.loadData();
    }
    else {
      this.taskDate = new Date(new Date(this.taskDate).getTime() + 24 * 60 * 60 * 1000);
      this.loadData();
    }
  }

  onBack() {
    this.messageEvent.emit('OK');
  }

  onScrollToTop() {
    document.getElementById("targetTop").scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }

  onSave() {
    let totalHours = +this.tasksLst?.map(li => li.totalHours).reduce((sum, val) => sum + +val, 0).toFixed(2);
    if (+totalHours > 24) {
      this.alertSvc.showToast('Total hours in a day cannot be more than 24', 'warning');
      return;
    }

    let data = [];
    data = this.tasksLst.map(e => { if (this.checkStatus(e.projectId)) return []; else return e.timesheetDtoList });
    let dta = [].concat(...data);

    if (dta.some(f => this.onHoursCheck(f.hours))) {
      this.alertSvc.showToast('Please enter minutes in scale: .25, .5, .75', 'warning');
      return;
    } else if (dta.some(f => (f.hours && !f.comments))) {
      this.alertSvc.showToast('Please Enter Description for the task', 'warning');
      return;
    } else if (dta.find(f => (f.hours && f.hours != null && +f.hours === 0))) {
      this.alertSvc.showToast('Zero Hours are not allowed', 'warning');
      return;
    } else if (!dta.some(f => (f.hours && f.comments))) {
      this.alertSvc.showToast('Please fill your tasks', 'warning');
      return;
    }

    let lst = dta.filter(f => (f.hours && f.comments));
    let ids = lst.filter(f => f.id != 0).map(li => li.id);
    let removedIds = [];
    removedIds = this.existingTaskIdsLst.filter(e => !ids.includes(e));

    let payloads = [];
    lst.forEach(e => {
      let pd = {
        employeeCode: this.employeeCode, timesheetDate: this.datePipe.transform(new Date(this.taskDate), 'yyyy-MM-dd'),
        projectId: e.projectId, taskId: e.taskId, hours: +e.hours, comments: e.comments
      };
      let call = this.cmnSvc.postTsData(`timesheets/business/addTimeSheet`, pd);
      payloads.push(call);
    });

    if (removedIds.length > 0) {
      let ids = removedIds.join(',')
      let dlt = this.cmnSvc.deleteTsData(`timesheets/business/deleteTimeSheet?ids=${ids}`)
      payloads.push(dlt);
    }

    let valid = true;
    for (let i = 0; i < payloads.length; i++) {
      if (valid)
        payloads[i].subscribe(res => {
          if (res == null) {
            valid = false;
          } else if (res?.status == 'OK' && i + 1 == payloads.length) {
            this.alertSvc.showToast('Saved Successfully', 'success');
            this.onScrollToTop();
          }
        });
      if (!valid)
        break;
    }

    // payloads.some((item, i)=> {
    //   item.subscribe(res => {
    //     if (res.status != 'OK') {
    //       return;
    //     }
    //     else if (res.status == 'OK' && i + 1 == payloads.length) {
    //       this.alertSvc.showToast('Saved Successfully', 'success');
    //       this.onScrollToTop();
    //     }
    // })

    return;
    forkJoin(payloads).subscribe(resp => {
      if (resp) {
        this.alertSvc.showToast('Saved Successfully', 'success');
        this.onScrollToTop();
      }
      // console.log(resp);
      // this.messageEvent.emit('OK');
    });
  }

  onReset() {
    this.tasksLst = JSON.parse(JSON.stringify(this.backupLst));
  }

  checkStatus(projId: number) {
    return this.projectDetails?.find(f => f.projectId == projId)?.type == 12;
  }

}

