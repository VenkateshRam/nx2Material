import { trigger, state, style, transition, animate } from '@angular/animations';
import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ApplyLeaveModalComponent } from '../../lms/modals/apply-leave-modal/apply-leave-modal.component';
import { ViewTasksComponent } from '../modals/view-tasks/view-tasks.component';

@Component({
  selector: 'app-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EmployeeInfoComponent implements OnInit {


  displayedColumns = [{ day: 'Sun', pos: 1 }, { day: 'Mon', pos: 2 }, { day: 'Tue', pos: 3 }, { day: 'Wed', pos: 4 }, { day: 'Thu', pos: 5 }, { day: 'Fri', pos: 6 }, { day: 'Sat', pos: 0 }];
  columnsToDisplay = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  monthlyColumns = ['date', 'name', 'hours', 'work', 'status'];
  projectsLst: Array<any> = []; statusLst: Array<any> = []; balanceLst: Array<any> = [];

  currentDates: any; selectedDate: any; projectDetails: any; empInfo: any;
  showChild: boolean = false; showAll: boolean = false;

  viewDay = new Date(); today = new Date(); msDay = new Date(); totalHours: number = null;
  projectId: string = ''; statusId: string = '';

  lstGrid: MatTableDataSource<any>;
  monthlyGrid: MatTableDataSource<any>;
  empCode: number;

  @ViewChild("modalTemplate") template: TemplateRef<any>;
  protected templateDialogRef: MatDialogRef<TemplateRef<any>>
  constructor(private datePipe: DatePipe,
    private dialog: MatDialog,
    private cmnSvc: CommonService,
    private alertSvc: AlertService,
    private authSvc: AuthService) {
  }

  ngOnInit(): void {
    this.empCode = this.authSvc.currentUserSubject.value.employeeCode;
    this.loadBalance();
    this.getEmpInfo();
    this.loadData();
    // this.getMonthlySummary();
  }

  getWeeksInMonth() {
    const weeks = [],
      firstDate = new Date(this.lstGrid.data[0].date);

    let dayOfWeekCounter = firstDate.getDay();

    this.lstGrid.data.forEach(e => {
      if (dayOfWeekCounter === 1 || weeks.length === 0) {
        weeks.push([]);
      }

      dayOfWeekCounter = (dayOfWeekCounter + 1) % 7;
      Object.assign(e, { position: dayOfWeekCounter, hours: null, 
      desc: null});
      weeks[weeks.length - 1].push(e);
    });

    let wks = [];
    weeks.forEach(e => {
      var object = e.reduce((obj, item) => (obj[item.position] = item, obj), {});
      wks.push(object)
    });

    return wks;
  }


  getDays(details: any, position: number) {
    return details.dates.find(g => g.position == position)?.date;
  }

  onDay(details: any, position: number) {
    console.log(details.dates.find(g => g.position == position)?.date)
  }

  isToday(details: any) {
    //  let date = details.dates.find(g=> g.position == position)?.date
    return this.datePipe.transform(details, 'dd/MM/yyyy') == this.datePipe.transform(this.today, 'dd/MM/yyyy');
  }

  checkDate(details: any) {
    return new Date(details).getTime() <= this.today.getTime();
  }

  onArrow(type: string) {
    if (type == 'prev') {
      this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() - 1, 1);
      this.loadData();
    }
    else if (type == 'next') {
      this.viewDay = new Date(this.viewDay.getFullYear(), this.viewDay.getMonth() + 1, 1);
      this.loadData();
    }
    else if (type == 'today') {
      this.viewDay = this.today;
      this.loadData();
    }
  }

  onMSArrow(action: string) {
    if (action == 'prev') {
      this.msDay = new Date(this.msDay.getFullYear(), this.msDay.getMonth() - 1, 1);
      this.getMonthlySummary();
    }
    else {
      this.msDay = new Date(this.msDay.getFullYear(), this.msDay.getMonth() + 1, 1);
      this.getMonthlySummary();
    }
  }

  openDialog() {
    this.templateDialogRef = this.dialog.open(this.template, {
      width: '400px'
    });

    this.templateDialogRef.afterClosed().subscribe(result => {
      // if (result == 'OK')
      //   this.loadData();
    });
  }

  openViewModal = (date: string, projId: number, title: string, hrs: string) => {
    const dialogRef = this.dialog.open(ViewTasksComponent, {
      width: '700px',
      data: {
        empCode: this.empCode,
        date: date,
        projectId: projId,
        title: title,
        hours: hrs
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'OK')
        console.log('OK')
    });
  }

  loadData() {
    let dt = this.datePipe.transform(this.viewDay, 'yyyy-MM-dd');
    this.cmnSvc.getTsData(`timesheets/business/getTimeSheets?employeeCode=${this.empCode}&month=${dt}`).subscribe(resp => {
      if (resp) {
        this.lstGrid = new MatTableDataSource(resp.data ? resp.data : []);
        this.currentDates = this.lstGrid ? this.getWeeksInMonth() : [];
        console.log(this.currentDates)
      }
    })
  }

  onDate(val: any) {
    this.projectDetails = val.lineInfoList;
    this.selectedDate = val.date;
    this.showChild = true;
  }

  onView(val: any, item: any) {
    let info = item.info.split('-');
    this.openViewModal(val.date, item.projectId, info[0], info[1]);
  }

  receiveMessage($event) {
    let data = $event;
    this.showChild = false;
    if (data == 'OK') {
      this.loadData();
      this.getMonthlySummary();
    }
    // else
    //   Common.loadDataTable(this.lstGrid, '#example');
  }

  getMonthlySummary() {
    let dt = this.datePipe.transform(this.msDay, 'yyyy-MM-dd');
    this.cmnSvc.getTsData(`timesheets/business/getMonthSummary?employeeCode=${this.empCode}&date=${dt}&projectId=${this.projectId}&status=${this.statusId}`).subscribe(resp => {
      if (resp) {
        this.monthlyGrid = new MatTableDataSource(resp.data ? resp.data.monthSummaryLineItemDtoList : []);
        this.totalHours = resp.data?.totalHours;

        if (this.projectId === '' && this.statusId === '') {
          const ids = resp.data.monthSummaryLineItemDtoList.map(o => o.projectId);
          this.projectsLst = resp.data.monthSummaryLineItemDtoList.filter(({ projectId }, index) => !ids.includes(projectId, index + 1));

          const statusIds = resp.data.monthSummaryLineItemDtoList.map(o => o.status);
          this.statusLst = resp.data.monthSummaryLineItemDtoList.filter(({ status }, index) => !statusIds.includes(status, index + 1));
        }
      }
    })
  }

  loadBalance = () => {
    this.cmnSvc.getData(`lms/business/leaveEligibility?employeeCode=${this.empCode}`).subscribe(res => {
      this.balanceLst = res ? res.data : [];
    });
  }

  getEmpInfo() {
    var self = this;
    self.cmnSvc.getData(`lms/business/employeeInfo?employeeCode=${this.empCode}`).subscribe(resp => {
      if (resp)
        this.empInfo = resp.data;
    });
  }

  openApplyLeaveModal = () => {
    const dialogRef = this.dialog.open(ApplyLeaveModalComponent, {
      data: {
        leaveTypes: this.balanceLst,
        empCode: this.empCode,
        empInfo: this.empInfo
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      // if (result == 'OK')
      //   Promise.all([this.loadBalance(), this.loadSummary()]);
    });
  }

  onFillAll() {
    this.showAll = !this.showAll;
  }

  onSave(ele: any) {
    let [hours, desc, date] = [ele.hours, ele.desc, ele.date]
    if (!hours || !desc)
      return;

    if (+hours > 24) {
      this.alertSvc.showToast('Total hours in a day cannot be more than 24', 'warning');
      return;
    }

    let data = [];
    let dta = [].concat(...data);

    if (this.onHoursCheck(hours)) {
      this.alertSvc.showToast('Please enter minutes in scale: .25, .5, .75', 'warning');
      return;
    } else if (hours && !desc) {
      this.alertSvc.showToast('Please Enter Description for the task', 'warning');
      return;
    } else if (hours != null && +hours === 0) {
      this.alertSvc.showToast('Zero Hours are not allowed', 'warning');
      return;
    }

    let pd = {
      employeeCode: this.empCode, timesheetDate: date,
      projectId: 17, taskId: 29, hours: +hours, comments: desc
    };
    this.cmnSvc.postTsData(`timesheets/business/addTimeSheet`, pd).subscribe(res => {
      if (res.status == 'OK')
        this.alertSvc.showToast('Saved Successfully', 'success');
    })


  }

  onHoursCheck(val: string) {
    if (val && !Number.isInteger(+val)) {
      let decPart = val.toString().split(".")[1];
      if (+decPart != 50 && +decPart != 25 && decPart !== '5' && +decPart != 75)
        return true;
    }

    return false;
  }
}
