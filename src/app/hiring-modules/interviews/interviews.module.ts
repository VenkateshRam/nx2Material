import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterviewsRoutingModule } from './interviews-routing.module';
import { InterviewsComponent } from './interviews.component';
import { ScheduledInterviewsComponent } from './scheduled-interviews/scheduled-interviews.component';
import { MyInterviewsComponent } from './my-interviews/my-interviews.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';
import { InterviewDetailsComponent } from './interview-details/interview-details.component';
import { OpenPositionsComponent } from './open-positions/open-positions.component';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { InterviewFeedbackComponent } from './interview-feedback/interview-feedback.component';

@NgModule({
  declarations: [
    InterviewsComponent,
    ScheduledInterviewsComponent,
    MyInterviewsComponent,
    ProfileDetailsComponent,
    InterviewDetailsComponent,
    OpenPositionsComponent,
    InterviewFeedbackComponent
  ],
  imports: [
    CommonModule,
    InterviewsRoutingModule,
    MaterialModule,
    CdkAccordionModule
  ]
})
export class InterviewsModule { }
