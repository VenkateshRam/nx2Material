import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationService } from "primeng/api";
import { AlertService } from "src/app/shared/services/alert.service";
import { SkillProficiency } from "../../shared-hiring/models/SkillProficiency";
import { AddProfileDataService } from "../../shared-hiring/services/add-profile-data.service";
import { HiringRequestDataService } from "../../shared-hiring/services/hiring-request-data.service";


@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit {
  @Input() profileId: string;
  @Input() disable: boolean;
  // skillset variables
    private techSkills: Array<SkillProficiency>;
    public techSkillsCatwise: any;
    private availableTechSkills: Array<string>;
    public successMessage: string;
    private headers: any;
    public categoryId: any;
    public categoryNames: any;
    public skillProficiency: any = [];
    public categoryList: any = [];
    public editMode: boolean = false;
    public viewMode: boolean = true;
    public rejectprofile:boolean = false;
    // skillset varialbles end
    display: boolean = false;
    comboData:any;
    isLoaded:boolean = false;
    profileDetails: any = {};
     resumeDetails: any;
    //skillset view child
    @ViewChild('skillDetailsForm') public skillForm: any;
    @ViewChild('tablist') tablist:any;
    constructor(private hiringRequestDataService: HiringRequestDataService, 
      private router: Router, 
      private route: ActivatedRoute, 
      private AddProfileDataService: AddProfileDataService, 
      private confirmationService: ConfirmationService,
      private alertSvc: AlertService,){}  
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
    showDialog(){
      var self = this;
      if (self.profileId) {
      	var data = self.AddProfileDataService.getProspectCombos();
        if(data){
          self.comboData = {
            employmenttype: data['EMPLOYMENT_TYPE'],
            documenttype: data['DOCUMENT_TYPE'],
            noticeperiod: data['NOTICE_PERIOD'],
            prospectSource: data['PROSPECT_SOURCE']
          }
         }
         self.loadProfileDataWithId(self.profileId);
       }
    }
    loadProfileDataWithId(id: any){
    var self = this;
    this.hiringRequestDataService.loadProspectDataById(id).subscribe(data => {
          self.AddProfileDataService.ProspectData = data;
          self.profileDetails = data;
           self.profileDetails.documents.forEach(function(elem:any, ind:any){
                if(elem['documentType']['code'] == 'RESUME'){
                  self.resumeDetails = {
                    'title': elem['title'],
                    'documentId': elem['documentId'],
                    'prospectId': elem['prospectId']
                  }
                }   
            });
            var date = data.dateOfBirth;
            if(date){
              self.profileDetails.dateOfBirth = this.formatDate(date);
            }
           if(!(self.profileDetails.skillSets.length > 0)){
             var skillDetails = self.tablist.getTabByTitle('Skill Set');
             skillDetails[0]['hidden'] = true;
           }else{
             self.loadSkillSet();
          }  
        },error =>{
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible:false,
            accept : ()=>{
            }
          });});
    }
    formatDate(date: any){
     var time = new Date(date),
     result, year, month, today;
     year = time.getFullYear();
     month = time.getMonth() + 1;
     today = time.getDate();
     month = ((month < 10)? ('0' + month) : month);
     today = ((today < 10)? ('0' + today) : today);
     result = year + '-' + month + '-' + today;
     return result;
    }
    loadSkillSet(){
      this.techSkillsCatwise = {
         'skillSets': []
        };
        var data = this.AddProfileDataService.getHiringCombos();
        if(data){
           this.categoryNames = data['SKILL_CATEGORY'];
           this.categoryId = 9999;
        }
        var data: any= this.AddProfileDataService.getSkillData(), self = this;
        var skillCategoryList = (data ? data["skillSets"]:undefined);
        skillCategoryList && skillCategoryList.forEach(function(elem: any, ind: any){
            var catId: any = elem['categoryId'];
            var categoryName = self.AddProfileDataService.getSkillsMap()['skillCatNames'][catId];
            elem['categoryName'] = categoryName;
            var skills = self.AddProfileDataService.getSkillsMap()['skills'][catId];
            var skillProficiencies = elem.skillProficiencies;
              skillProficiencies.forEach(function(elem: any, ind: any){
                  elem['skill'] = skills;
            });
        });
        this.techSkillsCatwise['skillSets'] = skillCategoryList;
        this.headers = [{
            "skillHeader": {
                "item": "Tool",
                "experience": "Years of Experience",
                "rating": "Rate yourself"
            }
        }];
    }
}
