import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterviewDetailsComponent } from './interview-details/interview-details.component';
import { InterviewFeedbackComponent } from './interview-feedback/interview-feedback.component';
import { InterviewsComponent } from './interviews.component';
import { MyInterviewsComponent } from './my-interviews/my-interviews.component';
import { OpenPositionsComponent } from './open-positions/open-positions.component';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';
import { ScheduledInterviewsComponent } from './scheduled-interviews/scheduled-interviews.component';

const routes: Routes = [{ path: '', component: InterviewsComponent },
{ path: 'my-interviews', component: MyInterviewsComponent },
{ path: 'scheduled-interviews', component: ScheduledInterviewsComponent },
{ path: 'profile-details', component: ProfileDetailsComponent },
{ path: 'interview-details', component: InterviewDetailsComponent },
{ path: 'open-positions', component: OpenPositionsComponent },
{ path: 'feedback', component: InterviewFeedbackComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterviewsRoutingModule { }
