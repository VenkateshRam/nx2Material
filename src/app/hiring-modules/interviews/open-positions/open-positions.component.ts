import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-open-positions',
  templateUrl: './open-positions.component.html',
  styleUrls: ['./open-positions.component.scss']
})
export class OpenPositionsComponent {

  items = ['Block 1', 'Block 2', 'Block 3', 'Block 4', 'Block 5'];
  lstGrid: Array<any> = [];
  requestNeed: any;
  expandedIndex = 0;
  constructor() { 
    this.lstGrid = [
      {
        "id": "31dd5d2d-f382-4a06-b8db-c7aabb606f62",
        "uid": "HRQ86",
        "purpose": "Build team for Trimble projects (Indian SKU, Global SKU, Test Automation)",
        "clientId": 86,
        "clientName": "Trimble (US)",
        "departmentId": 1,
        "departmentName": "Engineering",
        "practiceId": 9,
        "practiceName": "Connected Devices",
        "competencyId": 109,
        "competencyName": "CDPS-Linux & Android",
        "operatingModel": "Offshore",
        "resourcesCount": 5,
        "ownerId": 33,
        "hiringManagerName": "Vijay Manjula",
        "priority": "VERY_CRITICAL",
        "priorityDisplayName": "Very Critical",
        "status": "ACTIVE",
        "statusDisplayName": "Active",
        "type": "R",
        "typeDisplayName": "Regular",
        "creationTimestamp": 1543557113363,
        "createdBy": 33,
        "needCount": 0,
        "startDate": 1544427947984
      },
      {
        "id": "3734f07f-f735-41ec-9f11-e16f807eaad6",
        "uid": "HRQ79",
        "purpose": "Trimble Needs Sr and DevOps Engineers",
        "clientId": 76,
        "clientName": "Trimble (Pune)",
        "departmentId": 1,
        "departmentName": "Engineering",
        "practiceId": 21,
        "practiceName": "Cloud & DevOps",
        "competencyId": 33,
        "competencyName": "DevOps",
        "operatingModel": "Onsite",
        "resourcesCount": 4,
        "ownerId": 12,
        "hiringManagerName": "Srinivas Peddada",
        "priority": "VERY_CRITICAL",
        "priorityDisplayName": "Very Critical",
        "status": "ACTIVE",
        "statusDisplayName": "Active",
        "type": "R",
        "typeDisplayName": "Regular",
        "creationTimestamp": 1537858766467,
        "createdBy": 12,
        "needCount": 0,
        "startDate": 1563878305836
      },
    ];

    this.requestNeed = {
      "id": "0a52daab-97cd-4c35-8dbf-aa8b93689825",
      "uid": "HRQ483",
      "purpose": "Hire the experience Python engineers and place then in Client billing.",
      "clientId": 1,
      "clientName": "Innominds (HYD)",
      "departmentId": 1,
      "departmentName": "Engineering",
      "hiringManagerId": 231,
      "needs": [
        {
          "id": "6096f409-62a6-4057-af05-daaedbfd0808",
          "purpose": "To fill the skill set gap",
          "competencyId": 120,
          "competencyName": "Device/Platform Testing",
          "practiceId": 9,
          "practiceName": "Connected Devices",
          "workLocationId": 1,
          "workLocationName": "Innominds (HYD)",
          "minimumExperienceInYears": 2.0,
          "maximumExperienceInYears": 6.0,
          "expectedRole": "Till Sr Engineer",
          "resourcesCount": 6,
          "expectedStartDate": 1637213937665,
          "jobDescriptionId": "fcfae80f-9fbb-45f5-8c1f-a65cfa6dc946",
          "model": "OFFSHORE",
          "modelDesc": "Offshore",
          "interviewPanels": [
            {
              "id": "5ad8732c-92f3-4158-a988-e15de7a286c0",
              "name": "Final Round - Panel",
              "level": "FINAL",
              "panelMembers": [
                {
                  "id": 3112,
                  "fullName": "Sashmita Nath"
                }
              ],
              "panelCriteria": [
                {
                  "id": 11,
                  "title": "Professionalism",
                  "placeholderText": "Professionalism",
                  "key": "11"
                },
                {
                  "id": 10,
                  "title": "Attitude",
                  "placeholderText": "Attitude",
                  "key": "10"
                }
              ]
            },
            {
              "id": "0039ccec-7577-4482-a522-bc96e6bdb72c",
              "name": "Technical Round - Panel",
              "level": "TECHNICAL",
              "panelMembers": [
                {
                  "id": 236,
                  "fullName": "Pallavi Gorijala"
                }
              ],
              "panelCriteria": [
                {
                  "id": 10,
                  "title": "Attitude",
                  "placeholderText": "Attitude",
                  "key": "10"
                },
                {
                  "id": 6,
                  "title": "Quality Focus",
                  "placeholderText": "Quality Focus",
                  "key": "6"
                },
                {
                  "id": 1,
                  "title": "Communication Skills",
                  "placeholderText": "Communication Skills",
                  "key": "1"
                },
                {
                  "id": 4,
                  "title": "Technical Skills",
                  "placeholderText": "Technical Skills",
                  "key": "4"
                }
              ]
            },
            {
              "id": "2071e8ca-a6b2-41a8-bd22-a6c7eecf70a7",
              "name": "Managerial Round - Panel",
              "level": "MANAGERIAL",
              "panelMembers": [
                {
                  "id": 158,
                  "fullName": "Vamshi Tatiparthi"
                },
                {
                  "id": 231,
                  "fullName": "Rajesh Pothina"
                }
              ],
              "panelCriteria": [
                {
                  "id": 5,
                  "title": "Customer Focus",
                  "placeholderText": "Customer Focus",
                  "key": "5"
                },
                {
                  "id": 13,
                  "title": "Innovation",
                  "placeholderText": "Innovation",
                  "key": "13"
                },
                {
                  "id": 2,
                  "title": "Presentation Skills",
                  "placeholderText": "Presentation Skills",
                  "key": "2"
                },
                {
                  "id": 9,
                  "title": "Team Work",
                  "placeholderText": "Team Work",
                  "key": "9"
                }
              ]
            }
          ],
          "interviews": [
            
          ]
        },
        {
          "id": "4ffdf78d-972f-4055-9b6f-147bf8813c45",
          "purpose": "To refill the skill set",
          "competencyId": 120,
          "competencyName": "Device/Platform Testing",
          "practiceId": 9,
          "practiceName": "Connected Devices",
          "workLocationId": 1,
          "workLocationName": "Innominds (HYD)",
          "minimumExperienceInYears": 2.0,
          "maximumExperienceInYears": 6.0,
          "expectedRole": "Till Sr Engineer",
          "resourcesCount": 6,
          "expectedStartDate": 1637213190279,
          "jobDescriptionId": "fcfae80f-9fbb-45f5-8c1f-a65cfa6dc946",
          "model": "OFFSHORE",
          "modelDesc": "Offshore",
          "interviewPanels": [
            {
              "id": "d133f1aa-b97b-405e-be63-a10e19b9c810",
              "name": "Technical Round - Panel",
              "level": "TECHNICAL",
              "panelMembers": [
                {
                  "id": 236,
                  "fullName": "Pallavi Gorijala"
                }
              ],
              "panelCriteria": [
                {
                  "id": 12,
                  "title": "Being Proactive",
                  "placeholderText": "Being Proactive",
                  "key": "12"
                },
                {
                  "id": 1,
                  "title": "Communication Skills",
                  "placeholderText": "Communication Skills",
                  "key": "1"
                },
                {
                  "id": 6,
                  "title": "Quality Focus",
                  "placeholderText": "Quality Focus",
                  "key": "6"
                },
                {
                  "id": 10,
                  "title": "Attitude",
                  "placeholderText": "Attitude",
                  "key": "10"
                },
                {
                  "id": 13,
                  "title": "Innovation",
                  "placeholderText": "Innovation",
                  "key": "13"
                },
                {
                  "id": 9,
                  "title": "Team Work",
                  "placeholderText": "Team Work",
                  "key": "9"
                },
                {
                  "id": 4,
                  "title": "Technical Skills",
                  "placeholderText": "Technical Skills",
                  "key": "4"
                }
              ]
            },
            {
              "id": "4f835e96-1da8-49e7-bf0c-d24cbdd68587",
              "name": "Managerial Round - Panel",
              "level": "MANAGERIAL",
              "panelMembers": [
                {
                  "id": 231,
                  "fullName": "Rajesh Pothina"
                },
                {
                  "id": 158,
                  "fullName": "Vamshi Tatiparthi"
                }
              ],
              "panelCriteria": [
                {
                  "id": 1,
                  "title": "Communication Skills",
                  "placeholderText": "Communication Skills",
                  "key": "1"
                },
                {
                  "id": 5,
                  "title": "Customer Focus",
                  "placeholderText": "Customer Focus",
                  "key": "5"
                },
                {
                  "id": 9,
                  "title": "Team Work",
                  "placeholderText": "Team Work",
                  "key": "9"
                },
                {
                  "id": 2,
                  "title": "Presentation Skills",
                  "placeholderText": "Presentation Skills",
                  "key": "2"
                }
              ]
            },
            {
              "id": "248c66ed-5156-4027-a638-0895b0598278",
              "name": "Final Round - Panel",
              "level": "FINAL",
              "panelMembers": [
                {
                  "id": 3112,
                  "fullName": "Sashmita Nath"
                }
              ],
              "panelCriteria": [
                {
                  "id": 11,
                  "title": "Professionalism",
                  "placeholderText": "Professionalism",
                  "key": "11"
                },
                {
                  "id": 10,
                  "title": "Attitude",
                  "placeholderText": "Attitude",
                  "key": "10"
                }
              ]
            }
          ],
          "interviews": [
            
          ]
        }
      ],
      "recruiters": [
        {
          "id": 2732,
          "fullName": "Anusha Suri"
        },
        {
          "id": 5826,
          "fullName": "Sonali Sahu"
        }
      ],
      "profileScreeningMembers": [
        {
          "id": 33,
          "fullName": "Vijay Manjula"
        },
        {
          "id": 158,
          "fullName": "Vamshi Tatiparthi"
        }
      ],
      "hiringPriority": "VERY_CRITICAL",
      "hiringReason": "CORE_SKILLS_REFILL",
      "hiringRequestStatus": "ACTIVE",
      "hiringType": "R",
      "hiringTypeDescription": "Regular",
      "creationTimestamp": 1637213386178,
      "createdBy": 231,
      "isOwner": false
    };
  }

  ngOnInit(): void {
  }

}
