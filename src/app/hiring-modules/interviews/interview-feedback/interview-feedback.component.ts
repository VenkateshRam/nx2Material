import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ConfirmationService } from 'primeng/api';
import { HiringCommonService } from '../../shared-hiring/services/hiring-common.service';
import { InterviewDataService } from '../../shared-hiring/services/interview-data.service';
import { HiringRequestDataService } from '../../shared-hiring/services/hiring-request-data.service';

@Component({
  selector: 'app-interview-feedback',
  templateUrl: './interview-feedback.component.html',
  styleUrls: ['./interview-feedback.component.scss']
})
export class InterviewFeedbackComponent implements OnInit {
  
  val: number;
  experienceFeedBack: string;
  feedBackDetails: any;
  resultStatus: any;
  successMessage: string;
  isLoaded: boolean = false;
  interviewId: string;
  hiringId: string;
  needId: string;
  assetCategory: string = '';
  additionalHardware: string = '';
  photoFile: any;
  uploadImageFile: any;
  photoUrl: any;
  imageFile: File;
  assetList: any = [];
  constructor(private location: Location, 
    private interviewDataService: InterviewDataService, 
    public commonService: HiringCommonService, 
    private confirmationService: ConfirmationService,
     private route: ActivatedRoute,
      private newHiringRequestDataService: HiringRequestDataService) {
  }
  ngOnInit() {
    this.feedBackDetails = {
      "criterionList": []
    };
    this.loadDetails();
    this.getAssets();
  }
  loadDetails() {
    var self = this;
    self.route.params.subscribe(params => {
      if (params['interviewId'] && params['reqId'] && params['needId']) {
        self.hiringId = params['reqId'];
        self.needId = params['needId'];
        self.interviewId = params['interviewId'];
        this.interviewDataService.getCriteria(self.interviewId).subscribe(data => {
          if (data.interviewCriteria.length > 0) {
            var criteriaList: any = [];
            data.interviewCriteria.forEach(function (elem: any, ind: any) {
              criteriaList.push({
                "criterionName": elem
              });
            });
            criteriaList.push({
              "criterionName": "Overall Summary"
            });
            this.feedBackDetails.criterionList = criteriaList;
          }
        }, error => {
          this.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false
          });
        });
      }
    });
  }

  getAssets() {
    var self = this;
    this.newHiringRequestDataService.loadAssetsData().subscribe(data => {
      self.assetList = data;
    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    });
  }
  submitFeedback() {
    var payload: any = {}, self = this;
    var i = 0;
    this.feedBackDetails.criterionList.forEach((ele: any) => {
      if (ele.hasOwnProperty('feedback') && ele.hasOwnProperty('rating')) {
        i++;
      }
    });
    if ((this.feedBackDetails.criterionList.length !== i) || i == 0) {
      self.confirmationService.confirm({
        message: 'Please enter the feedback and give rating',
        header: 'Warning',
        icon: 'fa fa-warning',
        rejectVisible: false
      });
    } else {
      payload.feedback = JSON.stringify(this.feedBackDetails.criterionList);
      if (!this.feedBackDetails.resultStatus) {
        self.confirmationService.confirm({
          message: 'Please select Recommended or Not Recommended as the interview result status',
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
        return;
      }

      if (this.feedBackDetails.resultStatus === 'RECOMMENDED') {
        if (!this.assetCategory) {
          self.confirmationService.confirm({
            message: 'Please Select the Asset Category',
            header: 'Warning',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false
          });
          return;
        }
      } else {
        this.assetCategory = '';
        this.additionalHardware = '';
      }
      payload.resultStatus = this.feedBackDetails.resultStatus;
      payload.assetRequired = this.assetCategory;
      payload.assetRequiredComment = this.additionalHardware;
      if (self.hiringId && self.needId && self.interviewId) {
        this.interviewDataService.submitFeedBack(self.hiringId, self.needId, self.interviewId, payload, this.imageFile).subscribe(data => {
          if (data.status == 200) {
            self.successMessage = 'Feedback submitted successfully';
            self.confirmationService.confirm({
              message: self.successMessage,
              header: 'Success',
              icon: 'fa fa-check-circle',
              rejectVisible: false,
              accept: () => {
                this.navigateToInterviews();
              }, reject: () => {
                this.navigateToInterviews();
              }
            });
          }
        }, error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false
          });
        });
      }
    }
  }
  navigateToInterviews() {
    //this.router.navigate(['../myspacenx/interviews/my-interviews']);
    // making common for using component 
    this.location.back();
  }
  onFileUpload(event: any) {
    let fileList: FileList = event.target.files;
    this.photoFile = fileList;
    this.onPhotoPreview();
    if (fileList.length > 0) {
      try {
        var isValid = this.commonService.validateFile(fileList[0], 'others', 2097152, 69, ['png', 'jpg', 'jpeg']);
        if (isValid) {
          this.imageFile = fileList[0];
          // (<HTMLInputElement>document.getElementById('photo')).value = this.file.name;
        }
      } catch (e) {
        this.confirmationService.confirm({
          message: e.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      }
    }
  }
  onPhotoPreview() {
    var file = this.photoFile;
    this.uploadImageFile = file.item(0);
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.photoUrl = event.target.result;
    };
    reader.readAsDataURL(this.uploadImageFile);
  }
}
