import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationService } from "primeng/api";
import { AlertService } from "src/app/shared/services/alert.service";
import { InterviewDataService } from "../../shared-hiring/services/interview-data.service";
import { ProfilesDataService } from "../../shared-hiring/services/profiles-data.service";

@Component({
  selector: 'app-my-interviews',
  templateUrl: './my-interviews.component.html',
  styleUrls: ['./my-interviews.component.scss']
})
export class MyInterviewsComponent implements OnInit {

  displayedColumns = ['id', 'cn', 'job_des', 'mobNo', 'ilevel', 'imode', 'idate', 'sch_by', 'st',  'rs', 'resume'];
  columnDefs: any;
  allOfTheData: any;
  public statusOptions: any[];
  public scheduleStatus: string;
  public resultStatusOptions: any[];
  public resultStatus: string;
  public recordsCount: number;
  public buttonModes: any = {};
  public successMessage: string;
  public isLoaded: boolean = false;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  selection: any;
  commonService: any;
  constructor( private profileDataService: ProfilesDataService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private interviewDataService: InterviewDataService, 
    private confirmationService: ConfirmationService,
    AlertSvc:AlertService) {
    this.statusOptions = [];
    this.resultStatusOptions = [];
    this.scheduleStatus = "ALL";
    this.resultStatus = "ALL";
  }
  dateComparator(date1: any, date2: any) {
    var monthToComparableNumber = function (date: any) {
      if (date === undefined || date === null || date.length !== 10) {
        return null;
      }
      var yearNumber = date.substring(6, 10);
      var monthNumber = date.substring(3, 5);
      var dayNumber = date.substring(0, 2);

      var result = (yearNumber * 10000) + (monthNumber * 100) + dayNumber;
      return result;
    };
    var date1Number = monthToComparableNumber(date1);
    var date2Number = monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  formatDate(data: any) {
    var time = new Date(data.value),
      result, year, month, today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    result = today + '/' + month + '/' + year;
    var hours: any = time.getHours();
    var minutes: any = time.getMinutes();
    var ampm: any = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime: any = hours + ':' + minutes + ' ' + ampm;
    return result + ' ' + strTime;
  }

  onCellClicked(record: any) {
    this.checkModes();
  }
  checkModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var interviewStatus = selectedRecords[0]['interviewScheduledStatus'],
        previousInterview = selectedRecords[0]['previousInterviewId'],
        interviewDate = selectedRecords[0]['interviewDate'],
        prospectNeed = selectedRecords[0]['prospect']['prospectNeed'],
        status = prospectNeed && prospectNeed[0] && prospectNeed[0]['status'],
        dateTime = this.commonService.makingButtonEnable(interviewDate);
      // geeting the dates and spliting for comparision 
      const interviewScheduledDate = dateTime.split('#');
      var numberOfDaysToAllowFeedbackAfterScheduledDate = interviewScheduledDate[0],
        numberOfHoursToAllowFeedbackBeforeScheduledDate = interviewScheduledDate[1],
        allowFeedbackScheduledDate = interviewScheduledDate[2];
      if (interviewStatus == 'SCHEDULED' || interviewStatus == 'RESCHEDULED') {
        this.buttonModes = {
          accept: false,
          decline: false,
          submitFeedback: true,
          viewFeedback: true,
          open: false
        };
        // enabling the feedback submit button for before 4 hours and after 3 days
      } else if ((allowFeedbackScheduledDate >= numberOfHoursToAllowFeedbackBeforeScheduledDate && allowFeedbackScheduledDate <= numberOfDaysToAllowFeedbackAfterScheduledDate) && (interviewStatus === 'ACCEPTED' || interviewStatus === 'FEEDBACK_PENDING')) {
        this.buttonModes = {
          accept: true,
          decline: false,
          submitFeedback: false,
          viewFeedback: true,
          open: false
        };
      } else if (interviewStatus == 'ACCEPTED') {
        this.buttonModes = {
          accept: true,
          decline: false,
          submitFeedback: true,
          viewFeedback: true,
          open: false
        };
      } else {
        this.buttonModes = {
          accept: true,
          decline: true,
          submitFeedback: true,
          viewFeedback: true,
          open: false
        };
      }
      if ((interviewStatus == 'FEEDBACK_SUBMITTED') || previousInterview) {
        this.buttonModes['viewFeedback'] = false;
      }
    } else {
      this.resetModes();
    }
  }
  onGridInitialize() {
  //  this.resizeColumns();
    var self = this;
    this.interviewDataService.getCombos().subscribe(data => {
      self.statusOptions = data['INTERVIEW_SCHEDULE_STATUS'];
      self.resultStatusOptions = data['INTERVIEW_RESULT_STATUS'];
      self.loadGrid();
    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false,
        accept : ()=>{
          
        }
      });
    });
  }
  filterChanged() {
    this.pageNum = 0;
    this.loadGrid();
  }
  loadGrid() {
    var self = this;
    self.resetModes();
    self.interviewDataService.getMyInterviews(self.pageNum, self.pageSize, self.resultStatus, self.scheduleStatus).subscribe(data => {
      self.setRowData(data['content']);
      self.recordsCount = data.totalElements;
      self.totalPages = data.totalPages;
    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    });
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }
  exportData() {
    var self = this;
    setTimeout(() => {
      self.interviewDataService.exportMyInterviews(self.resultStatus, self.scheduleStatus).subscribe(data => {
        self.commonService.downloadCsv('My Interviews', data['_body']);
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    }, 100);
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
  }
  openFeedBack() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var hiringId = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'],
        interviewId = selectedRecords[0]['id'];
      this.router.navigate(['../myspacenx/interviews/submit-feedback', interviewId, hiringId, needId]);
    }
  }
  viewFeedBack() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['prospect']['id'],
        reqId = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'];
      this.router.navigate(['../myspacenx/interviews/view-feedback', id, reqId, needId]);
    }
  }
  accept() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'],
        interviewId = selectedRecords[0]['id'];
      this.interviewDataService.acceptInterview(id, needId, interviewId, 'ACCEPTED').subscribe(data => {
        if (data.status == 200) {
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              this.loadGrid();
            },
            reject: () => {
              this.loadGrid();
            }
          });
        }
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    }
  }
  decline() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'],
        interviewId = selectedRecords[0]['id'];
      this.interviewDataService.declineInterview(id, needId, interviewId, 'DECLINED').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Interview declined successfully';
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              this.loadGrid();
            },
            reject: () => {
              this.loadGrid();
            }
          });
        }
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    }
  }
  cancelInterview() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'],
        interviewId = selectedRecords[0]['id'];
      this.interviewDataService.cancelInterview(id, needId, interviewId).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Interview cancelled successfully';
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              this.loadGrid();
            },
            reject: () => {
              this.loadGrid();
            }
          });
        }
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    }
  }
  reschedule(status: string) {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var prospectName = selectedRecords[0]['prospect']['displayName'],
        id = selectedRecords[0]['prospect']['id'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'],
        interviewId = selectedRecords[0]['id'];
      if (status == 'Recommended For Next Round') {
        this.profileDataService.prospectDetails = {
          'prospectName': prospectName,
          'hiringId': hiringId,
          'hiringNeedId': needId
        };
      } else {
        this.profileDataService.prospectDetails = {
          'prospectName': prospectName,
          'hiringId': hiringId,
          'hiringNeedId': needId,
          'interviewId': interviewId
        };
      }
      this.router.navigate(['../myspacenx/interviews/schedule', id]);
    }
  }
  //open profile details of interviewer 
  showProfileDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['prospect']['id'];
      this.router.navigate(['../myspacenx/profiles/profile-details', id]);
    }
  }
  ngOnInit() {
    this.resetModes();
  }
  resetModes() {
    this.buttonModes = {
      accept: true,
      decline: true,
      submitFeedback: true,
      viewFeedback: true,
      open: true
    };
  }
}