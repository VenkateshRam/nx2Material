import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { AddProfileDataService } from '../../shared-hiring/services/add-profile-data.service';
import { HiringRequestDataService } from '../../shared-hiring/services/hiring-request-data.service';
import { ProfilesDataService } from '../../shared-hiring/services/profiles-data.service';


@Component({
  selector: 'app-scheduled-interviews',
  templateUrl: './scheduled-interviews.component.html',
  styleUrls: ['./scheduled-interviews.component.scss']
})
export class ScheduledInterviewsComponent implements OnInit {

  displayedColumns = ['id', 'cn', 'job_des', 'mobNo', 'ilevel', 'imode', 'idate', 'sch_by', 'st',  'rs', 'resume'];
  interviewLevelOptions: any;
  interviewModeOptions: any;
  interviewDetails: any = {};
  public statusOptions: any[];
  public scheduleStatus: string;
  public resultStatusOptions: any[];
  public resultStatus: string;
  hiringId: string;
  hiringNeedId: string;
  interviewId: string;
  successMessage: string;
  panels: any;
  interviewers: any;
  interviewDate: any;
  isLoaded: boolean = false;
  today: Date;
  isSingleSelection: any;
  assetCategory: string = '';
  additionalHardware: string = '';
  makeHideShow: boolean = false;
  formSubmitted: boolean = false;
  interviewResultStatus: any;
  file: File;
  panelMembersList: any = [];
  assetList: any = [];
  adhocInterviewId: string;
  makeDisableInterviwer: boolean = false;
  makeDisableadhocInterviwer: boolean = false;
  isAdhocInterviewer = false;
  commonService: any;

  constructor(private newHiringRequestDataService: HiringRequestDataService, 
    private addProfileDataService: AddProfileDataService, 
    private profileDataService: ProfilesDataService,
    private router: Router, 
    private route: ActivatedRoute, 
    private confirmationService: ConfirmationService) {
    
  }
  ngOnInit() {
    this.today = new Date();
    var data = this.addProfileDataService.getHiringCombos(), self = this;
    if (data) {
      //this.interviewLevelOptions = data['INTERVIEW_LEVEL'];
      this.interviewModeOptions = data['INTERVIEW_MODE'];
    }
    self.route.params.subscribe(params => {
      if (params['id']) {
        var prospectDetails: any = this.profileDataService.prospectDetails;
        self.interviewDetails.prospectiveHireName = prospectDetails['prospectName'];
        self.interviewId = prospectDetails['interviewId'];
        self.interviewDetails.prospectId = params['id'];
        //this.hiringId = prospectDetails['hiringId'];
        //this.hiringNeedId = prospectDetails['hiringNeedId'];
        self.hiringId = params['reqId'];
        self.hiringNeedId = params['needId'];
        if ((self.hiringId && self.hiringNeedId)) {
          if (self.interviewDetails.prospectiveHireName) {
            self.loadDetails();
          } else {
            self.addProfileDataService.loadProspectDataById(params['id']).subscribe(data => {
              self.interviewDetails.prospectiveHireName = data.displayName;
              if (self.interviewModeOptions) {
                self.loadDetails();
              } else {
                self.setInterviewModeOptionsAndLoad();
              }
            }, error => {
              self.confirmationService.confirm({
                message: error.message,
                header: 'Error',
                icon: 'fa fa-exclamation-circle',
                rejectVisible: false
              });
            });
          }
        }
      } else if (params['interviewId']) {
        self.interviewId = params['interviewId'];
        if (self.interviewModeOptions) {
          self.loadDetails();
        } else {
          self.setInterviewModeOptionsAndLoad();
        }
      }
    });
    this.getPanelList(this.adhocInterviewId);
    this.getAssets();
  }
  setInterviewModeOptionsAndLoad() {
    var self = this;
    self.addProfileDataService.getHiringComboDetails().subscribe(data => {
      self.interviewModeOptions = data['INTERVIEW_MODE'];
      self.loadDetails();
    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    });
  }
  loadDetails() {
    var self = this;
    if (self.interviewId) {
     // self.FeatureComponent.URLtitle = "Profiles / Reschedule Interview";
      self.profileDataService.getInterviewDetailsById(self.interviewId).subscribe(data => {
        var interviewDetails: any = {};
        interviewDetails.interviewLevel = data['levelId'];
        self.interviewDetails.interviewMode = data['mode'];
        self.interviewDetails.recruiterComments = data['recruiterComments'];
        interviewDetails.interviewPanelId = data['interviewPanel']['id'];
        self.interviewDetails.isAdhocInterviewer = data['isAdhocInterviewer'];
        if (self.interviewDetails.isAdhocInterviewer) {
          interviewDetails.adhocInterviewId = data['interviewer']['id'];
          self.disabledAdhocInterviewer(interviewDetails.adhocInterviewId);
        } else if (self.interviewDetails.interviewerId) {
          interviewDetails.interviewId = data['interviewer']['id'];
          self.disabledInterviewer(interviewDetails.interviewerId);
        }
        self.interviewDetails.prospectiveHireName = data['prospect'] && data['prospect']['displayName'];
        self.interviewDetails.prospectId = data['prospect'] && data['prospect']['id'];
        self.interviewDate = new Date(data['interviewDate']);
        self.hiringId = data['hiringRequest'] && data['hiringRequest']['id'];
        self.hiringNeedId = data['hiringRequestNeed'] && data['hiringRequestNeed']['id'];
        self.profileDataService.getInterviewLevels(self.hiringId, self.hiringNeedId).subscribe(data => {
          self.interviewLevelOptions = data;
          self.interviewDetails.interviewLevel = interviewDetails.interviewLevel;
          if (self.interviewDetails.interviewLevel) {
            self.profileDataService.getPanels(self.hiringId, self.hiringNeedId, self.interviewDetails.interviewLevel).subscribe(data => {
              self.panels = data;
              self.interviewDetails.interviewPanelId = interviewDetails.interviewPanelId;
              self.getPanelList(interviewDetails.adhocInterviewId);
              self.getInterviewers(interviewDetails.interviewId);
            }, error => {
              self.confirmationService.confirm({
                message: error.message,
                header: 'Error',
                icon: 'fa fa-exclamation-circle',
                rejectVisible: false
              });
            });
          }
        }, error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false
          });
        });
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    } else {
      this.profileDataService.getInterviewLevels(this.hiringId, this.hiringNeedId).subscribe(data => {
        self.interviewLevelOptions = data;
        //if(this.interviewLevelOptions.length ==1){
        //   this.profileDataService.getPanels(this.hiringId, this.hiringNeedId,this.interviewLevelOptions[0].id).subscribe(data=>{
        //     this.panels =data
        //   });
        //  }
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    }
  }
  getInterviewLevels() {
    var self = this;
    this.profileDataService.getInterviewLevels(this.hiringId, this.hiringNeedId).subscribe(data => {
      self.interviewLevelOptions = data;

    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    });
  }

  getAssets() {
    var self = this;
    this.newHiringRequestDataService.loadAssetsData().subscribe(data => {
      self.assetList = data;
    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    });
  }

  loadPanels() {
    var self = this;
    if (this.hiringId && this.hiringNeedId && this.interviewDetails.interviewLevel) {
      this.profileDataService.getPanels(this.hiringId, this.hiringNeedId, this.interviewDetails.interviewLevel).subscribe(data => {
        self.panels = data;
      }, error => {
        this.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
      self.interviewDetails.interviewPanelId = "";
    } else if (!this.interviewDetails.interviewLevel) {
      this.panels = [];
      this.interviewDetails.interviewPanelId = "";
      this.interviewers = [];
      this.interviewDetails.interviewerId = "";
    }
  }
  formatDate(date: any) {
    var time = new Date(date),
      result, year, month, today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    month = ((month < 10) ? ('0' + month) : month);
    today = ((today < 10) ? ('0' + today) : today);
    result = year + '-' + month + '-' + today;
    return result;
  }
  getDateAndTime(date: any) {
    var result, year, month, today;
    year = date.getFullYear();
    month = date.getMonth() + 1;
    today = date.getDate();
    month = ((month < 10) ? ('0' + month) : month);
    today = ((today < 10) ? ('0' + today) : today);
    result = year + '-' + month + '-' + today;
    var hours = date.getHours();
    var minutes = date.getMinutes();
    /* commented code as its leading to error scenario */
    // hours = hours % 12;
    // hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes;
    return result + ' ' + strTime;
  }
  getInterviewers(interviewerId?: string) {
    var self = this;
    if (this.hiringId && this.hiringNeedId && this.interviewDetails.interviewLevel && this.interviewDetails.interviewPanelId) {
      this.profileDataService.getInterviewersByPanelId(this.hiringId, this.hiringNeedId, this.interviewDetails.interviewLevel, this.interviewDetails.interviewPanelId).subscribe(data => {
        self.interviewers = data;
        self.interviewDetails.interviewerId = "";
        if (interviewerId) {
          self.interviewDetails.interviewerId = interviewerId;
        }
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    } else if (!this.interviewDetails.interviewPanelId) {
      self.interviewers = [];
      self.interviewDetails.interviewerId = "";
    }
  }
  // Scheduling interview
  scheduleInterview(formValid: boolean = false): void {
    this.formSubmitted = true;
    if (!formValid) {
      // this.confirmationService.confirm({
      //   message: 'Please select all mandatory fields',
      //   header: 'Warning',
      //   icon: 'fa fa-exclamation-circle',
      //   rejectVisible: false
      // });
     // this.toastr.warning('Please select all mandatory fields', 'Warning');
      return;
    }
    var self = this, userDetails: any = this.commonService.getItem('currentUserInfo');
    this.interviewDetails.interviewScheduledBy = (userDetails) ? userDetails.id : '';
    if (!this.interviewDate) {
      self.confirmationService.confirm({
        message: 'Please select the interview date',
        header: 'Warning',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
      return;
    }
    // adding the condition if user selected the checkox to upload feedback directly
    if (this.isSingleSelection === true) {
      if (!this.interviewResultStatus) {
        self.confirmationService.confirm({
          message: 'Please Choose  Recommended or Not Recommended as the interview Result status',
          header: 'Warning',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
        return;
      }

      // Commented as its not mandatory to upload file by recruiter
      // else if (!this.file) {
      //   self.confirmationService.confirm({
      //     message: 'Please Upload file',
      //     header: 'Error',
      //     icon: 'fa fa-exclamation-circle',
      //     rejectVisible: false
      //   });
      //   return;
      // }
    }

    if (this.interviewResultStatus === 'RECOMMENDED') {
      if (!this.assetCategory) {
        self.confirmationService.confirm({
          message: 'Please Select the Asset Category',
          header: 'Warning',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
        return;
      }
    } else {
      this.assetCategory = '';
      this.additionalHardware = '';
    }
    if (self.interviewDetails.adhocInterviewId) {
      this.interviewDetails.interviewerId = self.interviewDetails.adhocInterviewId;
    }
    this.interviewDetails.interviewDate = new Date(this.interviewDate).getTime();
    var interviewDateString: string = this.getDateAndTime(this.interviewDate),
      currentDateString: string = this.getDateAndTime(new Date());
    if (new Date(interviewDateString).getTime() < new Date(currentDateString).getTime()) {
      self.confirmationService.confirm({
        message: 'Interview date should not be less than current date',
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
      return;
    }
    if (this.hiringId && this.hiringNeedId) {
      // for direct feedback submit  from scheduling interiew 
      if (this.makeHideShow) {
        // self.loaderService.showLoading('overlay', '#overlay');
        // self.isLoaded = self.loaderService.isLoaded;
        this.interviewDetails.makeHideShow = this.makeHideShow;
        this.interviewDate.interviewResultStatus = this.interviewResultStatus;
        this.interviewDetails.interviewResultStatus = this.interviewResultStatus;
        this.interviewDetails.assetRequired = this.assetCategory;
        this.interviewDetails.assetRequiredComment = this.additionalHardware;
        this.profileDataService.scheduleInterview(this.hiringId, this.hiringNeedId, this.interviewDetails, this.file).subscribe(data => {
          if (data.status == 201) {
            self.successMessage = 'Interview scheduled successfully';
            // $('[name=scheduleModal]').modal('show');
            self.confirmationService.confirm({
              message: self.successMessage,
              header: 'Success',
              icon: 'fa fa-check-circle',
              rejectVisible: false,
              accept: () => {
                self.navigateToShorlistedProfiles();
              }, reject: () => {
                self.navigateToShorlistedProfiles();
              }
            });
          }
        }, error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false
          });
        });
      } else {
        if (self.interviewId) {
          this.profileDataService.rescheduleInterview(this.hiringId, this.hiringNeedId, self.interviewId, this.interviewDetails).subscribe(data => {
            if (data.status == 200) {
              self.successMessage = 'Interview rescheduled successfully';
              self.confirmationService.confirm({
                message: self.successMessage,
                header: 'Success',
                icon: 'fa fa-check-circle',
                rejectVisible: false,
                accept: () => {
                  self.navigateToShorlistedProfiles();
                }, reject: () => {
                  self.navigateToShorlistedProfiles();
                }
              });
            }
          }, error => {
            self.confirmationService.confirm({
              message: error.message,
              header: 'Error',
              icon: 'fa fa-exclamation-circle',
              rejectVisible: false
            });
          });
        } else {
          this.profileDataService.scheduleInterview(this.hiringId, this.hiringNeedId, this.interviewDetails, this.file).subscribe(data => {
            if (data.status == 201) {
              self.successMessage = 'Interview scheduled successfully';
              // $('[name=scheduleModal]').modal('show');
              self.confirmationService.confirm({
                message: self.successMessage,
                header: 'Success',
                icon: 'fa fa-check-circle',
                rejectVisible: false,
                accept: () => {
                  self.navigateToShorlistedProfiles();
                }, reject: () => {
                  self.navigateToShorlistedProfiles();
                }
              });
            }
          }, error => {
            self.confirmationService.confirm({
              message: error.message,
              header: 'Error',
              icon: 'fa fa-exclamation-circle',
              rejectVisible: false
            });
          });
        }
      }
    }
  }
  navigateToShorlistedProfiles() {
    /*(this.interviewId) ? this.router.navigate(['../hiring/interviews']) : this.router.navigate(['../hiring/shortlist']);*/
    this.router.navigate(['../myspacenx/interviews/scheduled-interviews']);
  }
  navigateToPrevious() {
    var route = (this.interviewId) ? '../myspacenx/interviews/scheduled-interviews' : '../myspacenx/profiles/shortlist';
    this.router.navigate([route]);
  }
  ngOnDestroy() {
    this.interviewId = null;
  }
  // providing feedback dirextly at the time of scheduling

  checkBoxClicked() {
    if (this.isSingleSelection === true) {
      this.makeHideShow = true;
    } else {
      this.makeHideShow = false;
    }
  }
  //  upload the feedback direcctly with scheduled interview
  uploadFile(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      try {
        var isValid = this.commonService.validateFile(fileList[0], 'others', 2097152, 69, ['doc', 'docx', 'rtf', 'pdf', 'txt']);
        if (isValid) {
          this.file = fileList[0];
          (<HTMLInputElement>document.getElementById('feedBackPreview')).value = this.file.name;
        }
      } catch (e) {
        this.confirmationService.confirm({
          message: e.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      }
    }
  }
  getPanelList(interviewId: any) {
    var self = this;
    self.newHiringRequestDataService.getPanelMembers().subscribe(data => {
      data = data['_embedded']['employees'];
      data.forEach(function (elem: any, ind: any) {
        let obj = {
          'id': elem.id,
          'fullName': elem.fullName
        }
        self.panelMembersList.push(obj);
      });
      if (interviewId) {
        self.interviewDetails.adhocInterviewId = interviewId;
        self.interviewDetails.interviewerId = "";
      }
    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    });

  }
  disabledInterviewer(val: any) {
    if (val) {
      this.makeDisableadhocInterviwer = true;
      this.interviewDetails.isAdhocInterviewer = false;
      this.interviewDetails.adhocInterviewId = '';
    } else {
      this.makeDisableadhocInterviwer = false;
      this.interviewDetails.isAdhocInterviewer = true;
    }

  }
  disabledAdhocInterviewer(val: any) {
    if (val) {
      this.makeDisableInterviwer = true;
      this.interviewDetails.isAdhocInterviewer = true;
      this.interviewDetails.interviewerId = '';
    } else {
      this.makeDisableInterviwer = false;
      this.interviewDetails.isAdhocInterviewer = false;
    }
  }
}
