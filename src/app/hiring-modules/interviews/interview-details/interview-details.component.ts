import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationService } from "primeng/api";
import { AlertService } from "src/app/shared/services/alert.service";
import { AddProfileDataService } from "../../shared-hiring/services/add-profile-data.service";
import { HiringRequestDataService } from "../../shared-hiring/services/hiring-request-data.service";
import { ProfilesDataService } from "../../shared-hiring/services/profiles-data.service";


@Component({
  selector: 'app-interview-details',
  templateUrl: './interview-details.component.html',
  styleUrls: ['./interview-details.component.scss']
})
export class InterviewDetailsComponent implements OnInit {
  interviewLevelOptions: any;
  interviewModeOptions: any;
  interviewDetails: any = {};
  hiringId: string;
  hiringNeedId: string;
  interviewId: string;
  panels: any;
  interviewers: any;
  interviewDate: any;
  isLoaded: boolean = false;
  today: Date;
  panelMembersList: any = [];

  constructor(private newHiringRequestDataService:HiringRequestDataService, 
    private addProfileDataService: AddProfileDataService, 
    private profileDataService: ProfilesDataService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private confirmationService: ConfirmationService,
    private alertSvc:AlertService) {
    
  }
  ngOnInit() {
    this.today = new Date();
    var data = this.addProfileDataService.getHiringCombos(), self = this;
    if (data) {
      this.interviewModeOptions = data['INTERVIEW_MODE'];
    }
    self.route.params.subscribe(params => {
      if (params['id']) {
        self.interviewId = params['id'];
          if (self.interviewId) {
            self.profileDataService.getInterviewDetailsById(self.interviewId).subscribe(data => {
              var interviewDetails: any = {};
              interviewDetails.interviewLevel = data['levelId'];
              if(!self.interviewModeOptions){
                var comboData = this.addProfileDataService.getHiringCombos();
                if (comboData) {
                  self.interviewModeOptions = comboData['INTERVIEW_MODE'];
                }
              }
              
              self.interviewDetails.interviewMode = data['mode'];
              self.interviewDetails.prospectId = data['prospect']['id']
              self.interviewDetails.prospectiveHireName = data['prospect']['displayName'];
              self.hiringId = data['hiringRequest']['id'];
              self.hiringNeedId = data['hiringRequestNeed']['id'];
              self.interviewDetails.recruiterComments = data['recruiterComments'];
              self.interviewDetails.isAdhocInterviewer=data['isAdhocInterviewer'];
              interviewDetails.interviewPanelId = data['interviewPanel']['id'];
              self.interviewDate = new Date(data['interviewDate']);
              if(self.interviewDetails.isAdhocInterviewer == true){
                self.interviewDetails.adhocInterviewId = data['interviewer']['id'];
              }else{
                interviewDetails.interviewId = data['interviewer']['id'];
              }
              self.profileDataService.getInterviewLevels(this.hiringId, this.hiringNeedId).subscribe(data => {
                self.interviewLevelOptions = data;
                self.interviewDetails.interviewLevel = interviewDetails.interviewLevel;
                if (self.interviewDetails.interviewLevel) {
                  self.profileDataService.getPanels(self.hiringId, self.hiringNeedId, self.interviewDetails.interviewLevel).subscribe(data => {
                    self.panels = data;
                    self.interviewDetails.interviewPanelId = interviewDetails.interviewPanelId;
                    if(self.interviewDetails.isAdhocInterviewer){
                      self.getPanelList( self.interviewDetails.adhocInterviewId);
                    }else{
                      self.getInterviewers(interviewDetails.interviewId);
                    }
                  }, error => {
                    self.confirmationService.confirm({
                      message: error.message,
                      header: 'Error',
                      icon: 'fa fa-exclamation-circle',
                      rejectVisible: false
                    });
                  });
                }
              }, error => {
                self.confirmationService.confirm({
                  message: error.message,
                  header: 'Error',
                  icon: 'fa fa-exclamation-circle',
                  rejectVisible: false
                });
              });
            }, error => {
              self.confirmationService.confirm({
                message: error.message,
                header: 'Error',
                icon: 'fa fa-exclamation-circle',
                rejectVisible: false
              });
            });
          }
      }
    });
  }
  getInterviewLevels() {
    var self = this;
    this.profileDataService.getInterviewLevels(this.hiringId, this.hiringNeedId).subscribe(data => {
      self.interviewLevelOptions = data;
    }, error => {
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    });
  }
  loadPanels() {
   var self = this;
    if (this.hiringId && this.hiringNeedId && this.interviewDetails.interviewLevel) {
      this.profileDataService.getPanels(this.hiringId, this.hiringNeedId, this.interviewDetails.interviewLevel).subscribe(data => {
        self.panels = data;
      }, error => {
        this.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
      self.interviewDetails.interviewPanelId = "";
    }else if(!this.interviewDetails.interviewLevel){
      this.panels = [];
      this.interviewDetails.interviewPanelId = "";
      this.interviewers = [];
      this.interviewDetails.interviewerId = "";
      this.interviewDetails.adhocInterviewId="";
    }
  }
  getInterviewers(interviewerId?: string) {
    var self = this;
    if (this.hiringId && this.hiringNeedId && this.interviewDetails.interviewLevel && this.interviewDetails.interviewPanelId) {
      this.profileDataService.getInterviewersByPanelId(this.hiringId, this.hiringNeedId, this.interviewDetails.interviewLevel, this.interviewDetails.interviewPanelId).subscribe(data => {
        self.interviewers = data;
        self.interviewDetails.interviewerId = "";
        if (interviewerId) {
            self.interviewDetails.interviewerId = interviewerId;
            self.interviewDetails.adhocInterviewId="";
        }
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      });
    }else if(!this.interviewDetails.interviewPanelId){
      self.interviewers = [];
      self.interviewDetails.interviewerId = "";
    }
  }
  getPanelList(interviewId:any){
    var self = this;
    self.newHiringRequestDataService.getPanelMembers().subscribe(data => {
      data =  data['_embedded']['employees'];
      data.forEach(function(elem:any, ind:any){
        let obj = {
          'id': elem.id,
          'fullName': elem.fullName
        }
       self.panelMembersList.push(obj);
      });
      if(interviewId){
        self.interviewDetails.adhocInterviewId=interviewId;
        self.interviewDetails.interviewerId = "";
      }
    },error =>{
      self.confirmationService.confirm({
        message: error.message,
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible:false
      });
    });
  }
  navigateToPrevious() {
    var route = localStorage.getItem('previousRoute');
    route = (route) ? route : '../myspacenx/interviews/scheduled-interviews';
    this.router.navigate([route]);
  }
  ngOnDestroy() {
    this.interviewId = null;
  }
}
