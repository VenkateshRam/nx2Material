import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { OffersDataService } from '../../shared-hiring/services/offers.service';


@Component({
  selector: 'app-released-offers',
  templateUrl: './released-offers.component.html',
  styleUrls: ['./released-offers.component.scss']
})
export class ReleasedOffersComponent implements OnInit {

  displayedColumns = ['name', 'practice', 'des', 'st', 'wl', 'joiningdate', 'ht', 'hm',  'cd'];
  public priorityOptions: any[];
  public statusOptions: any[];
  public priority: string;
  public status: string;
  columnDefs: any;
  allOfTheData: any;
  public buttonModes: any = {};
  public recordsCount: number;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  searchName: string = '';
  successMessage: string;
  isLoaded: boolean = false;

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  commonService: any;
  confirmationService: any;

  constructor(private offersDataService: OffersDataService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private alertSvc: AlertService) {
   
  
    this.priorityOptions = [
      "All", "Low", "Normal", "High", "Very High", "Critical", "Very Critical"
    ];
    this.statusOptions = [
      "All", "Active", "Draft"
    ];
    this.priority = "All";
    this.status = "All";
  }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }
  formatId(id: any) {
    return 'OF' + id.value.split('-')[0].toUpperCase();
  }
  formatDate(data: any) {
    var time = new Date(data.value * 1),
      result, year, month, today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    result = today + '/' + month + '/' + year;
    return result;
  }

  onGridInitialize() {
    this.loadGrid();
  }

  filterChanged() {
    // this.searchName = '';
    this.pageNum = 0;
    this.loadGrid();
  }

  loadGrid() {
    var self = this;
    self.resetModes();
    setTimeout(() => {
      this.offersDataService.getAllOffersByFilters(this.pageNum, this.pageSize, 'RELEASED', this.searchName).subscribe(data => {
        self.setRowData(data['content']);
        self.recordsCount = data.totalElements;
        self.totalPages = data.totalPages;
      });
    }, 100);
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
    }
  viewOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'];
      this.offersDataService.documentId = selectedRecords[0]['documentId'];
      this.router.navigate(['/myspacenx/offers/offer-letter', id]);
    }
  }
  openOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        offerStatus = selectedRecords[0]['offerStatus'],
        hiringNeedId = selectedRecords[0]['needId'];
      this.offersDataService.hiringDetails = {
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId,
        'offerStatus': offerStatus
      };
      this.router.navigate(['/myspacenx/offers/view', id]);
    }
  }
  editOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        offerStatus = selectedRecords[0]['offerStatus'],
        hiringNeedId = selectedRecords[0]['needId'];
      this.offersDataService.hiringDetails = {
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId,
        'offerStatus': offerStatus
      };
      this.router.navigate(['/myspacenx/offers/offerdetail', id]);
    }
  }
  deleteOffer() {
    var selectedRecords = this.selection.selected, self = this;
    self.confirmationService.confirm({
      message: 'Do you want to delete this offer?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      key: 'deleteReleasedOffer',
      accept: () => {
        if (selectedRecords.length > 0) {
          var id = selectedRecords[0]['offerId'],
            hiringId = selectedRecords[0]['hiringRequestId'],
            hiringNeedId = selectedRecords[0]['needId'];
          this.offersDataService.deleteOffer(hiringId, hiringNeedId, id).subscribe(data => {
            if (data.status == 200) {
              self.successMessage = 'Offer deleted successfully';
              self.confirmationService.confirm({
                message: self.successMessage,
                header: 'Success',
                icon: 'fa fa-check-circle',
                rejectVisible: false,
                accept: () => {
                  this.loadGrid();
                },
                reject: () => {
                  this.loadGrid();
                }
              });
            }
          });
        }
      }
    });
  }
  checkModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var status = selectedRecords[0]['offerStatusCode'];
      this.buttonModes['view'] = false;
      this.buttonModes['open'] = false;
      this.buttonModes['edit'] = (status == 'ACCEPTED' || status == 'CANCELLED' || status == 'DELETED') ? true : false;
      this.buttonModes['delete'] = (status == 'ACCEPTED' || status == 'CANCELLED' || status == 'DELETED') ? true : false;
    } else {
      this.resetModes();
    }
  }
  exportData() {
    var self = this;
    setTimeout(() => {
      self.offersDataService.exportOffers('RELEASED', self.searchName).subscribe(data => {
        self.commonService.downloadCsv('Released Offers', data['_body']);
      });
    }, 100);
  }
  resetModes() {
    this.buttonModes = {
      edit: true,
      view: true,
      open: true,
      delete: true
    }
  }

  SearchByName() {
    if (this.searchName.length > 2)
      this.filterChanged();
    else if (this.searchName == "")
      this.setPageNum(0);
  }
}