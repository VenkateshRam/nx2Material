import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from 'src/app/shared/material/material.module';
import { OffersRoutingModule } from './offers-routing.module';
import { OffersComponent } from './offers.component';
import { AllOffersComponent } from './all-offers/all-offers.component';
import { ReadyToOfferComponent } from './ready-to-offer/ready-to-offer.component';
import { ReleasedOffersComponent } from './released-offers/released-offers.component';
import { DeclinedOffersComponent } from './declined-offers/declined-offers.component';
import { NewOfferComponent } from './new-offer/new-offer.component';


@NgModule({
  declarations: [
    OffersComponent,
    AllOffersComponent,
    ReadyToOfferComponent,
    ReleasedOffersComponent,
    DeclinedOffersComponent,
    NewOfferComponent
    
  ],
  imports: [
    CommonModule,
    OffersRoutingModule,
    MaterialModule
  ]
})
export class OffersModule { }
