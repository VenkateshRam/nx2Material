import { SelectionModel } from "@angular/cdk/collections";
import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertService } from "src/app/shared/services/alert.service";
import { OffersDataService } from "../../shared-hiring/services/offers.service";

@Component({
  selector: 'app-all-offers',
  templateUrl: './all-offers.component.html',
  styleUrls: ['./all-offers.component.scss']
})
export class AllOffersComponent implements OnInit {

  displayedColumns = ['name', 'practice', 'des', 'st', 'wl', 'joiningdate', 'ol', 'ht', 'hm',  'cd'];
  public statusOptions: any[];
  public status: string;
  columnDefs: any;
  allOfTheData: any;
  public buttonModes: any = {};
  public recordsCount: number = 0;
  totalRecords: number = 0;
  index: number = 0;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  successMessage: string;
  isLoaded: boolean = false;
  byPassTitle: string = 'Bypass CPO Approval';
  byPassId: string = 'byPassId';
  searchName: string = '';
  public pageSizeNumbers = [10, 20, 50, 75, 100];
  page = 20;

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  confirmationService: any;
  commonService: any;

  constructor( private offersDataService: OffersDataService, 
    private alertSvc: AlertService,
   // private assignExecutiveService: AssignExecutiveComponent, 
    private router: Router, 
    private route: ActivatedRoute) {
   

    this.statusOptions = [];
    this.status = "ALL";
  }
  formatId(id: any) {
    console.log(id);
    return 'OF' + id.value.split('-')[0].toUpperCase();
  }
  formatDate(data: any) {
    var time = new Date(data.value * 1),
      result, year, month, today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    result = today + '/' + month + '/' + year;
    return result;
  }


  onGridInitialize() {
    var self = this;
    
    // this.assignExecutiveService.getCombos().subscribe(data => {
    //   self.statusOptions = data['OFFER_STATUS'];
    //   self.loadGrid();
    
    // });
  }
  filterChanged() {
    // this.searchName = '';
    this.pageNum = 0;
    this.loadGrid();
  }
  loadGrid() {
    var self = this;
    // if(this.status == "ALL"){
    //   this.pageSize = 20;
    // }
    if (this.pageSize > 0) {
      self.resetButtonModes();
      this.offersDataService.getAllOffersByFilters(this.pageNum, this.pageSize, (this.status == "ALL") ? "All" : this.status, this.searchName).subscribe(data => {
        self.setRowData(data['content']);
        self.recordsCount = data.totalElements;

        if (self.index == 0) {
          self.totalRecords = data.totalElements;
          self.index++;
        }
        self.totalPages = data.totalPages;
        
     
      });
    }
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
   // this.createNewDatasource();
  }
  viewOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'];
      this.offersDataService.documentId = selectedRecords[0]['documentId'];
      this.router.navigate(['/myspacenx/offers/offer-letter', id]);
    }
  }
  openOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        offerStatus = selectedRecords[0]['offerStatus'],
        hiringNeedId = selectedRecords[0]['needId'];
      this.offersDataService.hiringDetails = {
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId,
        'offerStatus': offerStatus
      };
      this.router.navigate(['/myspacenx/offers/view', id]);
    }
  }
  editOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        offerStatus = selectedRecords[0]['offerStatus'],
        hiringNeedId = selectedRecords[0]['needId'];
      this.offersDataService.hiringDetails = {
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId,
        'offerStatus': offerStatus
      };
      this.router.navigate(['/myspacenx/offers/offerdetail', id]);
    }
  }
  deleteOffer() {
    var selectedRecords = this.selection.selected, self = this;
    self.confirmationService.confirm({
      message: 'Do you want to delete this offer?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      key: 'deleteOffer',
      accept: () => {
        if (selectedRecords.length > 0) {
          var id = selectedRecords[0]['offerId'],
            hiringId = selectedRecords[0]['hiringRequestId'],
            hiringNeedId = selectedRecords[0]['needId'];
          this.offersDataService.deleteOffer(hiringId, hiringNeedId, id).subscribe(data => {
            if (data.status == 200) {
              self.successMessage = 'Offer deleted successfully';
              self.confirmationService.confirm({
                message: self.successMessage,
                header: 'Success',
                icon: 'fa fa-check-circle',
                rejectVisible: false,
                accept: () => {
                  this.loadGrid();
                },
                reject: () => {
                  this.loadGrid();
                }
              });
            }
          
          });
        }
      }, reject: () => {
      }
    });
  }
  submitOffer(opCode: string, reason?: string) {
    var selectedRecords = this.selection.selected, self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        hiringNeedId = selectedRecords[0]['needId'], payload;
      if (opCode == 'REJECT') {
        payload = {
          'rejectionComments': reason
        };
      } else if (opCode == 'CANCEL') {
        payload = {
          'cancellationComments': reason
        };
      } else {
        payload = null;
      }
      this.offersDataService.submitOffer(hiringId, hiringNeedId, id, opCode, payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = (opCode == 'APPROVE') ? 'Offer approved successfully' : 'Offer submitted successfully';
          if (opCode == 'RELEASE') {
            self.successMessage = 'Offer released successfully';
          } else if (opCode == 'REJECT') {
            self.successMessage = 'Offer rejected successfully';
          } else if (opCode == 'CANCEL') {
            self.successMessage = 'Offer cancelled successfully';
          }
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              this.loadGrid();
            },
            reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }
  byPassApproval(reason: string) {
    var selectedRecords = this.selection.selected, self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        hiringNeedId = selectedRecords[0]['needId'],
        opCode = 'APPROVE_WITH_BYPASS',
        payload = {
          'bypassOfferApprovalComments': reason
        };
      this.offersDataService.byPassCPOApproval(hiringId, hiringNeedId, id, opCode, payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Offer approved successfully';
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              this.loadGrid();
            },
            reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }
  checkModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var status = selectedRecords[0]['offerStatusCode'];
      this.buttonModes['open'] = false;
      this.buttonModes['view'] = (status == 'DRAFT') ? true : false;
      this.buttonModes['submit'] = (status == 'DRAFT') ? false : true;
      this.buttonModes['approve'] = (status == 'IN_REVIEW' || status == 'REJECTED') ? false : true;
      this.buttonModes['reject'] = (status == 'IN_REVIEW' || status == 'APPROVED') ? false : true;
      this.buttonModes['byPassApproval'] = (status == 'DRAFT' || status == 'IN_REVIEW' || status == 'DECLINED') ? false : true;
      this.buttonModes['edit'] = (status == 'CANCELLED' || status == 'DELETED') ? true : false;
      this.buttonModes['delete'] = (status == 'ACCEPTED' || status == 'CANCELLED' || status == 'DELETED') ? true : false;
      this.buttonModes['release'] = (status == 'APPROVED') ? false : true;
      this.buttonModes['cancel'] = (status == 'UNDER_NEGOTIATION' || status == 'EXPIRED') ? false : true;
    } else {
      this.resetButtonModes();
    }
  }
  exportData() {
    var self = this;
    setTimeout(() => {
      self.offersDataService.exportOffers(self.status, self.searchName).subscribe(data => {
        self.commonService.downloadCsv('All Offers', data['_body']);
      });
    }, 100);
  }
  ngOnInit() {
    this.resetButtonModes();
    // this.loadGrid();
  }
  resetButtonModes() {
    this.buttonModes = {
      edit: true,
      delete: true,
      submit: true,
      approve: true,
      reject: true,
      byPassApproval: true,
      view: true,
      open: true,
      release: true,
      cancel: true
    };
  }

  changePagesize(page: any) {
    // this.searchName = '';
    this.pageNum = 0;
    this.pageSize = (page == "ALL") ? this.totalRecords : page;

    if (this.pageSize > 0)
      this.loadGrid();

  }

  SearchByName() {
    if (this.searchName.length > 2)
      this.filterChanged();
    else if (this.searchName == "")
      this.setPageNum(0);
  }
}
