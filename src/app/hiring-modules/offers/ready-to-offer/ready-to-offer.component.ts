import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { OffersDataService } from '../../shared-hiring/services/offers.service';

@Component({
  selector: 'app-ready-to-offer',
  templateUrl: './ready-to-offer.component.html',
  styleUrls: ['./ready-to-offer.component.scss']
})
export class ReadyToOfferComponent implements OnInit {

  displayedColumns = ['name', 'practice', 'des', 'st', 'wl', 'joiningdate', 'ol', 'ht', 'hm',  'cd'];
  
  public priorityOptions: any[];
  public statusOptions: any[];
  public priority: string;
  public status: string;
  columnDefs: any;
  allOfTheData: any;
  public buttonModes: any = {};
  public recordsCount: number;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  successMessage: string;
  isLoaded: boolean = false;

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  commonService: any;
  confirmationService: any;

  constructor( private offersDataService: OffersDataService,  
    private router: Router, 
    private route: ActivatedRoute,
    private alertSvc: AlertService) {
   
    this.priorityOptions = [
      "All", "Low", "Normal", "High", "Very High", "Critical", "Very Critical"
    ];
    this.statusOptions = [
      "All", "Active", "Draft"
    ];
    this.priority = "All";
    this.status = "All";
  }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }
  formatId(id: any) {
    return 'OF' + id.value.split('-')[0].toUpperCase();
  }
  formatDate(data: any) {
    var time = new Date(data.value * 1),
      result, year, month, today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    result = today + '/' + month + '/' + year;
    return result;
  }
  onGridInitialize() {
    this.loadGrid();
  }
  loadGrid() {
    var self = this;
    self.resetModes();
    setTimeout(() => {
      this.offersDataService.getOffersByStatus(this.pageNum, this.pageSize, 'APPROVED').subscribe(data => {
        self.setRowData(data['content']);
        self.recordsCount = data.totalElements;
        self.totalPages = data.totalPages;
      });
    }, 100);
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
  }
  viewOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'];
      this.offersDataService.documentId = selectedRecords[0]['documentId'];
      this.router.navigate(['/myspacenx/offers/offer-letter', id]);
    }
  }
  openOffer(){
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        offerStatus = selectedRecords[0]['offerStatus'],
        hiringNeedId = selectedRecords[0]['needId'];
      this.offersDataService.hiringDetails = {
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId,
        'offerStatus': offerStatus
      };
      this.router.navigate(['/myspacenx/offers/view', id]);
    }
  }
  editOffer() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        offerStatus = selectedRecords[0]['offerStatus'],
        hiringNeedId = selectedRecords[0]['needId'];
      this.offersDataService.hiringDetails = {
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId,
        'offerStatus': offerStatus
      };
      this.router.navigate(['/myspacenx/offers/offerdetail', id]);
    }
  }
  deleteOffer() {
    var selectedRecords =this.selection.selected, self = this;
    self.confirmationService.confirm({
      message: 'Do you want to delete this offer?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      key: 'deleteApprovedOffer',
      accept: () => {
        if (selectedRecords.length > 0) {
        var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        hiringNeedId = selectedRecords[0]['needId'];
        this.offersDataService.deleteOffer(hiringId, hiringNeedId, id).subscribe(data => {
          if (data.status == 200) {
            self.successMessage = 'Offer deleted successfully';
            self.confirmationService.confirm({
              message: self.successMessage,
              header: 'Success',
              icon: 'fa fa-check-circle',
              rejectVisible: false,
              accept: () => {
                this.loadGrid();
              },
              reject: () => {
                this.loadGrid();
              }
            });
          }
        });
      }
    }
    });
  }
  submitOffer(opCode: string, reason?: string) {
    var selectedRecords = this.selection.selected, self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        hiringNeedId = selectedRecords[0]['needId'], payload;
      if(opCode == 'REJECT'){
        payload = {
          'rejectionComments': reason
        };
      }else{
        payload = null;
      }
      this.offersDataService.submitOffer(hiringId, hiringNeedId, id, opCode, payload).subscribe(data => {
        if (data.status == 200) {
          if (opCode == 'RELEASE') {
            self.successMessage = 'Offer released successfully';
          } else if (opCode == 'REJECT') {
            self.successMessage = 'Offer rejected successfully';
          }
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              this.loadGrid();
            },
            reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }
  checkModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var status = selectedRecords[0]['offerStatusCode'];
      if (status == 'APPROVED') {
        this.buttonModes = {
          edit: false,
          release: false,
          view: false,
          open: false,
          reject: false,
          delete: false
        };
      } else {
        this.buttonModes = {
          edit: true,
          release: true,
          view: false,
          open: false,
          reject: true,
          delete: true
        };
      }
      if(status == 'ACCEPTED' || status == 'CANCELLED' || status == 'DELETED' ){
        this.buttonModes['edit'] = true;
        this.buttonModes['delete'] = true;
      }else{
        this.buttonModes['edit'] = false;
        this.buttonModes['delete'] = false;
      }
    }else{
     this.resetModes();
    }
  }
  exportData() {
    var self = this;
    setTimeout(() => {
      self.offersDataService.exportOffers('READY_TO_OFFER').subscribe(data => {
        self.commonService.downloadCsv('Ready To Offer', data['_body']);
      });
    }, 100);
  }
  resetModes() {
    this.buttonModes = {
      edit: true,
      release: true,
      view: true,
      open: true,
      reject: true,
      delete: true
    };
  }
}