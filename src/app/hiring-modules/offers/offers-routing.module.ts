import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllOffersComponent } from './all-offers/all-offers.component';
import { DeclinedOffersComponent } from './declined-offers/declined-offers.component';
import { NewOfferComponent } from './new-offer/new-offer.component';
import { ReadyToOfferComponent } from './ready-to-offer/ready-to-offer.component';
import { ReleasedOffersComponent } from './released-offers/released-offers.component';

const routes: Routes = [{ path: 'all-offers', component: AllOffersComponent },
{ path: 'declined-offers', component: DeclinedOffersComponent },
{ path: 'ready-to-offer', component: ReadyToOfferComponent },
{ path: 'released-offers', component: ReleasedOffersComponent },
{ path: 'new-offer', component: NewOfferComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
