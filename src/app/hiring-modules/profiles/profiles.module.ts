import { NgModule } from '@angular/core';


import { ProfilesRoutingModule } from './profiles-routing.module';
import { ProfilesComponent } from './profiles.component';
import { AllProfilesComponent } from './all-profiles/all-profiles.component';
import { ProfileScreeningComponent } from './profile-screening/profile-screening.component';
import { RejectedComponent } from './rejected/rejected.component';
import { ShortlistedComponent } from './shortlisted/shortlisted.component';
import { SelectedComponent } from './selected/selected.component';
import { PreOfferPendingComponent } from './pre-offer-pending/pre-offer-pending.component';
import { PreOfferCompletedComponent } from './pre-offer-completed/pre-offer-completed.component';
import { ReadyToVerifyComponent } from './ready-to-verify/ready-to-verify.component';
import { VerifiedComponent } from './verified/verified.component';
import { OfferAcceptedComponent } from './offer-accepted/offer-accepted.component';
import { PreOnboardingPendingComponent } from './pre-onboarding-pending/pre-onboarding-pending.component';
import { ReadyToJoinComponent } from './ready-to-join/ready-to-join.component';
import { JoinedComponent } from './joined/joined.component';
import { NoShowComponent } from './no-show/no-show.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CommonModule } from '@angular/common';
import { ProfilesDataService } from '../shared-hiring/services/profiles-data.service';
import { AddProfileDataService } from '../shared-hiring/services/add-profile-data.service';
import { OffersDataService } from '../shared-hiring/services/offers.service';
import { AppConstants } from '../shared-hiring/config/app.constants';
import { ProfessionalComponent } from './add-profile/professional/professional.component';
import { PersonalComponent } from './add-profile/personal/personal.component';
import { SkillsetComponent } from './add-profile/skillset/skillset.component';
import { NewProfileComponent } from './add-profile/new-profile/new-profile.component';


@NgModule({
  declarations: [
    ProfilesComponent,
    AllProfilesComponent,
    ProfileScreeningComponent,
    RejectedComponent,
    ShortlistedComponent,
    SelectedComponent,
    PreOfferPendingComponent,
    PreOfferCompletedComponent,
    ReadyToVerifyComponent,
    VerifiedComponent,
    OfferAcceptedComponent,
    PreOnboardingPendingComponent,
    ReadyToJoinComponent,
    JoinedComponent,
    NoShowComponent,
    ProfessionalComponent,
    PersonalComponent,
    SkillsetComponent,
    NewProfileComponent
  ],
  imports: [
    CommonModule,
    ProfilesRoutingModule,
    MaterialModule,
  ],
  providers:[ProfilesDataService, AddProfileDataService, OffersDataService, AppConstants]
})
export class ProfilesModule { }
