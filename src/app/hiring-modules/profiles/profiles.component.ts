import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {

  displayedColumns = ['id', 'cn', 'relExp', 'cn', 'email', 'Mobno', 'res']
  constructor() { }

  ngOnInit(): void {
  }

}
