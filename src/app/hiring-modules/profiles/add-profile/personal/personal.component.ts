import { PlatformLocation } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AddProfileDataService } from 'src/app/hiring-modules/shared-hiring/services/add-profile-data.service';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'personal-details',
  templateUrl: './personal.component.html',
  styleUrls: [`./personal.component.scss`]
})
export class PersonalComponent {
  public personalDetailsFrmProspectData: any;
  public formSaved: boolean = false;
  public viewMode: boolean = false;
  public hide: boolean = false;
  isLoaded: boolean = false;
  mode: boolean = false;
  public alreadyExists = {
    email: false,
    mobile: false,
    citizenIdentity: false
  };
  personalFormSubmit: boolean = false;
  public identityTypeSelected: boolean = false;
  public pattern = /([0-9]{10,})/;
  platformLoc: any;
  stateData: any = [];
  stateDataPerm: any = [];
  countryData: any = [];
  previousValues: any = {
    email: '',
    mobile: '',
    citizenType: '',
    citizenIdentity: ''
  };
  defaultDate = new Date('1/1/1950');
  commonService: any;
  confirmationService: any;
  cityData: any[];
  constructor(
    private AddProfileDataService: AddProfileDataService,
    private router: Router,
    private route: ActivatedRoute,
    platformLocation: PlatformLocation,
    private alertSvc: AlertService
  ) {
    // var myEl = $('.left-sidebar');
    // myEl.addClass('personal-left-nav');
    this.platformLoc = platformLocation;
  }

  @ViewChild('personalDetailsForm') public personalForm: any;

  ngAfterViewChecked() {
    this.AddProfileDataService.personalForm = this.personalForm;
  }
  ngOnInit() {
    let currentUrl = this.router.url.match('attach');
    if (currentUrl && currentUrl.length) {
      this.hide = true;
    }
    var self = this;
    if (self.AddProfileDataService.profileId && self.AddProfileDataService.mode != 'new') {
      this.mode = true;
      if (!this.AddProfileDataService.ProspectData)
        self.loadProfileDataWithId(self.AddProfileDataService.profileId);
      else
        self.loadProfileData();
    } else {
      this.mode = false;
      self.loadProfileData();
    }
  }
  checkEmail(value: any) {
    var self = this,
      origin = self.platformLoc.location.origin,
      url;
    if (value && self.previousValues.email != value) {
      self.AddProfileDataService.getExistingProfileByEmail(value).subscribe(
        data => {
          if (data.status == 200 && data['_body'] && self.personalDetailsFrmProspectData) {
            self.personalDetailsFrmProspectData.email = '';
            self.alreadyExists.email = true;
            if (self.commonService.IsJsonString(data['_body'])) {
              url = JSON.parse(data['_body'])['id']
                ? origin + '/myspacenx/profiles/profile-details/' + JSON.parse(data['_body'])['id']
                : origin + '/myspacenx/profiles/search';
            } else {
              url = origin + '/myspacenx/profiles/search';
            }
            self.confirmationService.confirm({
              message: `Profile with this email id (${value}) already exists. <a href=${url} target="_blank"><span style="color:blue;">Click here</span></a> to view the details.`,
              header: 'Warning',
              icon: 'fa fa-warning',
              rejectVisible: false
            });
          } else if (data.status == 200) {
            self.alreadyExists.email = false;
          }
        });
    }
  }
  checkMobile(value: any) {
    var self = this,
      origin = self.platformLoc.location.origin,
      url;
    if (value && self.previousValues.mobile != value) {
      self.AddProfileDataService.getExistingProfileByMobile(value).subscribe(
        data => {
          if (data.status == 200 && data['_body'] && self.personalDetailsFrmProspectData) {
            self.personalDetailsFrmProspectData.mobileNumber = '';
            self.alreadyExists.mobile = true;
            if (self.commonService.IsJsonString(data['_body'])) {
              url = JSON.parse(data['_body'])['id']
                ? origin + '/myspacenx/profiles/profile-details/' + JSON.parse(data['_body'])['id']
                : origin + '/myspacenx/profiles/search';
            } else {
              url = origin + '/myspacenx/profiles/search';
            }
            self.confirmationService.confirm({
              message: `Profile with this mobile number (${value}) already exists. <a href=${url} target="_blank"><span style="color:blue;">Click here</span></a> to view the details.`,
              header: 'Warning',
              icon: 'fa fa-warning',
              rejectVisible: false
            });
          } else if (data.status == 200) {
            self.alreadyExists.mobile = false;
          }
        },
        error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false
          });
        }
      );
    }
  }
  checkCitizenIdentity(value: any) {
    var self = this,
      identityType,
      origin = self.platformLoc.location.origin,
      url;
    if (self.personalDetailsFrmProspectData) {
      identityType = self.personalDetailsFrmProspectData.citizenIdentity2Type;
    }
    if (
      identityType &&
      value &&
      (self.previousValues.citizenIdentity != value || self.previousValues.citizenType != identityType)
    ) {
      self.AddProfileDataService.getExistingProfileByCitizenIdentity(identityType, value).subscribe(
        data => {
          if (data.status == 200 && data['_body'] && self.personalDetailsFrmProspectData) {
            self.personalDetailsFrmProspectData.citizenIdentity2 = '';
            self.alreadyExists.citizenIdentity = true;
            if (self.commonService.IsJsonString(data['_body'])) {
              url = JSON.parse(data['_body'])['id']
                ? origin + '/myspacenx/profiles/profile-details/' + JSON.parse(data['_body'])['id']
                : origin + '/myspacenx/profiles/search';
            } else {
              url = origin + '/myspacenx/profiles/search';
            }
            self.confirmationService.confirm({
              message: `Profile with this citizen identity (${value}) already exists. <a href=${url} target="_blank"><span style="color:blue;">Click here</span></a> to view the details.`,
              header: 'Warning',
              icon: 'fa fa-warning',
              rejectVisible: false
            });
          } else if (data.status == 200) {
            self.alreadyExists.citizenIdentity = false;
          }
        },
        error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false
          });
        }
      );
    }
  }
  enableCitizenIdentity() {
    var personalData = this.personalDetailsFrmProspectData;
    if (personalData && personalData.citizenIdentity2Type) {
      this.identityTypeSelected = true;
    }
    if (personalData) {
      personalData.citizenIdentity2 = '';
    }
  }
  restrictMobilenumber(event: any) {
    return this.commonService.restrictMobilenumber(event);
  }
  restrictFloatnumber = function (e: any) {
    this.commonService.restrictFloatnumber(e);
  };
  restrictNumeric = function (e: any) {
    var input = String.fromCharCode(e.which);
    var cAdd = this.personalDetailsFrmProspectData.currentAddress.zipcode;
    var pAdd = this.personalDetailsFrmProspectData.permanentAddress.zipcode;
    if (
      ((cAdd && cAdd.length == 5 && parseInt(cAdd) == 0) || (pAdd && pAdd.length == 5 && parseInt(pAdd) == 0)) &&
      parseInt(input) == 0
    ) {
      return false;
    }
    return this.commonService.restrictNumeric(e);
  };
  restrictLetters = function (e: any) {
    return this.commonService.restrictLetters(e);
  };
 

  copyCurrentToPermanentAddress() {
    var form = this.personalDetailsFrmProspectData;
    form.permanentAddress.address1 = form.currentAddress.address1;
    form.permanentAddress.address2 = form.currentAddress.address2;
    form.permanentAddress.city = form.currentAddress.city;
    form.permanentAddress.state = form.currentAddress.state;
    form.permanentAddress.country = form.currentAddress.country;
    form.permanentAddress.zipcode = form.currentAddress.zipcode;
  }

  saveNext(prospectpersonaldata: any, formValid: boolean = false): void {
    this.personalFormSubmit = true;
    if (!formValid) {
      // this.confirmationService.confirm({
      //   message: 'Please select all mandatory fields',
      //   header: 'Warning',
      //   icon: 'fa fa-exclamation-circle',
      //   rejectVisible: false
      // });
      
      this.alertSvc.showToast('Please select all mandatory fields', 'Warning');
      return;
    }
    var self = this;
    setTimeout(() => {
      if (!prospectpersonaldata.citizenIdentity2) {
        self.alreadyExists.citizenIdentity = false;
      }
      if (!self.alreadyExists.email && !self.alreadyExists.mobile && !self.alreadyExists.citizenIdentity) {
        if (prospectpersonaldata.permanentAddressSameAsCurrentAddress == true) {
          self.copyCurrentToPermanentAddress();
        }
        if (prospectpersonaldata.permanentAddress.state != null && prospectpersonaldata.permanentAddress.state != '') {
          if (prospectpersonaldata.currentAddress.state != null && prospectpersonaldata.currentAddress.state != '') {
            var dob = prospectpersonaldata.dateOfBirth;
            var firstName = prospectpersonaldata.firstName;
            var middleName = prospectpersonaldata.middleName;
            var lastName = prospectpersonaldata.lastName;
            var mobNumber = prospectpersonaldata.mobileNumber;
            var emailId = prospectpersonaldata.email;
            prospectpersonaldata.dateOfBirth = new Date(dob).getTime();
            // removing the spaces
            prospectpersonaldata.firstName = firstName.replace(/\s+/g, '');
            prospectpersonaldata.middleName = (middleName && middleName != null) ? middleName.replace(/\s+/g, '') : '';
            prospectpersonaldata.lastName = lastName.replace(/\s+/g, '');
            prospectpersonaldata.mobileNumber = mobNumber.replace(/\s+/g, '');
            prospectpersonaldata.email = emailId.replace(/\s+/g, '');
            self.AddProfileDataService.updateProspectPersonalData(prospectpersonaldata);
            self.formSaved = true;
            self.AddProfileDataService.personalFormSaved = true;
            self.router.navigate(['../professional'], { relativeTo: self.route });
          } else {
            // self.confirmationService.confirm({
            //   message: 'Please select all mandatory fields',
            //   header: 'Warning',
            //   icon: 'fa fa-exclamation-circle',
            //   rejectVisible: false
            // });
            this.alertSvc.showToast('Please select all mandatory fields', 'Warning');
          }
        } else {
          this.alertSvc.showToast('Please select all mandatory fields', 'Warning');
          // self.confirmationService.confirm({
          //   message: 'Please select all mandatory fields',
          //   header: 'Warning',
          //   icon: 'fa fa-exclamation-circle',
          //   rejectVisible: false
          // });
        }
      }
    }, 100);
  }
  public loadProfileData = () => {
    var self = this;
    var data: any = self.AddProfileDataService.getPersonalData();
    if (data) {
      self.loadDetails();
    } else {
      var profileModalData: any = this.AddProfileDataService.getProfileData();
      self.AddProfileDataService.ProspectData = profileModalData;
      self.loadDetails();
    }
  };
  public loadProfileDataWithId = (id: any) => {
    var self = this;
   
    this.AddProfileDataService.loadProspectDataById(id).subscribe(
      data => {
        self.AddProfileDataService.ProspectData = data;
        self.loadDetails();
      }
    );
  };
  loadDetails() {
    var self = this;
    this.AddProfileDataService.getCountryData().subscribe((data: any) => {
      if (data.status == 200) {
        var country = data && data['result'];
        this.countryData = [];
        for (let element in country) {
          this.countryData.push({
            id: element,
            name: country[element]
          });
        }
        var data: any = self.AddProfileDataService.getPersonalData();
        self.personalDetailsFrmProspectData = data ? data : undefined;
        self.personalDetailsFrmProspectData.citizenIdentity1Type = 'AADHAR';
        self.personalDetailsFrmProspectData.citizenIdentity2Type = 'PAN';
        self.personalDetailsFrmProspectData = data ? data : undefined;
        var personalFormData = self.personalDetailsFrmProspectData;
        var date = personalFormData.dateOfBirth;
        if (date) {
          self.personalDetailsFrmProspectData.dateOfBirth = new Date(date);
        }
        if (personalFormData && personalFormData.citizenIdentity2Type) {
          self.identityTypeSelected = true;
        } else {
          self.identityTypeSelected = false;
        }
        if (data) {
          self.previousValues.citizenIdentity = data.citizenIdentity2;
          self.previousValues.citizenType = data.citizenIdentity2Type;
          self.previousValues.mobile = data.mobileNumber;
          self.previousValues.email = data.email;
          if (personalFormData['permanentAddressSameAsCurrentAddress'] == true) {
            let country = personalFormData['currentAddress']['country'];
            this.AddProfileDataService.getStateData(country).subscribe((data: any) => {
              if (data.status == 200) {
                var state = data && data['result'];
                this.stateData = [];
                this.stateDataPerm = [];
                for (let element in state) {
                  this.stateData.push({
                    id: element,
                    name: state[element]
                  });
                }
                this.stateDataPerm = this.stateData;
                // For setting data in permanent Address state
                var stateResult = this.findIterativeData(this.stateData, personalFormData['currentAddress']['state']);
                self.personalDetailsFrmProspectData['permanentAddress']['state'] = stateResult.name;
                self.personalDetailsFrmProspectData['currentAddress']['state'] = stateResult.id;

                // For setting data in permanent Address country
                var countryResult = this.findIterativeData(this.countryData, personalFormData['currentAddress']['country']);
                self.personalDetailsFrmProspectData['permanentAddress']['country'] = countryResult.name;
                self.getState(self.personalDetailsFrmProspectData['currentAddress']['country']);
              }
            });
          } else if (personalFormData['permanentAddressSameAsCurrentAddress'] == false) {
            this.getState(personalFormData['currentAddress']['country'], 'present');
            this.getState(personalFormData['permanentAddress']['country'], 'permanent');
          }
        }
        self.viewMode = self.AddProfileDataService.mode == 'view' ? true : false;
        // setTimeout(() => {
        //   var contentHeight = $('.content-wrapper').height();
        //   $('.sidebar-menu').height(contentHeight + 15);
        // }, 50);
      }
    });


  }

  formatDate(date: any) {
    var time = new Date(date),
      result,
      year,
      month,
      today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    month = month < 10 ? '0' + month : month;
    today = today < 10 ? '0' + today : today;
    result = year + '-' + month + '-' + today;
    return result;
  }

  navigateToPrevious() {
    var route = localStorage.getItem('previousRoute');
    route = route ? route : '/myspacenx/profiles/all';
    this.router.navigate([route]);
  }

  viewProfile(id: any) {
    this.router.navigate(['/myspacenx/profiles/profile-details/', id]);
  }

  getCountry() {
    this.AddProfileDataService.getCountryData().subscribe((data: any) => {
      if (data.status == 200) {
        var country = data && data['result'];
        this.countryData = [];
        for (let element in country) {
          this.countryData.push({
            id: element,
            name: country[element]
          });
        }
      }
    });
  }

  // For state list based on country
  getState(event: any, type: string = 'present') {
    var self = this;
    self.AddProfileDataService.getStateData(event).subscribe((data: any) => {
      if (data.status == 200) {
        var state = data && data['result'];
        if (type == 'present') {
          this.stateData = [];
          for (let element in state) {
            this.stateData.push({
              id: element,
              name: state[element]
            });
          }
          this.stateData.sort((a: any, b: any) => (a.name < b.name ? -1 : Number(a.name > b.name)));
        }
        else if (type == 'permanent') {
          this.stateDataPerm = [];
          for (let element in state) {
            this.stateDataPerm.push({
              id: element,
              name: state[element]
            });
          }
          this.stateDataPerm.sort((a: any, b: any) => (a.name < b.name ? -1 : Number(a.name > b.name)));
        }
      }
    });
  }

  // getCity() {
  //   this.AddProfileDataService.getCityData().subscribe((data: any) => {
  //     if (data.status == 200) {
  //       var city = data && data['result'];
  //       this.cityData = [];
  //       for (let element in city) {
  //         this.cityData.push({
  //           id: element,
  //           name: city[element]
  //         });
  //       }
  //     }
  //   });
  // }

  onCityChange(type: string = 'present'){
    if(type=='present')
      this.personalDetailsFrmProspectData.currentAddress.city = null;
    else
      this.personalDetailsFrmProspectData.permanentAddress.city = null;
  }

  onCountryChange(type: string = 'present') {
    if (type == 'present')
      this.personalDetailsFrmProspectData.currentAddress.state = null;
    else
      this.personalDetailsFrmProspectData.permanentAddress.state = null;
  }

  addressChange(evnt: any) {
    if (
      this.personalDetailsFrmProspectData['currentAddress']['country'] != null &&
      this.personalDetailsFrmProspectData['currentAddress']['state'] != null
    ) {
      this.AddProfileDataService.getCountryData().subscribe((data: any) => {
        if (data.status == 200) {
          var country = data && data['result'],
            countryData = [];
          for (let element in country) {
            countryData.push({
              id: element,
              name: country[element]
            });
          }
          var countryResult = this.findIterativeData(
            countryData,
            this.personalDetailsFrmProspectData['currentAddress']['country']
          );
          this.personalDetailsFrmProspectData['permanentAddress']['country'] = evnt ? countryResult.name : null;
        }
      });
      setTimeout(() => {
        this.AddProfileDataService.getStateData(this.personalDetailsFrmProspectData['currentAddress']['country']).subscribe((data: any) => {
          if (data.status == 200) {
            var state = data && data['result'],
              stateData = [];
            for (let element in state) {
              stateData.push({
                id: element,
                name: state[element]
              });
            }
            stateData.sort((a: any, b: any) => (a.name < b.name ? -1 : Number(a.name > b.name)));
            var stateResult = this.findIterativeData(
              stateData,
              this.personalDetailsFrmProspectData['currentAddress']['state']
            );
            this.personalDetailsFrmProspectData['permanentAddress']['state'] = evnt ? stateResult.name : null;
          }
        });

      }, 1000);
    }

    if (!evnt) {
      this.stateDataPerm = [];
      this.personalDetailsFrmProspectData['permanentAddress'] = {
        "address1": null,
        "address2": null,
        "city": null,
        "state": null,
        "zipcode": null,
        "country": null
      };
    }
  }
  findIterativeData(data: any, dataString: any) {
    if (data) {
      var result = data.find((ele: any) => ele.id == dataString);
      if (result == undefined) {
        var result = data.find((ele: any) => ele.name === dataString);
        return result;
      } else {
        return result;
      }
    }
  }
}
