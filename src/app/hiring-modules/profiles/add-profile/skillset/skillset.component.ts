import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants } from 'src/app/hiring-modules/shared-hiring/config/app.constants';
import { SkillProficiency } from 'src/app/hiring-modules/shared-hiring/models/SkillProficiency';
import { AddProfileDataService } from 'src/app/hiring-modules/shared-hiring/services/add-profile-data.service';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
    selector: 'skillset',
    templateUrl: './skillset.component.html'
})
export class SkillsetComponent {
    public msgs: any[] = []
    private techSkills: Array<SkillProficiency>;
    public techSkillsCatwise: any;
    private availableTechSkills: Array<string>;
    public successMessage: string;
    private headers: any;
    public categoryId: any;
    public categoryNames: any;
    public skillProficiency: any = [];
    public categoryList: any = [];
    public editMode: boolean = false;
    public viewMode: boolean = false;
    public isLoaded: boolean = false;
    public hideEdit: boolean = true;
    public hide: boolean = false;
  confirmationService: any;
    constructor(private appConstants: AppConstants, 
      private psService: AddProfileDataService, 
      private router: Router,
      private alertSvc:AlertService) { 
    }
    ngOnInit() {
        let currentUrl = this.router.url.match("attach");
        if (currentUrl && currentUrl.length) {
          this.hide = true;
        }
        this.techSkillsCatwise = {
            'skillSets': []
        };
        var data = this.psService.getHiringCombos(), self = this;
        if (data && data !=undefined) {
            this.categoryNames = data['SKILL_CATEGORY'];
            this.categoryId = 9999;
        }
        var data: any = this.psService.getSkillData();
        if(data && data!=undefined){
          self.loadDetails();
        }else{
          self.psService.loadProspectDataById(self.psService.profileId).subscribe(data => {
            self.psService.ProspectData = data;
            self.psService.getHiringComboDetails().subscribe(data=>{
              self.psService.hiringComboData = data;
              self.categoryNames = data['SKILL_CATEGORY'];
              self.categoryId = 9999;
              self.loadDetails();
            }, error => {
                self.confirmationService.confirm({
                  message: error.message,
                  header: 'Error',
                  icon: 'fa fa-exclamation-circle',
                  rejectVisible: false
                });
            });
          }, error => {
            self.confirmationService.confirm({
              message: "Please re-login to continue",
              header: 'Error',
              icon: 'fa fa-exclamation-circle',
              rejectVisible: false
            });
          });
        }
    }
    loadDetails(){
        var data: any = this.psService.getSkillData(), self = this;
        this.categoryList = (data ? data["skillSets"] : undefined);
        if (!this.categoryList) {
            this.categoryList = [];
        }
        this.categoryList && this.categoryList.forEach(function (elem: any, ind: any) {
            var catId: any = elem['categoryId'];
            var categoryName = self.psService.getSkillsMap()['skillCatNames'][catId];
            elem['categoryName'] = categoryName;
            var skills = self.psService.getSkillsMap()['skills'][catId];
            var skillProficiencies = elem.skillProficiencies;
            skillProficiencies.forEach(function (elem: any, ind: any) {
                elem['skill'] = skills;
            });
        });
        this.techSkillsCatwise['skillSets'] = this.categoryList;
        this.viewMode = (this.psService.mode == 'view') ? true : false;
        this.hideEdit = (this.psService.mode == 'edit') ? false : true;
        this.headers = [{
            "skillHeader": {
                "item": "Skill",
                "experience": "Years of Experience",
                "rating": "Rate yourself"
            }
        }];
    }
    public AddCategory(categoryId: any): void {
        var categoryName = this.psService.getSkillsMap()['skillCatNames'][categoryId];
        this.headers = [{
            "skillHeader": {
                "item": "Skill",
                "experience": "Years of Experience",
                "rating": "Rate yourself"
            }
        }];
        this.categoryList.push({
            "categoryName": categoryName,
            "categoryId": categoryId,
            "skillProficiencies": []
        });
        this.techSkillsCatwise['skillSets'] = this.categoryList;
        this.availableTechSkills = this.psService.getSkillsMap()['skills'][categoryId];
        this.categoryId = 9999;
    }
    public AddSkill(categoryIndex: number): void {
        this.headers = [{
            "skillHeader": {
                "item": "Skill",
                "experience": "Years of Experience",
                "rating": "Rate yourself"
            }
        }];
        var catId: any = this.techSkillsCatwise['skillSets'][categoryIndex]['categoryId'];
        var skills = this.psService.getSkillsMap()['skills'][catId];
        this.techSkillsCatwise['skillSets'][categoryIndex].skillProficiencies
            .push(
            {
                "skill": skills,
                "yearsOfExperience": 0,
                "rating": 0,
                "isNew": true
            }
            );
        this.skillProficiency = this.techSkillsCatwise['skillSets'][categoryIndex].skillProficiencies;
    }
    removeCategory(rowIndex: number): void {
        this.techSkillsCatwise['skillSets'].splice(rowIndex, 1);
    }
    deleteTool(rowIndex: number): void {
        // this.items.splice(rowIndex, 1);
    }
    deleteSkill(rowIndex: number): void {
        // this.technicalSkillsList.splice(rowIndex, 1);
    }
    @ViewChild('skillDetailsForm') public skillForm: any;
    ngOnDestroy() {
        //$('.sidebar-menu').height('100%');
    }
    ngAfterViewChecked() {
        //var contentHeight = $('.content-wrapper').height();
        //$('.sidebar-menu').height(contentHeight+15);
    }
    saveNext(techSkillsCatwise: any): void {
        var msg: string = this.validateSkillset(techSkillsCatwise.skillSets);
        if (msg) {
            this.confirmationService.confirm({
                message: msg,
                header: 'Error',
                icon: 'fa fa-exclamation-circle',
                rejectVisible: false
            });
            return;
        }
        this.psService.updateProspectSkillData(techSkillsCatwise);
        var prospectData = this.psService.getProspectData(), self = this;
        var mode = this.psService.mode;
        if (mode == 'edit') {
            this.psService.updateProspectData(prospectData, prospectData['id']).subscribe(data => {
                if (data.status == 200) {
                    self.successMessage = 'Profile updated successfully';
                    self.confirmationService.confirm({
                        message: self.successMessage,
                        header: 'Success',
                        icon: 'fa fa-check-circle',
                        rejectVisible: false,
                        accept: () => {
                            this.navigateToProfiles();;
                        }, reject: () => {
                            this.navigateToProfiles();;
                        }
                    });
                }
            }, error => {
                self.confirmationService.confirm({
                    message: error.message,
                    header: 'Error',
                    icon: 'fa fa-exclamation-circle',
                    rejectVisible: false
                });
            });
        } else if (mode == 'new') {
            if(prospectData['hiringRequestId'] == ''){
               prospectData['hiringRequestId'] = null;
            }
            if(prospectData['hiringRequestNeedId'] == ''){
               prospectData['hiringRequestNeedId'] = null;
            }
            this.psService.saveProspectData(prospectData).subscribe(data => {
                var profileId = data['_body'];
                if (profileId) {
                    self.successMessage = 'Profile added successfully';
                    //$('[name=myModal]').modal('show');
                    self.confirmationService.confirm({
                        message: self.successMessage,
                        header: 'Success',
                        icon: 'fa fa-check-circle',
                        rejectVisible: false,
                        accept: () => {
                            this.navigateToProfiles();;
                        }, reject: () => {
                            this.navigateToProfiles();;
                        }
                    });

                }
            }, error => {
                self.confirmationService.confirm({
                    message: error.message,
                    header: 'Error',
                    icon: 'fa fa-exclamation-circle',
                    rejectVisible: false
                });
            });
        }
    }
    validateSkillset(skillSets: any) {
        var msg: string = '';
        skillSets.forEach(function (elem: any, ind: any) {
            var proficiencies: any = elem.skillProficiencies;
            proficiencies.forEach(function (elem: any, ind: any) {
                if (!elem.skillId || !elem.yearsOfExperience || !elem.rating) {
                    msg = 'Fill all mandatory fields Skill, Years of experience and Rating for a skill category';
                }
            });
        });
        return msg;
    }
    navigateToProfiles() {
        this.psService.ProspectData = null;
        this.router.navigate(['../myspacenx/profiles/search']);
    }
    navigateToPrevious() {
        var route = localStorage.getItem('previousRoute');
        route = (route) ? route : '/myspacenx/profiles/all';
        this.router.navigate([route]);
    }
}


