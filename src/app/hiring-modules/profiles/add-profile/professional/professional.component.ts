import { Component, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppConstants } from "src/app/hiring-modules/shared-hiring/config/app.constants";
import { ProfileDetails } from "src/app/hiring-modules/shared-hiring/models/ProfileDetails";
import { AddProfileDataService } from "src/app/hiring-modules/shared-hiring/services/add-profile-data.service";
import { AlertService } from "src/app/shared/services/alert.service";

// var FileSaver = require('filesaver.js-npm');
// var converter = require('number-to-words');
@Component({
  selector: 'professional-details',
  templateUrl: './professional.component.html'
})
export class ProfessionalComponent {
  //ProfessionalDataFrmProspectData: ProfessionalInformation;
  ProfessionalDataFrmProspectData: ProfileDetails;
  counterOfferDetails: any = {
    'counterOfferCtcDetails': [{}]
  };
  counterOfferCtcWords: string;
  ProfessionalFrmNoticePeriodData: any;
  sendTitle: any = "Confirm & Send";
  sendId: any = "confirmSend";
  active: boolean = true;
  inactive: boolean = false;
  yes: boolean = false;
  no: boolean = true;
  isLoaded: boolean = false;
  formSubmitted: boolean = false;
  successMessage: string;
  viewMode: boolean = false;
  disableMode: boolean = true;
  hide: boolean = true;
  eHide: boolean = true;
  uploaded: boolean = false;
  public attachHide: boolean = false;
  public formSaved: boolean = false;
  comboData: any;
  resumeDetails: any;
  public isReferral: boolean;
  referralData: any = [{}];
  referralDetails: any = [{}];
  results: any;
  willingToRelocate: boolean = false;
  willingToRelocate1: any;
  public clientNameOptions: any;
  vendor: any;
  @ViewChild('professionalDetailsForm') public professionalForm: any;
  file: File;
  ctcToWord: string;
  etcToWord: string;
  public isVendor: boolean;
  commonService: any;
  confirmationService: any;
  constructor(
    private AddProfileDataService: AddProfileDataService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private appConstants: AppConstants,  
    private alertSvc: AlertService) {
  }
  ngOnInit() {
    var self = this;
    let currentUrl = this.router.url.match("attach");
    if (currentUrl && currentUrl.length) {
      this.attachHide = true;
    }
    var data = self.AddProfileDataService.getProspectCombos();
    if (data && data != undefined) {
      self.comboData = {
        employmenttype: data['EMPLOYMENT_TYPE'],
        documenttype: data['DOCUMENT_TYPE'],
        noticeperiod: data['NOTICE_PERIOD'],
        prospectSource: data['PROSPECT_SOURCE']
      }
    } else {
      self.AddProfileDataService.getProspectComboDetails().subscribe(data => {
        self.comboData = {
          employmenttype: data['EMPLOYMENT_TYPE'],
          documenttype: data['DOCUMENT_TYPE'],
          noticeperiod: data['NOTICE_PERIOD'],
          prospectSource: data['PROSPECT_SOURCE']
        }
      });
    }
    setTimeout(() => {
      var data: any = self.AddProfileDataService.getProfessionalData();
      if (data) {
        self.loadDetails();
      } else {
        self.AddProfileDataService.loadProspectDataById(self.AddProfileDataService.profileId).subscribe(data => {
          self.AddProfileDataService.ProspectData = data;
          self.AddProfileDataService.getProspectComboDetails().subscribe(data => {
            self.comboData = {
              employmenttype: data['EMPLOYMENT_TYPE'],
              documenttype: data['DOCUMENT_TYPE'],
              noticeperiod: data['NOTICE_PERIOD'],
              prospectSource: data['PROSPECT_SOURCE']
            }
            self.loadDetails();
          });
        });
      }
    }, 100);
    var data = self.AddProfileDataService.getHiringCombos();
    if (data) {
      self.clientNameOptions = data['CLIENT'];
    } else {
      self.AddProfileDataService.getHiringComboDetails().subscribe(data => {
        self.clientNameOptions = data['CLIENT'];
      });
    }
  }
  search(event: any) {
    var self = this;
    let query = event.query;
    self.AddProfileDataService.getEmployees(query, 0, 3000).subscribe(data => {
      self.results = data['content'];
    });
  }
  loadDetails() {
    var self = this;
    var data: any = self.AddProfileDataService.getProfessionalData();
    self.ProfessionalDataFrmProspectData = (data ? data : undefined);

    if (data['referralDetails'] && data['referralDetails'][0] && data['referralDetails'][0]['referralName']) {
      self.referralData[0]['fullName'] = data['referralDetails'][0]['referralName'];
    }
    if (self.ProfessionalDataFrmProspectData && self.ProfessionalDataFrmProspectData.counterOfferDetails) {
      self.counterOfferDetails = self.ProfessionalDataFrmProspectData.counterOfferDetails;
    }
    if (self.ProfessionalDataFrmProspectData) {
      var documents = self.ProfessionalDataFrmProspectData.documents;
      if(this.AddProfileDataService.fileData) {
        this.file = this.AddProfileDataService.fileData;
        setTimeout(()=>{(<HTMLInputElement>document.getElementById('fileNamePreview')).value = this.file.name})
        this.uploaded = true;
      }
      documents && documents.forEach(function (elem: any, ind: any) {
        if (elem['documentType']['code'] == 'RESUME') {
          self.resumeDetails = {
            'title': elem['title'],
            'documentId': elem['documentId'],
            'prospectId': elem['prospectId']
          }
        }
      });
      self.ProfessionalDataFrmProspectData.documentType = 'RESUME';
    }
    if (self.resumeDetails) {
      self.uploaded = true;
    }
    self.viewMode = (self.AddProfileDataService.mode == 'view') ? true : false;
    self.disableMode = (self.viewMode) ? false : true;
    self.propsectSourceChange();
  }
  locationsdisplay() {
    if (!this.ProfessionalDataFrmProspectData["willingToRelocate"]) {
      this.ProfessionalDataFrmProspectData["locationId"] = undefined;
      this.ProfessionalDataFrmProspectData["locationName"] = "";
    }
  }
  restrictFloatnumber = function (e: any) {
    return this.commonService.restrictFloatnumber(e);
  }
  propsectSourceChange() {
    var professData: any = this.ProfessionalDataFrmProspectData;
    if (professData && professData.source == "REFERRAL") {
      this.isReferral = true;
    } else {
      this.isReferral = false;
      this.ProfessionalDataFrmProspectData.referralDetails = [];
      this.referralData[0]['fullName'] = '';
    }
    if (professData && professData.source == "VENDOR") {
      this.AddProfileDataService.getVendorDetails(0, 100).subscribe(data => {
        this.vendor = data['content'];
        this.isVendor = true;
      })
    } else {
      this.isVendor = false;
    }
  }
  loadReferralDetails(event: any) {
    var self = this,
      referralId: any = event.id;
    var professionalForm = self.ProfessionalDataFrmProspectData;
    if (referralId) {
      self.AddProfileDataService.getReferralDetailsById(referralId).subscribe(data => {
        var referralDetails = {
          'referralId': data['employeeCode'],
          'referralName': data['fullName'],
          'referralEmail': data['companyEmail'],
          'referralPhone': data['mobileNumber']
        };
        professionalForm.referralDetails[0] = referralDetails;
      });
    }
  }
  restrictCharcters(value: any) {
    return this.commonService.restrictCharcters(value);
  }
  restrictExpCharcters(value: any) {
    return this.commonService.restrictExpCharcters(value);
  }
  validateExperience(value: any) {
    return this.commonService.validateExperience(value);
  }
  convertToWords(value: number, ctc: string) {
    if (ctc == "ctc") {
      if (value && this.commonService.convertNumberToWords(value)) {
        this.ctcToWord = this.commonService.convertNumberToWords(value) + " Rupees";
      }
      else {
        this.ctcToWord = '';
      }
    }
    else {
      if (value && this.commonService.convertNumberToWords(value)) {
        this.etcToWord = this.commonService.convertNumberToWords(value) + " Rupees";
      }
      else {
        this.etcToWord = '';
      }
    }
  }
  ngAfterViewChecked() {
    this.AddProfileDataService.professionalForm = this.professionalForm;
  }

  uploadFile(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      try {
        var isValid = this.commonService.validateFile(fileList[0], 'resume', 2097152, 69, ['doc', 'docx', 'rtf', 'pdf', 'txt']);
        if (isValid) {
          this.file = fileList[0];
          (<HTMLInputElement>document.getElementById('fileNamePreview')).value = this.file.name;
          this.uploaded = true;
        }
      } catch (e) {
        this.confirmationService.confirm({
          message: e.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false
        });
      }
    }
  }
  totalExpirenece(totExp: any, relExp: any) {
    var self = this;
    if (this.commonService.validateDecimalPlacesForExp(totExp) == false) {
      this.ProfessionalDataFrmProspectData.totalExperienceInYears = undefined;
      self.confirmationService.confirm({
        message: 'The value for Total Experience should be between 0 and 100',
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
      return;
    }
    if (relExp && relExp > totExp) {
      this.ProfessionalDataFrmProspectData.totalExperienceInYears = undefined;
      self.confirmationService.confirm({
        message: 'Total Experience should be greater than or equal to Relevant Experience',
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
    }
  }
  releventExpirence(totExp: any, relExp: any) {
    var self = this;
    if (this.commonService.validateDecimalPlacesForExp(relExp) == false) {
      this.ProfessionalDataFrmProspectData.relevantExperienceInYears = undefined;
      self.confirmationService.confirm({
        message: 'The value for Relevant Experience should be between 0 and 100',
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
      return;
    }
    if ((totExp || totExp == 0) && relExp > totExp) {
      this.ProfessionalDataFrmProspectData.relevantExperienceInYears = undefined;
      self.confirmationService.confirm({
        message: 'Relevant Experience should be less than or equal to Total Experience',
        header: 'Error',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
      return;
    }

    // this.calcCategory(relExp);

  }

  calcCategory(relExp: any) {
    this.ProfessionalDataFrmProspectData.category = relExp <= 10 ? 'A' : relExp > 10 && relExp <= 20 ? 'B' : 'C';
  }

  deleteDocument() {
    var self = this;
    if (self.resumeDetails && self.resumeDetails['documentId']) {
      var resumeDetails: any = self.resumeDetails;
      this.AddProfileDataService.deleteResumeData(resumeDetails['prospectId'], resumeDetails['documentId']).subscribe(data => {
        if (data.status == 200) {
          const ind = this.AddProfileDataService.ProspectData.documents.findIndex((f: any) => f['documentType']['code'] == 'RESUME');
          this.AddProfileDataService.ProspectData.documents.splice(ind, 1);
          this.AddProfileDataService.fileData = null;
          self.uploaded = false;
          self.file = null;
          self.resumeDetails['title'] = "";
          self.resumeDetails['documentId'] = "";
          self.successMessage = 'Resume deleted successfully';
          self.confirmationService.confirm({
            message: 'File removed successfully',
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false
          });
        }
      
      });
    } else {
      (<HTMLInputElement>document.getElementById('fileNamePreview')).value = "";
      self.uploaded = false;
      self.file = null;
    }
  }
  downloadFile(resumeDetails: any) {
    this.AddProfileDataService.downloadResume(resumeDetails['prospectId'], resumeDetails['documentId']).subscribe(data => {
      var blob = new Blob([data['_body']], { type: "application/octet-stream" });
      // FileSaver.saveAs(blob, resumeDetails['title']);
    
    });
  }

  navigateToProfiles() {
    this.router.navigate(['/hiring/recruiter/123/all']);
  }
  navigateToPrevious() {
    var route = localStorage.getItem('previousRoute');
    route = (route) ? route : '/myspacenx/profiles/all';
    this.router.navigate([route]);
  }

  saveNext(prospectprofessionaldata: any, formValid:boolean = false): void {
    this.formSubmitted = true;
    if (!formValid || !this.uploaded) {
      // this.confirmationService.confirm({
      //   message: 'Please select all mandatory fields',
      //   header: 'Warning',
      //   icon: 'fa fa-exclamation-circle',
      //   rejectVisible: false
      // });
      this.alertSvc.showToast('Please select all mandatory fields', 'Warning');
      return;
    }
    var reffDetails: any = prospectprofessionaldata.referralDetails;
    var vendordetails: any = prospectprofessionaldata.vendor;
    if (this.counterOfferDetails && this.counterOfferDetails.length > 0) {
      prospectprofessionaldata.counterOfferDetails = this.counterOfferDetails
    }
    if (this.isReferral && reffDetails.length == 0) {
      this.confirmationService.confirm({
        message: "Please enter the referral",
        header: 'Warning',
        icon: 'fa fa-warning',
        rejectVisible: false
      });
    } else if (prospectprofessionaldata.source == "VENDOR" && this.isVendor && vendordetails == undefined || vendordetails == "") {
      this.confirmationService.confirm({
        message: "Please select Vendor",
        header: 'Warning',
        icon: 'fa fa-warning',
        rejectVisible: false
      })
    }
    else {
      if (reffDetails && reffDetails[0] && !reffDetails[0].referralId) {
        delete prospectprofessionaldata.referralDetails;
      }
      this.AddProfileDataService.updateProspectProfessionalData(prospectprofessionaldata, this.file);
      this.router.navigate(['../skillset'], { relativeTo: this.route });
      this.formSaved = true;
      this.AddProfileDataService.professionalFormSaved = true;
      this.AddProfileDataService.personalFormSaved = true;
    }
  }
  // making  empty the refferal name and mail id after removing from the serach
  makingEmptyRefferalDetails() {
    this.ProfessionalDataFrmProspectData.referralDetails[0].referralEmail = '';
    this.ProfessionalDataFrmProspectData.referralDetails[0].referralPhone = '';
  }

  // adding for counter offer display
  convertToWordsC(value: number, index: number) {
    if (value) {
      this.counterOfferCtcWords = this.commonService.convertNumberToWords(value) + " Rupees";
    }
    else {
      this.counterOfferCtcWords = '';
    }
    if (this.counterOfferDetails.counterOfferCtcDetails && this.counterOfferDetails.counterOfferCtcDetails.length > 0) {
      this.counterOfferDetails.counterOfferCtcDetails[index].counterOfferCtcWords = this.counterOfferCtcWords
    }
  }
  counterOfferDisplay() {
    if (!this.ProfessionalDataFrmProspectData["hasCounterOfferCtc"]) {
      this.ProfessionalDataFrmProspectData.counterOfferDetails = [];
    } else {
      this.ProfessionalDataFrmProspectData.counterOfferDetails = this.counterOfferDetails;
    }
  }
  // adding multiple counter offer along with ctc and employer name
  addNewCounterDetails() {
    if (this.counterOfferDetails.counterOfferCtcDetails.length < 5) {
      this.counterOfferDetails.counterOfferCtcDetails.push({
        "counterOfferCtcNumber": this.counterOfferDetails.counterOfferCtcDetails.length + 1,
      });
      this.adjustCounterOfferDetails(this.counterOfferDetails.counterOfferCtcDetails);
    } else {
      this.confirmationService.confirm({
        message: 'You can not add more data ',
        header: 'Confirmation',
        icon: 'fa fa-check-circle',
        rejectVisible: false,
        accept: () => {
        }, reject: () => {
          //self.navigateToDocuments();
        }
      });
    }
  }
  // removing counter Offer component dynamically
  removeCounterOfferDetails(counterOfferDetail: any) {
    var index = this.counterOfferDetails.counterOfferCtcDetails.indexOf(counterOfferDetail), self = this;
    if (index !== -1 && this.counterOfferDetails.counterOfferCtcDetails.length > 1) {
      //   key: 'interview-panel-confirm',
      self.confirmationService.confirm({
        message: 'Are you sure , you want to remove the counter offer details?',
        header: 'Confirmation',
        icon: 'fa fa-trash',
        accept: () => {
          self.counterOfferDetails.counterOfferCtcDetails.splice(index, 1);
          self.adjustCounterOfferDetails(self.counterOfferDetails.counterOfferCtcDetails);
        }, reject: () => {
        }
      });
    }
  }
  adjustCounterOfferDetails(counterOfferDetailList: any) {
    counterOfferDetailList.forEach(function (element: any, index: any) {
      counterOfferDetailList[index]['counterOfferCtcNumber'] = index + 1;
    });
  }
}