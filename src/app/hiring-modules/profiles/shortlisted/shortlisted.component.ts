import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AddProfileDataService } from '../../shared-hiring/services/add-profile-data.service';
import { ProfilesDataService } from '../../shared-hiring/services/profiles-data.service';


@Component({
  selector: 'app-shortlisted',
  templateUrl: './shortlisted.component.html',
  styleUrls: ['./shortlisted.component.scss']
})
export class ShortlistedComponent implements OnInit {

  displayedColumns = ['id', 'name', 'resume','purpose', 'cwl', 'ce', 'np', 'st'];
  public noticePeriodOptions: any[];
  public noticePeriod: string;
  public status: string;
  public successMessage: string;
  public buttonModes: any = {};
  columnDefs: any;
  allOfTheData: any;
  disableShedule: boolean = true;
  public hiringRequestOptions: any;
  public hiringNeedOptions: any;
  hiringRequestId: any;
  hiringRequestNeedId: any;
  totalRecords: number = 0;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  rejectTitle: string = 'Reject';
  rejectId: string = "RejectProfile";
  isLoaded: boolean = false;
  searchName: string = '';

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  commonService: any;
  hiringRequestDataService: any;

  constructor( private router: Router, 
    private route: ActivatedRoute, 
    private addProfileDataService: AddProfileDataService, 
    private profileDataService: ProfilesDataService, 
    private alertSvc: AlertService) {
    
    this.noticePeriodOptions = [];
    this.hiringRequestId = '';
    this.hiringRequestNeedId = '';
    this.noticePeriod = "ALL";
    this.status = "SHORTLISTED";
  }
  dateComparator(date1: any, date2: any) {
    var monthToComparableNumber = function (date: any) {
      if (date === undefined || date === null || date.length !== 10) {
        return null;
      }
      var yearNumber = date.substring(6, 10);
      var monthNumber = date.substring(3, 5);
      var dayNumber = date.substring(0, 2);

      var result = (yearNumber * 10000) + (monthNumber * 100) + dayNumber;
      return result;
    };
    var date1Number = monthToComparableNumber(date1);
    var date2Number = monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  createNewDatasource() {
    if (!this.allOfTheData) {
      return;
    }
  }
  
  onGridInitialize() {
    var data = this.addProfileDataService.getProspectCombos();
    if (data) {
      this.noticePeriodOptions = data['NOTICE_PERIOD'];
    }
    this.showProfiles();
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
    this.createNewDatasource();
  }
  setModes() {
    var selectedRecord = this.selection.selected, hiringId, needId, isOwner;
    if(selectedRecord.length > 0){
      hiringId = selectedRecord[0]['hiringRequestId'];
      needId = selectedRecord[0]['hiringRequestNeedId'];
      isOwner = selectedRecord[0]['isOwnerOfHiringRequest'];
      this.buttonModes['reject'] = (isOwner == true) ? false : true;
      this.buttonModes['open'] = false;
      this.buttonModes['timeline'] = false;
      if(hiringId && needId){
        this.disableShedule = false;
      }else{
        this.disableShedule = true;
      }
    }else{
      this.disableShedule = true;
      this.resetModes();
    }
  }
  loadScheduleInterview() {
    var selectedRecord = this.selection.selected;
    if (selectedRecord.length > 0) {
      var prospectName = selectedRecord[0]['displayName'],
        id = selectedRecord[0]['id'],
        hiringId = selectedRecord[0]['hiringRequestId'],
        needId = selectedRecord[0]['hiringRequestNeedId'];
      this.profileDataService.prospectDetails = {
        'prospectName': prospectName,
        'hiringId': hiringId,
        'hiringNeedId': needId
      };
      this.router.navigate(['../schedule', id, hiringId, needId], { relativeTo: this.route });
    }
  }
  ngOnInit() {
    var self = this;
    this.addProfileDataService.getAllHiringRequests(this.pageNum, 1000).subscribe(data => {
      if (data['content']) {
        var activeRequests: any = [];
        data['content'].forEach(function (elem: any, ind: any) {
          if (elem.status == 'ACTIVE') {
            activeRequests.push(elem);
          }
        });
        self.hiringRequestOptions = activeRequests;
      }
    });
  }
  resetModes(){
    this.buttonModes = {
      'reject': true,
      'open': true,
      'timeline': true
    };
  }
  loadNeedsByHiringId() {
    var hiringId = this.hiringRequestId, self = this;
    self.hiringRequestNeedId = "";
    if (hiringId) {
      self.addProfileDataService.getNeedsByHiringId(hiringId).subscribe(data => {
        self.hiringNeedOptions = data;
      });
    } else if (hiringId == '') {
      self.hiringNeedOptions = [];
    }
    self.pageNum = 0;
    self.showProfiles();
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.showProfiles();
  }
  filterChanged(){
    this.pageNum = 0;
    this.showProfiles();
  }
  showProfiles() {
    var self = this;
    self.disableShedule = true;
    self.resetModes();
    
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.getAllProfilesByHiringIdAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        self.setRowData(data['content']);
        self.totalPages = data.totalPages;
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      });
    // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
    //   this.addProfileDataService.getAllProfilesByHiringIdAndNeedAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
    //     self.setRowData(data['content']);
    //     self.totalPages = data.totalPages;
    //     self.totalRecords = data.totalElements;
    //     self.loaderService.hideLoading('overlay', '#overlay');
    //     self.isLoaded = self.loaderService.isLoaded;
    //   }, error => {
    //     self.loaderService.hideLoading('overlay', '#overlay');
    //     self.isLoaded = self.loaderService.isLoaded;
    //     self.confirmationService.confirm({
    //       message: error.message,
    //       header: 'Error',
    //       icon: 'fa fa-exclamation-circle',
    //       rejectVisible: false,
    //       accept : ()=>{
            
    //       }
    //     });
    //   });
    } else {
      this.profileDataService.getAllProfilesByFilters(this.pageNum, this.pageSize, this.noticePeriod, this.status, this.searchName).subscribe(data => {
       self.setRowData(data['content']);
       self.totalPages = data.totalPages;
       this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      });
    }
  }
  exportData() { 
    var self = this;
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.exportAllProfilesByHiringIdAndFilters(this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        if(data){
        data = data['_body'];
        self.commonService.downloadCsv('Shortlisted Profiles', data);
      }
      });
    // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
    //   this.addProfileDataService.exportAllProfilesByHiringIdAndNeedAndFilters(this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
    //     data = data['_body'];
    //     self.commonService.downloadCsv('Shortlisted Profiles', data);
    //   }, error => {
    //     self.confirmationService.confirm({
    //       message: error.message,
    //       header: 'Error',
    //       icon: 'fa fa-exclamation-circle',
    //       rejectVisible: false,
    //       accept : ()=>{
            
    //       }
    //     });
    //   });
    } else {
      this.profileDataService.exportAllProfilesByFilters(this.noticePeriod, this.status, this.searchName).subscribe(data => {
       data = data['_body'];
        self.commonService.downloadCsv('Shortlisted Profiles', data);
      });
    }
  }
  resetFields() {
    if (this.hiringRequestOptions.length > 0) {
      this.hiringRequestId = this.hiringRequestOptions[0]['id'];
      this.hiringRequestNeedId = 0;
      this.setModes();
    }
  }
  showProfileDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../profile-details', id], { relativeTo: this.route });
    }
  }
  showProspectTimeline() {
    var selectedRecords = this.selection.selected, id;
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/profiles/prospect-timeline', id]);
    }
  }
  rejectProfile(reason: any) {
    var selectedRecords = this.selection.selected, id, hiringId, needId, self = this, payload = {};
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      hiringId = selectedRecords[0]['hiringRequestId'];
      needId = selectedRecords[0]['hiringRequestNeedId'];
      payload = {
        'hiringRequestId': hiringId,
        'hiringRequestNeedId': needId,
        'decisionComments': reason
      };
      this.hiringRequestDataService.updateProfileStatus(id, 'REJECT', payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Profile rejected successfully';
        }
      });
    }
  }

  SearchByName() {
    if (this.searchName.length > 2)
      this.filterChanged();
    else if (this.searchName == "")
      this.setPageNum(0);
  }
}