import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CommonService } from '../../../shared/services/common.service';
import { ProfilesDataService } from '../../shared-hiring/services/profiles-data.service';
import { AddProfileDataService } from '../../shared-hiring/services/add-profile-data.service';
import { OffersDataService } from '../../shared-hiring/services/offers.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { HiringCommonService } from '../../shared-hiring/services/hiring-common.service';

@Component({
  selector: 'app-all-profiles',
  templateUrl: './all-profiles.component.html',
  styleUrls: ['./all-profiles.component.scss']
})
export class AllProfilesComponent implements OnInit {
  public noticePeriodOptions: any[];
  public statusOptions: any[];
  public noticePeriod: string;
  public status: string;
  public buttonModes: any = {};
  columnDefs: any;
  allOfTheData: any;
  totalRecords: number = 0;
  recordNotSelected: boolean = false;
  public hiringRequestOptions: any;
  public hiringNeedOptions: any;
  hiringRequestId: any;
  hiringRequestNeedId: any;
  successMessage: string;
  pageNum: number = 0;
  totalPages: number;
  isLoaded: boolean = false;
  searchName: string = '';

  displayedColumns = ['id', 'name', 'resume', 'purpose', 'wl', 'ce', 'np', 'status', 'exp'];
  length: number; pageSize: number = 10; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  constructor(private router: Router,
    private route: ActivatedRoute,
    private profileDataService: ProfilesDataService,
    private addProfileDataService: AddProfileDataService,
    private offersDataService: OffersDataService,
    private commonService: HiringCommonService,
    private alertSvc: AlertService) {

    this.noticePeriodOptions = [];
    this.statusOptions = [];
    this.hiringRequestId = '';
    this.hiringRequestNeedId = '';
    this.noticePeriod = "ALL";
    this.status = "ALL";
  }

  ngOnInit() {
    var self = this;
    self.resetModes();
    this.addProfileDataService.getAllHiringRequests(this.pageNum, 1000).subscribe(data => {
      if (data['content']) {
        var activeRequests: any = [];
        data['content'].forEach(function (elem: any, ind: any) {
          if (elem.status == 'ACTIVE') {
            activeRequests.push(elem);
          }
        });
        self.hiringRequestOptions = activeRequests;
      }
    });
    this.showProfiles();
  }

  onReinit() {
    this.pageNum = 0;
    this.lstGrid = new MatTableDataSource([]);
  }

  onSubmit() {
    this.onReinit();
    this.showProfiles();
  }

  concatenateNames(params: any) {
    return params.data.firstName + " " + params.data.lastName;
  }

  onRowSelection(item) {
    if (this.selection.selected.length > 0) {
      if (this.selection.isSelected(item))
        this.selection.toggle(item);
      else {
        this.selection = new SelectionModel<any>(true, []);
        this.selection.toggle(item);
      }
    }
    else
      this.selection.toggle(item);

    this.checkModes();
  }

  checkModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      this.buttonModes['timeline'] = false;
      this.buttonModes['viewProfile'] = false;
      var status = selectedRecords[0]['statusCode'],
        offerExists: string = selectedRecords[0]['offerId'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'],
        isOwner = selectedRecords[0]['isOwnerOfHiringRequest'],
        joiningDate = selectedRecords[0]['expectedJoiningDate'],
        currentDate = new Date().getTime(),
        joiningEpochTime = this.getDate(new Date(joiningDate)).getTime(),
        currentEpochTime = this.getDate(new Date(currentDate)).getTime();
      if (status === 'ACTIVE' || status === 'ATTACHED_TO_THE_NEED' || status === 'SHORTLISTED' || status === 'REJECTED' || status === 'DELETED' || status === 'INTERVIEW_SCHEDULED' || status === 'INACTIVE') {
        this.buttonModes['viewFeedback'] = true;
      } else {
        this.buttonModes['viewFeedback'] = false;
      }
      // this.buttonModes['viewDetails'] = (status == 'VERIFICATION_IN_PROGRESS') ? false : true;
      // making disable for all prospect status 
      this.buttonModes['viewDetails'] = (status === 'VERIFICATION_IN_PROGRESS' || status === 'VERIFICATION_COMPLETED' || status === 'VERIFICATION_FAILED' || status === 'OFFER_ROLLED_OUT' || status === 'OFFER_ACCEPTED' || status === 'OFFER_DECLINED' || status === 'JOINED' || status === 'NO_SHOW' || status === 'INACTIVE' || status === 'DELETED') ? false : true;
      this.buttonModes['notify'] = (status === 'SELECTED') ? false : true;
      this.buttonModes['join'] = ((status === 'OFFER_ACCEPTED') && (currentEpochTime <= joiningEpochTime)) ? false : true;
      this.buttonModes['noShow'] = ((status === 'OFFER_ACCEPTED') && (currentEpochTime > joiningEpochTime)) ? false : true;
      this.buttonModes['reject'] = ((isOwner === true) && (status === 'ATTACHED_TO_THE_NEED' || status === 'SHORTLISTED')) ? false : true;
      this.buttonModes['shortlist'] = ((isOwner === true) && (status === 'ATTACHED_TO_THE_NEED' || status === 'REJECTED')) ? false : true;
      this.buttonModes['startVerification'] = (status === 'COMPLETED_PREJOINING_FORMALITIES') ? false : true;
      this.buttonModes['createOffer'] = ((status === 'VERIFICATION_COMPLETED') && (!offerExists)) ? false : true;
      this.buttonModes['schedule'] = (hiringId && needId && (status === 'SHORTLISTED')) ? false : true;
    } else {
      this.resetModes();
    }
  }

  getDate(date: any) {
    var mm: any = date.getMonth() + 1;
    var dd: any = date.getDate();
    var dateString: any = [date.getFullYear(), (mm > 9 ? '' : '0') + mm, (dd > 9 ? '' : '0') + dd].join('-');
    return new Date(dateString);
  }

  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.showProfiles();
  }

  loadProfile() {
    this.addProfileDataService.mode = 'new';
    this.router.navigate(['../details'], { relativeTo: this.route });
  }

  updateProfile() {
    this.addProfileDataService.mode = 'edit';
    this.loadProfileDetails();
  }

  viewProfile() {
    this.addProfileDataService.mode = 'view';
    this.loadProfileDetails('view');
  }

  viewFeedBack() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'],
        reqId = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'];
      this.router.navigate(['../myspacenx/interviews/view-feedback', id, reqId, needId]);
    }
  }

  loadProfileDetails(mode?: string) {
    var selectedRecords = this.selection.selected;
    if (selectedRecords && selectedRecords.length > 0) {
      this.recordNotSelected = false;
      var id = selectedRecords[0]['id'];
      if (mode === 'view') {
        this.router.navigate(['../' + mode, id], { relativeTo: this.route });
      } else {
        this.router.navigate(['../details', id], { relativeTo: this.route });
      }
    } else {
      this.recordNotSelected = true;
    }
  }

  loadNeedsByHiringId() {
    var hiringId = this.hiringRequestId, self = this;
    self.hiringRequestNeedId = "";
    if (hiringId) {
      self.addProfileDataService.getNeedsByHiringId(hiringId).subscribe(data => {
        if (data)
          self.hiringNeedOptions = data;
      });
    } else if (hiringId == '') {
      self.hiringNeedOptions = [];
    }
    self.pageNum = 0;
    self.showProfiles();
  }

  attachProfile() {
    var hiringId = this.hiringRequestId, needId = this.hiringRequestNeedId, self = this,
      selectedRecords = this.selection.selected, prospectId;
    if (selectedRecords.length > 0) {
      prospectId = selectedRecords[0]['id'];
      self.profileDataService.attachProfiletoNeed(hiringId, needId, prospectId).subscribe(
        data => {
          if (data.status == 200) {
            self.successMessage = 'Profile attached successfully';
            this.alertSvc.showToast(self.successMessage, 'success');
            self.showProfiles();
          }
        });
    }
  }

  filterChanged() {
    this.pageNum = 0;
    this.showProfiles();
  }

  showProfiles() {
    this.lstGrid = new MatTableDataSource(
      [
        {
          "id": "42ede08c-3c17-49b0-9a21-df8ac773ce04",
          "uid": "PC18011",
          "hiringRequestId": "ce1a3bb9-dc8a-4f33-855c-a0cb17a601b5",
          "hiringRequestNeedId": "5b00c2ab-78cc-4de7-b6b4-1150da52b6cd",
          "offerId": null,
          "displayName": "Ramesh Babu A",
          "currentWorkLocation": "",
          "currentEmployeer": "Value Labs",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Attached to the Need",
          "statusCode": "ATTACHED_TO_THE_NEED",
          "totalExperienceInYears": 8.0,
          "relevantExperienceInYears": 8.0,
          "expectedJoiningDate": null,
          "email": "akularameshbabu@yahoo.com",
          "mobileNumber": "9985199494",
          "hiringPurpose": "Fulfill the .Net staffing demand",
          "hiringTypeDescription": "Regular",
          "clientName": "Innominds SEZ (Waverock 2.3)",
          "isOwnerOfHiringRequest": false,
          "resumeId": "d24476a3-e665-389e-872c-a7bb9ccf0274",
          "resumeName": "H0368_Profile_12204.docx",
          "category": null,
          "needAttached": false
        }, {
          "id": "56c600f5-46b6-4f19-86bc-900786d7f1b1",
          "uid": "PC18257",
          "hiringRequestId": "ce1a3bb9-dc8a-4f33-855c-a0cb17a601b5",
          "hiringRequestNeedId": "5b00c2ab-78cc-4de7-b6b4-1150da52b6cd",
          "offerId": null,
          "displayName": "Anil Kumar Mootha",
          "currentWorkLocation": "hyd",
          "currentEmployeer": "UST ",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Interview In Progress",
          "statusCode": "INTERVIEW_IN_PROGRESS",
          "totalExperienceInYears": 13.0,
          "relevantExperienceInYears": 13.0,
          "expectedJoiningDate": null,
          "email": "aniljain50@gmail.com",
          "mobileNumber": "9000421467",
          "hiringPurpose": "Fulfill the .Net staffing demand",
          "hiringTypeDescription": "Regular",
          "clientName": "Innominds SEZ (Waverock 2.3)",
          "isOwnerOfHiringRequest": false,
          "resumeId": "94f7fe0e-00ea-36cc-b73a-8e0cdc460e8d",
          "resumeName": "H0433_Profile_8682.doc",
          "category": null,
          "needAttached": false
        },
        {
          "id": "b29e12fe-98f6-4a88-bfdc-ded742184959",
          "uid": "PC1054",
          "hiringRequestId": "cbc0570e-eeff-4f95-afae-baac80cd8388",
          "hiringRequestNeedId": "dc4a20ab-0574-4546-9616-5eab5c7d732e",
          "offerId": null,
          "displayName": "Arabinda Arabinda",
          "currentWorkLocation": "",
          "currentEmployeer": "Qvantel Software Solution Limited",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Pending Pre-Offer Formalities",
          "statusCode": "PENDING_PREJOINING_FORMALITIES",
          "totalExperienceInYears": 15.0,
          "relevantExperienceInYears": 15.0,
          "expectedJoiningDate": null,
          "email": "arabinda.choudhury2020@gmail.com",
          "mobileNumber": "9593927905",
          "hiringPurpose": "Proactive hiring of Sr Engineers for Hilti",
          "hiringTypeDescription": "Regular",
          "clientName": "Hilti (Pune)                            ",
          "isOwnerOfHiringRequest": false,
          "resumeId": "3a8a77f5-9388-3d2c-993c-a3ab3a86afa0",
          "resumeName": "H0672_Profile_16557.doc",
          "category": null,
          "needAttached": false
        }]
    );
    
    return;
    var self = this;
    self.resetModes();
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.getAllProfilesByHiringIdAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        // self.setRowData(data['content']);
        // self.totalPages = data.totalPages;
        // self.totalRecords = data.totalElements;
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      }
      );
      // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
      //   var self = this;
      //   this.addProfileDataService.getAllProfilesByHiringIdAndNeedAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
      //     self.setRowData(data['content']);
      //     self.totalPages = data.totalPages;
      //     self.totalRecords = data.totalElements;
      //     self.loaderService.hideLoading('overlay', '#overlay');
      //     self.isLoaded = self.loaderService.isLoaded;
      //   }, error => {
      //     self.loaderService.hideLoading('overlay', '#overlay');
      //     self.isLoaded = self.loaderService.isLoaded;
      //     self.confirmationService.confirm({
      //       message: error.message,
      //       header: 'Error',
      //       icon: 'fa fa-exclamation-circle',
      //       rejectVisible: false
      //     });
      //   });
    } else {
      var self = this;
      this.profileDataService.getAllProfilesByFilters(this.pageNum, this.pageSize, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        // self.setRowData(data['content']);
        // self.totalPages = data.totalPages;
        // self.totalRecords = data.totalElements;
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      });
    }
  }

  resetFields() {
    if (this.hiringRequestOptions.length > 0) {
      this.hiringRequestId = this.hiringRequestOptions[0]['id'];
      this.hiringRequestNeedId = "";
    }
  }

  resetModes() {
    this.buttonModes = {
      'schedule': true,
      'createOffer': true,
      'startVerification': true,
      'shortlist': true,
      'reject': true,
      'join': true,
      'noShow': true,
      'notify': true,
      'viewDetails': true,
      'viewProfile': true,
      'timeline': true,
      'viewFeedback': true
    };
  }

  exportData() {
    var self = this;
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.exportAllProfilesByHiringIdAndFilters(this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        if (data) {
          data = data['_body'];
          self.commonService.downloadCsv('All Profiles', data);
        }
      }
      );
      // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
      //   this.addProfileDataService.exportAllProfilesByHiringIdAndNeedAndFilters(this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
      //     data = data['_body'];
      //     self.commonService.downloadCsv('All Profiles', data);
      //   }, error => {
      //     self.confirmationService.confirm({
      //       message: error.message,
      //       header: 'Error',
      //       icon: 'fa fa-exclamation-circle',
      //       rejectVisible: false
      //     });
      //   });
    } else {
      this.profileDataService.exportAllProfilesByFilters(this.noticePeriod, this.status, this.searchName).subscribe(data => {
        if (data) {
          data = data['_body'];
          self.commonService.downloadCsv('All Profiles', data);
        }
      }
      );
    }
  }

  showProspectTimeline() {
    var selectedRecords = this.selection.selected, id;
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/profiles/prospect-timeline', id]);
    }
  }

  showVerificationDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var payload = {
        'hiringRequestId': selectedRecords[0]['hiringRequestId'],
        'hiringRequestNeedId': selectedRecords[0]['hiringRequestNeedId']
      };
      localStorage.setItem('verificationDetails', JSON.stringify(payload));
      this.recordNotSelected = false;
      var id = selectedRecords[0]['id'];
      if (status === 'VERIFICATION_IN_PROGRESS') {
        this.router.navigate(['../myspacenx/profiles/verification', id]);
      } else {
        this.router.navigate(['../myspacenx/profiles/verification-details', id]);
      }
    } else {
      this.recordNotSelected = true;
    }
  }

  notifyCandidate() {
    var self = this, selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var prospectId = selectedRecords[0]['id'];
      var payload = {
        'hiringRequestId': selectedRecords[0]['hiringRequestId'],
        'hiringRequestNeedId': selectedRecords[0]['hiringRequestNeedId']
      };
      this.profileDataService.notifyCandidate(prospectId, payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Notification sent successfully';
          this.alertSvc.showToast(self.successMessage, 'success');
          self.showProfiles();
        }
      }
      );
    }
  }

  updateProspectStatus(opCode: string) {
    var self = this, id,
      selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      var payload = {
        'hiringRequestId': selectedRecords[0]['hiringRequestId'],
        'hiringRequestNeedId': selectedRecords[0]['hiringRequestNeedId'],
        'offerId': selectedRecords[0]['offerId']
      };
      this.profileDataService.updateProspectStatus(id, opCode, payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = (opCode == 'JOIN') ? 'Prospect marked as Joined' : 'Prospect marked as No Show';
          this.alertSvc.showToast(self.successMessage, 'success');
          self.showProfiles();
        }
      }
      );
    }
  }

  rejectProfile(reason: any) {
    var selectedRecords = this.selection.selected, id, hiringId, needId, self = this, payload = {};
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      hiringId = selectedRecords[0]['hiringRequestId'];
      needId = selectedRecords[0]['hiringRequestNeedId'];
      payload = {
        'hiringRequestId': hiringId,
        'hiringRequestNeedId': needId,
        'decisionComments': reason
      };
      if (!this.commonService.validateCommentsLength(reason)) {
        this.alertSvc.showToast('Remarks entered exceeds the maximum length of 1024 characters', 'warning');
        return;
      }
      this.profileDataService.updateProspectStatus(id, 'REJECT', payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Profile rejected successfully';
          this.alertSvc.showToast(self.successMessage, 'success');
          self.showProfiles();
        }
      });
    }
  }

  shortListProfile(reason: any) {
    var selectedRecords = this.selection.selected, id, hiringId, needId, self = this, payload = {};
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      hiringId = selectedRecords[0]['hiringRequestId'];
      needId = selectedRecords[0]['hiringRequestNeedId'];
      payload = {
        'hiringRequestId': hiringId,
        'hiringRequestNeedId': needId,
        'decisionComments': reason
      };
      if (!this.commonService.validateCommentsLength(reason)) {
        this.alertSvc.showToast('Remarks entered exceeds the maximum length of 1024 characters', 'warning');
        return;
      }
      this.profileDataService.updateProspectStatus(id, 'SHORTLIST', payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Profile shortlisted successfully';
          this.alertSvc.showToast(self.successMessage, 'success');
        }
      });
    }
  }

  startVerification() {
    var self = this, selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id: string = selectedRecords[0]['id'];
      var payload = {
        'hiringRequestId': selectedRecords[0]['hiringRequestId'],
        'hiringRequestNeedId': selectedRecords[0]['hiringRequestNeedId']
      };
      this.profileDataService.updateProspectStatus(id, 'START_VERIFICATION', payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Verification started successfully';
          this.alertSvc.showToast(self.successMessage, 'success');
          self.showProfiles();
        }
      });
    }
  }

  navigateToOffers() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var prospectName = selectedRecords[0]['displayName'],
        relevantExp = selectedRecords[0]['relevantExperienceInYears'],
        totalExp = selectedRecords[0]['totalExperienceInYears'],
        id = selectedRecords[0]['id'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        category = selectedRecords[0]['category'],
        hiringNeedId = selectedRecords[0]['hiringRequestNeedId'];
      this.offersDataService.prospectDetails = {
        'prospectName': prospectName,
        'prospectId': id,
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId,
        'relevantExp': relevantExp,
        'totalExp': totalExp,
        'category': category
      };
      this.router.navigate(['../myspacenx/offers/create-offer/', hiringId, hiringNeedId, id]);
    }
  }

  loadScheduleInterview() {
    var selectedRecord = this.selection.selected;
    if (selectedRecord.length > 0) {
      var prospectName = selectedRecord[0]['displayName'],
        id = selectedRecord[0]['id'],
        hiringId = selectedRecord[0]['hiringRequestId'],
        needId = selectedRecord[0]['hiringRequestNeedId'];
      this.profileDataService.prospectDetails = {
        'prospectName': prospectName,
        'hiringId': hiringId,
        'hiringNeedId': needId
      };
      this.router.navigate(['../schedule', id, hiringId, needId], { relativeTo: this.route });
    }
  }

  SearchByName() {
    if (this.searchName.length > 2)
      this.filterChanged();
    else if (this.searchName == "")
      this.setPageNum(0);
  }
}