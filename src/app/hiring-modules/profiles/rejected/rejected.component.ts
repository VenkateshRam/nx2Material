import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AddProfileDataService } from '../../shared-hiring/services/add-profile-data.service';
import { ProfilesDataService } from '../../shared-hiring/services/profiles-data.service';



@Component({
  selector: 'app-rejected',
  templateUrl: './rejected.component.html',
  styleUrls: ['./rejected.component.scss']
})
export class RejectedComponent implements OnInit {

  displayedColumns = ['id', 'name', 'resume', 'cwl', 'ce','wl', 'np', 'st'];
  public noticePeriodOptions: any[];
  public noticePeriod: string;
  public status: string;
  public hiringRequestOptions: any;
  public hiringNeedOptions: any;
  public buttonModes: any = {};
  hiringRequestId: any;
  hiringRequestNeedId: any;
  public successMessage: string;
  columnDefs: any;
  gridOptions: GainOptions;
  allOfTheData: any;
  totalRecords: number = 0;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  shortListTitle: string = 'Shortlist';
  shortListId: string = "shortListProfile";
  searchName: string = '';
  isLoaded: boolean = false;

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  commonService: any;
  hiringRequestDataService: any;
 

  constructor(private router: Router, 
    private route: ActivatedRoute, 
    private addProfileDataService: AddProfileDataService, 
    private profileDataService: ProfilesDataService, 
    private alertSvc: AlertService) {
    
    this.noticePeriodOptions = [];
    this.hiringRequestId = '';
    this.hiringRequestNeedId = '';
    this.noticePeriod = "ALL";
    this.status = "REJECTED";
  }
  dateComparator(date1: any, date2: any) {
    var monthToComparableNumber = function (date: any) {
      if (date === undefined || date === null || date.length !== 10) {
        return null;
      }
      var yearNumber = date.substring(6, 10);
      var monthNumber = date.substring(3, 5);
      var dayNumber = date.substring(0, 2);

      var result = (yearNumber * 10000) + (monthNumber * 100) + dayNumber;
      return result;
    };
    var date1Number = monthToComparableNumber(date1);
    var date2Number = monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  createNewDatasource() {
    if (!this.allOfTheData) {
      return;
    }
    this.selection.selected;
    var allOfTheData = this.allOfTheData,
      self = this;
    var dataSource = {
      getRows: function (params: any) {
        setTimeout(function () {
          var rowsThisPage = allOfTheData.slice(params.startRow, params.endRow);
          var lastRow = -1;
          if (allOfTheData.length <= params.endRow) {
            lastRow = allOfTheData.length;
          }
          params.successCallback(rowsThisPage, lastRow);
          self.selection.selected;
          if (allOfTheData.length == 0) {
            self.selection.selected;
          }
        }, 500);
      }
    };
    
  }
  
  onGridInitialize() {
    var data = this.selection.selected;
    if (data) {
      this.noticePeriodOptions = data['NOTICE_PERIOD'];
    }
    this.showProfiles();
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
    this.createNewDatasource();
  }
  setModes() {
    var selectedRecord = this.selection.selected, isOwner;
    if (selectedRecord.length > 0) {
      isOwner = selectedRecord[0]['isOwnerOfHiringRequest'];
      this.buttonModes = {
        'shortlist': ((isOwner == true) ? false : true),
        'open': false,
        'timeline': false
      };
    } else {
      this.resetModes();
    }
  }
  /*loadScheduleInterview() {
    var selectedRecord = this.gridOptions.api.getSelectedRows();
    if (selectedRecord.length > 0) {
      var prospectName = selectedRecord[0]['displayName'],
        id = selectedRecord[0]['id'],
        hiringId = selectedRecord[0]['hiringRequestId'],
        needId = selectedRecord[0]['hiringRequestNeedId'];
      this.profileDataService.prospectDetails = {
        'prospectName': prospectName,
        'hiringId': hiringId,
        'hiringNeedId': needId
      };
      this.router.navigate(['../schedule', id], { relativeTo: this.route });
    }
  }*/
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.showProfiles();
  }
  ngOnInit() {
    var self = this;
    this.addProfileDataService.getAllHiringRequests(this.pageNum, 1000).subscribe(data => {
      if (data['content']) {
        var activeRequests: any = [];
        data['content'].forEach(function (elem: any, ind: any) {
          if (elem.status == 'ACTIVE') {
            activeRequests.push(elem);
          }
        });
        self.hiringRequestOptions = activeRequests;
      }
    });
  }
  resetModes(){
    this.buttonModes = {
      'shortlist': true,
      'open': true,
      'timeline': true
    };
  }
  loadNeedsByHiringId() {
    var hiringId = this.hiringRequestId, self = this;
    self.hiringRequestNeedId = "";
    if (hiringId) {
      self.addProfileDataService.getNeedsByHiringId(hiringId).subscribe(data => {
        self.hiringNeedOptions = data;
      });
    } else if (hiringId == '') {
      self.hiringNeedOptions = [];
    }
    self.pageNum = 0;
    self.showProfiles();
  }
  filterChanged(){
    this.pageNum = 0;
    this.showProfiles();
  }
  showProfiles() {
    this.lstGrid = new MatTableDataSource(
      [
        {
          "id": "42ede08c-3c17-49b0-9a21-df8ac773ce04",
          "uid": "PC18011",
          "hiringRequestId": "ce1a3bb9-dc8a-4f33-855c-a0cb17a601b5",
          "hiringRequestNeedId": "5b00c2ab-78cc-4de7-b6b4-1150da52b6cd",
          "offerId": null,
          "displayName": "Ramesh Babu A",
          "currentWorkLocation": "",
          "currentEmployeer": "Value Labs",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Attached to the Need",
          "statusCode": "ATTACHED_TO_THE_NEED",
          "totalExperienceInYears": 8.0,
          "relevantExperienceInYears": 8.0,
          "expectedJoiningDate": null,
          "email": "akularameshbabu@yahoo.com",
          "mobileNumber": "9985199494",
          "hiringPurpose": "Fulfill the .Net staffing demand",
          "hiringTypeDescription": "Regular",
          "clientName": "Innominds SEZ (Waverock 2.3)",
          "isOwnerOfHiringRequest": false,
          "resumeId": "d24476a3-e665-389e-872c-a7bb9ccf0274",
          "resumeName": "H0368_Profile_12204.docx",
          "category": null,
          "needAttached": false
        }, {
          "id": "56c600f5-46b6-4f19-86bc-900786d7f1b1",
          "uid": "PC18257",
          "hiringRequestId": "ce1a3bb9-dc8a-4f33-855c-a0cb17a601b5",
          "hiringRequestNeedId": "5b00c2ab-78cc-4de7-b6b4-1150da52b6cd",
          "offerId": null,
          "displayName": "Anil Kumar Mootha",
          "currentWorkLocation": "hyd",
          "currentEmployeer": "UST ",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Interview In Progress",
          "statusCode": "INTERVIEW_IN_PROGRESS",
          "totalExperienceInYears": 13.0,
          "relevantExperienceInYears": 13.0,
          "expectedJoiningDate": null,
          "email": "aniljain50@gmail.com",
          "mobileNumber": "9000421467",
          "hiringPurpose": "Fulfill the .Net staffing demand",
          "hiringTypeDescription": "Regular",
          "clientName": "Innominds SEZ (Waverock 2.3)",
          "isOwnerOfHiringRequest": false,
          "resumeId": "94f7fe0e-00ea-36cc-b73a-8e0cdc460e8d",
          "resumeName": "H0433_Profile_8682.doc",
          "category": null,
          "needAttached": false
        },
        {
          "id": "b29e12fe-98f6-4a88-bfdc-ded742184959",
          "uid": "PC1054",
          "hiringRequestId": "cbc0570e-eeff-4f95-afae-baac80cd8388",
          "hiringRequestNeedId": "dc4a20ab-0574-4546-9616-5eab5c7d732e",
          "offerId": null,
          "displayName": "Arabinda Arabinda",
          "currentWorkLocation": "",
          "currentEmployeer": "Qvantel Software Solution Limited",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Pending Pre-Offer Formalities",
          "statusCode": "PENDING_PREJOINING_FORMALITIES",
          "totalExperienceInYears": 15.0,
          "relevantExperienceInYears": 15.0,
          "expectedJoiningDate": null,
          "email": "arabinda.choudhury2020@gmail.com",
          "mobileNumber": "9593927905",
          "hiringPurpose": "Proactive hiring of Sr Engineers for Hilti",
          "hiringTypeDescription": "Regular",
          "clientName": "Hilti (Pune)                            ",
          "isOwnerOfHiringRequest": false,
          "resumeId": "3a8a77f5-9388-3d2c-993c-a3ab3a86afa0",
          "resumeName": "H0672_Profile_16557.doc",
          "category": null,
          "needAttached": false
        }]
    );
    
    return;

    var self = this;
    self.resetModes();
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.getAllProfilesByHiringIdAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      });
    // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
    //   this.addProfileDataService.getAllProfilesByHiringIdAndNeedAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
    //     self.setRowData(data['content']);
    //     self.totalPages = data.totalPages;
    //     self.totalRecords = data.totalElements;
    //     self.loaderService.hideLoading('overlay', '#overlay');
    //     self.isLoaded = self.loaderService.isLoaded;
    //   }, error => {
    //     self.loaderService.hideLoading('overlay', '#overlay');
    //     self.isLoaded = self.loaderService.isLoaded;
    //     self.confirmationService.confirm({
    //       message: error.message,
    //       header: 'Error',
    //       icon: 'fa fa-exclamation-circle',
    //       rejectVisible: false,
    //       accept : ()=>{
            
    //       }
    //     });
    //   });
    } else {
      this.profileDataService.getAllProfilesByFilters(this.pageNum, this.pageSize, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      
      });
    }
  }
  exportData() { 
    var self = this;
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.exportAllProfilesByHiringIdAndFilters(this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        if(data){
        data = data['_body'];
        self.commonService.downloadCsv('Rejected Profiles', data);
      }
      });
    // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
    //   this.addProfileDataService.exportAllProfilesByHiringIdAndNeedAndFilters(this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
    //     data = data['_body'];
    //     self.commonService.downloadCsv('Rejected Profiles', data);
    //   }, error => {
    //     self.confirmationService.confirm({
    //       message: error.message,
    //       header: 'Error',
    //       icon: 'fa fa-exclamation-circle',
    //       rejectVisible: false,
    //       accept : ()=>{
            
    //       }
    //     });
    //   });
    } else {
      this.profileDataService.exportAllProfilesByFilters(this.noticePeriod, this.status, this.searchName).subscribe(data => {
       data = data['_body'];
        self.commonService.downloadCsv('Rejected Profiles', data);
      });
    }
  }
  resetFields() {
    if (this.hiringRequestOptions.length > 0) {
      this.hiringRequestId = this.hiringRequestOptions[0]['id'];
      this.hiringRequestNeedId = 0;
      this.setModes();
    }
  }
  showProfileDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../profile-details', id], { relativeTo: this.route });
    }
  }
  showProspectTimeline() {
    var selectedRecords = this.selection.selected, id;
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/profiles/prospect-timeline', id]);
    }
  }
  shortListProfile(reason: any) {
    var selectedRecords = this.selection.selected, id, hiringId, needId, self = this, payload = {};
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      hiringId = selectedRecords[0]['hiringRequestId'];
      needId = selectedRecords[0]['hiringRequestNeedId'];
      payload = {
        'hiringRequestId': hiringId,
        'hiringRequestNeedId': needId,
        'decisionComments': reason
      };
      // if(!this.commonService.validateCommentsLength(reason)){
      //   this.confirmationService.confirm({
      //     message: 'Remarks entered exceeds the maximum length of 1024 characters',
      //     header: 'Warning',
      //     icon: 'fa fa-exclamation-circle',
      //     rejectVisible:false
      //   });
      //   return;
      // }
      
      this.hiringRequestDataService.updateProfileStatus(id, 'SHORTLIST', payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Profile shortlisted successfully';
        }
      });
    }
  }

  SearchByName() {
    if (this.searchName.length > 2)
      this.filterChanged();
    else if (this.searchName == "")
      this.setPageNum(0);
  }
}