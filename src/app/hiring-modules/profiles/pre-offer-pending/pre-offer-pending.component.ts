import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AddProfileDataService } from '../../shared-hiring/services/add-profile-data.service';
import { OffersDataService } from '../../shared-hiring/services/offers.service';
import { ProfilesDataService } from '../../shared-hiring/services/profiles-data.service';

@Component({
  selector: 'app-pre-offer-pending',
  templateUrl: './pre-offer-pending.component.html',
  styleUrls: ['./pre-offer-pending.component.scss']
})
export class PreOfferPendingComponent implements OnInit {

  displayedColumns = ['id', 'name', 'resume', 'purpose', 'wl', 'ce', 'np', 'st', 'ht', 'ce', 'total exp'];
  columnDefs: any;
  allOfTheData: any;
  isLoaded: boolean = false;
  public buttonModes: any = {};
  public noticePeriodOptions: any[];
  public noticePeriod: string;
  public status: string;
  public hiringRequestOptions: any;
  public hiringNeedOptions: any;
  hiringRequestId: any;
  hiringRequestNeedId: any;
  recordsCount: number = 0;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  totalRecords: number = 0;
  index: number = 0;
  public pageSizeNumbers = [10, 20, 50, 75, 100];
  page = 20;
  searchName: string = '';

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  commonService: any;
  
  constructor( private addProfileDataService: AddProfileDataService, 
    private profilesDataService: ProfilesDataService, 
    private offersDataService: OffersDataService,  
    private router: Router, 
    private route: ActivatedRoute, 
    private alertSvc: AlertService) {
    
    this.noticePeriodOptions = [];
    this.hiringRequestId = '';
    this.hiringRequestNeedId = '';
    this.noticePeriod = "ALL";
    this.status = "PENDING_PREJOINING_FORMALITIES";
  }
  createNewDatasource() {
    if (!this.allOfTheData) {
      return;
    }
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.showProfiles();
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
    this.createNewDatasource();
  }
  navigateToOffers() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var prospectName = selectedRecords[0]['displayName'],
        id = selectedRecords[0]['id'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        hiringNeedId = selectedRecords[0]['hiringRequestNeedId'];
      this.offersDataService.prospectDetails = {
        'prospectName': prospectName,
        'prospectId': id,
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId
      };
      this.router.navigate(['../myspacenx/offers/offerdetail']);
    }
  }
  viewFeedBack() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'],
        reqId = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'];
      this.router.navigate(['../myspacenx/interviews/view-feedback', id, reqId, needId]);
    }
  }
  checkModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      this.buttonModes['open'] = false;
      this.buttonModes['viewFeedback'] = false;
      this.buttonModes['timeline'] = false;
    } else {
      this.resetModes();
    }
  }
  ngOnInit() {
    var self = this;
    this.addProfileDataService.getAllHiringRequests(this.pageNum, 1000).subscribe(data => {
      if (data['content']) {
        var activeRequests: any = [];
        data['content'].forEach(function (elem: any, ind: any) {
          if (elem.status == 'ACTIVE') {
            activeRequests.push(elem);
          }
        });
        self.hiringRequestOptions = activeRequests;
      }
    });
  }
  loadNeedsByHiringId() {
    var hiringId = this.hiringRequestId, self = this;
    self.hiringRequestNeedId = "";
    if (hiringId) {
      self.addProfileDataService.getNeedsByHiringId(hiringId).subscribe(data => {
        self.hiringNeedOptions = data;
      });
    } else if (hiringId == '') {
      self.hiringNeedOptions = [];
    }
    self.pageNum = 0;
    self.showProfiles();
  }
  filterChanged() {
    this.pageNum = 0;
    this.showProfiles();
  }
  showProfiles() {
    var self = this;
    self.resetModes();
    
    if (this.hiringRequestId && !this.hiringRequestNeedId && this.pageSize > 0) {
      this.addProfileDataService.getAllProfilesByHiringIdAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
        if (self.index == 0) {
          self.totalRecords = data.totalElements;
          self.index++;
        }
      });
    // } else if (this.hiringRequestId && this.hiringRequestNeedId && this.pageSize > 0) {
    //   this.addProfileDataService.getAllProfilesByHiringIdAndNeedAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
    //     self.setRowData(data['content']);
    //     self.totalPages = data.totalPages;
    //     self.recordsCount = data.totalElements;
    //     self.recordsCount = data.totalElements;

    //     if (self.index == 0) {
    //       self.totalRecords = data.totalElements;
    //       self.index++;
    //     }
    //     self.loaderService.hideLoading('overlay', '#overlay');
    //     self.isLoaded = self.loaderService.isLoaded;
    //   }, error => {
    //     self.loaderService.hideLoading('overlay', '#overlay');
    //     self.isLoaded = self.loaderService.isLoaded;
    //     self.confirmationService.confirm({
    //       message: error.message,
    //       header: 'Error',
    //       icon: 'fa fa-exclamation-circle',
    //       rejectVisible: false,
    //       accept: () => {
            
    //       }
    //     });
    //   });
    } else if(this.pageSize > 0){
      this.profilesDataService.getAllProfilesByFilters(this.pageNum, this.pageSize, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
        if (self.index == 0) {
          self.totalRecords = data.totalElements;
          self.index++;
        }
      });
    }
  }
  resetFields() {
    if (this.hiringRequestOptions.length > 0) {
      this.hiringRequestId = this.hiringRequestOptions[0]['id'];
      this.hiringRequestNeedId = 0;
    }
  }
  showProfileDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../profile-details', id], { relativeTo: this.route });
    }
  }
  showProspectTimeline() {
    var selectedRecords = this.selection.selected, id;
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/profiles/prospect-timeline', id]);
    }
  }
  exportData() {
    var self = this;
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.exportAllProfilesByHiringIdAndFilters(this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        data = data['_body'];
        self.commonService.downloadCsv('Prejoining Pending', data);
      });
    // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
    //   this.addProfileDataService.exportAllProfilesByHiringIdAndNeedAndFilters(this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
    //     data = data['_body'];
    //     self.commonService.downloadCsv('Prejoining Pending', data);
    //   }, error => {
    //     self.confirmationService.confirm({
    //       message: error.message,
    //       header: 'Error',
    //       icon: 'fa fa-exclamation-circle',
    //       rejectVisible: false,
    //       accept: () => {
            
    //       }
    //     });
    //   });
    } else {
      this.profilesDataService.exportAllProfilesByFilters(this.noticePeriod, this.status, this.searchName).subscribe(data => {
        data = data['_body'];
        self.commonService.downloadCsv('Prejoining Pending', data);
      });
    }
  }
  resetModes() {
    this.buttonModes = {
      'open': true,
      'viewFeedback': true,
      'timeline': true
    };
  }

  changePagesize(page: any) {
    this.pageNum = 0;
    this.pageSize = (page == "ALL") ? this.totalRecords : page;

    if (this.pageSize > 0)
      this.showProfiles();
  }

  SearchByName() {
    if (this.searchName.length > 2)
      this.filterChanged();
    else if (this.searchName == "")
      this.setPageNum(0);
  }
}
