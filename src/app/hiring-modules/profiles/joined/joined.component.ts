import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AddProfileDataService } from '../../shared-hiring/services/add-profile-data.service';
import { HiringCommonService } from '../../shared-hiring/services/hiring-common.service';
import { OffersDataService } from '../../shared-hiring/services/offers.service';
import { ProfilesDataService } from '../../shared-hiring/services/profiles-data.service';

@Component({
  selector: 'app-joined',
  templateUrl: './joined.component.html',
  styleUrls: ['./joined.component.scss']
})
export class JoinedComponent implements OnInit {
  displayedColumns = ['id', 'name', 'resume', 'purpose', 'wl', 'cl', 'np', 'status', 'ht', 'ce', 'exp'];
  columnDefs: any;
  allOfTheData: any;
  public buttonModes: any = {};
  isLoaded: boolean = false;
  public noticePeriodOptions: any[];
  public noticePeriod: string;
  public status: string;
  public hiringRequestOptions: any;
  public hiringNeedOptions: any;
  hiringRequestId: any;
  hiringRequestNeedId: any;
  successMessage: string;
  totalRecords: number = 0;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  searchName: string = '';

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  constructor(private addProfileDataService: AddProfileDataService,
    private profilesDataService: ProfilesDataService,
    private offersDataService: OffersDataService,
    private commonService: HiringCommonService,
    private router: Router,
    private route: ActivatedRoute,
    private alertSvc: AlertService) {

    this.noticePeriodOptions = [];
    this.hiringRequestId = '';
    this.hiringRequestNeedId = '';
    this.noticePeriod = "ALL";
    this.status = "JOINED";
  }

  showVerificationDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var payload = {
        'hiringRequestId': selectedRecords[0]['hiringRequestId'],
        'hiringRequestNeedId': selectedRecords[0]['hiringRequestNeedId']
      };
      localStorage.setItem('verificationDetails', JSON.stringify(payload));
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../myspacenx/profiles/verification-details', id]);
    }
  }

  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.showProfiles();
  }

  navigateToOffers() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var prospectName = selectedRecords[0]['displayName'],
        id = selectedRecords[0]['id'],
        hiringId = selectedRecords[0]['hiringRequestId'],
        hiringNeedId = selectedRecords[0]['hiringRequestNeedId'];
      this.offersDataService.prospectDetails = {
        'prospectName': prospectName,
        'prospectId': id,
        'hiringId': hiringId,
        'hiringNeedId': hiringNeedId
      };
      this.router.navigate(['../myspacenx/offers/offerdetail']);
    }
  }

  ngOnInit() {
    var self = this;
    this.addProfileDataService.getAllHiringRequests(this.pageNum, 1000).subscribe(data => {
      if (data['content']) {
        var activeRequests: any = [];
        data['content'].forEach(function (elem: any, ind: any) {
          if (elem.status == 'ACTIVE') {
            activeRequests.push(elem);
          }
        });
        self.hiringRequestOptions = activeRequests;
      }
    });

    var data = this.addProfileDataService.getProspectCombos();
    if (data) {
      this.noticePeriodOptions = data['NOTICE_PERIOD'];
    }
    this.showProfiles();
  }

  loadNeedsByHiringId() {
    var hiringId = this.hiringRequestId, self = this;
    self.hiringRequestNeedId = "";
    if (hiringId) {
      self.addProfileDataService.getNeedsByHiringId(hiringId).subscribe(data => {
        if (data)
          self.hiringNeedOptions = data;
      });
    } else if (hiringId == '') {
      self.hiringNeedOptions = [];
    }
    self.pageNum = 0;
    self.showProfiles();
  }

  filterChanged() {
    this.pageNum = 0;
    this.showProfiles();
  }

  viewFeedBack() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'],
        reqId = selectedRecords[0]['hiringRequestId'],
        needId = selectedRecords[0]['hiringRequestNeedId'];
      this.router.navigate(['../myspacenx/interviews/view-feedback', id, reqId, needId]);
    }
  }

  showProfiles() {
    this.lstGrid = new MatTableDataSource(
      [
        {
          "id": "42ede08c-3c17-49b0-9a21-df8ac773ce04",
          "uid": "PC18011",
          "hiringRequestId": "ce1a3bb9-dc8a-4f33-855c-a0cb17a601b5",
          "hiringRequestNeedId": "5b00c2ab-78cc-4de7-b6b4-1150da52b6cd",
          "offerId": null,
          "displayName": "Ramesh Babu A",
          "currentWorkLocation": "",
          "currentEmployeer": "Value Labs",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Attached to the Need",
          "statusCode": "ATTACHED_TO_THE_NEED",
          "totalExperienceInYears": 8.0,
          "relevantExperienceInYears": 8.0,
          "expectedJoiningDate": null,
          "email": "akularameshbabu@yahoo.com",
          "mobileNumber": "9985199494",
          "hiringPurpose": "Fulfill the .Net staffing demand",
          "hiringTypeDescription": "Regular",
          "clientName": "Innominds SEZ (Waverock 2.3)",
          "isOwnerOfHiringRequest": false,
          "resumeId": "d24476a3-e665-389e-872c-a7bb9ccf0274",
          "resumeName": "H0368_Profile_12204.docx",
          "category": null,
          "needAttached": false
        }, {
          "id": "56c600f5-46b6-4f19-86bc-900786d7f1b1",
          "uid": "PC18257",
          "hiringRequestId": "ce1a3bb9-dc8a-4f33-855c-a0cb17a601b5",
          "hiringRequestNeedId": "5b00c2ab-78cc-4de7-b6b4-1150da52b6cd",
          "offerId": null,
          "displayName": "Anil Kumar Mootha",
          "currentWorkLocation": "hyd",
          "currentEmployeer": "UST ",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Interview In Progress",
          "statusCode": "INTERVIEW_IN_PROGRESS",
          "totalExperienceInYears": 13.0,
          "relevantExperienceInYears": 13.0,
          "expectedJoiningDate": null,
          "email": "aniljain50@gmail.com",
          "mobileNumber": "9000421467",
          "hiringPurpose": "Fulfill the .Net staffing demand",
          "hiringTypeDescription": "Regular",
          "clientName": "Innominds SEZ (Waverock 2.3)",
          "isOwnerOfHiringRequest": false,
          "resumeId": "94f7fe0e-00ea-36cc-b73a-8e0cdc460e8d",
          "resumeName": "H0433_Profile_8682.doc",
          "category": null,
          "needAttached": false
        },
        {
          "id": "b29e12fe-98f6-4a88-bfdc-ded742184959",
          "uid": "PC1054",
          "hiringRequestId": "cbc0570e-eeff-4f95-afae-baac80cd8388",
          "hiringRequestNeedId": "dc4a20ab-0574-4546-9616-5eab5c7d732e",
          "offerId": null,
          "displayName": "Arabinda Arabinda",
          "currentWorkLocation": "",
          "currentEmployeer": "Qvantel Software Solution Limited",
          "noticePeriod": "1 Month",
          "noticePeriodCode": "ONE_MONTH",
          "status": "Pending Pre-Offer Formalities",
          "statusCode": "PENDING_PREJOINING_FORMALITIES",
          "totalExperienceInYears": 15.0,
          "relevantExperienceInYears": 15.0,
          "expectedJoiningDate": null,
          "email": "arabinda.choudhury2020@gmail.com",
          "mobileNumber": "9593927905",
          "hiringPurpose": "Proactive hiring of Sr Engineers for Hilti",
          "hiringTypeDescription": "Regular",
          "clientName": "Hilti (Pune)                            ",
          "isOwnerOfHiringRequest": false,
          "resumeId": "3a8a77f5-9388-3d2c-993c-a3ab3a86afa0",
          "resumeName": "H0672_Profile_16557.doc",
          "category": null,
          "needAttached": false
        }]
    );
    
    return;
    var self = this;
    self.resetModes();
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.getAllProfilesByHiringIdAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      });
      // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
      //   this.addProfileDataService.getAllProfilesByHiringIdAndNeedAndFilters(this.pageNum, this.pageSize, this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
      //     self.setRowData(data['content']);
      //     self.totalPages = data.totalPages;
      //     self.totalRecords = data.totalElements;
      //     self.loaderService.hideLoading('overlay', '#overlay');
      //     self.isLoaded = self.loaderService.isLoaded;
      //   }, error => {
      //     self.loaderService.hideLoading('overlay', '#overlay');
      //     self.isLoaded = self.loaderService.isLoaded;
      //     self.confirmationService.confirm({
      //       message: error.message,
      //       header: 'Error',
      //       icon: 'fa fa-exclamation-circle',
      //       rejectVisible: false,
      //       accept : ()=>{

      //       }
      //     });
      //   });
    } else {
      this.profilesDataService.getAllProfilesByFilters(this.pageNum, this.pageSize, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        this.lstGrid = new MatTableDataSource(data ? data.content : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      });
    }
  }

  showProfileDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../profile-details', id], { relativeTo: this.route });
    }
  }

  showProspectTimeline() {
    var selectedRecords = this.selection.selected, id;
    if (selectedRecords.length > 0) {
      id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/profiles/prospect-timeline', id]);
    }
  }

  exportData() {
    var self = this;
    if (this.hiringRequestId && !this.hiringRequestNeedId) {
      this.addProfileDataService.exportAllProfilesByHiringIdAndFilters(this.hiringRequestId, this.noticePeriod, this.status, this.searchName).subscribe(data => {
        if(data) {
          data = data['_body'];
          self.commonService.downloadCsv('Joined', data);
        }
      });
      // } else if (this.hiringRequestId && this.hiringRequestNeedId) {
      //   this.addProfileDataService.exportAllProfilesByHiringIdAndNeedAndFilters(this.hiringRequestId, this.hiringRequestNeedId, this.noticePeriod, this.status).subscribe(data => {
      //     data = data['_body'];
      //     self.commonService.downloadCsv('Joined', data);
      //   }, error => {
      //     self.confirmationService.confirm({
      //       message: error.message,
      //       header: 'Error',
      //       icon: 'fa fa-exclamation-circle',
      //       rejectVisible: false,
      //       accept : ()=>{

      //       }
      //     });
      //   });
    } else {
      this.profilesDataService.exportAllProfilesByFilters(this.noticePeriod, this.status, this.searchName).subscribe(data => {
        data = data['_body'];
        self.commonService.downloadCsv('Joined', data);
      });
    }
  }

  checkModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      this.buttonModes['open'] = false;
      this.buttonModes['timeline'] = false;
      this.buttonModes['viewFeedback'] = false;
      this.buttonModes['viewDetails'] = false;
    } else {
      this.resetModes();
    }
  }
  resetModes() {
    this.buttonModes = {
      'open': true,
      'viewFeedback': true,
      'timeline': true,
      'viewDetails': true
    };
  }

  SearchByName() {
    if (this.searchName.length > 2)
      this.filterChanged();
    else if (this.searchName == "")
      this.setPageNum(0);
  }

  onRowSelection(item) {
    if (this.selection.selected.length > 0) {
      if (this.selection.isSelected(item))
        this.selection.toggle(item);
      else {
        this.selection = new SelectionModel<any>(true, []);
        this.selection.toggle(item);
      }
    }
    else
      this.selection.toggle(item);

    this.checkModes();
  }

  onReinit() {
    this.pageNum = 0;
    this.lstGrid = new MatTableDataSource([]);
  }

  onSubmit() {
    this.onReinit();
    this.showProfiles();
  }
  
}
