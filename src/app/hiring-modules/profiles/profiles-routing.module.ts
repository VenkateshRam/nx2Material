import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewProfileComponent } from './add-profile/new-profile/new-profile.component';
import { AllProfilesComponent } from './all-profiles/all-profiles.component';
import { JoinedComponent } from './joined/joined.component';
import { NoShowComponent } from './no-show/no-show.component';
import { OfferAcceptedComponent } from './offer-accepted/offer-accepted.component';
import { PreOfferCompletedComponent } from './pre-offer-completed/pre-offer-completed.component';
import { PreOfferPendingComponent } from './pre-offer-pending/pre-offer-pending.component';
import { PreOnboardingPendingComponent } from './pre-onboarding-pending/pre-onboarding-pending.component';
import { ProfileScreeningComponent } from './profile-screening/profile-screening.component';
import { ProfilesComponent } from './profiles.component';
import { ReadyToJoinComponent } from './ready-to-join/ready-to-join.component';
import { ReadyToVerifyComponent } from './ready-to-verify/ready-to-verify.component';
import { RejectedComponent } from './rejected/rejected.component';
import { SelectedComponent } from './selected/selected.component';
import { ShortlistedComponent } from './shortlisted/shortlisted.component';
import { VerifiedComponent } from './verified/verified.component';

const routes: Routes = [
  { path: 'search', component: ProfilesComponent },
  { path: 'all', component: AllProfilesComponent },
  { path: 'joined', component: JoinedComponent },
  { path: 'no-show', component: NoShowComponent },
  { path: 'offer-accepted', component: OfferAcceptedComponent },
  { path: 'pre-offer-completed', component: PreOfferCompletedComponent },
  { path: 'pre-offer-pending', component: PreOfferPendingComponent },
  { path: 'pre-onboarding-pending', component: PreOnboardingPendingComponent },
  { path: 'profile-screening', component: ProfileScreeningComponent },
  { path: 'ready-to-join', component: ReadyToJoinComponent },
  { path: 'ready-to-verify', component: ReadyToVerifyComponent },
  { path: 'rejected', component: RejectedComponent },
  { path: 'selected', component: SelectedComponent },
  { path: 'shortlisted', component: ShortlistedComponent},
  { path: 'verified', component: VerifiedComponent },
  { path: 'new-profile', component: NewProfileComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }
