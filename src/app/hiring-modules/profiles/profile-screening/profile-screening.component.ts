import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-screening',
  templateUrl: './profile-screening.component.html',
  styleUrls: ['./profile-screening.component.scss']
})
export class ProfileScreeningComponent implements OnInit {

  displayedColumns = ['id', 'name', 'resume', 'cwl', 'ce', 'np', 'st'];
  constructor() { }

  ngOnInit(): void {
  }

}
