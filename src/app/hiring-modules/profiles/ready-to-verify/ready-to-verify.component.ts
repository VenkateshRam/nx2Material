import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AddProfileDataService } from '../../shared-hiring/services/add-profile-data.service';
import { ProfilesDataService } from '../../shared-hiring/services/profiles-data.service';

// var FileSaver = require('filesaver.js-npm');
@Component({
  selector: 'app-ready-to-verify',
  templateUrl: './ready-to-verify.component.html',
  styleUrls: ['./ready-to-verify.component.scss']
})
export class ReadyToVerifyComponent implements OnInit {

  displayedColumns = ['id', 'name', 'resume', 'purpose', 'cl' ,  'np','wl', 'ce', 'st', 'ht', 'ce', 'total exp'];
  sendTitle: any = 'Send Message';
  sendId: any = 'sendMessageAfterVerify';
  id: string;
  successMessage: string;
  isLoaded: boolean = false;
  verificationDeatils: any;
  appointmentLetter: any = {};
  hikeLetter: any = {};
  bankStatement: any = {};
  idProof: any = {};
  addressProof: any = {};
  relievingLetter: any = {};
  postBackTitle: string = 'Request For Corrections';
  postBackId: string = 'postBackProfile';
  playships: any[] = [];
  ishide: boolean = false;
  documentCode: any[] = [];
  uploadDocumentList: any = [];
  docCategories: any = [];
  degreeData: any;
  commonService: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addProfileDataService: AddProfileDataService,
    private profileDataService: ProfilesDataService,
    private alertSvc: AlertService
  ) {}
  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.id = params['id'];
        if (location.pathname.includes('verification-details')) {
          this.ishide = true;
        } else {
          this.ishide = false;
        }
      }
    });
    this.addProfileDataService.getVerficationDetails(this.id).subscribe(
      res => {
        this.verificationDeatils = res;
        this.verificationDeatils.educationHistoryDetails = this.verificationDeatils.educationHistoryDetails.sort((a: any, b: any) => a.endDate - b.endDate);
        this.verificationDeatils.employmentHistoryDetails = this.verificationDeatils.employmentHistoryDetails.sort((a: any, b: any) => a.startDate - b.startDate);
        this.docCategories = ['Personal Documents'];
        if (this.verificationDeatils.educationHistoryDetails.length > 0)
          this.docCategories.push('Education');
        if (this.verificationDeatils.employmentHistoryDetails.length > 0)
          this.docCategories.push('Work Experience');
        if (
          !this.commonService.hideFeature('DOWNLOAD_CANDIDATE_SUPPORTING_DOCUMENTS') &&
          this.verificationDeatils &&
          this.verificationDeatils.documents.length
        ) {
          this.DocumentShow(res);
          this.verificationDeatils.documents.forEach((element: any) => {
            var description = element.documentType.description;
            var title = element.title;
            var date = this.formatDate(element.creationTimestamp);
            var prospectId = element.prospectId;
            var documentId = element.documentId;
            this.documentCode.push({
              description: description,
              title: title,
              date: date,
              prospectId: prospectId,
              documentId: documentId
            });
          });
          this.addProfileDataService.getCertificateType().subscribe((data: any) => {
            if (data.status == 200) {
              var data = data['result'],
                degreeData = [];
              for (let element in data) {
                degreeData.push({
                  id: element,
                  name: data[element]
                });
              }
              for (let i = 0; i < res.educationHistoryDetails.length; i++) {
                var degreeResult = degreeData.find(
                  (ele: any) => ele.id === res['educationHistoryDetails'][i]['degree']
                );
                this.verificationDeatils['educationHistoryDetails'][i]['degree'] = degreeResult.name;
              }
            }
          });
        }
      });
  }
  getYear(time: any) {
    var d = new Date(time);
    return d.getFullYear();
  }
  openOfferLetterDetails() {
    this.router.navigate(['../offerdetail'], { relativeTo: this.route });
  }
  navigateToProfiles() {
    var route = localStorage.getItem('previousRoute');
    route = route ? route : '/myspacenx/profiles/readytoverify';
    this.router.navigate([route]);
  }
  verficationPass() {
    var self = this,
      details = JSON.parse(localStorage.getItem('verificationDetails'));
    if (details) {
      
      var payload = {
        hiringRequestId: details['hiringRequestId'],
        hiringRequestNeedId: details['hiringRequestNeedId']
      };
      this.profileDataService.updateProspectStatus(this.id, 'COMPLETE_VERIFICATION', payload).subscribe(
        data => {
          if (data.status == 200) {
            self.successMessage = 'Verification Passed';
          }
        }
      );
    }
  }
  verficationFail() {
    var self = this,
      details = JSON.parse(localStorage.getItem('verificationDetails'));
    if (details) {
      
      var payload = {
        hiringRequestId: details['hiringRequestId'],
        hiringRequestNeedId: details['hiringRequestNeedId']
      };
      this.profileDataService.updateProspectStatus(this.id, 'FAIL_VERIFICATION', payload).subscribe(
        data => {
          if (data.status == 200) {
            self.successMessage = 'Verification Failed';
          }
        });
    }
  }
  formatDate(date: any) {
    var time = new Date(date),
      result,
      year,
      month,
      today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    month = month < 10 ? '0' + month : month;
    today = today < 10 ? '0' + today : today;
    result = year + '-' + month + '-' + today;
    return result;
  }

  downloadFile(resumeDetails: any) {
    var self = this;
    this.profileDataService.downloadResume(this.id, resumeDetails['documentId']).subscribe(
      (data: any) => {
          var blob = new Blob([data['_body']], { type: 'application/octet-stream' });
        // FileSaver.saveAs(blob, resumeDetails['fileName']);
      });
  }
  postBack(reason: any) {
    var self = this,
      details = JSON.parse(localStorage.getItem('verificationDetails'));
    if (details) {
      
      var payload = {
        hiringRequestId: details['hiringRequestId'],
        hiringRequestNeedId: details['hiringRequestNeedId'],
        verificationComments: reason
      };
      this.profileDataService.updateProspectStatus(this.id, 'REQUEST_FOR_CORRECTIONS', payload).subscribe(
        data => {
          if (data.status == 200) {
            self.successMessage = 'Posted back successfully';
       
          }
        });
    }
  }

  DocumentShow(resData: any) {
    this.uploadDocumentList = [];
    var listDocuments = [];
    if (resData.documents && resData.documents.length > 0) {
      for (let a = 0; a < resData.documents.length; a++) {
        var documentFileName = resData.documents[a].title;
        var documentName = resData.documents[a].documentType.description;
        var docKey = +resData.documents[a].documentType.key;
        var docDesc = resData.documents[a].description;
        var documentType = resData.documents[a].documentType.code;
        var createdDate = this.formatDate(resData.documents[a].creationTimestamp);
        var documentId = resData.documents[a].documentId;
        listDocuments.push({
          documentName: documentName,
          fileName: documentFileName,
          docType: documentType,
          date: createdDate,
          documentId: documentId,
          docDesc: docDesc,
          docKey: docKey
        });
      }
      this.uploadDocumentList = listDocuments;
    }
  }

  getUploadDocumentList(val: string) {
    let st = val == 'Personal Documents' ? 'ID Proof' : val == 'Education' ? 'Education' : 'Other';
    return st == 'ID Proof' ? this.uploadDocumentList.filter((f: any) => f.docKey == 4)
      : st == 'Education' ? this.uploadDocumentList.filter((f: any) => f.docKey >= 11 && f.docKey <= 14)
        : this.uploadDocumentList.filter((f: any) => f.docKey != 4 && f.docKey != 1 && !(f.docKey >= 11 && f.docKey <= 14));
  }
}
