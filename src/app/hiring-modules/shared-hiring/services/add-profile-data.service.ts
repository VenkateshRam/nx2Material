import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConstants } from '../config/app.constants';
import { ProfileDetails } from '../models/ProfileDetails';
import { HiringCommonService } from './hiring-common.service';

@Injectable()
export class AddProfileDataService {
  getCityData() {
    throw new Error('Method not implemented.');
  }
    //private ProspectData: SkillsProficiencyByCategory[];
    public ProspectData: ProfileDetails;
    public mode: string;
    public profileId: string;
    public myspacenxApiUrl: string;
    public fileData: any;
    public hiringComboData: any;
    private prospectsComboData: any;
    private hiringRequestData: any;
    private tokenDetails: any;
    private authorizationToken: string;
    private hiringQueryBasePath: string;
    private hiringCommandBasePath: string;
    private prospectQueryBasePath: string;
    private prospectCommandBasePath: string;
    public personalForm: any;
    public personalFormSaved: boolean = false;
    public professionalFormSaved: boolean = false;
    public professionalForm: any;
    public prospectData: any;
    public documentMap: any = {};
    public attachmentMap: any = {};
    public docLength: any;
    public attachLength: any;
    public payslipDocs: any = [];
    private notify = new Subject<any>();
    notifyObservable$ = this.notify.asObservable();
    constructor(private _http: HttpClient, public commonService: HiringCommonService, private appConstants: AppConstants) {
        this.ProspectData = null;
    }
    setToken() {
        this.myspacenxApiUrl = this.appConstants.getConstants().myspacenxApiUrl;
        // this.tokenDetails = this.commonService.getItem('tokenDetails');
        this.authorizationToken = 'Bearer ' + (this.tokenDetails && this.tokenDetails['access_token']);
        this.hiringQueryBasePath = this.appConstants.getConstants().hiringQueryBasePath;
        this.hiringCommandBasePath = this.appConstants.getConstants().hiringCommandBasePath;
        this.prospectQueryBasePath = this.appConstants.getConstants().prospectQueryBasePath;
        this.prospectCommandBasePath = this.appConstants.getConstants().prospectCommandBasePath;
    }
    loadHiringComboData = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/seeddata', 'get', null, header).pipe(map(response => response.json())).subscribe(data => {
            self.hiringComboData = data;
        }, error => { });
    }
    loadProspectComboData = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/seeddata', 'get', null, header).pipe(map(response => response.json())).subscribe(data => {
            self.prospectsComboData = data;
        }, error => { });
    }
    getHiringCombos = () => {
        return (this.hiringComboData) ? Object.assign({}, this.hiringComboData) : undefined;
    }
    getProspectCombos = () => {
        return (this.prospectsComboData) ? Object.assign({}, this.prospectsComboData) : undefined;
    }
    getProspectComboDetails = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/seeddata', 'get', null, header).pipe(map(response => response.json()));
    }
    getHiringComboDetails = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/seeddata', 'get', null, header).pipe(map(response => response.json()));
    }
    getHiringRequestData = () => {
        return (this.hiringRequestData) ? Object.assign({}, this.hiringRequestData) : undefined;
    }
    getAllHiringRequests = (pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getHiringRequests = (pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/all?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getNeedsByHiringId = (hiringId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/' + hiringId + '/needs/purpose', 'get', null, header).pipe(map(response => response.json()));
    }
    downloadResume = (prospectId: string, documentId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken }, responseType = "ArrayBuffer";
        return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/' + prospectId + '/documents/' + documentId, 'get', null, header, responseType).pipe(map(response => response));
    }
    getSkillsMap = () => {
        var skillsCat = {},
            skills = {},
            result = {};
        this.hiringComboData && this.hiringComboData['SKILL_CATEGORY'].forEach(function (element: any, index: any) {
            skillsCat[element['id']] = element['name'];
            skills[element['id']] = element['skills'];
        });
        result['skillCatNames'] = skillsCat;
        result['skills'] = skills;
        return result;
    }
    loadNoticePeriodCombo = (): Observable<any> => {
        var self = this;
        return self._http.get(this.appConstants.getConstants().myspacenxApiUrl + '/prospects/noticePeriod').pipe(map(response => response));
    }

    /* loadProspectDataById = (id: string) => {
         var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
         self.commonService.send(this.myspacenxApiUrl  + self.prospectQueryBasePath + '/prospects/'+ id, 'get', null, header).pipe(map(response => response.json())).subscribe(data => {
             self.ProspectData = data;
         }, error =>{});
     } */
    loadProspectDataById = (id: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/' + id, 'get', null, header).pipe(map(response => response.json()));
    }
    getVerficationDetails = (id: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/' + id + "/verificationdetails", 'get', null, header).pipe(map(response => response.json()));
    }
    updateProspectVerfication = (id: string, verificationData: any, opCode: string) => {
        this.setToken();
        var header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectCommandBasePath + '/prospects/' + id + '/operation/' + opCode, 'put', verificationData, header).pipe(map(response => response));
    }
    loadProspectData = () => {
        var self = this;
        return self._http.get('../src/app/recruiter/models/profileData.json').pipe(map(response => response));
    }
    getProfileData = () => {
        var data = require('../models/profileData.json');
        return (data) ? Object.assign({}, data) : undefined;
    }
    getProspectData = () => {
        let Prospectdata: any;
        if (this.ProspectData)
            Prospectdata = this.ProspectData;
        return Prospectdata;

    }
    /*isDataLoad = () =>{
        let p:boolean = false;
        if(this.ProspectData.length != 0)
         p = true;
         return p;
    }*/
    getSkillData = () => {
        return (this.ProspectData) ? Object.assign({}, this.ProspectData) : undefined;
    }
    getProfessionalData = () => {
        return (this.ProspectData) ? Object.assign({}, this.ProspectData) : undefined;
    }
    getPersonalData = () => {
        return (this.ProspectData) ? Object.assign({}, this.ProspectData) : undefined;
    }
    updateAddProfileData = (addprofiledata: any) => {
        let tempaddprofiledata = this.ProspectData;
        tempaddprofiledata['hiringRequestId'] = addprofiledata['hiringRequestId'];
        tempaddprofiledata['hiringRequestNeedId'] = addprofiledata['hiringRequestNeedId'];
        this.ProspectData = tempaddprofiledata;
    }
    updateProspectSkillData = (skilldata: any) => {
        let tempupdprospectdata = this.ProspectData;
        tempupdprospectdata["skillSets"] = skilldata['skillSets'];
    }
    updateProspectPersonalData = (personaldata: any) => {
        let tempupdprospectdata = this.ProspectData;
        personaldata['hiringRequestId'] = tempupdprospectdata['hiringRequestId'];
        personaldata['hiringRequestNeedId'] = tempupdprospectdata['hiringRequestNeedId'];
        tempupdprospectdata = personaldata;
        this.ProspectData = tempupdprospectdata;
    }
    updateProspectProfessionalData = (professionaldata: any, fileData: any) => {
        let tempupdprospectdata = this.ProspectData;
        tempupdprospectdata = professionaldata;
        this.ProspectData = tempupdprospectdata;
        this.fileData = fileData;
    }
    saveProspectData = (ProspectData: any) => {
        this.setToken();
        let formData: FormData = new FormData();
        this.fileData && formData.append('resume', this.fileData, this.fileData.name);
        formData.append("prospectMetadata", new Blob([JSON.stringify(ProspectData)], {
            type: "application/json"
        }));
        var header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectCommandBasePath + '/prospects', 'post', formData, header).pipe(map(response => response));
    }
    updateProspectData = (ProspectData: any, id: any) => {
        this.setToken();
        let formData: FormData = new FormData();
        this.fileData && formData.append('resume', this.fileData, this.fileData.name);
        formData.append("prospectMetadata", new Blob([JSON.stringify(ProspectData)], {
            type: "application/json"
        }));
        var header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectCommandBasePath + '/prospects/' + id, 'post', formData, header).pipe(map(response => response));
    }
    getAllProfilesByHiringId = (pageNum: number, pageSize: number, id: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        // return self.commonService.send(this.myspacenxApiUrl  + self.hiringQueryBasePath + '/hiring/requests/' + id + '/prospects', 'get', null, header).pipe(map(response => response.json()));
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id, 'get', null, header).pipe(map(response => response.json()));
    }
    getAllProfilesByHiringIdAndNeed = (pageNum: number, pageSize: number, id: string, needId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        // return self.commonService.send(this.myspacenxApiUrl  + self.hiringQueryBasePath + '/hiring/requests/' + id + '/needs/' + needId + '/prospects', 'get', null, header).pipe(map(response => response.json()));
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&needIds=' + needId, 'get', null, header).pipe(map(response => response.json()));
    }
    getAllProfilesByHiringIdAndFilters = (pageNum: number, pageSize: number, id: string, noticePeriod: string, status: string, name: string = '') => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod +`${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&statusCodes=' + status + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    getAllProfilesByHiringIdAndNeedAndFilters = (pageNum: number, pageSize: number, id: string, needId: string, noticePeriod: string, status: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&needIds=' + needId + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&needIds=' + needId + '&statusCodes=' + status, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&needIds=' + needId + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response.json()));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&needIds=' + needId, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    getAllProfilesByFilters = (pageNum: number, pageSize: number, noticePeriod: string, status: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&statusCodes=' + status, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response.json()));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    exportAllProfilesByHiringIdAndFilters = (id: string, noticePeriod: string, status: string, name: string = '') => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod + `${name != '' ? `?displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + '&statusCodes=' + status + `${name != '' ? `?displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + '&noticePeriods=' + noticePeriod + `${name != '' ? `?displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + `${name != '' ? `?displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
    }
    exportAllProfilesByHiringIdAndNeedAndFilters = (id: string, needId: string, noticePeriod: string, status: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + '&needIds=' + needId + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + '&needIds=' + needId + '&statusCodes=' + status, 'get', null, header).pipe(map(response => response));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + '&needIds=' + needId + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/exportCsv?requestIds=' + id + '&needIds=' + needId, 'get', null, header).pipe(map(response => response));
        }
    }
    getAllProfilesByHiringIdAndStatus = (pageNum: number, pageSize: number, id: string, status: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        // return self.commonService.send(this.myspacenxApiUrl  + self.hiringQueryBasePath + '/hiring/requests/' + id + '/prospects', 'get', null, header).pipe(map(response => response.json()));
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&statusCodes=' + status, 'get', null, header).pipe(map(response => response.json()));
    }
    getAllProfilesByHiringIdAndNeedAndStatus = (pageNum: number, pageSize: number, id: string, needId: string, status: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        // return self.commonService.send(this.myspacenxApiUrl  + self.hiringQueryBasePath + '/hiring/requests/' + id + '/needs/' + needId + '/prospects', 'get', null, header).pipe(map(response => response.json()));
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&requestIds=' + id + '&needIds=' + needId + '&statusCodes=' + status, 'get', null, header).pipe(map(response => response.json()));
    }
    getReferrals = () => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/employees?projection=employeeprojection', 'get', null, header).pipe(map(response => response.json()));
    }
    getReferralDetailsById = (referralId: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/employees/' + referralId + '?projection=employeeprojection', 'get', null, header).pipe(map(response => response.json()));
    }
    uploadDocumentData = (documentType: string, fileData: any, prospectId: string) => {
        this.setToken();
        let formData: FormData = new FormData();
        fileData && formData.append('uploadedFile', fileData, fileData.name);
        formData.append("documentType", documentType);
        var self = this, header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + self.prospectCommandBasePath + '/prospects/' + prospectId + '/documents/sensitive', 'post', formData, header).pipe(map(response => response));
    }
    deleteDocumentData = (prospectId: string, documentId: string) => {
        this.setToken();
        var self = this, header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + self.prospectCommandBasePath + '/prospects/' + prospectId + '/documents/sensitive/' + documentId, 'delete', null, header).pipe(map(response => response));
    }
    deleteResumeData = (prospectId: string, documentId: string) => {
        this.setToken();
        var self = this, header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + self.prospectCommandBasePath + '/prospects/' + prospectId + '/documents/' + documentId, 'delete', null, header).pipe(map(response => response));
    }
    createDocumentMap = (documents: any) => {
        var self = this;
        self.payslipDocs = [];
        self.documentMap = [];
        documents && documents.forEach(function (elem: any, ind: any) {
            if (elem.documentType.code == 'OTHERS') {
                self.payslipDocs.push(elem);
            }
        });
        self.docLength = self.payslipDocs.length;
        self.payslipDocs.forEach(function (elem: any, ind: any) {
            self.documentMap['OTHERS' + (ind + 1)] = elem;
        });
    }
    uploadAttachment = (hiringId: any, needId: any, offerId: any, fileData: any) => {
        this.setToken();
        let formData: FormData = new FormData();
        fileData && formData.append('offer_attachment', fileData, fileData.name);
        var self = this, header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + self.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/offers/' + offerId + '/attachments', 'post', formData, header).pipe(map(response => response));
    }
    deleteAttachment = (hiringId: any, needId: any, offerId: any, attachmentId: any) => {
        this.setToken();
        var self = this, header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + self.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/offers/' + offerId + '/attachments/' + attachmentId, 'delete', null, header).pipe(map(response => response));
    }
    createAttachmentMap = (attachments: any) => {
        var self = this, attachmentCount: any;
        self.attachmentMap = [];
        attachmentCount = 0;
        attachments && attachments.forEach(function (elem: any, ind: any) {
           self.attachmentMap['OTHERS' + (ind + 1)] = elem;
           attachmentCount++;
        });
        self.attachLength = attachmentCount;
    }
    notifyAttachments = (data: any) => {
      if (data) {
        this.notify.next(data);
      }
    }
    getEmployees = (query: any, pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/employees/search?searchString=' + query + '&page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getExistingProfileByEmail = (emailId: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/search/findByEmail?email=' + emailId, 'get', null, header).pipe(map(response => response));
    }
    getExistingProfileByMobile = (mobileNo: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/search/findByMobileNumber?mobileNumber=' + mobileNo, 'get', null, header).pipe(map(response => response));
    }
    getExistingProfileByCitizenIdentity = (citizenIdentityType: any, citizenIdentity: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/search/findByCitizenIdentity?citizenIdentity=' + citizenIdentity + '&citizenIdentityType=' + citizenIdentityType, 'get', null, header).pipe(map(response => response));
    }
    // For vendor List
    getVendorDetails  = (pageNum:number,pageSize:number) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/vendors?number=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
      getCountryData = () => {
        this.setToken();
        var self = this, header={'Content-Type':'application/json', 'Authorization': self.authorizationToken};
        return self.commonService.send(
            this.myspacenxApiUrl+ self.prospectQueryBasePath +'/lookups/countries',
            'get',
            null,
            header
          ).pipe(map(response => response.json()));
      };
      getStateData = (countryCode:string) => {
        this.setToken();
        var self = this, header={'Content-Type':'application/json', 'Authorization': self.authorizationToken};
        return self.commonService.send(
            this.myspacenxApiUrl+ self.prospectQueryBasePath +'/lookups/countries/'+countryCode+'/states',
            'get',
            null,
            header
          ).pipe(map(response => response.json()));
      };
      getCertificateType = () => {
        this.setToken();
        var self = this, header={'Content-Type':'application/json', 'Authorization': self.authorizationToken};
        return self.commonService.send(
            this.myspacenxApiUrl+ self.prospectQueryBasePath +'/lookups/certificateTypes',
            'get',
            null,
            header
          ).pipe(map(response => response.json()));
      };
      getRoles =(page:any,size:any) =>{
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/roles?page='+page+'&size='+size, 'get', null, header).pipe(map(response => response.json()));
      };

      getEmployeeRoles=(employeeCode:any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/roles/search/getEmployeeRoles?employeeCode='+employeeCode, 'get', null, header).pipe(map(response => response.json()));
    };
    updateAssignedRoles = (empCode:any,roleIds:any) => {
        this.setToken();
        var header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/employees/'+empCode+'/roles', 'put', roleIds, header).pipe(map(response => response));
    }
}
