import { Injectable } from '@angular/core';
import { HiringCommonService } from './hiring-common.service';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import { AppConstants } from '../config/app.constants';

@Injectable()
export class ProfilesDataService {
    public myspacenxApiUrl: string;
    private tokenDetails: any;
    private authorizationToken: string;
    private hiringQueryBasePath: string;
    private hiringCommandBasePath: string;
    private prospectQueryBasePath: string;
    private prospectCommandBasePath: string;
    public prospectDetails: any = {};
    public fileData: any;
    constructor(private httpService: HttpService, public commonService: HiringCommonService, private appConstants: AppConstants) { }
    setToken() {
        this.myspacenxApiUrl = this.appConstants.getConstants().myspacenxApiUrl;
        // this.tokenDetails = this.commonService.getItem('tokenDetails');
        this.authorizationToken = 'Bearer ' + (this.tokenDetails && this.tokenDetails['access_token']);
        this.hiringQueryBasePath = this.appConstants.getConstants().hiringQueryBasePath;
        this.hiringCommandBasePath = this.appConstants.getConstants().hiringCommandBasePath;
        this.prospectQueryBasePath = this.appConstants.getConstants().prospectQueryBasePath;
        this.prospectCommandBasePath = this.appConstants.getConstants().prospectCommandBasePath;
    }
    getAllProfiles = (pageNum: number, pageSize: number) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getAllProfilesByFilters = (pageNum: number, pageSize: number, noticePeriod: string, status: string,  name: string = '') => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&statusCodes=' + status + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    exportAllProfilesByFilters = (noticePeriod: string, status: string, name:string = '') => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv?statusCodes=' + status + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv?statusCodes=' + status + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv?noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv'  + `${name != '' ? `?displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response));
        }
    }
    exportProfileData = (noticePeriod: string, status: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv?statusCodes=' + status + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv?statusCodes=' + status, 'get', null, header).pipe(map(response => response));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv?noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/exportCsv', 'get', null, header).pipe(map(response => response));
        }
    }
    /*attachProfiletoNeed = (hiringId: string, needId: string, prospects: any) => {
        this.setToken();
    	var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl  + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/prospects', 'post', prospects, header).pipe(map(response => response));
    }*/
    attachProfiletoNeed = (hiringId: string, needId: string, prospectId: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectCommandBasePath + '/prospects/' + prospectId + '/requests/' + hiringId + '/needs/' + needId + '/attach', 'post', null, header).pipe(map(response => response));
    }
    getPanels = (hiringId: string, needId: string, levelId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/interviewLevels/' + levelId + '/panels', 'get', null, header).pipe(map(response => response.json()));
    }
    getInterviewLevels = (hiringId: string, needId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/interviewLevels', 'get', null, header).pipe(map(response => response.json()));
    }
    getInterviewersByPanelId = (hiringId: string, needId: string, levelId: string, panelId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/interviewLevels/' + levelId + '/panels/' + panelId + '/panelMembers', 'get', null, header).pipe(map(response => response.json()));
    }
    // scheduleInterview = (hiringId: string, needId: string, details: any) => {
    //     this.setToken();
    //     var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
    //     return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/interviews', 'post', details, header).pipe(map(response => response));
    // }
    // making service method  for direct feedack submit 
    scheduleInterview = (hiringId: string, needId: string, details: any, file: any) => {
        this.setToken();
        let formData: FormData = new FormData();
        file && formData.append('recruiterFeedbackDoc', file, file.name);
        formData.append("scheduleInterviewMetaData", new Blob([JSON.stringify(details)], {
            type: "application/json"
        }));
        var header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/interviews', 'post', formData ,header).pipe(map(response => response));
    }
    rescheduleInterview = (hiringId: string, needId: string, interviewId: string, details: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/interviews/' + interviewId, 'put', details, header).pipe(map(response => response));
    }
    getInterviewDetailsById = (interviewId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/interviews/' + interviewId, 'get', null, header).pipe(map(response => response.json()));
    }
    updateInterviewStatus = (prospectId: string, opCode: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectCommandBasePath + '/prospects/' + prospectId + '/operation/' + opCode, 'put', null, header).pipe(map(response => response));
    }
    getProfilesByStatus = (pageNum: number, pageSize: number, status: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/all?page=' + pageNum + '&size=' + pageSize + '&statusCodes=' + status, 'get', null, header).pipe(map(response => response.json()));
    }
    notifyCandidate = (prospectId: string, payload: any) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectCommandBasePath + '/prospects/' + prospectId + '/notification', 'post', payload, header).pipe(map(response => response));
    }

    sendRemainder = (prospectMail: string, payload: any) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + `/prospects/sendReminderToCompletePreOnboarding?email=${prospectMail}`, 'post', payload, header).pipe(map(response => response));
    }
    updateProspectStatus = (prospectId: string, opCode: string, payload: any) => {
        this.setToken();
        var header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectCommandBasePath + '/prospects/' + prospectId + '/operation/' + opCode, 'put', payload, header).pipe(map(response => response));
    }
    downloadResume = (prospectId: string, documentId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken }, responseType = "ArrayBuffer";
        return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/' + prospectId + '/documents/' + documentId, 'get', null, header, responseType).pipe(map(response => response));
    }
    submitIssue = (issueData: any, fileData: any) => {
        this.setToken();
        let formData: FormData = new FormData();
        formData.append("issueMetadata", new Blob([JSON.stringify(issueData)], {
            type: "application/json"
        }));
        fileData && formData.append('screenshot', fileData, fileData.name);
        var header = { 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/support/issues', 'post', formData, header).pipe(map(response => response));
    }
}
