
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AppConstants } from '../config/app.constants';
import { NewHiringRequestDetails } from '../models/hiring-request-details1';
import { JobDescriptionDetails } from '../models/job-description';
import { HiringCommonService } from './hiring-common.service';

@Injectable({
    providedIn: 'root'
})
export class HiringRequestDataService {
    private hiringRequestData: NewHiringRequestDetails;
    private hiringData: NewHiringRequestDetails;
    private jobDescTemplateData: JobDescriptionDetails;
    private hiringComboData: any;
    private interviewPanelMembers: any;
    private hiringManagers: any;
    private hiringRequestUpdateData: any;
    public mode: string;
    public jobDescMode: string;
    public needNum: any;
    public hiringDepId: any;
    public jobDescId: string;
    public myspacenxApiUrl: string;
    public myspacenxCoreUrl: string;
    private tokenDetails: any;
    private authorizationToken: string;
    private hiringQueryBasePath: string;
    private hiringCommandBasePath: string;
    private prospectQueryBasePath: string;
    private prospectCommandBasePath: string;
    // declaring the variable to notify the data needs to be pass when event is trigger
    public notify = new Subject<any>();
    eventFire$ = this.notify.asObservable();
    constructor(private httpClient: HttpClient,
        public commonService: HiringCommonService,
        private appConstants: AppConstants) {
        this.hiringRequestData = null;
        this.jobDescTemplateData = null;
        this.myspacenxApiUrl = environment.hrbaseUrl;
        this.myspacenxCoreUrl = environment.corebaseUrl;
    }
    setToken() {
        // this.tokenDetails = this.commonService.getItem('tokenDetails');
        // this.authorizationToken = 'Bearer ' + (this.tokenDetails && this.tokenDetails['access_token']);
        this.hiringQueryBasePath = this.appConstants.getConstants().hiringQueryBasePath;
        this.hiringCommandBasePath = this.appConstants.getConstants().hiringCommandBasePath;
        this.prospectQueryBasePath = this.appConstants.getConstants().prospectQueryBasePath;
        this.prospectCommandBasePath = this.appConstants.getConstants().prospectCommandBasePath;
    }

    loadHiringComboData = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        self.commonService.send(this.myspacenxApiUrl + 'hiring/seeddata', 'get', null, header).pipe(map(response => response)).subscribe(data => {
            self.hiringComboData = data;
        }, error => { });
    }

    loadAssetsData = () => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/assetCategories', 'get', null, header).pipe(map(response => response.json()));
    }
    loadITAssetsData = (method: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/' + method, 'get', null, header).pipe(map(response => response.json()));
    }
    loadPanelMembers = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        self.commonService.send(this.myspacenxApiUrl + '/api/core/employees/search/findEmployeesByRole?roleName=Interviewer&projection=employeeprojection', 'get', null, header).pipe(map(response => response.json())).subscribe(data => {
            self.interviewPanelMembers = data;
        }, error => { });
    }
    loadHiringManagers = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        self.commonService.send(this.myspacenxApiUrl + '/api/core/employees/search/findEmployeesByRole?roleName=Hiring Manager&projection=employeeprojection', 'get', null, header).pipe(map(response => response.json())).subscribe(data => {
            self.hiringManagers = data;
        }, error => { });
    }
    getInterviewPanelMembers = () => {
        return (this.interviewPanelMembers) ? Object.assign({}, this.interviewPanelMembers) : undefined;
    }
    getHiringManagerData = () => {
        return (this.hiringManagers) ? Object.assign({}, this.hiringManagers) : undefined;
    }
    getDepartmentMap = () => {
        var depObj = {},
            pracInDep = {},
            result = {};
        this.hiringComboData && this.hiringComboData['DEPARTMENT'].forEach(function (element: any, index: any) {
            depObj[element['id']] = element['name'];
            pracInDep[element['id']] = element['practices'];
        });
        result['depNames'] = depObj;
        result['depPractices'] = pracInDep;
        return result;
    }
    getPracticeMap = () => {
        var pracObj = {},
            compInPrac = {},
            result = {},
            practices: any = [];
        this.hiringComboData && this.hiringComboData['DEPARTMENT'].forEach(function (element: any) {
            practices = practices.concat(element['practices']);
        });
        this.hiringComboData['PRACTICE'] = practices;
        this.hiringComboData && this.hiringComboData['PRACTICE'].forEach(function (element: any, index: any) {
            pracObj[element['id']] = element['name'];
            compInPrac[element['id']] = element['competencies'];
        });
        result['pracNames'] = pracObj;
        result['pracCompetencies'] = compInPrac;
        return result;
    }
    getClientMap = () => {
        var clientObj = {};
        this.hiringComboData && this.hiringComboData['CLIENT'].forEach(function (element: any, index: any) {
            clientObj[element['id']] = element['clientName'];
        });
        return clientObj;
    }
    getHiringCombos = () => {
        return (this.hiringComboData) ? Object.assign({}, this.hiringComboData) : undefined;
    }
    getHiringComboDetails = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + 'hiring/seeddata', 'get', null, header).pipe(map(response => response));
    }
    getCoreComboDetails = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxCoreUrl + 'core/seeddata', 'get', null, header).pipe(map(response => response));
    }
    getAllCombos = () => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + 'hiring/seeddata', 'get', null, header).pipe(map(response => response));
    }
    getPanelMembers = () => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/employees/search/findEmployeesByRole?roleName=Interviewer&projection=employeeprojection', 'get', null, header).pipe(map(response => response.json()));
    }
    getListData = (listName: string) => {
        this.setToken();
        var role: string;
        if (listName == 'interview_panel_list') {
            role = 'Interviewer';
        } else if (listName == 'profile_screening_list') {
            role = 'Profile Screening Committee';
        }
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/employees/search/findEmployeesByRole?roleName=' + role + '&projection=employeeprojection', 'get', null, header).pipe(map(response => response.json()));
    }
    getHiringManagers = () => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + '/api/core/employees/search/findEmployeesByRole?roleName=Hiring Manager&projection=employeeprojection', 'get', null, header).pipe(map(response => response.json()));
    }
    loadHiringRequestData = () => {
        var data = require('../models/hiringRequestInitialData1.json');
        this.hiringRequestData = <NewHiringRequestDetails>data;
        /*var self = this;
        self.httpClient.get('../src/app/hiring-manager/models/hiringRequestInitialData1.json').pipe(map(response => response.json())).subscribe(data => {
            self.hiringRequestData = <NewHiringRequestDetails>data;
        }, error => {});*/
    }
    /* getHiringInitialData(): Promise<NewHiringRequestDetails> {
       return Promise.resolve(HiringRequestInitialData);
     }*/
    loadHiringRequestDataById = (id: string) => {
        /*var self = this;
        self.httpClient.get('../src/app/hiring-manager/models/hiringRequestData1.json').pipe(map(response => response.json())).subscribe(data => {
            self.hiringRequestData = <NewHiringRequestDetails>data;
        }, error => {});*/
        this.setToken();
        var self = this;
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/' + id, 'get', null, header).pipe(map(response => response.json()));
        /*.subscribe(data => {
            self.hiringRequestUpdateData = data;
        }, error => {});*/
    }
    loadHiringRequestDataByUid = (id: string, location: string) => {
        this.setToken();
        var self = this;
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/' + location + '/uid/' + id, 'get', null, header).pipe(map(response => response.json()));
    }
    loadHiringRequestDataByIdandNeed = (id: string, needId: string) => {
        this.setToken();
        var self = this;
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/' + id + '/needs/' + needId, 'get', null, header).pipe(map(response => response.json()));
    }
    saveHiringRequestDetails = (hiringRequestDetails: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests', 'post', hiringRequestDetails, header).pipe(map(response => response));
    }
    saveHiringRequestNeedDetails = (hiringNeedDetails: any, hiringId: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs', 'post', hiringNeedDetails, header).pipe(map(response => response));
    }
    updateHiringRequestNeedDetails = (hiringNeedDetails: any, hiringId: any, needId: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId, 'put', hiringNeedDetails, header).pipe(map(response => response));
    }
    deleteHiringRequestNeedDetails = (hiringId: any, needId: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId, 'delete', null, header).pipe(map(response => response));
    }
    updateHiringRequest = (hiringDetails: any, hiringId: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId, 'put', hiringDetails, header).pipe(map(response => response));
    }
    submitHiringRequest = (hiringId: string, opCode: string, payload?: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/operation/' + opCode, 'put', ((payload) ? payload : null), header).pipe(map(response => response));
    }
    deleteHiringReuest = (hiringId: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId, 'delete', null, header).pipe(map(response => response));
    }
    getHiringRequestUpdateData = () => {
        return (this.hiringRequestUpdateData) ? Object.assign({}, this.hiringRequestUpdateData) : undefined;
    }
    getHiringRequestData = () => {
        return (this.hiringRequestData) ? Object.assign({}, this.hiringRequestData) : undefined;
    }
    updateHiringDetailsData = (detailsFrmHiringRequestData: any) => {
        this.hiringData = detailsFrmHiringRequestData;
    }
    getHiringSummaryData = () => {
        return (this.hiringRequestData) ? Object.assign({}, this.hiringRequestData) : undefined;
    }
    getAllHiringRequests = (pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/all?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getAllHiringRequestsByFilters = (pageNum: number, pageSize: number, status: string, priority: string, department?: string, practice?: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/all?page=' + pageNum + '&size=' + pageSize + ((department) ? ('&departments=' + department) : '') + ((practice) ? ('&practices=' + practice) : '') + ((status) ? ('&status=' + status) : '') + ((priority) ? ('&priority=' + priority) : ''), 'get', null, header).pipe(map(response => response.json()));
    }

    getAllHiringRequestsTimeline = (pageNum: number, pageSize: number, hrqid: string, prospectName: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/timeline?' + ((hrqid) ? ('hrquid=' + hrqid.toUpperCase() + '&') : '') + ((prospectName) ? ('prospectName=' + prospectName + '&') : '') + 'page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }

    getProspectsTimeline = (pageNum: number, pageSize: number, status: string, priority: string, department?: string, practice?: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };

        return this.commonService.send(this.myspacenxApiUrl + this.prospectQueryBasePath + '/prospects/timeline?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getMyHiringRequests = (pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/me?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getMyHiringRequestsByFilters = (pageNum: number, pageSize: number, status: string, priority: string, department?: string, practice?: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/me?page=' + pageNum + '&size=' + pageSize + ((department) ? ('&departments=' + department) : '') + ((practice) ? ('&practices=' + practice) : '') + ((status) ? ('&status=' + status) : '') + ((priority) ? ('&priority=' + priority) : ''), 'get', null, header).pipe(map(response => response.json()));
    }
    /*getAssignedHiringRequests = (pageNum: number, pageSize: number) => {
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl  + this.hiringQueryBasePath + '/hiring/requests/assigned?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }*/
    getAssignedHiringRequestsByFilters = (pageNum: number, pageSize: number, status: string, priority: string, department?: string, practice?: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/assigned?page=' + pageNum + '&size=' + pageSize + ((department) ? ('&departments=' + department) : '') + ((practice) ? ('&practices=' + practice) : '') + ((status) ? ('&status=' + status) : '') + ((priority) ? ('&priority=' + priority) : ''), 'get', null, header).pipe(map(response => response.json()));
    }
    exportMyHiringRequestsByFilters = (status: string, priority: string, department?: string, practice?: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken }, request = ((department) ? ('&departments=' + department) : '') + ((practice) ? ('&practices=' + practice) : '') + ((status) ? ('&status=' + status) : '') + ((priority) ? ('&priority=' + priority) : '');

        request = (request) ? (request.substr(1)) : '';
        request = (request) ? ('?' + request) : '';

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/me/exportCsv' + request, 'get', null, header).pipe(map(response => response));
    }
    exportAllHiringRequestsByFilters = (status: string, priority: string, department?: string, practice?: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken },
            request = ((department) ? ('&departments=' + department) : '') + ((practice) ? ('&practices=' + practice) : '') + ((status) ? ('&status=' + status) : '') + ((priority) ? ('&priority=' + priority) : '');

        request = (request) ? (request.substr(1)) : '';
        request = (request) ? ('?' + request) : '';

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/all/exportCsv' + request, 'get', null, header).pipe(map(response => response));
    }
    exportAssignedHiringRequestsByFilters = (status: string, priority: string, department?: string, practice?: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken },
            request = ((department) ? ('&departments=' + department) : '') + ((practice) ? ('&practices=' + practice) : '') + ((status) ? ('&status=' + status) : '') + ((priority) ? ('&priority=' + priority) : '');

        request = (request) ? (request.substr(1)) : '';
        request = (request) ? ('?' + request) : '';

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/assigned/exportCsv' + request, 'get', null, header).pipe(map(response => response));
    }
    exportData = (method: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };

        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/' + method, 'get', null, header).pipe(map(response => response));
    }
    loadJobDescriptionTemplateData = () => {
        var self = this;
        self.httpClient.get('../src/app/hiring-manager/models/jobDescription.json').pipe(map(response => response)).subscribe(data => {
            self.jobDescTemplateData = <JobDescriptionDetails>data;
        }, error => { });
    }
    loadJobDescTempData = () => {
        var data = require('../models/jobDescription.json');
        this.jobDescTemplateData = (data) ? <JobDescriptionDetails>data : undefined;
    }
    loadJobDescriptionDataById = (jobDescId: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + 'hiring/crud/getAllWithPagination?entityName=JobDescription&fieldName=id&valueToMatch=' + jobDescId, 'get', null, header).pipe(map(response => response));
    }
    /*getJobDescriptionTemplateDetails = () => {
        var self = this;
        return self.httpClient.get('../src/app/hiring-manager/models/jobDescription.json').pipe(map(response => response.json()));
    }*/
    getJobDescriptionTemplateData = () => {
        return (this.jobDescTemplateData) ? Object.assign({}, this.jobDescTemplateData) : undefined;
    }
    getJobDescTempData = () => {
        var data = require('../models/jobDescription.json');
        return (data) ? Object.assign({}, data) : undefined;
        /*return this.httpClient.get('../src/app/hiring-manager/models/jobDescription.json').pipe(map(response => response));*/
    }

    saveJobDescriptionDetails = (jobDescDetails: any) => {
        return this.commonService.send(this.myspacenxApiUrl + 'hiring/crud/create?entityName=JobDescription', 'post', jobDescDetails, null);
    }

    updateJobDescriptionDetails = (jobDescDetails: any, jobDescId: any) => {
        return this.commonService.send(this.myspacenxApiUrl + 'hiring/crud/update?entityName=JobDescription', 'put', jobDescDetails, null);
    }

    getAllJobDescriptions = (pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/jobdescriptions?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }


    getAllJobDescriptionsByFilters = (pageNum: number, pageSize: number, departmentId: string, practiceId: string, isDeprecated: boolean) => {
        this.setToken();
        if (departmentId && practiceId) {
            var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
            return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/jobdescriptions/all?page=' + pageNum + '&size=' + pageSize + '&deprecated=' + isDeprecated + '&departments=' + departmentId + '&practices=' + practiceId, 'get', null, header).pipe(map(response => response.json()));
        } else if (departmentId) {
            var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
            return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/jobdescriptions/all?page=' + pageNum + '&size=' + pageSize + '&deprecated=' + isDeprecated + '&departments=' + departmentId, 'get', null, header).pipe(map(response => response.json()));
        } else {
            var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
            return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/jobdescriptions/all?page=' + pageNum + '&size=' + pageSize + '&deprecated=' + isDeprecated, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    loadJobDescriptionTemplateName = (depId: any, pracId: any, pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/jobdescriptions/templatenames?practiceId=' + pracId + '&departmentId=' + depId + '&deprecated=false' + '&page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response));
    }
    startHiringRequest = (hiringId: string, opCode: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/operation/' + opCode, 'put', null, header).pipe(map(response => response));
    }
    getAllProfilesByHiringId = (id: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?hiringRequestId=' + id, 'get', null, header).pipe(map(response => response.json()));
    }
    getAllProfilesByHiringIdAndNeed = (id: string, needId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?hiringRequestId=' + id + '&needId=' + needId, 'get', null, header).pipe(map(response => response.json()));
    }
    getAllProfilesByFilters = (pageNum: number, pageSize: number, noticePeriod: string, status: string, name: string = '') => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };

        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&statusCodes=' + status + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    getAllProfilesByHiringIdAndFilters = (pageNum: number, pageSize: number, id: string, noticePeriod: string, status: string, name: string = '') => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };

        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + '&statusCodes=' + status + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + '&noticePeriods=' + noticePeriod + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + `${name != '' ? `&displayName=${name}` : ''}`, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    getAllProfilesByHiringIdAndNeedAndFilters = (pageNum: number, pageSize: number, id: string, needId: string, noticePeriod: string, status: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };

        if (status !== "ALL" && noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + '&needId=' + needId + '&statusCodes=' + status + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (status !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + '&needId=' + needId + '&statusCodes=' + status, 'get', null, header).pipe(map(response => response.json()));
        }
        else if (noticePeriod !== "ALL") {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + '&needId=' + needId + '&noticePeriods=' + noticePeriod, 'get', null, header).pipe(map(response => response.json()));
        }
        else {
            return this.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/screening?page=' + pageNum + '&size=' + pageSize + '&hiringRequestId=' + id + '&needId=' + needId, 'get', null, header).pipe(map(response => response.json()));
        }
    }
    getNeedsByHiringId = (hiringId: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/' + hiringId + '/needs/purpose', 'get', null, header).pipe(map(response => response.json()));
    }
    updateProfileStatus = (profileId: string, opCode: string, payload: any) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.prospectCommandBasePath + '/prospects/' + profileId + '/operation/' + opCode, 'put', payload, header).pipe(map(response => response));
    }
    getMetrics = (year: any, quarter: any, month: any, practiceId: any) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        if (year) {
            if (!quarter && !month && !practiceId) {
                return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/dashboardMetrics?year=' + year, 'get', null, header).pipe(map(response => response.json()));
            }
            else {
                if (month) {
                    if (practiceId) {
                        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/dashboardMetrics?year=' + year + '&month=' + month + '&practiceId=' + practiceId, 'get', null, header).pipe(map(response => response.json()));
                    }
                    else {
                        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/dashboardMetrics?year=' + year + '&month=' + month, 'get', null, header).pipe(map(response => response.json()));
                    }
                }
                else {
                    if (quarter) {
                        if (practiceId) {
                            return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/dashboardMetrics?year=' + year + '&quarter=' + quarter + '&practiceId=' + practiceId, 'get', null, header).pipe(map(response => response.json()));
                        }
                        else {
                            return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/dashboardMetrics?year=' + year + '&quarter=' + quarter, 'get', null, header).pipe(map(response => response.json()));
                        }
                    }
                    else {
                        return self.commonService.send(this.myspacenxApiUrl + self.hiringQueryBasePath + '/hiring/requests/dashboardMetrics?year=' + year + '&practiceId=' + practiceId, 'get', null, header).pipe(map(response => response.json()));
                    }
                }
            }
        }

        return null;
    }
    getPractices = () => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': self.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + '/api/core/departments', 'get', null, header).pipe(map(response => response.json()));
    }
    loadProspectDataById = (id: string) => {
        this.setToken();
        var self = this, header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return self.commonService.send(this.myspacenxApiUrl + self.prospectQueryBasePath + '/prospects/' + id, 'get', null, header).pipe(map(response => response.json()));
    }
    getHiringTimeline = (hiringId: string, pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/' + hiringId + '/timeline?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    getOpenHiringRequests = (pageNum: number, pageSize: number) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/public/all?page=' + pageNum + '&size=' + pageSize, 'get', null, header).pipe(map(response => response.json()));
    }
    applyRequest = (hiringId: string, needId: string) => {
        this.setToken();
        var header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/needs/' + needId + '/apply', 'post', null, header).pipe(map(response => response));
    }

    /*
    * Gets the statistics about a hiring request
    * @param hiringId: hiring request id
    */
    getHiringRequestDetails = (hiringId: string) => {
        this.setToken();
        const header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringQueryBasePath + '/hiring/requests/' + hiringId + '/stats', 'get', null, header).pipe(map(response => response));
    }

    /*
    * Marks a hiring request as completed
    * @param hiringId: hiring request id
    */
    closeHiringRequest = (hiringId: string) => {
        this.setToken();
        const header = { 'Content-Type': 'application/json', 'Authorization': this.authorizationToken };
        return this.commonService.send(this.myspacenxApiUrl + this.hiringCommandBasePath + '/hiring/requests/' + hiringId + '/operation/COMPLETE', 'put', null, header).pipe(map(response => response));
    }




    // ***************************************** New

    getAll(): Observable<any> {
        return this.commonService.send(this.myspacenxApiUrl + 'hiring/crud/getAllWithPagination?entityName=JobDescription', 'get', null, null).pipe(map(response => response));
    }

    save(apiUrl: string, payload: any): Observable<any> {
        let method = payload.id == 0 ? 'create' : 'update';

        if (method == 'create') {
            delete payload.id;
            return this.httpClient.post(`${this.myspacenxApiUrl}${apiUrl}`, payload);
        }
        else
            return this.httpClient.put(`${this.myspacenxApiUrl}${apiUrl}`, payload);

    }

    put(apiUrl: string, payload: any): Observable<any> {
        return this.httpClient.put(`${this.myspacenxApiUrl}${apiUrl}`, payload);
    }

    getWithPagination(entityName: string, pageIndex: number, pageSize: number, colName: string = '', search: string = ''): Observable<any> {
        let apiUrl = `hiring/crud/getAllWithPagination?entityName=${entityName}&pageNumber=${pageIndex}&pageSize=${pageSize}&fieldName=${colName}&valueToMatch=${search}`;
        return this.httpClient.get(`${this.myspacenxApiUrl}${apiUrl}`);
    }

    getAllCrud(apiUrl: string): Observable<any> {
        ;
        return this.httpClient.get(`${this.myspacenxApiUrl}${apiUrl}`);
    }

    delete(entityName: string, payload: any): Observable<any> {
        let apiUrl = `hiring/crud/delete?entityName=${entityName}&ids=${payload}`;
        return this.httpClient.delete(`${this.myspacenxApiUrl}${apiUrl}`);
    }

    getCore(apiUrl: string): Observable<any> {
        ;
        return this.httpClient.get(`${this.myspacenxCoreUrl}${apiUrl}`);
    }
}
