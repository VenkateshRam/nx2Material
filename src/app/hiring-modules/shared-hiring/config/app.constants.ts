import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable()
export class AppConstants {
    private constants = {
        uiSkills: [ 'Angular JS', 'Angular 2', 'React', 'Native Script', 'NodeJS', 'Grunt', 'Webpack', 'Bower'],
        uiTools: [ 'UI Tool1', 'UI Tool2', 'UI Tool3', 'UI Tool4'],
        // destinationURL: 'http://192.168.204.204',
        destinationURL: 'http://192.168.204.212',
        prospectQueryBasePath: '/api/q/prospectivehires',
        prospectCommandBasePath: '/api/c/prospectivehires',
        hiringQueryBasePath: 'core',
        hiringCommandBasePath: 'core',
        myspacenxApiUrl: environment.hrbaseUrl,
        lmsUrl:'https://192.168.204.212:8561'
    };
    private host = {
       "hostname": "localhost",
       "port": "8080",
       "baseUrl": "localhost:8080"
    };
    getConstants(){
        return this.constants;
    }
    getHost(){
        return this.host;
    }
}
