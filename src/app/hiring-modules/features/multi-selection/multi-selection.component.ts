// styleUrls: ['./multi-selection.component.scss']
import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectionListChange } from '@angular/material/list';
import { LoaderService } from '../../../shared/services/loader.service';
import { HiringRequestDataService } from '../../shared-hiring/services/hiring-request-data.service';

@Component({
  selector: 'app-multi-selection',
  templateUrl: './multi-selection.component.html',
  styleUrls: ['./multi-selection.component.scss']
})

export class MultiSelectionComponent implements OnInit, OnChanges {
  constructor(private loaderService: LoaderService,
    private hiringReqService: HiringRequestDataService,
    public dialogRef: MatDialogRef<MultiSelectionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }
  display: boolean = false;
  @Input() items: any;
  resetItems: any;
  @Input() list: any;
  @Input() selectedList: any = [];
  @Input() searchTitle: string;
  @Input() title: string;
  @Input() listName: string;
  @Output() loadSelectedItems = new EventEmitter<any>();
  alreadySelectedItems: any = [];
  public isLoaded: boolean = false;
  disable: boolean = true;
  search: string;

  selectedItems: Array<any> = [];
  ngOnInit(): void {
    if (this.data) {
      this.title = this.data.title;
      this.selectedItems = this.data.selectedList ?? [],
        this.searchTitle = this.data.searchTitle;
      this.items = this.data.data;
    }
  }

  showDialog() {
    if (this.listName) {
      this.hiringReqService.getListData(this.listName).subscribe(data => {
        var listData: any = [];
        if (data['_embedded'] && data['_embedded']['employees']) {
          data['_embedded']['employees'].forEach(function (ele: any, ind: any) {
            listData.push({
              'id': ele.id,
              'name': ele.fullName
            });
          });
          this.items = listData;
          this.removeSelectedItems();
        }
        this.display = true;
      }, error => {
        this.display = false;
        setTimeout(() => {
          alert(error.message);
        }, 0);
      });
    } else {
      this.items = this.list;
      this.removeSelectedItems();
      setTimeout(() => {
        // this.loaderService.hideLoading('overlay', '#overlay');
        // this.isLoaded = false;
        this.display = true;
      }, 1000)
    }
  }
  itemSelected() {
    setTimeout(() => {
      var i = 0;
      this.items.forEach((element: any, indx: any) => {
        if (element.checked == true) {
          i = 1;
        }
      });
      if (i == 1) {
        this.disable = false;
      }
      else {
        this.disable = true;
      }
    });
  }
  reset() {
    this.disable = true;
    this.items.forEach((element: any, indx: any) => {
      if (element.checked == true) {
        element.checked = false;
      }
    });

  }
  addSelection(value: any) {
    this.loadSelectedItems.emit(this.getSelectedItems());
    this.display = false;
  }
  getSelectedItems(): any {
    var selectedItems: string[] = [],
      selectedNames: string[] = [];
    this.items.forEach(function (element: any, indx: any) {
      if (element.checked == true) {
        selectedNames.push(element.name);
        selectedItems.push(element);
        element.checked = false;
      }
    });
    return { 'items': selectedItems, 'itemNames': selectedNames };
  }

  removeSelectedItems() {
    let data: any = [];
    if (this.items && this.alreadySelectedItems != null && this.alreadySelectedItems && this.alreadySelectedItems.length > 0) {
      this.items.forEach((e: any) => {
        if (!(this.alreadySelectedItems.find((f: any) => f == e.name)))
          data.push(e);
      });
      this.items = [...data];
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property === 'selectedList') {
        this.alreadySelectedItems = changes[property].currentValue;
        this.removeSelectedItems();
      }
    }
  }

  onAdd() {
    // console.log(this.selectedItems)
    this.dialogRef.close({ status: 'OK', list: this.items, data: this.selectedItems });
  }

  selectionChanged($event: MatSelectionListChange) {
    if ($event.option.selected) {
      this.selectedItems.push($event.option.value)
      this.selectedItems = [...new Set(this.selectedItems)];
    }
    else {
      let index = this.selectedItems.indexOf($event.option.value);
      if (index !== -1 && this.selectedItems.length > 0)
        this.selectedItems.splice(index, 1);
    }
  }

  checkSelected(id: any) {
    return this.selectedItems.find(f => f == +id);
  }
}

