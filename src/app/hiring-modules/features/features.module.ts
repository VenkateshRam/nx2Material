import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesComponent } from './features.component';
import { HiringTimelineComponent } from './hiring-timeline/hiring-timeline.component';
import { HrReportComponent } from './hr-report/hr-report.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ItReportComponent } from './it-report/it-report.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminFunctionsComponent } from './admin-functions/admin-functions.component';
import { BaseComponent } from './base/base.component';

@NgModule({
  declarations: [
    FeaturesComponent,
    HiringTimelineComponent,
    HrReportComponent,
    ItReportComponent,
    DashboardComponent,
    AdminFunctionsComponent,
    BaseComponent,
  ],
  imports: [
    CommonModule,
    FeaturesRoutingModule,
    MaterialModule
  ]
})
export class FeaturesModule { }
