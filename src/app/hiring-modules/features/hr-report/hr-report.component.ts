import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hr-report',
  templateUrl: './hr-report.component.html',
  styleUrls: ['./hr-report.component.scss']
})
export class HrReportComponent implements OnInit {


  displayedColumns = ['s_no', 'st', 'name', 'des', 'rm', 'skill', 'pract', 'wl', 'ord',  'doj', 'c_doj', 'src', 'jm', 'cl', 'pl', 'ac', 'addConf', 'ctc', 'exp', 'contact'];
  constructor() { }

  ngOnInit(): void {
  }

}
