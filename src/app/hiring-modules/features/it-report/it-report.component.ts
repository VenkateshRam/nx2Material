import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-it-report',
  templateUrl: './it-report.component.html',
  styleUrls: ['./it-report.component.scss']
})
export class ItReportComponent implements OnInit {

  displayedColumns = ['s_no', 'st', 'name', 'des', 'rm', 'skill', 'pract', 'wl', 'ord',  'doj', 'c_doj', 'src', 'jm', 'cl', 'pl', 'ac', 'addConf', 'ctc', 'exp', 'contact'];
  constructor() { }

  ngOnInit(): void {
  }

}
