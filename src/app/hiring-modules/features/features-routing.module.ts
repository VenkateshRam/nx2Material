import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminFunctionsComponent } from './admin-functions/admin-functions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FeaturesComponent } from './features.component';
import { HiringTimelineComponent } from './hiring-timeline/hiring-timeline.component';
import { HrReportComponent } from './hr-report/hr-report.component';
import { ItReportComponent } from './it-report/it-report.component';

const routes: Routes = [{ path: '', component: FeaturesComponent },
{ path: 'hiring-timeline', component: HiringTimelineComponent },
{ path: 'hr-report', component: HrReportComponent },
{ path: 'it-report', component: ItReportComponent },
{ path: 'dashboard', component: DashboardComponent },
{ path: 'admin', component: AdminFunctionsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
