import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hiring-timeline',
  templateUrl: './hiring-timeline.component.html',
  styleUrls: ['./hiring-timeline.component.scss']
})
export class HiringTimelineComponent implements OnInit {

   displayedColumns = [ 'id', 'dept', 'cd', 'cb', 'at', 'pf'];
  constructor() { }

  ngOnInit(): void {
  }

}
