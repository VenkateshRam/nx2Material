import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { AlertService } from 'src/app/shared/services/alert.service';
import { MultiSelectionComponent } from '../../features/multi-selection/multi-selection.component';
import { HiringCommonService } from '../../shared-hiring/services/hiring-common.service';
import { HiringRequestDataService } from '../../shared-hiring/services/hiring-request-data.service';

@Component({
  selector: 'app-create-hiring-request',
  templateUrl: './create-hiring-request.component.html',
  styleUrls: ['./create-hiring-request.component.scss']
})
export class CreateHiringRequestComponent implements OnInit {

  @Input() selectedRow: any;
  @Input() viewMode: boolean = false;
  @Output() messageEvent = new EventEmitter<any>();


  createHiringRequest: FormGroup;

  departmentOptions: Array<any> = []; practiceOptions: Array<any> = []; templatesLst: Array<any> = [];
  screeningList: Array<any> = []; criteria: Array<any> = []; panelMembers: Array<any> = []; panelGrid: Array<any> = [];
  rolesLst: Array<any> = []; interviewCriteriaLst: Array<any> = []; hiringForOptions: Array<any> = [];
  clientNameOptions: Array<any> = []; hiringTypeOptions: Array<any> = []; priorityOptions: Array<any> = [];
  opModelLst: Array<any> = []; competencyOptions: Array<any> = []; interviewerLst: Array<any> = [];
  hiringManagerLst: Array<any> = []; screeningMembersLst: Array<any> = [];

  departmentId: string = ''; practiceId: string = ''; templateId: string = ''; interviewLevelLst: Array<any> = [];
  jobCompBg: string = ''; jobPracBg: string = ''; jobds: string = ''; jobresp: string = ''; jobDsSkill: string = '';
  jobTsSkill: string = ''; jobCert: string = ''; jobOtherSkill: string = ''; jobEdu: string = ''; jobScrnQs: string = '';
  level: string = ''; panelName: string = '';

  companyOverview: any = `Innominds is an AI-first, platform-led digital transformation and full cycle product engineering services company headquartered in San Jose, CA. Innominds powers the Digital Next initiatives of global enterprises, software product companies, OEMs and ODMs with integrated expertise in devices & embedded engineering, software apps & product engineering, analytics & data engineering, quality engineering, and cloud & devops, security. It works with ISVs to build next-generation products, SaaSify, transform total experience, and add cognitive analytics to applications

  For more information please visit:
  www.innominds.com
  Thank you.`;

  templateInfo: any; panelEditData: any; showJdInfo: boolean = false;
  panelSubmitted: boolean = false; formSubmitted: boolean = false;

  today = new Date();

  constructor(private fb: FormBuilder,
    private hiringRequestDataService: HiringRequestDataService
    , private commonService: HiringCommonService
    , private alertSvc: AlertService
    , private datePipe: DatePipe
    , private dialog: MatDialog) {
  }

  ngOnInit() {
    this.formInit();
    this.getMasters();
  }

  formInit() {
    this.createHiringRequest = this.fb.group({
      clientId: [null, Validators.required],
      departmentId: [null, Validators.required],
      hiringManagerId: [null, Validators.required],
      priorityId: [null, Validators.required],
      hiringReasonId: [null, Validators.required],
      hiringType: ['R', Validators.required],
      purpose: ['', Validators.required],
      profileScreeningMemberCodes: [[]],
      fasttrack: [false],
      isPublic: [true],
      competencyId: ['', Validators.required],
      expectedRole: ['', Validators.required],
      expectedStartDate: ['', Validators.required],
      interviewPanels: [[]],
      jobDescriptionId: [null],
      maxExperience: [null, Validators.required],
      minExperience: [null, Validators.required],
      operatingModelId: [null, Validators.required],
      practiceId: [null, Validators.required],
      resourcesCount: [null, Validators.required],
      workLocationId: [null, Validators.required],
      id: [0],
      // hiringRequestId: [null, Validators.required],
      // "statusId": 3,
      // "comments": "TEST",
      // "startDate": "03-Jun-2022",
      // "closeDate": null,
      // "processedProfilesCount": 3,
      // "releasedOffersCount": 0,
      // "joinedResourcesCount": 0,
      // "uid": "HRQ60",
      // "skillSummary": "TEST",
    });
  }

  get f() { return this.createHiringRequest?.controls; }

  postData() {
    console.log(this.createHiringRequest);
  }

  loadTemplates() {
    this.departmentId = this.createHiringRequest.value.departmentId;
    this.practiceId = this.createHiringRequest.value.practiceId;
    this.loadCompetencyByPractices(this.practiceId);
    this.getTemplates();
  }

  getTemplates() {
    let self = this;

    if (!this.departmentId || !this.practiceId) {
      this.alertSvc.showToast('Please select Department and Practice', 'warning');
      return;
    }
    self.hiringRequestDataService.getAllCrud(`hiring/hiringRequest/searchJobDescriptions?pageNumber=0&pageSize=1000&deprecated=false&departments=${this.departmentId}&practices=${this.practiceId}`).subscribe(res => {
      if (res) {
        let data = res.data;
        this.templatesLst = (data ? data.content : []);

        if (data && data.content.length == 0)
          this.alertSvc.showToast('No Templates Found', 'info');

        if (this.selectedRow) {
          this.templateId = this.selectedRow.jobDescriptionId;
          this.onTemplateChange(this.templateId);
          this.onJobSelection();
        }
      }
    });
  }

  getMasters() {
    let self = this;
    this.hiringRequestDataService.getAllCrud(`hiring/crud/getAll?entityName=Role`).subscribe(res => {
      if (res) {
        this.rolesLst = res ? res.data : [];
        let apis: Array<any> = [];
        let interviewer = this.rolesLst.find(f => f.name.toLowerCase().trim() == 'interviewer')?.id;
        let hm = this.rolesLst.find(f => f.name.toLowerCase().trim() == 'hiring manager')?.id;
        let sc = this.rolesLst.find(f => f.name.toLowerCase().trim() == 'profile screening committee')?.id;
        if (interviewer)
          apis.push(this.hiringRequestDataService.getAllCrud(`employee/business/getEmployeesByRole?roleId=${interviewer}`));
        if (hm)
          apis.push(this.hiringRequestDataService.getAllCrud(`employee/business/getEmployeesByRole?roleId=${hm}`));
        if (sc)
          apis.push(this.hiringRequestDataService.getAllCrud(`employee/business/getEmployeesByRole?roleId=${sc}`));
          apis.push(this.hiringRequestDataService.getCore('core/seeddata'));
          apis.push(this.hiringRequestDataService.getAllCrud('hiring/seeddata'));
        forkJoin(...apis).subscribe(
          {
            next: (res: any) => {
              res.forEach((e, ind) => {
                if (e.status === 'OK') {
                  const assignList = {
                    '0': () => {
                      e.data && e.data.forEach(e => {
                        this.interviewerLst.push({ id: e.id, name: e.employeeName });
                      });
                    },
                    '1': () => this.hiringManagerLst = e.data,
                    '2': () => {
                      e.data && e.data.forEach(e => {
                        this.screeningMembersLst.push({ id: e.id, name: e.employeeName });
                      });
                    },
                    '3' : () => {
                      let data = e.data;
                      self.departmentOptions = data['DEPARTMENT'];
                      self.clientNameOptions = data['CLIENT'];
                    },
                    '4' : () => {
                      let data = e.data;
                      self.interviewLevelLst = data['INTERVIEW_LEVEL'];
                      self.hiringForOptions = data['HIRING_REASON'];
                      self.hiringTypeOptions = data['HIRING_TYPE'];
                      self.priorityOptions = data['HIRING_PRIORITY'];
                      self.opModelLst = data['OPERATING_MODEL'];
                      data['INTERVIEW_CRITERIA'].forEach((e: any) => {
                        this.interviewCriteriaLst.push({ id: e.id, name: e.title });
                      });
                    },
                  }
                  assignList[ind]();
                }
              });
            },
            error: err => this.alertSvc.showToast('something wrong occurred', 'error'),
            complete: () => {
              if (this.selectedRow)
                this.onEdit(this.selectedRow);
            }
          });
      }
    });
  }

  loadPracticesByDepartment(id: any) {
    this.practiceOptions = this.departmentOptions.find(f => f.id == +id)?.practices ?? [];
    this.practiceId = <any>"";
  }

  loadCompetencyByPractices(id: any) {
    console.log(id)
    this.competencyOptions = this.practiceOptions.find(f => f.id == +id)?.competencies ?? [];
  }

  onTemplateChange(id: any) {
    this.templateInfo = JSON.parse(this.templatesLst.find(f => f.id == id)?.details);
  }

  onJobSelection() {
    this.clearJD();

    if (this.templateInfo) {
      this.jobCompBg = this.templateInfo.companyOverview ? this.templateInfo.companyOverview : this.companyOverview;
      this.jobPracBg = this.templateInfo.practiceOverview;
      this.jobds = this.templateInfo.jobDesc;
      this.jobresp = this.templateInfo.responsibilities;
      this.jobDsSkill = this.templateInfo.skills.uxDesignSkills?.map(f => f.display).join(',');
      this.jobTsSkill = this.templateInfo.skills.technicalSkills?.map(f => f.display).join(',');
      this.jobCert = this.templateInfo.skills.certifications?.map(f => f.display).join(',');
      this.jobOtherSkill = this.templateInfo.skills.others;
      this.jobEdu = this.templateInfo.education;
      this.jobScrnQs = this.templateInfo.screeningQuestions;
    }
    this.showJdInfo = this.templateInfo ? true : false;

  }

  clearJD() {
    this.jobCompBg = ''; this.jobPracBg = ''; this.jobds = ''; this.jobresp = ''; this.jobDsSkill = '';
    this.jobTsSkill = ''; this.jobCert = ''; this.jobOtherSkill = ''; this.jobEdu = ''; this.jobScrnQs = '';
  }

  openSelectionModal(type: string) {
    const dialogRef = this.dialog.open(MultiSelectionComponent, {
      width: '400px',
      data: {
        title: type == 'screening' ? "Profile Screening Members" : type == 'panel' ? 'Panel Members' : 'Selection Criteria',
        selectedList: type == 'screening' ? this.screeningList : type == 'panel' ? this.panelMembers : this.criteria,
        data: type == 'screening' ? this.screeningMembersLst : type == 'panel' ? this.interviewerLst : this.interviewCriteriaLst,
        searchTitle: "screening member name"
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.status == 'OK') {

        console.log(result.data);
        let [lst, selectedLst] = [result.list, result.data];
        if (type == 'screening')
          this.screeningList = [];
        else if (type == 'panel')
          this.panelMembers = [];
        else if (type == 'criteria')
          this.criteria = [];

        selectedLst.forEach(e => {
          if (type == 'screening')
            this.screeningList.push(e);
          else if (type == 'panel')
            this.panelMembers.push(e);
          else if (type == 'criteria')
            this.criteria.push(e);
        });
      }

      // if (result == 'OK')
      //   this.loadPendingReqs();
    });
  }

  addPanel() {
    this.panelSubmitted = true;

    if (!this.level || !this.panelName || this.panelMembers.length == 0 || this.criteria.length == 0) {
      this.alertSvc.showToast('Please Select All Mandatory Fields', 'warning');
      return;
    }

    if (this.panelEditData) {
      this.panelGrid.splice(this.panelEditData.index, 1);
    }

    this.panelGrid.push({ name: this.panelName, level: this.level, members: this.panelMembers, criteria: this.criteria });
    console.log(this.panelGrid)
    this.clearPanel();
  }

  clearPanel() {
    this.panelSubmitted = false;
    this.level = ''; this.panelName = ''; this.panelMembers = []; this.criteria = [];
    this.panelEditData = null;
  }

  onPanelAction(data: any, action: string, index: number) {
    if (action == 'edit') {
      this.panelEditData = { i: index, obj: data };
      this.level = data.level; this.panelName = data.name;
      this.panelMembers = data.members; this.criteria = data.criteria;
    }
    else
      this.panelGrid.splice(index, 1);
  }

  getNames(str: any, type: string) {
    let names = [];

    if (str && Array.isArray(str)) {
      str.forEach(e => {
        names.push((type == 'criteria' ? this.interviewCriteriaLst : this.interviewerLst).find((f: any) => f.id == e)?.name);
      });
      return names.join(',');
    } else if (str)
      return (type == 'criteria' ? this.interviewCriteriaLst : this.interviewerLst).find((f: any) => f.id == str)?.name;
  }

  getTagNames(id: any, type: string) {
    return id ? (type == 'criteria' ? this.interviewCriteriaLst : type == 'screening' ? this.screeningMembersLst : this.interviewerLst).find(f => f.id == +id).name : '';
  }

  getLevelName(id: any) {
    return id ? this.interviewLevelLst.find(f => f.id == +id)?.description : null;
  }

  onLevelChange(id: any) {
    this.panelName = this.interviewLevelLst.find(f => f.id == +id).description + ' Round - Panel';
  }

  onSave() {
    this.formSubmitted = true;
    if (this.createHiringRequest.invalid) {
      // Common.getFormValidationErrors(this.createHiringRequest)
      this.alertSvc.showToast('Please select all Mandatory Fields', 'warning');
      return;
    } else if (!this.templateId) {
      this.alertSvc.showToast('Please select a Job Description', 'warning');
      return;
    } else if (this.panelGrid.length == 0) {
      this.alertSvc.showToast('Please add atleast one interview panel', 'warning');
      return;
    }
    // let finalLevelid = this.interviewLevelLst.find(f => f.description.toLowerCase().trim() == 'final');
    // if (!this.panelGrid.find(f => f.level == finalLevelid)) {
    //   this.alertSvc.showToast('Please select a Final Round', 'warning');
    // }

    let payload = this.createHiringRequest.getRawValue();
    payload.expectedStartDate = this.datePipe.transform(this.createHiringRequest.value.expectedStartDate, 'yyyy-MM-dd');
    payload.jobDescriptionId = this.templateId;
    payload.interviewPanels = this.panelGrid;
    payload.profileScreeningMemberCodes = this.screeningList;

    if(payload.id == 0)
    delete payload.id;
    this.hiringRequestDataService.put(`hiring/hiringRequest/hiringRequestAction?action=Create`, payload).subscribe(res => {
      if (res) {
        this.alertSvc.showToast('Hiring Request created successfully', 'success');
        this.clearForm();
      }
    })
  }

  restrictExpCharcters(value: any) {
    return this.commonService.restrictExpCharcters(value);
  }

  restrictFloatnumber(e: any) {
    return this.commonService.restrictFloatnumber(e);
  }

  clearForm() {
    this.formSubmitted = false;
    this.formInit();
    // this.createHiringRequest.markAsUntouched();
    this.criteria = [];
    this.screeningList = [];
    this.panelMembers = [];
    this.panelGrid = [];
    this.templateId = '';
    this.panelSubmitted = false;
    this.showJdInfo = false;
  }

  onClose(): void {
    this.messageEvent.emit({ load: false });
  }

  onEdit(data: any) {
    this.loadPracticesByDepartment(data.departmentId);
    this.loadCompetencyByPractices(data.practiceId);
    this.createHiringRequest.patchValue(data);
    this.departmentId = data.departmentId;
    this.practiceId = data.practiceId;
    this.getTemplates();
    this.panelGrid = data.interviewPanels;
  }
}

