import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationService } from "primeng/api";
import { AlertService } from "src/app/shared/services/alert.service";
import { HiringRequestDataService } from "../../shared-hiring/services/hiring-request-data.service";

@Component({
  selector: 'app-assigned-requests',
  templateUrl: './assigned-requests.component.html',
  styleUrls: ['./assigned-requests.component.scss']
})
export class AssignedRequestsComponent implements OnInit {

  displayedColumns = ['id', 'hm', 'purpose', 'department', 'practice', 'ct', 'cn', 'model', 'status', 'ht', 'priority', 'res', 'cd', 'rsdate'];
  public departmentOptions: any[];
  public practiceOptions: any[];
  public priorityOptions: any[];
  public statusOptions: any[];
  columnDefs: any;
  disableShedule: boolean = true;
  public isLoaded: boolean = false;
  allOfTheData: any;
  recordsCount: number;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  public filterData: any = {};
  searchId: string;
  selection: any;
  commonService: any;
  constructor( private router: Router, 
    private route: ActivatedRoute, 
    private hiringRequestDataService: HiringRequestDataService, 
    private AlertSvc: AlertService,
    private confirmationService: ConfirmationService) {
    this.priorityOptions = [];
    this.statusOptions = [];
    this.filterData.department = "";
    this.filterData.practice = "";
    this.filterData.priority = "";
    this.filterData.status = "";
  }
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
  formatId(id: any) {
    return 'HR' + id.value.split('-')[0].toUpperCase();
  }
  formatDate(data: any) {
    var time = new Date(data.value),
      result, year, month, today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    result = today + '/' + month + '/' + year;
    return result;
  }
  dateComparator(date1: any, date2: any) {
    var monthToComparableNumber = function (date: any) {
      if (date === undefined || date === null || date.length !== 10) {
        return null;
      }
      var yearNumber = date.substring(6, 10);
      var monthNumber = date.substring(3, 5);
      var dayNumber = date.substring(0, 2);

      var result = (yearNumber * 10000) + (monthNumber * 100) + dayNumber;
      return result;
    };
    var date1Number = monthToComparableNumber(date1);
    var date2Number = monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  onCellClicked(value: any) {
    this.setModes();
  }
  setModes() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      this.disableShedule = false;
    } else {
      this.disableShedule = true;
    }
  }
  onGridInitialize() {
    this.loadCombos();
  }
  loadCombos(){
    var data = this.hiringRequestDataService.getHiringCombos(), self = this;
    if (data) {
      this.departmentOptions = data['DEPARTMENT'];
      this.priorityOptions = data['HIRING_PRIORITY'];
      this.statusOptions = data['HIRING_REQUEST_STATUS'];
      //this.filterStatusOptions();
      this.loadGrid();
    }
    else {
      self.hiringRequestDataService.getHiringComboDetails().subscribe(data=>{
        self.departmentOptions = data['DEPARTMENT'];
        self.priorityOptions = data['HIRING_PRIORITY'];
        self.statusOptions = data['HIRING_REQUEST_STATUS'];
        //self.filterStatusOptions();
        self.loadGrid();
      },error =>{
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible:false,
          accept : ()=>{
            
          }
        });
      });
    }
  }
  loadPracticesByDepartment(){
    var depId = this.filterData.department;
    this.practiceOptions = this.hiringRequestDataService.getDepartmentMap()['depPractices'][depId];
    this.filterData.practice = <any>"";
    this.filterChanged();
  }
  filterChanged(){
    this.pageNum = 0;
    this.loadGrid();
    this.searchId = '';
  }
  loadGrid() {
    var self = this;
    setTimeout(() => {
      this.hiringRequestDataService.getAssignedHiringRequestsByFilters(self.pageNum, self.pageSize, self.filterData.status, self.filterData.priority, self.filterData.department, self.filterData.practice).subscribe(data => {
       // self.setRowData(data['content']);
        self.recordsCount = data.totalElements;
        self.totalPages = data.totalPages;
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false,
          accept : ()=>{
            
          }
        });
      });
    }, 100);
  }
  exportData() {
    var self = this;
    setTimeout(() => {
      self.hiringRequestDataService.exportAssignedHiringRequestsByFilters(self.filterData.status, self.filterData.priority, self.filterData.department, self.filterData.practice).subscribe(data => {
        self.commonService.downloadCsv('Assigned Hiring Requests', data['_body']);
      }, error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false,
            accept : ()=>{
            }
          });
      });
    }, 100);
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }

  navigateToHiringRequestDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      //this.recordNotSelected = false;
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../view', id], { relativeTo: this.route });
    } else {
      //this.recordNotSelected = true;
    }
  }
  showHiringTimeline(){
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/hiring-requests/hiring-timeline', id]);
    }
  }
  SearchById(){ 
    var fetcheddata : any =[];
    if(this.searchId.length > 3){
      this.hiringRequestDataService.loadHiringRequestDataByUid(this.searchId.toUpperCase(),"assigned").subscribe(data=>{
        fetcheddata.push(data);
        this.pageNum =0;
        this.pageSize = 20;
        this.totalPages = 1;
        this.recordsCount = fetcheddata.length;
        },error=>{           
            this.recordsCount =0;
            this.pageNum = 0;
            this.totalPages = 1;
        });
               
      } else if(this.searchId == "") {        
        this.setPageNum(0);
      }
    }
}