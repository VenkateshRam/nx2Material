import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { HiringCommonService } from '../../shared-hiring/services/hiring-common.service';
import { HiringRequestDataService } from '../../shared-hiring/services/hiring-request-data.service';

@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.scss']
})
export class JobDescriptionComponent implements OnInit {

  displayedColumns = ['dn', 'pn', 'jdt','onr'];
  columnDefs: any;
  allOfTheData: any;
  recordNotSelected: boolean = false;
  pageNum: number = 0;
  pageSize: number = 10;
  totalPages: number;
  public departmentMap: any;
  public practiceMap: any;
  public recordsCount: number;
  public departmentOptions: any;
  public practiceOptions: any;
  public isDisable:boolean = true;
  public isLoaded: boolean = false;
  public filterData: any = {};

  length: number; pageIndex: number = 0; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);

  constructor(private router: Router, 
    private route: ActivatedRoute, 
    private hiringRequestDataService: HiringRequestDataService, 
    private commonService: HiringCommonService, 
    private confirmationService: ConfirmationService){
   this.filterData.isDeprecated = false;
 }

  ngOnInit() {
    var self = this;
      self.hiringRequestDataService.getCoreComboDetails().subscribe(res => {
      let data = res.data;
        self.departmentOptions = data['DEPARTMENT'];
        self.practiceOptions = data['PRACTICE'];
      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false,
          accept: () => {

          }
        });
      });
    this.loadGrid();
  }

  dateComparator(date1: any, date2: any) {
    var monthToComparableNumber = function (date: any) {
      if (date === undefined || date === null || date.length !== 10) {
        return null;
      }

      var yearNumber = date.substring(6, 10);
      var monthNumber = date.substring(3, 5);
      var dayNumber = date.substring(0, 2);

      var result = (yearNumber * 10000) + (monthNumber * 100) + dayNumber;
      return result;
    };
    var date1Number = monthToComparableNumber(date1);
    var date2Number = monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }

    return date1Number - date2Number;
  }

  loadPracticesByDepartment() {
    var depId = this.filterData.departmentId;
    this.practiceOptions = this.departmentOptions.find(f=> f.id == +depId)?.practices ?? [];
    this.filterData.practiceId = <any>"";
    this.filterChanged();
  }
  
  filterChanged() {
    this.onReinit();
    this.loadGrid();
  }

  loadGrid() {
    var self = this;
    self.hiringRequestDataService.getAllCrud(`hiring/hiringRequest/searchJobDescriptions?pageNumber=${this.pageIndex}&pageSize=${this.pageSize}&deprecated=${this.filterData.isDeprecated}` + (self.filterData.departmentId ? `&departments=${self.filterData.departmentId}` : '') + (self.filterData.practiceId ? `&practices=${self.filterData.practiceId}` : '')).subscribe(res => {
      if (res) {
        let data = res.data;
        this.lstGrid = new MatTableDataSource(data ? data.content ?? data : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      }
    });
  }

  onPaginationChange(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadGrid();
  }

  onRowSelection(item) {
    if (this.selection.selected.length > 0) {
      if (this.selection.isSelected(item))
        this.selection.toggle(item);
      else {
        this.selection = new SelectionModel<any>(true, []);
        this.selection.toggle(item);
      }

      var selectedRecords = this.selection.selected;
      if (selectedRecords.length > 0) {
        var isOwner = selectedRecords[0]['isOwner'];
        this.isDisable = (isOwner) ? false : true;
      } else {
        this.isDisable = true;
      }
    }
    else
      this.selection.toggle(item);
  }

  onReinit() {
    this.pageIndex = 0;
    this.lstGrid = new MatTableDataSource([]);
  }

  navigateToJobDescriptionCreation() {
    this.router.navigate(['../jobdesc'], { relativeTo: this.route });
  }

  editJobDescription() {
    this.hiringRequestDataService.jobDescMode = 'edit';
    this.loadJobDescriptionDetails();
  }

  viewJobDescription() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      this.recordNotSelected = false;
      var jobDescId = selectedRecords[0]['id'];
      this.hiringRequestDataService.jobDescId = selectedRecords[0]['id'];
      this.router.navigate(['../jobdesc-details', jobDescId], { relativeTo: this.route });
    } else {
      this.recordNotSelected = true;
    }
  }

  loadJobDescriptionDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      this.recordNotSelected = false;
      var jobDescId = selectedRecords[0]['id'];
      this.hiringRequestDataService.jobDescId = selectedRecords[0]['id'];
      this.router.navigate(['../jobdesc', jobDescId], { relativeTo: this.route });
    } else {
      this.recordNotSelected = true;
    }
  }

  getDeptName(id: any) {
    return this.departmentOptions?.find(f=> f.id == +id)?.departmentName ?? '';
  }

  getPracName(deptId: any, pracId: any) {
    let pracs= this.departmentOptions?.find(f=> f.id == +deptId)?.practices ?? [];
    return pracs?.find(f=> f.id == +pracId)?.practiceName ?? '';
  }
}
