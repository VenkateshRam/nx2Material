import { SelectionModel } from "@angular/cdk/collections";
import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationService } from "primeng/api";
import { AlertService } from "src/app/shared/services/alert.service";
import { HiringCommonService } from "../../shared-hiring/services/hiring-common.service";
import { HiringRequestDataService } from "../../shared-hiring/services/hiring-request-data.service";


@Component({
  selector: 'app-all-hiring-requests',
  templateUrl: './all-hiring-requests.component.html',
  styleUrls: ['./all-hiring-requests.component.scss']
})
export class AllHiringRequestsComponent implements OnInit {

  displayedColumns = ['id', 'purpose', 'department', 'practice', 'ct', 'cn', 'model', 'status', 'ht', 'priority', 'res', 'cd', 'rsdate'];
  columnDefs: any;
  allOfTheData: any;
  public recordsCount: number;
  public successMessage: string;
  public departmentOptions: any[];
  public practiceOptions: any[];
  public priorityOptions: any[];
  public statusOptions: any[];
  nextDisable: boolean = true;
  prevDisable: boolean = true;
  pageNum: number = 0;
  pageSize: number = 10;
  totalPages: number;
  public buttonModes: any = {};
  public isLoaded: boolean = false;
  public filterData: any = {};
  searchId: string = "";

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  showChild: boolean = false;
  childData: any;

  constructor(private router: Router,
    private hiringRequestDataService: HiringRequestDataService,
    private confirmationService: ConfirmationService,
    private route: ActivatedRoute,
    private commonService: HiringCommonService,
    private alertSvc: AlertService) {
    this.priorityOptions = [];
    this.statusOptions = [];
    this.filterData.department = "";
    this.filterData.practice = "";
    this.filterData.priority = "";
    this.filterData.status = "";
  }

  ngOnInit() {
    this.resetModes();
    this.loadCombos();
  }

  formatId(id: any) {
    return 'HR' + id.value.split('-')[0].toUpperCase();
  }

  formatDate(data: any) {
    if (data.value === null || data.value === undefined) {
      const date = "";
      return date;
    } else {
      var time = new Date(data.value),
        result, year, month, today;
      year = time.getFullYear();
      month = time.getMonth() + 1;
      today = time.getDate();
      result = today + '/' + month + '/' + year;
      return result;
    }
  }

  dateComparator(date1: any, date2: any) {
    // eg 29/08/2004 gets converted to 20040829
    var monthToComparableNumber = function (date: any) {
      if (date === undefined || date === null || date.length !== 10) {
        return null;
      }

      var yearNumber = date.substring(6, 10);
      var monthNumber = date.substring(3, 5);
      var dayNumber = date.substring(0, 2);

      var result = (yearNumber * 10000) + (monthNumber * 100) + dayNumber;
      return result;
    };
    var date1Number = monthToComparableNumber(date1);
    var date2Number = monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }

    return date1Number - date2Number;
  }

  onRowSelection(item) {
    if (this.selection.selected.length > 0) {
      if (this.selection.isSelected(item))
        this.selection.toggle(item);
      else {
        this.selection = new SelectionModel<any>(true, []);
        this.selection.toggle(item);
      }
    }
    else
      this.selection.toggle(item);

    this.checkModes();
  }

  checkModes() {
    var selectedRecords = this.selection.selected, pracIds: any,
      userDetails: any = this.commonService.getItem('currentUserInfo');
    if (userDetails) {
      pracIds = userDetails.practiceHeadForPractices;
    }
    if (selectedRecords.length > 0) {
      var status = selectedRecords[0]['status'],
        pracId = selectedRecords[0]['practiceId'];
      this.buttonModes['view'] = false;
      this.buttonModes['timeline'] = false;
      this.buttonModes['approve'] = ((pracIds && (pracIds.length > 0) && (pracIds.indexOf(pracId) > -1)) && (status == 'PENDING_APPROVAL' || status == 'REJECTED')) ? false : true;
      this.buttonModes['reject'] = ((pracIds && (pracIds.length > 0) && (pracIds.indexOf(pracId) > -1)) && (status == 'PENDING_APPROVAL' || status == 'APPROVED')) ? false : true;
    } else {
      this.resetModes();
    }
  }

  filterStatusOptions() {
    for (var i = 0; i < this.statusOptions.length; i++) {
      var obj = this.statusOptions[i];
      if (obj.id == 1) {
        this.statusOptions.splice(i, 1);
      }
    }
  }

  loadCombos() {
    var data = this.hiringRequestDataService.getHiringCombos(), self = this;
    if (data) {
      this.departmentOptions = data['DEPARTMENT'];
      this.priorityOptions = data['HIRING_PRIORITY'];
      this.statusOptions = data['HIRING_REQUEST_STATUS'];
      // this.filterStatusOptions();
      this.loadGrid();
    }
    else {
      self.hiringRequestDataService.getHiringComboDetails().subscribe(res => {
        let data =res.data;
        self.departmentOptions = data['DEPARTMENT'];
        self.priorityOptions = data['HIRING_PRIORITY'];
        self.statusOptions = data['HIRING_REQUEST_STATUS'];
        // self.filterStatusOptions();
        self.loadGrid();
      });
    }
  }

  loadPracticesByDepartment() {
    var depId = this.filterData.department;
    this.practiceOptions = this.departmentOptions.find(f=> f.id == +depId)?.practices ?? [];
    this.filterData.practice = <any>"";
  }

  filterChanged() {
    this.pageNum = 0;
    this.loadGrid();
    this.searchId = '';
  }

  loadGrid() {
    var self = this;
    self.resetModes();
    self.hiringRequestDataService.getWithPagination(`HiringRequest`, this.pageNum, this.pageSize).subscribe(res => {
      if (res) {
        let data = res.data;
        this.lstGrid = new MatTableDataSource(data ? data.content ?? data : []);
        this.pageData = { data: data, table: 'lb' };
        this.length = data.totalElements;
      }
    });
  }

  exportData() {
    var self = this;
    setTimeout(() => {
      self.hiringRequestDataService.exportAllHiringRequestsByFilters(self.filterData.status, self.filterData.priority, self.filterData.department, self.filterData.practice).subscribe(data => {
        self.commonService.downloadCsv('All Hiring Requests', data['_body']);
      });
    }, 100);
  }

  resetModes() {
    this.buttonModes = {
      approve: true,
      reject: true,
      view: true,
      timeline: true
    };
  }

  onPaginationChange(event) {
    this.pageNum = event.pageIndex;
    this.pageSize = event.pageSize;
    this.length = event.length;
    this.loadGrid();
  }

  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }

  createHiringRequest() {
    this.hiringRequestDataService.mode = 'new';
    //this.router.navigate(['hiring/12345/hiringmanager/new']);
    this.router.navigate(['hiring/new']);
  }

  updateHiringRequest() {
    this.hiringRequestDataService.mode = 'edit';
    this.loadHiringRequestDetails();
  }

  loadHiringRequestDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      //this.recordNotSelected = false;
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../new', id], { relativeTo: this.route });
    } else {
      //this.recordNotSelected = true;
    }
  }

  viewRequest() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      //this.recordNotSelected = false;
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../view', id], { relativeTo: this.route });
    } else {
      //this.recordNotSelected = true;
    }
  }

  submitHiringRequest() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.hiringRequestDataService.submitHiringRequest(id, 'SUBMIT').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request submitted successfully';
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              self.loadGrid();

            }, reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }

  submitForApproval() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.hiringRequestDataService.submitHiringRequest(id, 'SUBMIT_FOR_APPROVAL').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request submitted for approval successfully';
        }
      });
    }
  }

  approve() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.hiringRequestDataService.submitHiringRequest(id, 'APPROVE').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request approved successfully';
          //$('[name=myModal]').modal('show');
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              self.loadGrid();
            }, reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }

  reject(reason: any) {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'],
        payload = {
          'decisionComments': reason
        };
      this.hiringRequestDataService.submitHiringRequest(id, 'REJECT', payload).subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request rejected successfully';
          //    $('[name=myModal]').modal('show');
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              self.loadGrid();

            }, reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }

  showHiringTimeline() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/hiring-requests/hiring-timeline', id]);
    }
  }

  SearchById() {
    var fetcheddata: any = [];
    if (this.searchId.length > 3) {
      this.hiringRequestDataService.loadHiringRequestDataByUid(this.searchId.toUpperCase(), 'all').subscribe(data => {
        fetcheddata.push(data);
        this.pageNum = 0;
        this.pageSize = 20;
        this.totalPages = 1;
        this.recordsCount = fetcheddata.length;
      }, error => {
        //this.gridOptions.api.showNoRowsOverlay(); 
        //this.setRowData([]);
        this.recordsCount = 0;
        this.pageNum = 0;
        this.totalPages = 1;
      });
    } else if (this.searchId == "") {
      this.setPageNum(0);
    }
  }

  onViewRequest() {
    let selectedRecord = this.selection.selected;
    if (selectedRecord.length == 0) {
      this.alertSvc.showToast('Please select a record', 'warning');
      return;
    }

    this.childData = selectedRecord[0];
    this.showChild = true;
  }

  receiveMessage($event) {

    if ($event.load)
      this.filterChanged();

    this.showChild = false;
  }
 
}
