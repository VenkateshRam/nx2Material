import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { HiringRequestsRoutingModule } from './hiring-requests-routing.module';
import { HiringRequestsComponent } from './hiring-requests.component';
import { AllHiringRequestsComponent } from './all-hiring-requests/all-hiring-requests.component';
import { MyHiringRequestsComponent } from './my-hiring-requests/my-hiring-requests.component';
import { AssignedRequestsComponent } from './assigned-requests/assigned-requests.component';
import { CreateHiringRequestComponent } from './create-hiring-request/create-hiring-request.component';
import { JobDescriptionComponent } from './job-description/job-description.component';
import { CreateJobDescriptionComponent } from './create-job-description/create-job-description.component';
import { HiringRequestNeedComponent } from './hiring-request-need/hiring-request-need.component';
import { AssignExecutiveComponent } from './assign-executive/assign-executive.component';


@NgModule({
  declarations: [
    HiringRequestsComponent,
    AllHiringRequestsComponent,
    MyHiringRequestsComponent,
    AssignedRequestsComponent,
    CreateHiringRequestComponent,
    JobDescriptionComponent,
    CreateJobDescriptionComponent,
    HiringRequestNeedComponent,
    AssignExecutiveComponent
  ],
  imports: [
    CommonModule,
    HiringRequestsRoutingModule,
    MaterialModule 
  ]
})
export class HiringRequestsModule { }
