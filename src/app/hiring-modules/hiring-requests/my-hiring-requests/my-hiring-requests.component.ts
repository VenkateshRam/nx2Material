import { SelectionModel } from "@angular/cdk/collections";
import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationService } from "primeng/api";
import { AlertService } from "src/app/shared/services/alert.service";
import { HiringCommonService } from "../../shared-hiring/services/hiring-common.service";
import { HiringRequestDataService } from "../../shared-hiring/services/hiring-request-data.service";


@Component({
  selector: 'app-my-hiring-requests',
  templateUrl: './my-hiring-requests.component.html',
  styleUrls: ['./my-hiring-requests.component.scss']
})
export class MyHiringRequestsComponent implements OnInit {

  displayedColumns = ['id', 'purpose', 'department', 'practice', 'ct', 'cn', 'model', 'status', 'ht', 'priority', 'res', 'cd', 'rsdate'];
  columnDefs: any;
  allOfTheData: any;
  public recordsCount: number;
  public successMessage: string;
  public departmentOptions: any[];
  public practiceOptions: any[];
  public priorityOptions: any[];
  public statusOptions: any[];
  public statsModal: boolean = false;
  public hiringReqStats: any = {
    interviewStatistics: {},
    offerStatistics: {},
    profileStatistics: {},
    showStats: false
  };
  nextDisable: boolean = true;
  prevDisable: boolean = true;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  enableConfirm: boolean = false;
  public buttonModes: any = {};
  public isLoaded: boolean = false;
  public filterData: any = {};
  public searchId: any;

  length: number; pageData: any;
  lstGrid: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  department: any;
  priority: any;
  status: any;
  showChild: boolean = false;
  childData: any;


  constructor(private router: Router,
    private commonService: HiringCommonService,
    private hiringRequestDataService: HiringRequestDataService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private alertSvc: AlertService) {
    this.priorityOptions = [];
    this.statusOptions = [];
    this.filterData.department = "";
    this.filterData.practice = "";
    this.filterData.priority = "";
    this.filterData.status = "";
  }

  formatId(id: any) {
    return 'HR' + id.value.split('-')[0].toUpperCase();
  }

  formatDate(data: any) {
    // To avoid setting default value if requistion date is not started
    if (data.value === null || data.value === undefined) {
      const date = "";
      return date;
    } else {
      var time = new Date(data.value),
        result, year, month, today;
      year = time.getFullYear();
      month = time.getMonth() + 1;
      today = time.getDate();
      result = today + '/' + month + '/' + year;

      
      return result;
    }
  }

  dateComparator(date1: any, date2: any) {
    // eg 29/08/2004 gets converted to 20040829
    var monthToComparableNumber = function (date: any) {
      if (date === undefined || date === null || date.length !== 10) {
        return null;
      }
      var yearNumber = date.substring(6, 10);
      var monthNumber = date.substring(3, 5);
      var dayNumber = date.substring(0, 2);
      var result = (yearNumber * 10000) + (monthNumber * 100) + dayNumber;
      return result;
    };
    var date1Number = monthToComparableNumber(date1);
    var date2Number = monthToComparableNumber(date2);
    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  checkModes() {
    var selectedRecords = this.selection.selected,
      hiringManager: boolean, practiceHead: boolean,
      // userDetails: any = this.commonService.getItem('currentUserInfo');
      userDetails: any = null;
    if (userDetails) {
      hiringManager = (userDetails.roles.indexOf('Hiring Manager') != -1) ? true : false;
      practiceHead = (userDetails.roles.indexOf('Practice Head') != -1) ? true : false;
    }
    if (selectedRecords.length > 0) {
      var status = selectedRecords[0]['status'],
        needCount = selectedRecords[0]['needCount'];
      if (status == 'DRAFT') {
        this.buttonModes = {
          submitForApproval: false,
          submit: false,
          edit: false,
          timeline: false
        };
      } else if (status == 'PENDING_APPROVAL' || status == 'APPROVED') {
        this.buttonModes = {
          submitForApproval: true,
          submit: false,
          edit: false,
          timeline: false
        };
      } else if (status == 'REJECTED') {
        this.buttonModes = {
          submitForApproval: false,
          submit: false,
          edit: false,
          timeline: false
        };
      } else {
        this.buttonModes = {
          submitForApproval: true,
          submit: true,
          edit: false,
          timeline: false
        };
      }
      const STATUS_FOR_DELETE = ['DRAFT', 'PENDING_APPROVAL', 'APPROVED', 'REJECTED', 'UNASSIGNED', 'ASSIGNED'];
      this.buttonModes['delete'] = !STATUS_FOR_DELETE.includes(status);

      const STATUS_FOR_COMPLETE = ['DRAFT', 'PENDING_APPROVAL', 'APPROVED', 'REJECTED', 'UNASSIGNED', 'ASSIGNED', 'COMPLETED', 'DELETED'];
      this.buttonModes['close'] = STATUS_FOR_COMPLETE.includes(status);

      this.buttonModes['edit'] = status == 'DELETED' || status == 'COMPLETED';
    } else {
      this.resetModes();
    }
  }

  filterStatusOptions() {
    var count = 0;
    (this.statusOptions).forEach((ele: any) => {
      if (ele.id == 1) {
        count++;
      }
    });
    if (count == 0) {
      this.statusOptions.splice(1, 0, { id: 1, code: "DRAFT", description: "Draft", key: "1" });
    }
  }

  onGridInitialize() {
    this.loadCombos();
  }

  loadCombos() {
   let self = this;
      self.hiringRequestDataService.getCoreComboDetails().subscribe(res => {
        let data= res.data;
        self.departmentOptions = data['DEPARTMENT'];
        self.priorityOptions = data['HIRING_PRIORITY'];
        self.statusOptions = data['HIRING_REQUEST_STATUS'];
        //self.filterStatusOptions();
        self.loadGrid();
      });
  }

  loadPracticesByDepartment() {
    var depId = this.filterData.department;
    this.practiceOptions = this.departmentOptions.find(f=> f.id == +depId)?.practices ?? [];
    this.filterData.practice = <any>"";
  }

  filterChanged() {
    this.pageNum = 0;
    this.loadGrid();
    this.searchId = '';
  }

  loadGrid() {
    var self = this;
    self.resetModes();
      self.hiringRequestDataService.getAllCrud(`hiring/hiringRequest/myHiringRequests?departments=${this.filterData.department}&practices=${this.filterData.practice}&status=${this.filterData.status}&priority=${this.filterData.priority}&me=true`).subscribe(res => {
        if (res) {
          let data = res.data;
          this.lstGrid = new MatTableDataSource(data ? data.content ?? data : []);
          this.pageData = { data: data, table: 'lb' };
          this.length = data.totalElements;
        }
      });
  }

  exportData() {
    var self = this;
    setTimeout(() => {
      self.hiringRequestDataService.exportMyHiringRequestsByFilters(self.filterData.status, self.filterData.priority, self.filterData.department, self.filterData.practice).subscribe(data => {
        self.commonService.downloadCsv('My Hiring Requests', data['_body']);
      });
    }, 100);
  }

  ngOnInit() {
    this.resetModes();
    this.loadCombos();
  }

  resetModes() {
    this.buttonModes = {
      submitForApproval: true,
      submit: true,
      edit: true,
      delete: true,
      close: true,
      timeline: true
    };
  }

  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }

  createHiringRequest() {
    this.hiringRequestDataService.mode = 'new';
    //this.router.navigate(['hiring/12345/hiringmanager/new']);
    this.router.navigate(['home/hiringRequests/create-hiring-request']);
  }

  updateHiringRequest() {
    this.hiringRequestDataService.mode = 'edit';
    this.loadHiringRequestDetails();
  }
  /*
  * Method to get stats about the hiring request
  */
  closeHiringRequest() {
    const selectedRecords = this.selection.selected;
    const self = this;
    if (selectedRecords.length <= 0) {
      return false;
    }
    const id = selectedRecords[0]['id'];
    this.hiringRequestDataService.getHiringRequestDetails(id).subscribe(data => {
      if (data.status === 200) {
        self.statsModal = true;
        self.hiringReqStats = JSON.parse(data._body);
        self.hiringReqStats.reqId = id;
        self.hiringReqStats.showStats = self.hiringReqStats.numberOfNeeds ||
          self.hiringReqStats.interviewStatistics.totalNumberOfUpcomingInterviews || self.hiringReqStats.interviewStatistics.totalNumberOfInterviews ||
          self.hiringReqStats.profileStatistics.numberOfProfilesProcessed || self.hiringReqStats.profileStatistics.numberOfCandidatesJoined ||
          self.hiringReqStats.offerStatistics.numberOfOffersReleased || self.hiringReqStats.offerStatistics.numberOfOffersAccepted || self.hiringReqStats.offerStatistics.numberOfOffersRejected;
      }
    });
    return null;
  }
  /*
  * Method which completes the deletion of hiring request.
  * @param id: hiring request id
  */
  close(id: string) {
    const self = this;

    this.hiringRequestDataService.closeHiringRequest(id).subscribe(data => {
      if (data.status === 200) {
        self.confirmationService.confirm({
          message: "Hiring request closed successfully.",
          header: 'Success',
          icon: 'fa fa-check-circle',
          rejectVisible: false,
          accept: () => {
            self.loadGrid();
          }, reject: () => {
            self.loadGrid();
          }
        });
      }

    });
  }

  loadHiringRequestDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      //this.recordNotSelected = false;
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../new', id], { relativeTo: this.route });
    } else {
      //this.recordNotSelected = true;
    }
  }

  viewRequest() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      //this.recordNotSelected = false;
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../view', id], { relativeTo: this.route });
    } else {
      //this.recordNotSelected = true;
    }
  }

  deleteRequest() {
    var selectedRecords = this.selection.selected, self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.enableConfirm = true;
      setTimeout(() => {
        this.confirmationService.confirm({
          message: 'Do you want to delete this record?',
          header: 'Delete Confirmation',
          icon: 'fa fa-trash',
          accept: () => {
            this.enableConfirm = false;
            self.delete(id);
          },
          reject: () => {
            this.enableConfirm = false;
          }
        });
      }, 100);
    }
  }

  delete(id: string) {
    var self = this;
    this.hiringRequestDataService.deleteHiringReuest(id).subscribe(data => {
      if (data.status == 200) {
        self.confirmationService.confirm({
          message: "Hiring request deleted successfully",
          header: 'Success',
          icon: 'fa fa-check-circle',
          rejectVisible: false,
          accept: () => {
            self.loadGrid();
          }, reject: () => {
            self.loadGrid();
          }
        });
      }
    });
  }

  submitHiringRequest() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.hiringRequestDataService.submitHiringRequest(id, 'SUBMIT').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request submitted successfully';
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              self.loadGrid();

            }, reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }

  submitForApproval() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.hiringRequestDataService.submitHiringRequest(id, 'SUBMIT_FOR_APPROVAL').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request submitted for approval successfully';
        }
      });
    }
  }

  approve() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.hiringRequestDataService.submitHiringRequest(id, 'APPROVE').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request approved successfully';
          // $('[name=myModal]').modal('show');
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              self.loadGrid();

            }, reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }

  reject() {
    var selectedRecords = this.selection.selected,
      self = this;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.hiringRequestDataService.submitHiringRequest(id, 'REJECT').subscribe(data => {
        if (data.status == 200) {
          self.successMessage = 'Hiring request rejected successfully';
          // $('[name=myModal]').modal('show');
          self.confirmationService.confirm({
            message: self.successMessage,
            header: 'Success',
            icon: 'fa fa-check-circle',
            rejectVisible: false,
            accept: () => {
              self.loadGrid();

            }, reject: () => {
              this.loadGrid();
            }
          });
        }
      });
    }
  }

  showHiringTimeline() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/hiring-requests/hiring-timeline', id]);
    }
  }

  SearchById() {
    var fetcheddata: any = [];
    if (this.searchId.length > 3) {
      this.hiringRequestDataService.loadHiringRequestDataByUid(this.searchId.toUpperCase(), 'me').subscribe(data => {
        fetcheddata.push(data);
        this.pageNum = 0;
        this.pageSize = 20;
        this.totalPages = 1;
        this.recordsCount = fetcheddata.length;
      }, error => {
        //this.gridOptions.api.showNoRowsOverlay(); 
        this.recordsCount = 0;
        this.pageNum = 0;
        this.totalPages = 1;
      });
    } else if (this.searchId == "") {
      this.setPageNum(0);
    }
  }

  onView() {
    let selectedRecord = this.selection.selected;
    if (selectedRecord.length == 0) {
      this.alertSvc.showToast('Please select a record', 'warning');
      return;
    }

    this.childData = selectedRecord[0];
    this.showChild = true;
  }

  receiveMessage($event) {

    if ($event.load)
      this.filterChanged();

    this.showChild = false;
  }

  onComplete(){

  }
  onDelete(){
    
  }
}
