import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllHiringRequestsComponent } from './all-hiring-requests/all-hiring-requests.component';
import { AssignExecutiveComponent } from './assign-executive/assign-executive.component';
import { AssignedRequestsComponent } from './assigned-requests/assigned-requests.component';
import { CreateHiringRequestComponent } from './create-hiring-request/create-hiring-request.component';
import { CreateJobDescriptionComponent } from './create-job-description/create-job-description.component';
import { HiringRequestNeedComponent } from './hiring-request-need/hiring-request-need.component';
import { HiringRequestsComponent } from './hiring-requests.component';
import { JobDescriptionComponent } from './job-description/job-description.component';
import { MyHiringRequestsComponent } from './my-hiring-requests/my-hiring-requests.component';

const routes: Routes = [
  { path: 'all-hiring-requests', component: AllHiringRequestsComponent },
  { path: 'assigned-requests', component: AssignedRequestsComponent },
  { path: 'my-hiring-requests', component: MyHiringRequestsComponent},
  { path: 'create-hiring-request', component: CreateHiringRequestComponent},
  { path: 'jobdesclist', component: JobDescriptionComponent},
  { path: 'jobdesc/:jobDescId', component: CreateJobDescriptionComponent},
  { path: 'jobdesc-details/:jobDescId', component: CreateJobDescriptionComponent},
  { path: 'jobdesc', component: CreateJobDescriptionComponent},
  { path: 'hiring-request-need', component: HiringRequestNeedComponent},
  { path: 'assign-executive', component: AssignExecutiveComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HiringRequestsRoutingModule { }
