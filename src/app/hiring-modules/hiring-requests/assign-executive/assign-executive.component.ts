import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertService } from "src/app/shared/services/alert.service";
import { HiringRequestDataService } from "../../shared-hiring/services/hiring-request-data.service";

@Component({
  selector: 'app-assign-executive',
  templateUrl: './assign-executive.component.html',
  styleUrls: ['./assign-executive.component.scss']
})
export class AssignExecutiveComponent implements OnInit {

  displayedColumns = ['id', 'purpos', 'dept', 'pract', 'compt', 'cl', 'model', 'status', 'ht', 'priority', 'res', 'cd'];
  public departmentOptions: any[];
  public practiceOptions: any[];
  public priorityOptions: any[];
  public statusOptions: any[];
  columnDefs: any;
  allOfTheData: any;
  public successMessage: string;
  public departmentMap: any;
  public clientMap: any;
  public hiringReqId: string;
  public recordsCount: number;
  public buttonModes: any = {
    'isAssignDisable': true,
    'isEditAssignDisable': true
  };
  public isLoaded: boolean = false;
  public recordNotSelected: boolean = false;
  pageNum: number = 0;
  pageSize: number = 20;
  totalPages: number;
  public filterData: any = {};
  searchId: string;
  selection: any;
  confirmationService: any;
  constructor( private hiringRequestDataService: HiringRequestDataService,
    private router: Router, 
    private route: ActivatedRoute,
    private alertSvc: AlertService) {
    this.priorityOptions = [
      { code: "LOW", description: 'Low' },
      { code: "NORMAL", description: 'Normal' },
      { code: "HIGH", description: 'High' },
      { code: "VERY_HIGH", description: 'Very High' },
      { code: "CRITICAL", description: 'Critical' },
      { code: "VERY_CRITICAL", description: 'Very Critical' }
    ];
    this.statusOptions = [
      { code: 'ACTIVE', description: 'Active' },
      { code: 'ASSIGNED', description: 'Assigned' },
      { code: 'UNASSIGNED', description: 'Unassigned' }
    ];
    this.filterData.department = "";
    this.filterData.practice = "";
    this.filterData.priority = "";
    this.filterData.status = "";
  }
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
  formatId(id: any) {
    return 'HR' + id.value.split('-')[0].toUpperCase();
  }
  formatDate(data: any) {
    var time = new Date(data.value),
      result, year, month, today;
    year = time.getFullYear();
    month = time.getMonth() + 1;
    today = time.getDate();
    result = today + '/' + month + '/' + year;
    return result;
  }

  onGridInitialize() {
    this.loadCombos();
  }
  loadCombos(){
    var data = this.hiringRequestDataService.getHiringCombos(), self = this;
    if (data) {
      this.departmentOptions = data['DEPARTMENT'];
      this.loadGrid();
    }
    else {
      self.hiringRequestDataService.getHiringComboDetails().subscribe(data=>{
        self.departmentOptions = data['DEPARTMENT'];
        self.loadGrid();
      });
    }
  }
  loadPracticesByDepartment(){
    var depId = this.filterData.department;
    this.practiceOptions = this.hiringRequestDataService.getDepartmentMap()['depPractices'][depId];
    this.filterData.practice = <any>"";
    this.filterChanged();
  }
  filterChanged(){
    this.pageNum = 0;
    this.loadGrid();
    this.searchId = '';
  }
  loadGrid() {
    var self = this;
    self.resetModes();
    // setTimeout(() => {
    //   self.assignExecutiveDataService.getAllHiringRequestsByFiltersForAssign(self.pageNum, self.pageSize, self.filterData.status, self.filterData.priority, self.filterData.department, self.filterData.practice, 'ASSIGNED,UNASSIGNED,ACTIVE').subscribe(data => {
    //     self.setRowData(data['content']);
    //     self.recordsCount = data.totalElements;
    //     self.totalPages = data.totalPages;
    //   });
    // }, 100);
  }
  setPageNum(pageNum: any) {
    this.pageNum = pageNum;
    this.loadGrid();
  }
  setRowData(rowData: any) {
    this.allOfTheData = rowData;
  //  this.createNewDatasource();
  }
  createHiringRequest() {
    this.router.navigate(['hiring/12345/hiringmanager/new']);
  }
  loadRecruiters(items: any) {
    var selectedRecords = this.selection.selected;
    var recruiterIds = items.map(function (x: any) {
      return x * 1;
    });
    if (selectedRecords.length > 0) {
      var hiringId: any = selectedRecords[0]['id'],
        self = this;
      var recruiterData = {
        recruiterIds: recruiterIds,
        isEditAssignment: ((this.buttonModes['isAssignDisable'])? true: false)
      }

      // this.assignExecutiveDataService.assignRecruiter(hiringId, recruiterData).subscribe(data => {
      //   if (data.status == 200) {
      //     self.successMessage = 'Recruiters assigned to request successfully';
      //     self.confirmationService.confirm({
      //       message: self.successMessage,
      //       header: 'Success',
      //       icon: 'fa fa-check-circle',
      //       rejectVisible: false,
      //       accept: () => {
      //         this.loadGrid();
      //       },
      //       reject: () => {
      //         this.loadGrid();
      //       }
      //     });
      //   }
      // });
    }
  }
  checkRecordSelection() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      this.recordNotSelected = false;
    } else {
      this.recordNotSelected = true;
    }
  }
  setModes(){
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var status = selectedRecords[0]['status'];
      this.hiringReqId = selectedRecords[0]['id'];
      this.buttonModes['isAssignDisable'] = (status == 'UNASSIGNED') ? false : true;
      this.buttonModes['isEditAssignDisable'] = (status == 'ASSIGNED' || status == 'ACTIVE') ? false : true;
      this.buttonModes['open'] = false;
      this.buttonModes['timeline'] = false;
    }else{
      this.resetModes();
    }
  }
  resetModes(){
    this.buttonModes = {
      'open': true,
      'timeline': true,
      'isAssignDisable': true,
      'isEditAssignDisable': true
    };
  }
  navigateToHiringRequestDetails() {
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['../view', id], { relativeTo: this.route });
    }
  }
  showHiringTimeline(){
    var selectedRecords = this.selection.selected;
    if (selectedRecords.length > 0) {
      var id = selectedRecords[0]['id'];
      this.router.navigate(['myspacenx/hiring-requests/hiring-timeline', id]);
    }
  }

 
    SearchById(){ 
      var fetcheddata : any =[];
      if(this.searchId.length > 3){
        this.hiringRequestDataService.loadHiringRequestDataByUid(this.searchId.toUpperCase(),"Assign executive").subscribe(data=>{
          fetcheddata.push(data);
          this.setRowData(fetcheddata);
          this.pageNum =0;
          this.pageSize = 20;
          this.totalPages = 1;
          this.recordsCount = fetcheddata.length;
        },error=>{           
          //this.gridOptions.api.showNoRowsOverlay(); 
          this.setRowData([]);
          this.recordsCount =0;
          this.pageNum = 0;
          this.totalPages = 1;
      });   
        } else if(this.searchId == "") {        
          this.setPageNum(0);
        }
      }
}

