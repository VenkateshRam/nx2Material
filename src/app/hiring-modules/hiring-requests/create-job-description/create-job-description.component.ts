import { Component, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { JobDescriptionDetails } from '../../shared-hiring/models/job-description';
import { CommonService } from '../../../shared/services/common.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl, NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { HiringRequestDataService } from '../../shared-hiring/services/hiring-request-data.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { HiringCommonService } from '../../shared-hiring/services/hiring-common.service';
import { MatTabGroup } from '@angular/material/tabs';

@Component({
  selector: 'app-create-job-description',
  templateUrl: './create-job-description.component.html',
  styleUrls: ['./create-job-description.component.scss']
})
export class CreateJobDescriptionComponent {

  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  uxDesignSkills: any[] = [];
  technicalSkills: any[] = [];
  certifications: any[] = [];

  public code: string;
  public description: string;
  public location: string;
  public country: string;
  public designTags: string[];
  public technicalTags: string[];
  public certificationTags: string[];
  public progressPercentage: number;
  public jobDescData: JobDescriptionDetails;
  public departmentOptions: any;
  public practiceOptions: any;
  public jobDescId: string;
  public successMessage: string;
  public editMode: boolean = false;
  public viewMode: boolean = false;
  public isLoaded: boolean = false;
  public formSubmitted: boolean = false;
  public jobDescSubmitted: boolean = false;
  public respSubmitted: boolean = false;
  public indexData: number;
  // public tabData: Tab;
  public jobDescriptionData: any = {
    'details': {}
  };
  public companyOverview: any = `Innominds is an AI-first, platform-led digital transformation and full cycle product engineering services company headquartered in San Jose, CA. Innominds powers the Digital Next initiatives of global enterprises, software product companies, OEMs and ODMs with integrated expertise in devices & embedded engineering, software apps & product engineering, analytics & data engineering, quality engineering, and cloud & devops, security. It works with ISVs to build next-generation products, SaaSify, transform total experience, and add cognitive analytics to applications

  For more information please visit:
  www.innominds.com
  Thank you.`;
  @ViewChild('jobDescTemplateDetails') jobDescTemplateDetails: ElementRef;
  @ViewChild('jobDescriptionDetails') jobDescriptionDetails: ElementRef;
  @ViewChild('jobDescriptionNext') jobDescriptionNext: ElementRef;
  @ViewChild('jobDescriptionFinish') jobDescriptionFinish: ElementRef;
  @ViewChild('tablist') tablist: any;
  @ViewChild('myForm', {static: false}) myForm: NgForm;

  constructor(private hiringRequestDataService: HiringRequestDataService,
    public commonService: HiringCommonService,
    private router: Router,
    private route: ActivatedRoute,
    private alertSvc: AlertService,
    private confirmationService: ConfirmationService) {
    this.code = 'UX120981';
    this.description = 'UX Designer';
    this.location = 'Hyderabad';
    this.country = 'India';
    this.certificationTags = ['CUA(Usability Certification)'];
    this.progressPercentage = 0;
  }

  ngOnInit() {
    var self = this;
    self.editMode = (self.hiringRequestDataService.jobDescMode == 'edit') ? true : false;
    self.route.params.subscribe(params => {
      let currentUrl = this.router.url.match("jobdesc-details");
      self.viewMode = (currentUrl && currentUrl.length) ? true : false;
      self.editMode =  (!this.viewMode && params['jobDescId']) ? true : false;
      var hiringCombos = self.hiringRequestDataService.getHiringCombos();
      if (!hiringCombos) {
        self.hiringRequestDataService.getCore('core/seeddata').subscribe(res => {
          let data = res.data;
          self.departmentOptions = data['DEPARTMENT'];
          self.practiceOptions = data['PRACTICE'];
          self.loadDetails(params);
        }, error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false,
            accept: () => {
            }
          });
        });
      } else if (hiringCombos) {
        self.departmentOptions = hiringCombos['DEPARTMENT'];
        self.practiceOptions = hiringCombos['PRACTICE'];
        self.loadDetails(params);
      }
    });
  }

  loadDetails(params: any) {
    var self = this;
    if (params['jobDescId']) {
      // self.FeatureComponent.URLtitle = "Hiring Requests / Edit Job Description";
      self.editMode = true;
      this.formSubmitted = true;
      this.jobDescSubmitted = true;
      this.respSubmitted = true;
      self.hiringRequestDataService.loadJobDescriptionDataById(params['jobDescId']).subscribe(res => {
        let data = res.data.content[0];
        self.jobDescriptionData = data;
        self.jobDescriptionData.details = JSON.parse(data.details);
        self.jobDescData = self.jobDescriptionData;
        if (self.jobDescData.departmentId) {
          this.practiceOptions = this.departmentOptions.find(f => f.id == +self.jobDescData.departmentId)?.practices ?? [];
          self.jobDescData.practiceId = data.practiceId;
        }

      }, error => {
        self.confirmationService.confirm({
          message: error.message,
          header: 'Error',
          icon: 'fa fa-exclamation-circle',
          rejectVisible: false,
          accept: () => {

          }
        });
      });
    } else {
      self.editMode = false;
      var jobData: any = self.hiringRequestDataService.getJobDescTempData();
      jobData.details.skills = (jobData.details.skills) ? jobData.details.skills : {};
      self.jobDescData = jobData;
      if (self.jobDescData) {
        self.jobDescData.departmentId = <any>"";
        self.jobDescData.practiceId = <any>"";
      }
      if (self.jobDescData.details.companyOverview == null) {
        self.jobDescData.details.companyOverview = this.companyOverview;
      }
    }
  }

  ngOnDestroy() {
    this.hiringRequestDataService.jobDescMode = null;
    if (this.jobDescData) {
      this.jobDescData.details.companyOverview = null;
      this.jobDescData.details.practiceOverview = null;
      this.jobDescData.details.jobDesc = null;
      this.jobDescData.details.responsibilities = null;
      this.jobDescData.details.skills = null;
      this.jobDescData.details.education = null;
      this.jobDescData.details.screeningQuestions = null;
    }
  }
  // making the next button as disabled for null values of jobdescription and postion responsilites
  makeButtonDisable() {
    const tabIndexZero = 0, tabIndexOne = 1, tabIndexTwo = 2, tabIndexFive = 5, tabIndexFour = 4;
    if (this.indexData === tabIndexZero || this.indexData === tabIndexFour || this.indexData === tabIndexFive) {
      return true;
    } else if (this.indexData === tabIndexOne) {
      this.jobDescSubmitted = true;
      if (!this.jobDescData.details.jobDesc) {
        this.alertSvc.showToast('Please select all mandatory fields', 'warning');
      }
      return this.jobDescData.details.jobDesc;
    } else if (this.indexData === tabIndexTwo) {
      this.respSubmitted = true;
      if (!this.jobDescData.details.responsibilities) {
        this.alertSvc.showToast('Please select all mandatory fields', 'warning');

      }
      return this.jobDescData.details.responsibilities;

    }
    else return true;
  }

  tabSelects(currentTab: any) { //Tab
    const tabGroup = this.tablist;
    if (!tabGroup || !(tabGroup instanceof MatTabGroup)) return;
    this.indexData = tabGroup.selectedIndex;
  }

  next() {
    const tabGroup = this.tablist;
    if (!tabGroup || !(tabGroup instanceof MatTabGroup)) return;
    let
      tabs = ['Company Background', 'Job Description', 'Position/Responsibilities', 'Skills', 'Education', 'Initial Screening Questions'],
      index = tabGroup.selectedIndex;
    // const tabCount = tabGroup._tabs.length;
    // tabGroup.selectedIndex = (tabGroup.selectedIndex + 1) % tabCount;
    this.indexData = tabGroup.selectedIndex;

    let valid = this.makeButtonDisable();
    if (!valid)
      return;

    this.indexData = index;
    if (index == tabs.length - 1) {
      tabGroup.selectedIndex = 0;
    } else {
      tabGroup.selectedIndex = (tabGroup.selectedIndex + 1);
    }
  }

  updateProgress(index: number) {
    var tabs = ['Company Background', 'Job Description', 'Position/Responsibilities', 'Skills', 'Education', 'Initial Screening Questions'];
    this.progressPercentage = Math.round(((index + 1) / (tabs.length)) * 100);
  }

  finishJobDescription() {
    this.formSubmitted = true;
    this.myForm.form.markAllAsTouched();
    var jobDescData = this.jobDescData,
      jobDescriptionPayload = {
        "departmentId": jobDescData.departmentId,
        "practiceId": jobDescData.practiceId,
        "templateName": jobDescData.templateName,
        "details": JSON.stringify(jobDescData.details),
        "fasttrack":false,
        "deprecated": ((this.hiringRequestDataService.jobDescMode == 'edit') ? jobDescData.deprecated : false)
      };
    if (!jobDescriptionPayload.departmentId || !jobDescriptionPayload.practiceId || !jobDescriptionPayload.templateName) {
      this.alertSvc.showToast('Please select all mandatory fields', 'warning');
      return;
    } else if (!jobDescData.details.jobDesc) {
      this.confirmationService.confirm({
        message: 'Please provide a name for the job description.',
        header: 'Warning',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
      return;
    } else if (!jobDescData.details.responsibilities) {
      this.confirmationService.confirm({
        message: 'Please add few details about the responsibilities for this job description.',
        header: 'Warning',
        icon: 'fa fa-exclamation-circle',
        rejectVisible: false
      });
      return;
    }
    var self = this,
      jobId;
    if (this.editMode) {
      jobId = self.hiringRequestDataService.jobDescId;
      Object.assign(jobDescriptionPayload,{id: jobId})
      self.hiringRequestDataService.updateJobDescriptionDetails(jobDescriptionPayload, jobId).subscribe(
        data => {
          if (data.status == 'OK') {
            self.successMessage = 'Job description updated successfully';
            self.confirmationService.confirm({
              message: self.successMessage,
              header: 'Success',
              icon: 'fa fa-check-circle',
              rejectVisible: false,
              accept: () => {
                self.showJobDescList();
              }, reject: () => {
                self.showJobDescList();
              }
            });
          }
        });
    } else {
      self.hiringRequestDataService.saveJobDescriptionDetails(jobDescriptionPayload).subscribe(
        data => {
          self.jobDescId = data['_body'];
          // if (self.jobDescId) {
          if (data.status == 'OK') {
            self.successMessage = 'Job description created successfully';
            self.confirmationService.confirm({
              message: self.successMessage,
              header: 'Success',
              icon: 'fa fa-check-circle',
              rejectVisible: false,
              accept: () => {
                self.showJobDescList();

              }, reject: () => {
                self.showJobDescList();
              }
            });
          }
        }, error => {
          self.confirmationService.confirm({
            message: error.message,
            header: 'Error',
            icon: 'fa fa-exclamation-circle',
            rejectVisible: false,
            accept: () => {
            }
          });
        });
    }
  }

  showJobDescList() {
    this.hiringRequestDataService.jobDescMode = null;
    this.router.navigate(['../home/hiringRequests/jobdesclist']);
  }

  loadPracticesByDepartment() {
    var depId = this.jobDescData.departmentId;
    this.practiceOptions = this.departmentOptions.find(f => f.id == +depId)?.practices ?? [];
    this.jobDescData.practiceId = <any>"";
  }

}
