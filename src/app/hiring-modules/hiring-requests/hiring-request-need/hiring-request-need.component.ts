import { Component, EventEmitter, Input, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ConfirmationService } from "primeng/api";
import { AlertService } from "src/app/shared/services/alert.service";
import { MultiSelectionComponent } from "../../features/multi-selection/multi-selection.component";
import { HiringRequestDataService } from "../../shared-hiring/services/hiring-request-data.service";

@Component({
  selector: 'app-hiring-request-need',
  templateUrl: './hiring-request-need.component.html',
  styleUrls: ['./hiring-request-need.component.scss']
})
export class HiringRequestNeedComponent {

  departmentOptions: Array<any> = []; practiceOptions: Array<any> = []; templatesLst: Array<any> = [];
  screeningList: Array<any> = []; criteria: Array<any> = []; panelMembers: Array<any> = []; panelGrid: Array<any> = [];
  rolesLst: Array<any> = []; interviewCriteriaLst: Array<any> = [];

  departmentId: string = ''; practiceId: string = ''; templateId: string = ''; interviewLevelLst: Array<any> = [];
  jobCompBg: string = ''; jobPracBg: string = ''; jobds: string = ''; jobresp: string = ''; jobDsSkill: string = '';
  jobTsSkill: string = ''; jobCert: string = ''; jobOtherSkill: string = ''; jobEdu: string = ''; jobScrnQs: string = '';
  level: string = ''; panelName: string = '';

  companyOverview: any = `Innominds is an AI-first, platform-led digital transformation and full cycle product engineering services company headquartered in San Jose, CA. Innominds powers the Digital Next initiatives of global enterprises, software product companies, OEMs and ODMs with integrated expertise in devices & embedded engineering, software apps & product engineering, analytics & data engineering, quality engineering, and cloud & devops, security. It works with ISVs to build next-generation products, SaaSify, transform total experience, and add cognitive analytics to applications

  For more information please visit:
  www.innominds.com
  Thank you.`;

  templateInfo: any; panelEditData: any; showJdInfo: boolean = false; panelSubmitted: boolean = false;

  constructor(private hiringRequestDataService: HiringRequestDataService
    , private alertSvc: AlertService
    , private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getMasters();
    this.getRoles();
  }

  getMasters() {
    let self = this;
    this.hiringRequestDataService.getHiringComboDetails().subscribe(res => {
      let data = res.data;
      self.departmentOptions = data['DEPARTMENT'];
      self.practiceOptions = data['PRACTICE'];
      self.interviewLevelLst = data['INTERVIEW_LEVEL'];
      data['INTERVIEW_CRITERIA'].forEach(e => {
        this.interviewCriteriaLst.push({id: e.id, name: e.title});
      });
    });
  }

  getTemplates() {
    let self = this;
    if (!this.departmentId || !this.practiceId) {
      this.alertSvc.showToast('Please select Department and Practice', 'warning');
      return;
    }
    self.hiringRequestDataService.getAllCrud(`hiring/hiringRequest/searchJobDescriptions?pageNumber=0&pageSize=1000&deprecated=false&departments=${this.departmentId}&practices=${this.practiceId}`).subscribe(res => {
      if (res) {
        let data = res.data;
        this.templatesLst = (data ? data.content : []);

        if (data && data.content.length == 0)
          this.alertSvc.showToast('No Templates Found', 'info');
      }
    });
  }

  getRoles() {
    this.hiringRequestDataService.getAllCrud(`hiring/crud/getAll?entityName=Role`).subscribe(res => {
        this.rolesLst = res ? res.data : [];
        console.log(this.rolesLst.find(f=> f.name.toLowerCase() == 'interviewer'))
        console.log(this.rolesLst.find(f=> f.name.toLowerCase() == 'hiring manager'))
        console.log(this.rolesLst.find(f=> f.name.toLowerCase() == 'profile screening committee'))
    });
  }

  loadPracticesByDepartment() {
    var depId = this.departmentId;
    this.practiceOptions = this.departmentOptions.find(f => f.id == +depId)?.practices ?? [];
    this.practiceId = <any>"";
  }

  onTemplateChange(id: any) {
    this.templateInfo = JSON.parse(this.templatesLst.find(f => f.id == id)?.details);
  }

  onJobSelection() {
    this.clearJD();

    if (this.templateInfo) {
      this.jobCompBg = this.templateInfo.companyOverview ? this.templateInfo.companyOverview : this.companyOverview;
      this.jobPracBg = this.templateInfo.practiceOverview;
      this.jobds = this.templateInfo.jobDesc;
      this.jobresp = this.templateInfo.responsibilities;
      this.jobDsSkill = this.templateInfo.skills.uxDesignSkills?.map(f => f.display).join(',');
      this.jobTsSkill = this.templateInfo.skills.technicalSkills?.map(f => f.display).join(',');
      this.jobCert = this.templateInfo.skills.certifications?.map(f => f.display).join(',');
      this.jobOtherSkill = this.templateInfo.skills.others;
      this.jobEdu = this.templateInfo.education;
      this.jobScrnQs = this.templateInfo.screeningQuestions;
    }
    this.showJdInfo = this.templateInfo ? true : false;

  }

  clearJD() {
    this.jobCompBg = ''; this.jobPracBg = ''; this.jobds = ''; this.jobresp = ''; this.jobDsSkill = '';
    this.jobTsSkill = ''; this.jobCert = ''; this.jobOtherSkill = ''; this.jobEdu = ''; this.jobScrnQs = '';
  }

  openSelectionModal(type: string) {
    const dialogRef = this.dialog.open(MultiSelectionComponent, {
      width: '400px',
      data: {
        title: type == 'screening' ? "Profile Screening Members" : type == 'panel' ? 'Panel Members' : 'Selection Criteria',
        selectedList: type == 'screening' ? this.screeningList : type == 'panel' ? this.panelMembers : this.criteria,
        data: this.interviewCriteriaLst,
        searchTitle: "screening member name"
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.status == 'OK') {

        console.log(result.data);
        let [lst, selectedLst] = [result.list, result.data];
        if (type == 'screening')
          this.screeningList = [];
        else if (type == 'panel')
          this.panelMembers = [];
        else if (type == 'criteria')
          this.criteria = [];

        selectedLst.forEach(e => {
          if (type == 'screening')
            this.screeningList.push(e);
          else if (type == 'panel')
            this.panelMembers.push(e);
          else if (type == 'criteria')
            this.criteria.push(e);
        });
      }

      // if (result == 'OK')
      //   this.loadPendingReqs();
    });
  }

  addPanel() {
    this.panelSubmitted = true;

    if (!this.level || !this.panelName || this.panelMembers.length == 0 || this.criteria.length == 0) {
      this.alertSvc.showToast('Please Select All Mandatory Fields', 'warning');
      return;
    }

    this.panelGrid.push({ name: this.panelName, level: this.level, members: this.panelMembers.map(f => f.id), criteria: this.criteria });
    console.log(this.panelGrid)
    this.clearPanel();
  }

  clearPanel() {
    this.panelSubmitted = false;
    this.level = ''; this.panelName = ''; this.panelMembers = []; this.criteria = [];
  }

  onPanelAction(data: any, action: string, index: number) {
    if (action == 'edit') {
      this.panelEditData = {i: index, obj:data};
      this.level = data.level; this.panelName = data.name;
      this.panelMembers = data.members; this.criteria = data.criteria;
    }
    else
    this.panelGrid.splice(index, 1);
  }

  getCriteriaNames(id: any) {
    return id ? this.interviewCriteriaLst.find(f => f.id == +id).name : '';
  }

  // getNames(id: any) {
  //   return id ? this.interviewCriteriaLst.find(f => f.id == +id).name : '';
  // }
  // getCriteriaNames(id: any) {
  //   return id ? this.interviewCriteriaLst.find(f => f.id == +id).name : '';
  // }
}